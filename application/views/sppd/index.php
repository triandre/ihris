<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $title; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('tendik') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Permohonan</a></li>
                        <li class="breadcrumb-item active">SPPD</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>

    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <?php if ($this->session->flashdata('gagal_store')) { ?>
                    <div class="alert alert-danger col-md-12">
                        <?= $this->session->flashdata('gagal_store') ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php } ?>

                <?= form_error('username', '<div class="alert alert-danger" role="alert">', '</div>') ?>
                <?= form_error('password', '<div class="alert alert-danger" role="alert">', '</div>') ?>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="example1" class="table table-bordered table-striped table-sm">
                        <thead>
                            <tr>
                                <th>No</th>
                            <th>Surat Perintah Perjalanan Dinas </th>
                            <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1;
                            foreach ($sppd as $row) : ?>
                                <tr>
                                    <td><?= $i; ?></td>
                                    <td>Perintah Pimpinan Bapak <?= $row['nama']; ?>
                                    <br>
                                    <?= $row['no_surat']; ?>/SPPD/II.3.AU/UMSU/D/<?= date('Y') ?>
                                    <br>
                                    <small>
                                        <?= $row['nama_pegawai']; ?> | <?= $row['jabatan_pegawai']; ?>
                                        <br>
                                        <?= $row['nama_pegawai2']; ?> | <?= $row['jabatan_pegawai2']; ?>
                                        <br>
                                        <?= $row['tujuan_pd']; ?>
                                        <br>
                                        Tanggal Berangkat : <?= Sppd::format($row['tgl_berangkat']); ?> | Tanggal Kembali : <?= Sppd::format($row['tgl_kembali']); ?>
                                        <br>
                                        <?php
                                        if ($row['status'] == 1) {
                                            echo '<span class="badge badge-info">Sedang Diverifikasi</span> ';
                                        } elseif ($row['status'] == 2) {
                                            echo '<span class="badge badge-success">Disetujui</span>';
                                        } else {
                                            echo '<span class="badge badge-danger">Ditolak</span>';
                                        }  ?>
                                    </small>
                                </td>
                                

                                 <td>
                                    <div class="btn-group">


                                        <button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown">
                                            <i class="fas fa-cog"> </i>
                                            <span class="caret"></span>
                                        </button>
                                        <div class="dropdown-menu">
                                         <a href="<?= base_url(); ?>pimpinan_sppd/setuju/<?= $row['id']; ?>" class="dropdown-item btn-sm">SETUJU</a>
                                         <a type="button" data-toggle="modal" data-target="#myModal<?php echo $row['id'] ?>" class="dropdown-item btn-sm">TOLAK</a>
                                         <a href="<?= base_url(); ?>pimpinan_sppd/detail/<?= $row['id']; ?>" target="_blank" class="dropdown-item btn-sm">DETAIL</a>
                                           
                                        </div>
                                    </div>
                                </td>
                                </tr>
                            <?php $i++; ?>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">

            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->

</div>

<?php $i = 1;
foreach ($sppd as $row) : ?>
    <!-- The Modal -->
    <div class="modal fade" id="myModal<?php echo $row['id'] ?>" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Tolak Permohonan</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <form action="<?= base_url('pimpinan_sppd/tolak') ?>" method="post">
                        <label>REFF ID</label>
                        <input class="form-control" type="text" id="id" name="id" value="<?= $row['id']; ?>" required readonly>
                        <label>Catatan</label>
                        <textarea class="form-control" type="text" id="catatan_pimpinan" name="catatan_pimpinan" rows="5" required></textarea>

                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                    <button type="submit" class="btn btn-success"> <i class="fa fa-save"></i> Kirim</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <?php $i++; ?>
<?php endforeach; ?>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
    $(function() {
        $("#example1").DataTable({
            "language": {
                "sSearch": "Cari"
            }
        });
    });
</script>