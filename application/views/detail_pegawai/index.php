<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Detail Pegawai</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('home') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Data Pegawai</a></li>
                        <li class="breadcrumb-item active">Detail</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Detail Pegawai</h3>
                <div class="card-tools">
                    <a href="javascript:history.back()" class="btn btn-info"><i class="fa fa-backward"></i> Kembali</a>
                </div>

            </div>
            <div class="card-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#home">Biodata</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#menu1">Kepegawaian</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#menu2">Kemuhammadiyahan</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#menu3">Lain - lain</a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div id="home" class="tab-pane active"><br>
                        <h4>Biodata</h4>

                        <img src="<?= base_url('assets/images/profil/') ?><?= $gs['image'] ?>" style="width:300px" class="">
                        <br><br>
                        <table class="table table-striped" id="users">
                            <tbody>
                                <tr>
                                    <td width="100px">NIK</td>
                                    <td width="50px">:</td>
                                    <td><?= $gk['nik'] ?></td>
                                </tr>
                                <tr>
                                    <td width="100px">Nama</td>
                                    <td width="50px">:</td>
                                    <td><?= $gs['name'] ?></td>
                                </tr>
                                <tr>
                                    <td width="100px">Agama</td>
                                    <td width="50px">:</td>
                                    <td><?= $gk['agama'] ?></td>
                                </tr>
                                <tr>
                                    <td width="100px">Jenis Kelamin</td>
                                    <td width="50px">:</td>
                                    <td><?= $gb['jk'] ?></td>
                                </tr>
                                <tr>
                                    <td width="100px">Tempat/ Tanggal Lahir</td>
                                    <td width="50px">:</td>
                                    <td><?= $gb['t_lahir'] ?>, <?= $gb['tgl_lahir'] ?> </td>
                                </tr>
                                <tr>
                                    <td width="100px">Alamat</td>
                                    <td width="50px">:</td>
                                    <td><?= $ga['alamat'] ?></td>
                                </tr>

                                <tr>
                                    <td width="100px">RT/RW</td>
                                    <td width="50px">:</td>
                                    <td><?= $ga['rt'] ?>/<?= $ga['rw'] ?></td>
                                </tr>

                                <tr>
                                    <td width="100px">Kelurahan</td>
                                    <td width="50px">:</td>
                                    <td><?= $ga['kelurahan'] ?></td>
                                </tr>

                                <tr>
                                    <td width="100px">Kecamatan</td>
                                    <td width="50px">:</td>
                                    <td><?= $ga['kecamatan'] ?></td>
                                </tr>

                                <tr>
                                    <td width="100px">Kota</td>
                                    <td width="50px">:</td>
                                    <td><?= $ga['kota'] ?></td>
                                </tr>
                                <tr>
                                    <td width="100px">Kode Pos</td>
                                    <td width="50px">:</td>
                                    <td><?= $ga['kode_pos'] ?></td>
                                </tr>
                                <tr>
                                    <td width="100px">Provinsi</td>
                                    <td width="50px">:</td>
                                    <td><?= $ga['provinsi'] ?></td>
                                </tr>
                                <tr>
                                    <td width="100px">Kewarganrgaraan</td>
                                    <td width="50px">:</td>
                                    <td><?= $gk['kewarganegaraan'] ?></td>
                                </tr>
                            </tbody>
                        </table>

                        <h4>Kontak</h4>
                        <table class="table table-striped" id="users">
                            <tbody>
                                <tr>
                                    <td width="100px">No. HP</td>
                                    <td width="50px">:</td>
                                    <td><?= $ga['no_hp'] ?></td>
                                </tr>
                                <tr>
                                    <td width="100px">No. Telp</td>
                                    <td width="50px">:</td>
                                    <td><?= $ga['no_telpon'] ?></td>
                                </tr>
                                <tr>
                                    <td width="100px">Email</td>
                                    <td width="50px">:</td>
                                    <td><?= $gs['email'] ?></td>
                                </tr>

                            </tbody>
                        </table>


                    </div>

                    <div id="menu1" class="tab-pane fade"><br>
                        <h4>Kepegawaian </h4>
                        <table class="table table-striped" id="menu1">
                            <tbody>
                                <tr>
                                    <td width="100px">NIDN/NITK/NITKS</td>
                                    <td width="50px">:</td>
                                    <td><?= $gs['nidn'] ?></td>
                                </tr>
                                <tr>
                                    <td width="100px">NPP</td>
                                    <td width="50px">:</td>
                                    <td><?= $gs['npp'] ?></td>
                                </tr>
                                <tr>
                                    <td width="100px">Penempatan</td>
                                    <td width="50px">:</td>
                                    <td><?= $gpn['nickname_unit'] ?></td>
                                </tr>
                                <tr>
                                    <td width="100px">Status Kepegawaian</td>
                                    <td width="50px">:</td>
                                    <td><?= $gpek['status_pegawai'] ?></td>
                                </tr>
                                <tr>
                                    <td width="100px">Status Keaktifan</td>
                                    <td width="50px">:</td>
                                    <td><?= $gpek['status_keaktifan'] ?></td>
                                </tr>
                                <tr>
                                    <td width="100px">No. Sk PNS</td>
                                    <td width="50px">:</td>
                                    <td><?= $gpek['no_sk_pns'] ?></td>
                                </tr>
                                <tr>
                                    <td width="100px">Tgl SK PNS</td>
                                    <td width="50px">:</td>
                                    <td><?= $gpek['tgl_sk_pns'] ?></td>
                                </tr>
                                <tr>
                                    <td width="100px">No. SK TMT</td>
                                    <td width="50px">:</td>
                                    <td><?= $gpek['no_sk_tmmd'] ?></td>
                                </tr>

                                <tr>
                                    <td width="100px">Tgl SK TMT</td>
                                    <td width="50px">:</td>
                                    <td><?= $gpek['tgl_sk_tmmd'] ?></td>
                                </tr>

                                <tr>
                                    <td width="100px">Pangkat</td>
                                    <td width="50px">:</td>
                                    <td><?= $gpek['pangkat'] ?></td>
                                </tr>

                                <tr>
                                    <td width="100px">Golongan</td>
                                    <td width="50px">:</td>
                                    <td><?= $gpek['golongan'] ?></td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                    <div id="menu2" class="tab-pane fade"><br>
                        <h4>Kemuhammadiyahan </h4>
                        <table class="table table-striped" id="menu1">
                            <tbody>
                                <tr>
                                    <td width="100px">NKTAM</td>
                                    <td width="50px">:</td>
                                    <td><?= $gkm['nktam'] ?></td>
                                </tr>
                                <tr>
                                    <td width="100px">Ranting</td>
                                    <td width="50px">:</td>
                                    <td><?= $gkm['ranting'] ?></td>
                                </tr>
                                <tr>
                                    <td width="100px">Cabang</td>
                                    <td width="50px">:</td>
                                    <td><?= $gkm['cabang'] ?></td>
                                </tr>
                                <tr>
                                    <td width="100px">Daerah</td>
                                    <td width="50px">:</td>
                                    <td><?= $gkm['daerah'] ?></td>
                                </tr>
                                <tr>
                                    <td width="100px">Wilayah</td>
                                    <td width="50px">:</td>
                                    <td><?= $gkm['wilayah'] ?></td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                    <div id="menu3" class="tab-pane fade"><br>
                        <h4>Lain Lain </h4>
                        <table class="table table-striped" id="menu1">
                            <tbody>
                                <tr>
                                    <td width="300px">NPWP</td>
                                    <td width="50px">:</td>
                                    <td><?= $gll['npwp'] ?></td>
                                </tr>
                                <tr>
                                    <td width="300px">Nama Wajib Pajak</td>
                                    <td width="50px">:</td>
                                    <td><?= $gll['nama_wajib_pajak'] ?></td>
                                </tr>
                                <tr>
                                    <td width="300px">ID SINTA</td>
                                    <td width="50px">:</td>
                                    <td><?= $gll['sinta_id'] ?></td>
                                </tr>
                                <tr>
                                    <td width="300px">ID Scopus</td>
                                    <td width="50px">:</td>
                                    <td><?= $gll['scopus_id'] ?></td>
                                </tr>
                                <tr>
                                    <td width="300px">Google Scholar</td>
                                    <td width="50px">:</td>
                                    <td><?= $gll['gs'] ?></td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>

            </div>

            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>

</div>
<!-- /.content-wrapper -->