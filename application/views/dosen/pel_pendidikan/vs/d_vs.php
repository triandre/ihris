<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Detail Visiting Scientist</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('home') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Pelaks. Pendidikan</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url('') ?>">Visiting Scientist</a></li>
                        <li class="breadcrumb-item active">Detail</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Detail Visiting Scientist</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">

                <table class="table table-striped" id="users">
                    <tbody>
                        <tr>
                            <td width="100px">NIDN</td>
                            <td width="50px">:</td>
                            <td><?= $d['nidn'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Aktivitas Litabmas</td>
                            <td width="50px">:</td>
                            <td><?= $d['aktivitas']; ?> </td>
                        </tr>
                        <tr>
                            <td width="200px">Kategori Capaian Luaran</td>
                            <td width="50px">:</td>
                            <td><?= $d['kategori'] ?></td>
                        </tr>
                        <tr>
                            <td width="200px">Perguruan Tinggi Pengundang</td>
                            <td width="50px">:</td>
                            <td><?= $d['pt'] ?></td>
                        </tr>
                        <tr>
                            <td width="200px">Lama Kegiatan (Hari)</td>
                            <td width="50px">:</td>
                            <td><?= $d['lama_kgt'] ?> Hari</td>
                        </tr>
                        <tr>
                            <td width="200px">Kategori Kegiatan </td>
                            <td width="50px">:</td>
                            <td><?= $d['kategori_kgt'] ?></td>
                        </tr>
                        <tr>
                            <td width="200px">Kegiatan Penting</td>
                            <td width="50px">:</td>
                            <td><?= $d['kgt_penting'] ?></td>
                        </tr>

                        <tr>
                            <td width="100px">Tanggal Pelaksanaan</td>
                            <td width="50px">:</td>
                            <td><?= date('d F Y', strtotime($d['tgl_pelaksanaan'])) ?> </td>
                        </tr>

                        </tr>
                        <tr>
                            <td width="200px">Nomor SK Penugasan</td>
                            <td width="50px">:</td>
                            <td><?= $d['no_sk'] ?></td>
                        </tr>

                        <tr>
                            <td width="100px">Tanggal SK Penugasan</td>
                            <td width="50px">:</td>
                            <td><?= date('d F Y', strtotime($d['tgl_sk'])) ?> </td>
                        </tr>

                        <tr>
                            <td width="100px">Dokumen Pendukung</td>
                            <td width="50px">:</td>
                            <td> <a href="<?= base_url('archive/vs/'); ?><?= $d['file'] ?>" target="_blank"> Lihat Data</a> </td>
                        </tr>

                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <a href="<?= base_url('dosenVs') ?>">
                    <button type="button" class="btn btn-danger"><i class="fa fa-backward"></i> Kembali</button>
                </a>
            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->