<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Detail Detasering</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('home') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Pelaks. Pendidikan</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url('') ?>">Detasering</a></li>
                        <li class="breadcrumb-item active">Detail</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Detail Detasering</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">

                <table class="table table-striped" id="users">
                    <tbody>
                        <tr>
                            <td width="100px">NIDN</td>
                            <td width="50px">:</td>
                            <td><?= $d['nidn'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Kategori Kegiatan</td>
                            <td width="50px">:</td>
                            <td><?= $d['kategori_kgt']; ?> </td>
                        </tr>
                        <tr>
                            <td width="200px">Perguruan Tinggi Sasaran</td>
                            <td width="50px">:</td>
                            <td><?= $d['pt'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Tanggal Mulai</td>
                            <td width="50px">:</td>
                            <td><?= date('d F Y', strtotime($d['tgl_mulai'])) ?> </td>
                        </tr>
                        <tr>
                            <td width="100px">Tanggal Selesai</td>
                            <td width="50px">:</td>
                            <td><?= date('d F Y', strtotime($d['tgl_selesai'])) ?> </td>
                        </tr>
                        <tr>
                            <td width="200px">Bidang Tugas</td>
                            <td width="50px">:</td>
                            <td><?= $d['bidang_tugas'] ?></td>
                        </tr>
                        <tr>
                            <td width="200px">Deskripsi Kegiatan</td>
                            <td width="50px">:</td>
                            <td><?= $d['des'] ?> Hari</td>
                        </tr>
                        <tr>
                            <td width="200px">Metode Pelaksanaan </td>
                            <td width="50px">:</td>
                            <td><?= $d['metode'] ?></td>
                        </tr>

                        <tr>
                            <td width="200px">No SK Penugasan</td>
                            <td width="50px">:</td>
                            <td><?= $d['no_sk'] ?></td>
                        </tr>

                        <tr>
                            <td width="100px">Tanggal SK Penugasan</td>
                            <td width="50px">:</td>
                            <td><?= date('d F Y', strtotime($d['tgl_sk'])) ?> </td>
                        </tr>

                        </tr>

                        <tr>
                            <td width="100px">Dokumen Pendukung</td>
                            <td width="50px">:</td>
                            <td> <a href="<?= base_url('archive/detasering/'); ?><?= $d['file'] ?>" target="_blank"> Lihat Data</a> </td>
                        </tr>

                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <a href="<?= base_url('detasering') ?>">
                    <button type="button" class="btn btn-danger"><i class="fa fa-backward"></i> Kembali</button>
                </a>
            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->