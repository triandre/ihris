<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Data Bimbingaan Dosen</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('home') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Pel. Pendidikan</a></li>
                        <li class="breadcrumb-item"><a href="#">Bimbingan Dosen</a></li>
                        <li class="breadcrumb-item active">Detail</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Detail Usulan</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-striped" id="users">
                    <tbody>
                        <tr>
                            <td width="100px">nidn</td>
                            <td width="50px">:</td>
                            <td><?= $d['nidn'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">NIDN Pembimbing</td>
                            <td width="50px">:</td>
                            <td><?= $d['nidn_pembimbing']; ?> </td>
                        </tr>
                        <tr>
                            <td width="200px">Nama Pembimbing</td>
                            <td width="50px">:</td>
                            <td><?= $d['nama_pembimbing'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Nama Bimbingan</td>
                            <td width="50px">:</td>
                            <td><?= $d['nama_bimbingan'] ?></td>
                        </tr>

                        <tr>
                            <td width="100px">Tanggal Mulai</td>
                            <td width="50px">:</td>
                            <td><?= $d['tgl_mulai'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Tanggal Selesai</td>
                            <td width="50px">:</td>
                            <td><?= $d['tgl_selesai'] ?></td>
                        </tr>

                        <tr>
                            <td width="100px">Dokumen Pendukung</td>
                            <td width="50px">:</td>
                            <td> <a href="<?= base_url('archive/bimbingan_dosen/'); ?><?= $d['file']; ?>" target="_blank" rel="noopener noreferrer">File Pendukung </a></td>
                        </tr>
                    </tbody>
                </table>

            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <a href="<?= base_url('pemdos') ?>">
                    <button type="button" class="btn btn-danger"><i class="fa fa-backward"></i> Kembali</button>
                </a>
            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->