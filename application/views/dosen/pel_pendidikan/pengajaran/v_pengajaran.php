<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $title; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dosen') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Pelaks. Pendidikan</a></li>
                        <li class="breadcrumb-item active">Pengajaran</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <?php if ($this->session->flashdata('gagal_store')) { ?>
                    <div class="alert alert-danger col-md-12">
                        <?= $this->session->flashdata('gagal_store') ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php } ?>
                <h3 class="card-title">
                    <!-- <a href="<?= base_url('createPengajaran'); ?>" class="btn btn-block bg-gradient-primary"><i class="fa fa-plus"></i> Tambah</a> -->
                </h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">

                <?= $this->session->flashdata('message'); ?>
                <div class="table-responsive">
                    <table id="example1" class="table table-bordered table-striped table-sm">
                        <thead>

                            <tr>
                                <th>No.</th>
                                <th>ID. Matakul</th>
                                <th>Kode Matakul</th>
                                <th>Matakul</th>
                                <th>Prodi</th>
                                <th>Kelas</th>
                                <th>Ruangan</th>
                                <th>Jadwal</th>
                                <th>Sks</th>
                                <th>T.A</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            <?php foreach ($v as $jp) : ?>
                                <tr>
                                    <td><?= $i; ?></td>
                                    <td><?= $jp['ID_MATKUL']; ?></td>
                                    <td><?= $jp['KODE_MATKUL']; ?></td>
                                    <td><?= $jp['NAMA_MATKUL']; ?></td>
                                    <td><?= $jp['NAMA_PRODI']; ?></td>
                                    <td><?= $jp['KELAS']; ?></td>
                                    <td><?= $jp['RUANGAN']; ?></td>
                                    <td><?= $jp['JADWAL']; ?></td>
                                    <td><?= $jp['SKS']; ?></td>
                                    <td><?= $jp['TAHUN_AKADEMIK']; ?></td>
                                </tr>
                                <?php $i++; ?>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">

            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->

</div>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
    $(function() {
        $("#example1").DataTable({
            "language": {
                "sSearch": "Cari"
            }
        });
    });
</script>