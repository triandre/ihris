<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Detail Data Sertifikasi</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('home') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Profil</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url('aset_berwujud') ?>">Kompetensi</a></li>
                        <li class="breadcrumb-item active">Detail Data Sertifikasi</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">

                <table class="table table-striped" id="users">
                    <tbody>
                        <tr>
                            <td width="100px">NIDN</td>
                            <td width="50px">:</td>
                            <td><?= $d['nidn'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Jenis Sertifikasi</td>
                            <td width="50px">:</td>
                            <td><?= $d['jenis_sertifikasi']; ?> </td>
                        </tr>
                        <tr>
                            <td width="100px">Bidang Studi</td>
                            <td width="50px">:</td>
                            <td><?= $d['bidang_studi']; ?> </td>
                        </tr>
                        <tr>
                            <td width="200px">No. SK Sertifikasi</td>
                            <td width="50px">:</td>
                            <td><?= $d['no_sk_sertifikasi'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Tahun Sertifikasi</td>
                            <td width="50px">:</td>
                            <td><?= $d['tahun_sertifikasi'] ?></td>
                        </tr>

                        <tr>
                            <td width="100px">No. Peserta </td>
                            <td width="50px">:</td>
                            <td><?= $d['no_peserta']; ?></td>
                        </tr>

                        <tr>
                            <td width="100px">No. Registrasi</td>
                            <td width="50px">:</td>
                            <td><?= $d['no_registrasi'] ?></td>
                        </tr>


                        <tr>
                            <td width="100px">Dokumen</td>
                            <td width="50px">:</td>
                            <td> <a href="<?= base_url('archive/sertifikasi/'); ?><?= $d['file'] ?>" target="_blank"> Lihat Data</a> </td>
                        </tr>

                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <a href="javascript:history.back()" class="btn btn-danger"> <i class="fa fa-backward"></i> Kembali</a>
            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->