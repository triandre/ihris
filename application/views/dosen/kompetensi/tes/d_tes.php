<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Detail Data Sertifikasi</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('home') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Profil</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url('aset_berwujud') ?>">Kompetensi</a></li>
                        <li class="breadcrumb-item active">Detail Data Sertifikasi</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">

                <table class="table table-striped" id="users">
                    <tbody>
                        <tr>
                            <td width="100px">NIDN</td>
                            <td width="50px">:</td>
                            <td><?= $d['nidn'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Jenis Tes</td>
                            <td width="50px">:</td>
                            <td><?= $d['jenis_tes']; ?> </td>
                        </tr>
                        <tr>
                            <td width="100px">Nama Tes</td>
                            <td width="50px">:</td>
                            <td><?= $d['nama_tes']; ?> </td>
                        </tr>
                        <tr>
                            <td width="200px">Penyelenggara</td>
                            <td width="50px">:</td>
                            <td><?= $d['penyelenggara'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Tanggal Tes</td>
                            <td width="50px">:</td>
                            <td><?= $d['tgl_tes'] ?></td>
                        </tr>

                        <tr>
                            <td width="100px">Tahun </td>
                            <td width="50px">:</td>
                            <td><?= $d['tahun']; ?></td>
                        </tr>

                        <tr>
                            <td width="100px">Skor Tes</td>
                            <td width="50px">:</td>
                            <td><?= $d['skor_tes'] ?></td>
                        </tr>


                        <tr>
                            <td width="100px">Dokumen</td>
                            <td width="50px">:</td>
                            <td> <a href="<?= base_url('archive/tes/'); ?><?= $d['file'] ?>" target="_blank"> Lihat Data</a> </td>
                        </tr>

                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <a href="javascript:history.back()" class="btn btn-danger"> <i class="fa fa-backward"></i> Kembali</a>
            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->