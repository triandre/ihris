<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Detail Jabatan Fungsional</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('home') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Profil</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url('aset_berwujud') ?>">Jabatan Fungsional</a></li>
                        <li class="breadcrumb-item active">Detail Jabatan Fungsional</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Detail Jabatan Fungsional</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">

                <table class="table table-striped" id="users">
                    <tbody>
                        <tr>
                            <td width="100px">NIDN</td>
                            <td width="50px">:</td>
                            <td><?= $d['nidn'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Jabatan Fungsional</td>
                            <td width="50px">:</td>
                            <td><?= $d['jafung']; ?> </td>
                        </tr>
                        <tr>
                            <td width="200px">Nomor SK</td>
                            <td width="50px">:</td>
                            <td><?= $d['no_sk_jafung'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Tanggal SK </td>
                            <td width="50px">:</td>
                            <td><?= $d['sk_terhitung'] ?></td>
                        </tr>

                        <tr>
                            <td width="100px">Angka Kredit </td>
                            <td width="50px">:</td>
                            <td><?= $d['skor']; ?></td>
                        </tr>

                        <tr>
                            <td width="100px">Kelebihan Pengajaran </td>
                            <td width="50px">:</td>
                            <td><?= $d['kelebihan_pengajaran'] ?></td>
                        </tr>

                        <tr>
                            <td width="100px">Kelebihan Penelitian </td>
                            <td width="50px">:</td>
                            <td><?= $d['kelebihan_penelitian'] ?></td>
                        </tr>


                        <tr>
                            <td width="100px">Kelebihan Pengabdian Masyarakat </td>
                            <td width="50px">:</td>
                            <td><?= $d['kelebihan_pangmas'] ?></td>
                        </tr>

                        <tr>
                            <td width="100px">Kelebihan Kegiatan Penunjang </td>
                            <td width="50px">:</td>
                            <td><?= $d['kelebihan_penunjang'] ?></td>
                        </tr>


                        <tr>
                            <td width="100px">Dokumen</td>
                            <td width="50px">:</td>
                            <td> <a href="<?= base_url('archive/jafung/'); ?><?= $d['file'] ?>" target="_blank"> Lihat Data</a> </td>
                        </tr>

                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <a href="javascript:history.back()" class="btn btn-danger"> <i class="fa fa-backward"></i> Kembali</a>
            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->