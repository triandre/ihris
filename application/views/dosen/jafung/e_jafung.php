<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Form edit Jabatan Fungsional</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dosen') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">profil</a></li>
                        <li class="breadcrumb-item active">Jabatan Fungsional</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>
    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title">Form edit Jabatan Fungsional</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-danger col-md-8 alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?= validation_errors(); ?>
                    </div>
                <?php endif ?>


                <form class="form-horizontal" action="<?= base_url('updateJafung'); ?>" enctype="multipart/form-data" autocomplete="off" method="post">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tbody>
                                    <input type="hidden" name="id_jafung" value="<?= $d['id_jafung'] ?>">
                                    <tr>
                                        <td>Jabatan Fungsional <span style="color:red;">*</span> </td>
                                        <td>
                                            <select id="id_jfungsional" name="id_jfungsional" class="form-control selectx" required>
                                                <option selected disabled value="">Pilih</option>
                                                <?php foreach ($mjf as $key) { ?>
                                                    <option value="<?= $key['id_jfungsional']; ?>" <?= ($d['id_jfungsional'] == $key['id_jfungsional']) ? 'selected' : ''; ?>><?= $key['jafung']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Nomor SK <span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="text" id="no_sk_jafung" name="no_sk_jafung" value="<?= $d['no_sk_jafung']; ?>" required> </td>
                                    </tr>

                                    <tr>
                                        <td>Terhitung Mulai Tanggal <span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="date" id="sk_terhitung" name="sk_terhitung" value="<?= $d['sk_terhitung']; ?>" required> </td>
                                    </tr>


                                    <tr>
                                        <td>Kelebihan Pengajaran</td>
                                        <td><input class="form-control" type="text" id="kelebihan_pengajaran" name="kelebihan_pengajaran" value="<?= $d['kelebihan_pengajaran']; ?>">
                                            <span><i>Masukkan angka menggunakan tanda titik.</i></span>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Kelebihan Penelitian</td>
                                        <td><input class="form-control" type="text" id="kelebihan_penelitian" name="kelebihan_penelitian" value="<?= $d['kelebihan_penelitian']; ?>">
                                            <span><i>Masukkan angka menggunakan tanda titik.</i></span>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Kelebihan Pengabdian Masyarakat</td>
                                        <td><input class="form-control" type="text" id="kelebihan_pangmas" name="kelebihan_pangmas" value="<?= $d['kelebihan_pangmas']; ?>">
                                            <span><i>Masukkan angka menggunakan tanda titik.</i></span>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Kelebihan Kegiatan Penunjang</td>
                                        <td><input class="form-control" type="text" id="kelebihan_penunjang" name="kelebihan_penunjang" value="<?= $d['kelebihan_penunjang']; ?>">
                                            <span><i>Masukkan angka menggunakan tanda titik.</i></span>
                                        </td>
                                    </tr>


                                    <tr>
                                        <td>Upload Dokumen<br />
                                            <small><i>(Maksimal total ukuran file dalam sekali proses upload : <br> <b><u>5 MB, Format Pdf (Jadikan 1 File)</u></b>)</i></small>
                                        </td>
                                        <td><input type="file" class="form-control" name="file" id="file" accept="application/pdf" required></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>


                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <a href="<?= base_url('aset_wujud') ?>">
                            <a href="javascript:history.back()" class="btn btn-danger"> <i class="fa fa-backward"></i> Kembali</a>
                        </a>
                        <button type="submit" class="btn btn-primary"> <i class="fa fa-save"></i> Simpan</button>
                    </div>
                    <!-- /.card-footer -->
                </form>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">

            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->





</div>
<!-- /.content-wrapper -->
<!-- Select2 -->
<script src="<?= base_url() ?>assets/vendor/backend/plugins/select2/js/select2.full.min.js"></script>
<script src="<?= base_url() ?>assets/vendor//js/rupiah.js"></script>
<script src="<?= base_url() ?>assets/vendor//js/duit.js"></script>
<script>
    $(document).ready(function() {
        $('.selectx').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $(document).ready(function() {
        $('.selecty').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $(document).ready(function() {
        $('.selectz').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $('#id_user').change(function() {
        var id_user = $(this).val();
        $.ajax({
            url: "<?= site_url('get-user'); ?>",
            method: "POST",
            data: {
                id_user: id_user
            },
            async: true,
            dataType: 'json',
            success: function(data) {

                var jbt = '';

                var i;
                for (i = 0; i < data.length; i++) {
                    jbt = data[i].jabatan;
                }
                document.getElementById("jabatan").value = jbt;
            }
        });
        return false;
    });


    $(document).ready(function() {

        $('#id_kampus').change(function() {
            var id_kampus = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-tempat'); ?>",
                method: "POST",
                data: {
                    id_kampus: id_kampus
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_gedung + '>' + data[i].gedung + '</option>';
                    }
                    $('#id_gedung').html(html);

                }
            });
            return false;
        });

        $('.selectw').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });



    $(document).ready(function() {

        $('#id_gedung').change(function() {
            var id_gedung = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-gedung'); ?>",
                method: "POST",
                data: {
                    id_gedung: id_gedung
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_lantai + '>' + data[i].lantai + '</option>';
                    }
                    $('#id_lantai').html(html);

                }
            });
            return false;
        });

        $('.selectw').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });

    $(document).ready(function() {

        $('#id_lantai').change(function() {
            var id_lantai = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-ruangan'); ?>",
                method: "POST",
                data: {
                    id_lantai: id_lantai
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_ruangan + '>' + data[i].ruangan + '</option>';
                    }
                    $('#id_ruangan').html(html);

                }
            });
            return false;
        });

        $('.selecta').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });
</script>