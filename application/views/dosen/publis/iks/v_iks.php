<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $title; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dosen') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Permohonan Insentif</a></li>
                        <li class="breadcrumb-item active">Usulan Insentif Buku Ajar</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <?php if ($this->session->flashdata('gagal_store')) { ?>
                    <div class="alert alert-danger col-md-12">
                        <?= $this->session->flashdata('gagal_store') ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php } ?>

                <h3 class="card-title">
                    <a href="<?= base_url('createIks'); ?>" class="btn btn-block bg-gradient-primary"><i class="fa fa-plus"></i> Tambah</a>
                </h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <?= $this->session->flashdata('message'); ?>
                <div class="table-responsive">

                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <table class="table table-bordered" id="example1" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Permohonan Insentif Keynote Speaker</th>
                            <?php if ($akun['role_id'] == '4') { ?>
                                <th>Catatan Validator</th>
                            <?php } ?>
                            <?php if ($akun['role_id'] == '5') { ?>
                                <th>Catatan WR I</th>
                            <?php } ?>
                            <th>#</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        <?php foreach ($iks as $ks) : ?>
                            <tr>
                                <th scope="row"><?= $i; ?></th>
                                <td><small>Tanggal Permohonan, <?= date("d F Y", strtotime($ks['date_created'])) ?></small>
                                    <br>
                                    <?= $ks['materi']; ?> - <?= $ks['lokasi']; ?>
                                    <br>
                                    <small>Tingkat Kegiatan : <?= $ks['tingkat_kegiatan']; ?></small>
                                    <br>
                                    <small><i>Tanggal Kegiatan : <?= $ks['tgl_kegiatan']; ?></i></small>
                                    <br>
                                    <small><i>Status : <?php
                                                        if ($ks['sts'] == 1) {
                                                            echo '<span class="badge badge-info">Proses LP2M</span> ';
                                                        } elseif ($ks['sts'] == 2) {
                                                            echo '<span class="badge badge-primary">Proses Wakil Rektor I</span>';
                                                        } elseif ($ks['sts'] == 3) {
                                                            echo '<span class="badge badge-warning">Proses Wakil Rektor II</span>';
                                                        } elseif ($ks['sts'] == 4) {
                                                            echo '<span class="badge badge-success">Di Terima</span>';
                                                        } elseif ($ks['sts'] == 6) {
                                                            echo '<span class="badge badge-danger">Berkas DiTolak</span>';
                                                        }  ?></i></small>
                                    <br>
                                    Bukti Kegiatan : <a href="<?= base_url('file/') ?><?= $ks['upload_bukti_kegiatan']; ?>" target="_blank"><i class="fa fa-download"></i> Download </a><br>
                                    Bukti Undangan : <a href="<?= base_url('file/') ?><?= $ks['upload_undangan']; ?>" target="_blank"><i class="fa fa-download"></i> Download </a><br>
                                    Bukti Sertifikat : <a href="<?= base_url('file/') ?><?= $ks['upload_sertifikat']; ?>" target="_blank"><i class="fa fa-download"></i> Download </a><br>
                                    <?php
                                    if ($akun['role_id'] >= '3') { ?>
                                        <a href="<?= base_url('user/detail/'); ?><?= $ks['email']; ?>"><small><i>Pemohon:<?= $ks['email']; ?></i></small></a> <br>
                                        Insentif Diusulkan : Rp. <?= number_format($ks['insentif']); ?>
                                    <?php } ?>
                                    <br>
                                    <?php
                                    if ($ks['sts'] == '6') { ?>
                                        <small style="color:red;">Catatan Validator : <?= $ks['ket']; ?><br>
                                            Catatan WR II: <?= $ks['tolak2']; ?>
                                        </small>
                                    <?php } ?>

                                    <?php
                                    if ($akun['role_id'] == '4') { ?>
                                <td> <?= $ks['ket']; ?> </td>
                            <?php } ?>
                            <?php
                            if ($akun['role_id'] == '5') { ?>
                                <td> <?= $ks['catatan1']; ?> </td>
                            <?php } ?>
                            <td>
                                <div class="dropdown">
                                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-cog"></i>
                                    </button>
                                    <div class="dropdown-menu">

                                        <a href="<?= base_url('iks/detail/'); ?><?= $ks['id']; ?>" class="dropdown-item">Detail</a>


                                        <?php
                                        if ($akun['role_id'] == '2') { ?>
                                            <?php
                                            if ($ks['sts'] == '6') { ?>
                                                <?php
                                                if ($ks['batch'] == '6') { ?>
                                                    <a href="<?= base_url('iks/perbaikan/'); ?><?= $ks['id']; ?>" class="dropdown-item">Perbaiki Berkas</a>
                                                <?php } ?>
                                            <?php } ?>
                                        <?php } ?>

                                        <?php
                                        if ($akun['role_id'] == '3') { ?>
                                            <?php
                                            if ($ks['sts'] == '1') { ?>
                                                <a href="<?= base_url('insentif/iks/setuju/'); ?><?= $ks['id']; ?>" class="dropdown-item">Setuju Berkas</i></a>
                                                <a href="<?= base_url('insentif/iks/tolak/'); ?><?= $ks['id']; ?>" class="dropdown-item">Tolak Berkas</i></a>
                                            <?php } ?>
                                        <?php } ?>

                                        <?php
                                        if ($akun['role_id'] == '4') { ?>
                                            <?php
                                            if ($ks['sts'] == '2') { ?>
                                                <a href="<?= base_url('wr1/iks/fee/'); ?><?= $ks['id']; ?>" class="dropdown-item">Setuju</i></a>
                                                <a href="<?= base_url('wr1/iks/tolak/'); ?><?= $ks['id']; ?>" class="dropdown-item">Tolak</i></a>
                                            <?php } ?>
                                        <?php } ?>

                                        <?php
                                        if ($akun['role_id'] == '5') { ?>
                                            <?php
                                            if ($ks['sts'] == '3') { ?>
                                                <a href="<?= base_url('wr2/iks/fee/'); ?><?= $ks['id']; ?>" class="dropdown-item">Setuju</i></a>
                                                <a href="<?= base_url('wr2/iks/tolak/'); ?><?= $ks['id']; ?>" class="dropdown-item">Tolak</i></a>
                                            <?php } ?>
                                        <?php } ?>

                                        <?php
                                        if ($akun['role_id'] == '2') { ?>
                                            <?php
                                            if ($ks['sts'] == '4') { ?>
                                                <a href="<?= base_url('iks/cetak/'); ?><?= $ks['id']; ?>" class="dropdown-item" target="_blank">Cetak Kwitansi</a>
                                            <?php } ?>
                                        <?php } ?>


                                    </div>
                                </div>


                            </td>
                            </tr>
                            <?php $i++; ?>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->

</div>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
    $(function() {
        $("#example1").DataTable({
            "language": {
                "sSearch": "Cari"
            }
        });
    });
</script>