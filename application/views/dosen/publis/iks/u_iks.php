<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Form Usulan Insentif Keynote Speaker</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dosen') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Permohonan Insentif</a></li>
                        <li class="breadcrumb-item active">UI. Keynote Speaker</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>
    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title">Form Usulan Insentif Keynote Speaker</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?= validation_errors(); ?>
                    </div>
                <?php endif ?>
                <form action="<?= base_url('iks/perbaikanGo'); ?>" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <h4>Ubah Form Usulan Insentif Keynote Speaker</h4>
                        <small>Isi data-data Berikut </small>
                        <hr>
                        <input type="text" class="form-control" id="id" name="id" value="<?= $d['id']; ?>" hidden>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <label>Materi : <small class="text-danger">(*)Wajib Diisi.</small></label>
                                <input type="text" class="form-control" id="materi" name="materi" value="<?= $d['materi']; ?>">
                                <?= form_error('materi', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <label>Lokasi : <small class="text-danger">(*)Wajib Diisi.</small></label>
                                <input type="text" class="form-control" id="lokasi" name="lokasi" value="<?= $d['lokasi']; ?>">
                                <?= form_error('lokasi', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-4">
                                <label>Tgl Pelaksanaan : <small class="text-danger">(*)Wajib Diisi.</small></label>
                                <input type="date" class="form-control" id="tgl_kegiatan" name="tgl_kegiatan" value="<?= $d['tgl_kegiatan']; ?>">
                                <?= form_error('tgl_kegiatan', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                            <div class="col-sm-4">
                                <label>Tingkat Kegiatan : <small class="text-danger">(*)Wajib Diisi.</small></label>
                                <select class="form-control" id="tingkat_kegiatan" name="tingkat_kegiatan">
                                    <option selected disabled value="<?= $d['tingkat_kegiatan']; ?>"> <?= $d['tingkat_kegiatan']; ?></option>
                                    <option value="Nasional">Nasional</option>
                                    <option value="Internasioanal">Internasioanal</option>
                                </select>
                                <?= form_error('tingkat_kegiatan', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                            <div class="col-sm-4">
                                <label>Nomor Sertifikat : <small class="text-danger">(*)Wajib Diisi.</small></label>
                                <input type="text" class="form-control" id="no_sertifikat" name="no_sertifikat" value="<?= $d['no_sertifikat']; ?>">
                                <?= form_error('no_sertifikat', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>



                        <div class="form-group row">
                            <div class="col-sm-4">
                                <label> Upload Undangan Sebagai Keynote Speaker : <small class="text-danger">File PDF (*)Wajib Diisi.</small></label>
                                <input type="file" class="form-control" id="upload_undangan" name="upload_undangan" accept="application/pdf">
                                <?= form_error('upload_undangan', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>

                            <div class="col-sm-4">
                                <label> Upload Documentasi Acara: <small class="text-danger">Minimal 3 Foto Dalam 1 File PDF (*)Wajib Diisi.</small></label>
                                <input type="file" class="form-control" id="upload_bukti_kegiatan" name="upload_bukti_kegiatan" accept="application/pdf">
                                <?= form_error('upload_bukti_kegiatan', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>


                            <div class="col-sm-4">
                                <label>Upload Sertifikat: <small class="text-danger">Pdf (*)Wajib Diisi.</small></label>
                                <input type="file" class="form-control" id="upload_sertifikat" name="upload_sertifikat" accept="application/pdf">
                                <?= form_error('upload_sertifikat', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>

                    </div>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                </form>
            </div>

        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->





</div>
<!-- /.content-wrapper -->
<!-- Select2 -->
<script src="<?= base_url() ?>assets/vendor/backend/plugins/select2/js/select2.full.min.js"></script>
<script src="<?= base_url() ?>assets/vendor//js/rupiah.js"></script>
<script src="<?= base_url() ?>assets/vendor//js/duit.js"></script>
<script>
    $(document).ready(function() {
        $('.selectx').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $(document).ready(function() {
        $('.selecty').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $(document).ready(function() {
        $('.selectz').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $('#id_user').change(function() {
        var id_user = $(this).val();
        $.ajax({
            url: "<?= site_url('get-user'); ?>",
            method: "POST",
            data: {
                id_user: id_user
            },
            async: true,
            dataType: 'json',
            success: function(data) {

                var jbt = '';

                var i;
                for (i = 0; i < data.length; i++) {
                    jbt = data[i].jabatan;
                }
                document.getElementById("jabatan").value = jbt;
            }
        });
        return false;
    });


    $(document).ready(function() {

        $('#id_kampus').change(function() {
            var id_kampus = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-tempat'); ?>",
                method: "POST",
                data: {
                    id_kampus: id_kampus
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_gedung + '>' + data[i].gedung + '</option>';
                    }
                    $('#id_gedung').html(html);

                }
            });
            return false;
        });

        $('.selectw').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });



    $(document).ready(function() {

        $('#id_gedung').change(function() {
            var id_gedung = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-gedung'); ?>",
                method: "POST",
                data: {
                    id_gedung: id_gedung
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_lantai + '>' + data[i].lantai + '</option>';
                    }
                    $('#id_lantai').html(html);

                }
            });
            return false;
        });

        $('.selectw').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });

    $(document).ready(function() {

        $('#id_lantai').change(function() {
            var id_lantai = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-ruangan'); ?>",
                method: "POST",
                data: {
                    id_lantai: id_lantai
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_ruangan + '>' + data[i].ruangan + '</option>';
                    }
                    $('#id_ruangan').html(html);

                }
            });
            return false;
        });

        $('.selecta').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });
</script>