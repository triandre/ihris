<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $title; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dosen') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">profil</a></li>
                        <li class="breadcrumb-item active">Inpassing</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <?php if ($this->session->flashdata('gagal_store')) { ?>
                    <div class="alert alert-danger col-md-12">
                        <?= $this->session->flashdata('gagal_store') ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php } ?>

                <h3 class="card-title">
                    <a href="<?= base_url('createAji'); ?>" class="btn btn-block bg-gradient-primary"><i class="fa fa-plus"></i> Tambah</a>
                </h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <?= $this->session->flashdata('message'); ?>
                <div class="table-responsive">
                    <table id="example1" class="table table-bordered table-striped table-sm">
                        <thead>
                            <tr>
                                <th class="text-center" style="width:5%;">No</th>
                                <th style="width:70%;">Permohonan Insentif Artikel Dijurnal</th>
                                <?php if ($akun['role_id'] == '4') { ?>
                                    <th>Catatan Validator</th>
                                <?php } ?>
                                <?php if ($akun['role_id'] == '5') { ?>
                                    <th class="text-center">Catatan WR I</th>
                                <?php } ?>
                                <th class="text-center" style="width:5%;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            <?php foreach ($aji as $aj) : ?>
                                <tr>
                                    <th class="text-center" scope="row"><?= $i; ?></th>
                                    <td><small>Tanggal permohonan, <?= date("d F Y", strtotime($aj['date_created'])) ?></small><br>
                                        <?= $aj['judul']; ?> - <?= $aj['nama_jurnal']; ?> <br>
                                        <small><?= $aj['kategori_jurnal']; ?></small> <br>
                                        <small><?= $aj['lembaga_pengindeks']; ?> - <?= $aj['eissn']; ?> <br>
                                            <small>Status : <?php
                                                            if ($aj['sts'] == 1) {
                                                                echo '<span class="badge badge-info">Proses LP2M</span> ';
                                                            } elseif ($aj['sts'] == 2) {
                                                                echo '<span class="badge badge-primary">Proses Wakil Rektor I</span>';
                                                            } elseif ($aj['sts'] == 3) {
                                                                echo '<span class="badge badge-warning">Proses Wakil Rektor II</span>';
                                                            } elseif ($aj['sts'] == 4) {
                                                                echo '<span class="badge badge-success">Di Terima</span>';
                                                            } elseif ($aj['sts'] == 6) {
                                                                echo '<span class="badge badge-danger">Berkas DiTolak</span>';
                                                            }
                                                            ?> </small> <br>
                                            <?php
                                            if ($akun['role_id'] >= '3') { ?>
                                                Standart Pagu Anggaran Lembaga Pengindeks: <b><?= $aj['lembaga_pengindeks']; ?> = Rp. <?= number_format($aj['pagu']);  ?> </b> <br>
                                                Insentif Diajukan LP2M = <b> Rp. <?= number_format($aj['insentif']); ?> </b> <br>

                                                <a href="<?= base_url('user/detail/'); ?><?= $aj['email']; ?>"><i>Pemohon:<?= $aj['email']; ?></i></a>
                                            <?php } ?>
                                        </small>
                                        <br>
                                        <small> <?php
                                                if ($aj['sts'] == '6') { ?>
                                                Catatan : <?= $aj['ket']; ?>
                                            <?php } ?></small>
                                    </td>
                                    <?php
                                    if ($akun['role_id'] == '4') { ?>
                                        <td> <?= $aj['ket']; ?> </td>
                                    <?php } ?>
                                    <?php
                                    if ($akun['role_id'] == '5') { ?>
                                        <td> <?= $aj['catatan1']; ?> </td>
                                    <?php } ?>


                                    <td class="text-center">
                                        <div class="dropdown">
                                            <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
                                                <i class="fa fa-cog"></i>
                                            </button>
                                            <div class="dropdown-menu">
                                                <a href="<?= base_url('aji/detail/'); ?><?= $aj['id']; ?>" class="dropdown-item">Detail</a>


                                                <?php
                                                if ($akun['role_id'] == '2') { ?>
                                                    <?php
                                                    if ($aj['sts'] == '6') { ?>
                                                        <?php
                                                        if ($aj['batch'] == '6') { ?>
                                                            <a href="<?= base_url('aji/perbaikan/'); ?><?= $aj['id']; ?>" class="dropdown-item">Perbaiki Berkas</a>
                                                        <?php } ?>
                                                    <?php } ?>
                                                    <?php
                                                    if ($aj['sts'] == '2') { ?>
                                                        <a href="<?= base_url('aji/edit/'); ?><?= $aj['id']; ?>" class="dropdown-item">Ubah Data</a>
                                                    <?php } ?>

                                                <?php } ?>

                                                <?php
                                                if ($akun['role_id'] == '3') { ?>
                                                    <?php
                                                    if ($aj['sts'] == '1') { ?>
                                                        <a href="<?= base_url('insentif/aji/setuju/'); ?><?= $aj['id']; ?>" class="dropdown-item">Setuju Berkas</i></a>
                                                        <a href="<?= base_url('insentif/aji/tolak/'); ?><?= $aj['id']; ?>" class="dropdown-item">Tolak Berkas</i></a>
                                                        <a href="<?= base_url('aji/history/'); ?><?= $aj['email']; ?>" class="dropdown-item">History</a>
                                                    <?php } ?>
                                                <?php } ?>

                                                <?php
                                                if ($akun['role_id'] == '4') { ?>
                                                    <?php
                                                    if ($aj['sts'] == '2') { ?>
                                                        <a href="<?= base_url('wr1/aji/fee/'); ?><?= $aj['id']; ?>" class="dropdown-item">Setuju</i></a>
                                                        <a href="<?= base_url('wr1/aji/tolak/'); ?><?= $aj['id']; ?>" class="dropdown-item">Tolak</i></a>
                                                        <a href="<?= base_url('aji/history/'); ?><?= $aj['email']; ?>" class="dropdown-item">History</a>
                                                    <?php } ?>
                                                <?php } ?>

                                                <?php
                                                if ($akun['role_id'] == '5') { ?>
                                                    <?php
                                                    if ($aj['sts'] == '3') { ?>
                                                        <a href="<?= base_url('wr2/aji/fee/'); ?><?= $aj['id']; ?>" class="dropdown-item">Setuju</i></a>
                                                        <a href="<?= base_url('wr2/aji/tolak/'); ?><?= $aj['id']; ?>" class="dropdown-item">Tolak</i></a>
                                                        <a href="<?= base_url('aji/history/'); ?><?= $aj['email']; ?>" class="dropdown-item">History</a>
                                                    <?php } ?>
                                                <?php } ?>



                                            </div>
                                        </div>

                                    </td>


                                </tr>
                                <?php $i++; ?>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">

            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->

</div>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
    $(function() {
        $("#example1").DataTable({
            "language": {
                "sSearch": "Cari"
            }
        });
    });
</script>