<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Detail Usulan Bantuan Hki/Paten</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">Dashboard</>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Permohonan Bantuan</a></li>
                        <li class="breadcrumb-item">Usulan Bantuan Hki/Paten</li>
                        <li class="breadcrumb-item active">Detail</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Detail Usulan</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-striped" id="users">
                    <tbody>
                        <tr>
                            <td width="100px">Email</td>
                            <td width="50px">:</td>
                            <td><?= $d['email'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Judul Hki/Paten</td>
                            <td width="50px">:</td>
                            <td><?= $d['judul_hki']; ?> </td>
                        </tr>
                        <tr>
                            <td width="200px">Jenis Hki/Paten</td>
                            <td width="50px">:</td>
                            <td><?= $d['jenis_hki'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Kategori HKI</td>
                            <td width="50px">:</td>
                            <td><?= $d['kategori_hki'] ?></td>
                        </tr>

                        <tr>
                            <td width="100px">Nomor Registrasi</td>
                            <td width="50px">:</td>
                            <td><?= $d['no_reg'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Tanggal Pengajuan HKI</td>
                            <td width="50px">:</td>
                            <td><?= $d['tgl_pengajuan_hki'] ?></td>
                        </tr>

                        <tr>
                            <td width="100px">Apakah HKI Merupakan Hasil Penelitian</td>
                            <td width="50px">:</td>
                            <td><?= $d['pertanyaan1'] ?></td>
                        </tr>


                        <tr>
                            <td width="100px">Berkas Pendukung</td>
                            <td width="50px">:</td>
                            <td><a href="<?= base_url('file/') ?><?= $d['upload_file']; ?>" target="_blank"><i class="fa fa-download"></i></td>
                        </tr>
                    </tbody>
                </table>

            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <a href="javascript:history.back()" class="btn btn-danger"> <i class="fa fa-backward"></i> Kembali</a>
            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->