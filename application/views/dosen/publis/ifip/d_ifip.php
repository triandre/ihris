<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Detail Usulan Insentif Forum Ilmiah (Proceeding)</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('home') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Permohonan Insentif</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url('') ?>">UI. Forum Ilmiah (Proceeding)</a></li>
                        <li class="breadcrumb-item active">Detail</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Detail Usulan</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <h5>IDENTITAS ARTIKEL</h5>
                <table class="table table-striped" id="users">
                    <tbody>
                        <tr>
                            <td width="100px">Email</td>
                            <td width="50px">:</td>
                            <td><?= $d['email'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Kontribusi Pemakalah</td>
                            <td width="50px">:</td>
                            <td><?= $d['kontribusi']; ?> </td>
                        </tr>
                        <tr>
                            <td width="200px">Kategori Forum Ilmiah</td>
                            <td width="50px">:</td>
                            <td><?= $d['kategori'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Nama Forum Ilmiah (Konfrensi/Seminar) </td>
                            <td width="50px">:</td>
                            <td><?= $d['nama_forum'] ?></td>
                        </tr>

                        <tr>
                            <td width="100px">Institusi Penyelenggara</td>
                            <td width="50px">:</td>
                            <td><?= $d['institusi_penyelenggara'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Tempat/Lokasi Kegiatan (isi Kota - Provinsi)</td>
                            <td width="50px">:</td>
                            <td><?= $d['lokasi'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Tanggal Pelaksanaan</td>
                            <td width="50px">:</td>
                            <td><?= $d['tgl_pelaksanaan'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">URL Website Forum Ilmiah</td>
                            <td width="50px">:</td>
                            <td><?= $d['url_web_forum'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Judul Artikel Prosiding</td>
                            <td width="50px">:</td>
                            <td><?= $d['judul_artikel_proseding'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">URL Artikel Proseding di Publikasi</td>
                            <td width="50px">:</td>
                            <td><?= $d['url_artikel_proseding_publikasi'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">ISSN/ISBN</td>
                            <td width="50px">:</td>
                            <td><?= $d['issn'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Tahun Publikasi Proseding</td>
                            <td width="50px">:</td>
                            <td><?= $d['tahun_publikasi_proseding'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Lembaga Pengindeks Proseding</td>
                            <td width="50px">:</td>
                            <td><?= $d['lembaga_pengindeks'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Nama Program Penelitian</td>
                            <td width="50px">:</td>
                            <td><?= $d['nama_program'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Tahun Penelitian</td>
                            <td width="50px">:</td>
                            <td><?= $d['tahun_penelitian'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">ARTIKEL PROSIDING</td>
                            <td width="50px">:</td>
                            <td><?= $d['issn'] ?></td>
                        </tr>

                        <tr>
                            <td width="100px">Dokumen Artikel Prosidding</td>
                            <td width="50px">:</td>
                            <td><a href="<?= base_url('file/') ?><?= $d['upload_artikel_proseding'] ?>" target="_blank"><i class="fa fa-download"></i></td>
                        </tr>
                        <tr>
                            <td width="100px">Dokumen LoA</td>
                            <td width="50px">:</td>
                            <td><a href="<?= base_url('file/') ?><?= $d['upload_loa']; ?>" target="_blank"><i class="fa fa-download"></i></td>
                        </tr>

                    </tbody>
                </table>

            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <a href="javascript:history.back()" class="btn btn-danger"> <i class="fa fa-backward"></i> Kembali</a>
            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->