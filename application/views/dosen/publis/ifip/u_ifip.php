<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Form Usulan Insentif Forum Ilmiah (Proceeding)</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dosen') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Permohonan Insentif</a></li>
                        <li class="breadcrumb-item active">UI. Forum Ilmiah (Proceeding)</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>
    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title">Form Usulan Insentif Forum Ilmiah (Proceeding)</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?= validation_errors(); ?>
                    </div>
                <?php endif ?>
                <form action="<?= base_url('ifip/perbaikanGo'); ?>" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <div class="row form-group">
                            <input hidden type="text" class="form-control" id="id" name="id" required value="<?= $d['id']; ?>" readonly>
                            <label class="col-md-4 text-md-right" for="nama_barang"><b>A. ARTIKEL PROSEDING (Forum Ilmiah)</b></label>
                        </div>
                        <div class="row form-group">
                            <label class="col-md-3 text-md-right" for="nama_barang">Kontribusi Pemakalah :</label>
                            <div class="col-md-6">
                                <select class="form-control" id="kontribusi" name="kontribusi" required>
                                    <option value="<?= $d['kontribusi']; ?>"><?= $d['kontribusi']; ?></option>
                                    <option value="Preseter (Pemakalah)">Presenter (Pemakalah)</option>
                                    <option value="Partisipant (Peserta)">Partisipant (Peserta)</option>
                                </select>
                                <small class="text-danger">(*)Wajib Diisi.</small>
                                <?= form_error('kontribusi', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>

                        <div class="row form-group">
                            <label class="col-md-3 text-md-right" for="nama_barang">Kategori Forum Ilmiah :</label>
                            <div class="col-md-6">
                                <select class="form-control" id="kategori" name="kategori" required>
                                    <option value="<?= $d['kategori']; ?>"><?= $d['kategori']; ?></option>
                                    <option value="Internasional">Internasional</option>
                                    <option value="Nasional">Nasional</option>
                                </select>
                                <small class="text-danger">(*)Wajib Diisi.</small>
                                <?= form_error('kategori', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>

                        <div class="row form-group">
                            <label class="col-md-3 text-md-right" for="nama_barang">Nama Forum Ilmiah (Konfrensi/Seminar) :</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="nama_forum" name="nama_forum" required value="<?= $d['nama_forum']; ?>">
                                <small class="text-danger">(*)Wajib Diisi.</small>
                                <?= form_error('nama_forum', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>

                        <div class="row form-group">
                            <label class="col-md-3 text-md-right" for="nama_barang">Institusi Penyelenggara :</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="institusi_penyelenggara" name="institusi_penyelenggara" required value="<?= $d['institusi_penyelenggara']; ?>">
                                <small class="text-danger">(*)Wajib Diisi.</small>
                                <?= form_error('institusi_penyelenggara', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>

                        <div class="row form-group">
                            <label class="col-md-3 text-md-right" for="nama_barang">Tempat/Lokasi Kegiatan (isi Kota - Provinsi):</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="lokasi" name="lokasi" required value="<?= $d['lokasi']; ?>">
                                <small class="text-danger">(*)Wajib Diisi.</small>
                                <?= form_error('lokasi', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>

                        <div class="row form-group">
                            <label class="col-md-3 text-md-right" for="nama_barang">Tanggal Pelaksanaan:</label>
                            <div class="col-md-6">
                                <input type="date" class="form-control" id="tgl_pelaksanaan" name="tgl_pelaksanaan" required value="<?= $d['tgl_pelaksanaan']; ?>">
                                <small class="text-danger">(*)Wajib Diisi.</small>
                                <?= form_error('tgl_pelaksanaan', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>

                        <div class="row form-group">
                            <label class="col-md-3 text-md-right" for="nama_barang">URL Website Forum Ilmiah:</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="url_web_forum" name="url_web_forum" required value="<?= $d['url_web_forum']; ?>">
                                <small class="text-danger">(*)Wajib Diisi.</small>
                                <?= form_error('url_web_forum', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>

                        <div class="row form-group">
                            <label class="col-md-3 text-md-right" for="nama_barang">Judul Artikel Prosiding:</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="judul_artikel_proseding" name="judul_artikel_proseding" required value="<?= $d['judul_artikel_proseding']; ?>">
                                <small class="text-danger">(*)Wajib Diisi.</small>
                                <?= form_error('judul_artikel_proseding', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>

                        <div class="row form-group">
                            <label class="col-md-3 text-md-right" for="nama_barang">URL Artikel Proseding di Publikasi:</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="url_artikel_proseding_publikasi" name="url_artikel_proseding_publikasi" required value="<?= $d['url_artikel_proseding_publikasi']; ?>">
                                <small class="text-danger">(*)Wajib Diisi.</small>
                                <?= form_error('url_artikel_proseding_publikasi', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>

                        <div class="row form-group">
                            <label class="col-md-3 text-md-right" for="nama_barang">ISSN/ISBN:</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="issn" name="issn" required value="<?= $d['issn']; ?>">
                                <small class="text-danger">(*)Wajib Diisi.</small>
                                <?= form_error('issn', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>

                        <div class="row form-group">
                            <label class="col-md-3 text-md-right" for="nama_barang">Tahun Publikasi Proseding:</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="tahun_publikasi_proseding" name="tahun_publikasi_proseding" required value="<?= $d['tahun_publikasi_proseding']; ?>">
                                <small class="text-danger">(*)Wajib Diisi.</small>
                                <?= form_error('tahun_publikasi_proseding', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>

                        <div class="row form-group">
                            <label class="col-md-3 text-md-right" for="nama_barang">Lembaga Pengindeks Proseding :</label>
                            <div class="col-md-6">
                                <select class="form-control" id="lembaga_pengindeks" name="lembaga_pengindeks" required>
                                    <option value="<?= $d['lembaga_pengindeks']; ?>"><?= $d['lembaga_pengindeks']; ?></option>
                                    <option value="Tidak Terindeks">Tidak Terindeks</option>
                                    <option value="Google Scholar">Google Scholar</option>
                                    <option value="Scopus">Scopu</option>
                                </select>
                                <small class="text-danger">(*)Wajib Diisi.</small>
                                <?= form_error('lembaga_pengindeks', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>

                        <div class="row form-group">
                            <label class="col-md-3 text-md-right" for="nama_barang">UPLOAD ARTIKEL PROSIDING (Lampirkan : Cover Prosiding yang memperlihatkan ISBN/ISSN, Daftar Isi yang ada Nama Pengusul, Artikel Proseding, Sertifikat (Dijadikan dalam 1 File PDF maksimal 3 MB):</label>
                            <div class="col-md-6">
                                <input type="file" class="form-control" id="upload_artikel_proseding" name="upload_artikel_proseding" accept="application/pdf" required>
                                <small class="text-danger">(*)Wajib Diisi.</small>
                                <?= form_error('upload_artikel_proseding', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>


                        <div class="row form-group">
                            <label class="col-md-3 text-md-right" for="nama_barang">UPLOAD LoA/Undangan Presenter (Pemakalah) (PDF atau JPEG maks 3 MB):</label>
                            <div class="col-md-6">
                                <input type="file" class="form-control" id="upload_loa" name="upload_loa" accept="application/pdf" required>
                                <small class="text-danger">(*)Wajib Diisi.</small>
                                <?= form_error('upload_loa', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>

                        <div class="row form-group">
                            <label class="col-md-5 text-md-right" for="nama_barang"><b>B. Identitas Yang Menghasilkan Artikel Proseding</b></label>
                        </div>

                        <div class="row form-group">
                            <label class="col-md-3 text-md-right" for="nama_barang">Nama Program Penelitian:</label>
                            <div class="col-md-6">
                                <select class="form-control" id="nama_program" name="nama_program" required>
                                    <option value="<?= $d['nama_program']; ?>"><?= $d['nama_program']; ?></option>
                                    <option value="Penelitian Internal">Penelitian Internal</option>
                                    <option value="Penelitian Dikti">Penelitian Dikti</option>
                                    <option value="Penelitian Mandiri">Penelitian Mandiri</option>
                                    <option value="Penelitian Hibah Lain">Penelitian Hibah Lain</option>
                                </select>
                                <small class="text-danger">(*)Wajib Diisi.</small>
                                <?= form_error('nama_program', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>

                        <div class="row form-group">
                            <label class="col-md-3 text-md-right" for="nama_barang">Tahun Penelitian :</label>
                            <div class="col-md-6">
                                <select class="form-control" id="tahun_penelitian" name="tahun_penelitian" required>
                                    <option value="<?= $d['tahun_penelitian']; ?>"><?= $d['tahun_penelitian']; ?></option>
                                    <option value="2016">2016</option>
                                    <option value="2017">2017</option>
                                    <option value="2018">2018</option>
                                    <option value="2019">2019</option>
                                    <option value="2020">2020</option>
                                    <option value="2021">2021</option>
                                </select>
                                <small class="text-danger">(*)Wajib Diisi.</small>
                                <?= form_error('tahun_penelitian', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                </form>
            </div>

        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->





</div>
<!-- /.content-wrapper -->
<!-- Select2 -->
<script src="<?= base_url() ?>assets/vendor/backend/plugins/select2/js/select2.full.min.js"></script>
<script src="<?= base_url() ?>assets/vendor//js/rupiah.js"></script>
<script src="<?= base_url() ?>assets/vendor//js/duit.js"></script>
<script>
    $(document).ready(function() {
        $('.selectx').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $(document).ready(function() {
        $('.selecty').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $(document).ready(function() {
        $('.selectz').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $('#id_user').change(function() {
        var id_user = $(this).val();
        $.ajax({
            url: "<?= site_url('get-user'); ?>",
            method: "POST",
            data: {
                id_user: id_user
            },
            async: true,
            dataType: 'json',
            success: function(data) {

                var jbt = '';

                var i;
                for (i = 0; i < data.length; i++) {
                    jbt = data[i].jabatan;
                }
                document.getElementById("jabatan").value = jbt;
            }
        });
        return false;
    });


    $(document).ready(function() {

        $('#id_kampus').change(function() {
            var id_kampus = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-tempat'); ?>",
                method: "POST",
                data: {
                    id_kampus: id_kampus
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_gedung + '>' + data[i].gedung + '</option>';
                    }
                    $('#id_gedung').html(html);

                }
            });
            return false;
        });

        $('.selectw').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });



    $(document).ready(function() {

        $('#id_gedung').change(function() {
            var id_gedung = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-gedung'); ?>",
                method: "POST",
                data: {
                    id_gedung: id_gedung
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_lantai + '>' + data[i].lantai + '</option>';
                    }
                    $('#id_lantai').html(html);

                }
            });
            return false;
        });

        $('.selectw').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });

    $(document).ready(function() {

        $('#id_lantai').change(function() {
            var id_lantai = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-ruangan'); ?>",
                method: "POST",
                data: {
                    id_lantai: id_lantai
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_ruangan + '>' + data[i].ruangan + '</option>';
                    }
                    $('#id_ruangan').html(html);

                }
            });
            return false;
        });

        $('.selecta').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });
</script>