<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h4>UB. Publikasi Jurnal dari Skripsi/Thesis/Disertasi</h4>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dosen') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Permohonan Insentif</a></li>
                        <li class="breadcrumb-item active">UB. Publikasi Jurnal dari Skripsi/Thesis/Disertasi</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>
    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title">UB. Publikasi Jurnal dari Skripsi/Thesis/Disertasi</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?= validation_errors(); ?>
                    </div>
                <?php endif ?>
                <form action="<?= base_url('createUbpjs'); ?>" method="post" enctype="multipart/form-data">
                    <div class="form-group">

                        <small>Isi data-data Berikut </small>
                        <hr>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <label>Judul Artikel : <small class="text-danger">(*)Wajib Diisi.</small></label>
                                <input type="text" class="form-control" id="judul_artikel" name="judul_artikel" value="<?= set_value('judul_artikel'); ?>">
                                <?= form_error('judul_artikel', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label>Nama Jurnal : <small class="text-danger">(*)Wajib Diisi.</small></label>
                                <input type="text" class="form-control" id="nama_jurnal" name="nama_jurnal" required value="<?= set_value('nama_jurnal'); ?>">
                                <?= form_error('nama_jurnal', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>

                            <div class="col-sm-6">
                                <label>Co-Author : <small class="text-danger">(*)Wajib Diisi.</small></label>
                                <input type="text" class="form-control" id="co_author" name="co_author" required value="<?= set_value('co_author'); ?>">
                                <?= form_error('co_author', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>

                        <div class="form-group row">

                            <div class="col-sm-6">
                                <label>Sebagai Penulis Ke: <small class="text-danger">(*)Wajib Diisi.</small></label>
                                <select class="form-control" id="penulis" name="penulis" required>
                                    <option selected disabled value="">Pilih</option>
                                    <option value="Kedua">Kedua</option>
                                    <option value="Ketiga">Ketiga</option>

                                </select>
                                <?= form_error('penulis', '<small class="text-danger pl-3">', '</small>'); ?>

                            </div>

                            <div class="col-sm-6">
                                <label>Kategori Publikasi: <small class="text-danger">(*)Wajib Diisi.</small></label>
                                <select class="form-control" id="kategori_publikasi" name="kategori_publikasi" required>
                                    <option selected disabled value="">Pilih</option>
                                    <option value="Internasional">Internasional</option>
                                    <option value="Nasional">Nasional</option>

                                </select>
                                <?= form_error('kategori_publikasi', '<small class="text-danger pl-3">', '</small>'); ?>

                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label>Web Jurnal : <small class="text-danger">(*)Wajib Diisi.</small></label>
                                <input type="text" class="form-control" id="url_artikel" name="url_artikel" required value="<?= set_value('url_artikel'); ?>">
                                <?= form_error('url_artikel', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                            <div class="col-sm-6">
                                <label>ISSN Artikel: <small class="text-danger">(*)Wajib Diisi.</small></label>
                                <input type="text" class="form-control" id="issn" name="issn" required value="<?= set_value('issn'); ?>">
                                <?= form_error('issn', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label>Volume/Nomor/Tahun: <small class="text-danger">(*)Wajib Diisi.</small></label>
                                <input type="text" class="form-control" id="vnt" name="vnt" required value="<?= set_value('vnt'); ?>">
                                <?= form_error('vnt', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                            <div class="col-sm-6">
                                <label>Lembaga Pengindeks : <small class="text-danger">(*)Wajib Diisi.</small></label>
                                <select class="form-control selecty" id="lembaga_pengindeks" name="lembaga_pengindeks" required>
                                    <option selected disabled value="">Pilih</option>
                                    <?php foreach ($lembaga as $row4) : ?>
                                        <option value="<?= $row4['lembaga']; ?>"><?= $row4['lembaga']; ?></option>
                                    <?php endforeach ?>
                                </select>
                                <?= form_error('lembaga_pengindeks', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label> Jumlah Biaya Publikasi : <small class="text-danger">(*)Wajib Diisi.</small></label>
                                <input type="text" class="form-control" id="duit" name="biaya_publikasi" required value="<?= set_value('biaya_publikasi'); ?>">
                                <?= form_error('biaya_publikasi', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>


                            <div class="col-sm-6">
                                <label>UPLOAD BERKAS (Surat Permohonan,LoA, Artikel Yang Akan di Publikasi (satu File PDF)): <small class="text-danger">(*)Wajib Diisi.</small></label>
                                <input type="file" class="form-control" id="upload_permohonan" name="upload_permohonan" accept="application/pdf" required>
                                <?= form_error('upload_permohonan', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>

                    </div>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                </form>
            </div>

        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->





</div>
<!-- /.content-wrapper -->
<!-- Select2 -->
<script src="<?= base_url() ?>assets/vendor/backend/plugins/select2/js/select2.full.min.js"></script>
<script src="<?= base_url() ?>assets/js/rupiah.js"></script>
<script src="<?= base_url() ?>assets/js/duit.js"></script>
<script>
    $(document).ready(function() {
        $('.selectx').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $(document).ready(function() {
        $('.selecty').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $(document).ready(function() {
        $('.selectz').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $('#id_user').change(function() {
        var id_user = $(this).val();
        $.ajax({
            url: "<?= site_url('get-user'); ?>",
            method: "POST",
            data: {
                id_user: id_user
            },
            async: true,
            dataType: 'json',
            success: function(data) {

                var jbt = '';

                var i;
                for (i = 0; i < data.length; i++) {
                    jbt = data[i].jabatan;
                }
                document.getElementById("jabatan").value = jbt;
            }
        });
        return false;
    });


    $(document).ready(function() {

        $('#id_kampus').change(function() {
            var id_kampus = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-tempat'); ?>",
                method: "POST",
                data: {
                    id_kampus: id_kampus
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_gedung + '>' + data[i].gedung + '</option>';
                    }
                    $('#id_gedung').html(html);

                }
            });
            return false;
        });

        $('.selectw').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });



    $(document).ready(function() {

        $('#id_gedung').change(function() {
            var id_gedung = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-gedung'); ?>",
                method: "POST",
                data: {
                    id_gedung: id_gedung
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_lantai + '>' + data[i].lantai + '</option>';
                    }
                    $('#id_lantai').html(html);

                }
            });
            return false;
        });

        $('.selectw').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });

    $(document).ready(function() {

        $('#id_lantai').change(function() {
            var id_lantai = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-ruangan'); ?>",
                method: "POST",
                data: {
                    id_lantai: id_lantai
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_ruangan + '>' + data[i].ruangan + '</option>';
                    }
                    $('#id_ruangan').html(html);

                }
            });
            return false;
        });

        $('.selecta').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });
</script>