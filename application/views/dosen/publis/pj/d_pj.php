<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Detail Usulan Insentif Pengelola Jurnal</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('home') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Permohonan Insentif</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url('aset_berwujud') ?>">Usulan Insentif Pengelola Jurnal</a></li>
                        <li class="breadcrumb-item active">Detail</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Detail Usulan</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-striped" id="users">
                    <tbody>
                        <tr>
                            <td width="100px">Email</td>
                            <td width="50px">:</td>
                            <td><?= $d['email'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Nama Jurnal</td>
                            <td width="50px">:</td>
                            <td><?= $d['nama_jurnal']; ?> </td>
                        </tr>
                        <tr>
                            <td width="200px">Url Jurnal</td>
                            <td width="50px">:</td>
                            <td><?= $d['url_jurnal'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Peringkat Jurnal</td>
                            <td width="50px">:</td>
                            <td><?= $d['peringkat_jurnal'] ?></td>
                        </tr>

                        <tr>
                            <td width="100px">Kategori</td>
                            <td width="50px">:</td>
                            <td><?= $d['kategori_pj'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">>Volume/Nomor/Tahun yang Diajukan</td>
                            <td width="50px">:</td>
                            <td><?= $d['vnt'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">URL Google Schoolar Jurnal</td>
                            <td width="50px">:</td>
                            <td><?= $d['url_schoolar'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">URL Portal Garuda Jurnal</td>
                            <td width="50px">:</td>
                            <td><?= $d['url_garuda'] ?></td>
                        </tr>


                    </tbody>
                </table>

            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <a href="javascript:history.back()" class="btn btn-danger"> <i class="fa fa-backward"></i> Kembali</a>
            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->