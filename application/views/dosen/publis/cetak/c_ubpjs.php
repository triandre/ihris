<?php

$mpdf = new \Mpdf\Mpdf([
    'mode' => 'utf-8',
    'format' => [210, 330],
    'orientation' => 'P'
]);

$isi =
    '<!DOCTYPE html>
<html>
<head>
    <title>Cetak Tiket Klaim </title>
    <style>@page {
        margin-top: 10px;
       }</style>
</head>

<body>

<h3  align="center"><img src="assets/ttd/kop.png" style="width:200px;"><br><br>
<u>PERMOHONAN BANTUAN PUBLIKASI JURNAL DARI SKRIPSI/THESIS/DISERTASI</u></h3><br>
<p><b>UNIVERSITAS MUHAMMADIYAH SUMATERA UTARA</b><br>
Jl. Kapt. Mukhtar Basri B.A. No. 3 <br>
Medan 20238 Telp(061) 6622400 </p>
<table>
<tbody>
<tr>
<td>No.Reff</td>
<td>:</td>
<td>' . $d["no_reff"] . '</td>
</tr>
<tr>
<td>Nama</td>
<td>:</td>
<td>' . $akun["name"] . '</td>
</tr>
<tr>
<td>NIDN</td>
<td>:</td>
<td>' . $akun["nidn"] . '</td>
</tr>

<tr>
<td>Jenis Insentif</td>
<td>:</td>
<td>Bantuan Publikasi Jurnal dari Skripsi/Thesis/Disertasi</td>
</tr>
<tr>
<td>Email</td>
<td>:</td>
<td>' . $akun["email"] . '</td>
</tr>
</tbody>
</table> 
<br>
<h3> Perincian Biaya </h3>

<table style="border-collapse: collapse; border:0px solid black;">
<tbody>
<tr>
<td style="width:450px;"> 
' .  $d["judul_artikel"] . ' 
<br>
<small>Nama Jurnal' . $d["nama_jurnal"] . ' </small> 
<br>
<small>Penulis ' . $d["penulis"] . ' </small> 
<br>
<small>ISSN : ' . $d["issn"] . '</small>
</td>
<td style="width:200px;"><center><b>Rp.' . number_format($d["biaya_disetujui"]) . '</b></center></td>
</tr>
</tbody>
</table> 

<h4>Catatan Validator</h4>
<p>
 ' . $d["ket"] . '
</p> 

<h4>Catatan Wakil Rektor I</h4>
<p>
 ' . $d["catatan1"] . '
</p> 


<h4>Catatan Wakil Rektor II</h4>
<p>
 ' . $d["catatan2"] . '
</p> 
<br>
<br>

<table>
<tbody>
<tr>
<td style="width:300px;"><center>Disetujui <br> Wakil Rektor II<br>
<img src="assets/ttd/wr2.png" class="center" style="width:150px;"></center> 
<td style="width:250px; height:100px"><center>Medan, ' . date('d F Y') . '  <br> Pemohon<br>
<img src="assets/ttd/pemohon.png" class="center" style="width:150px;"></center> 
</tr>

<tr>
<td style="width:100px;"><center><b>Prof. Dr. Akrim, M.Pd.</b></center></td>
<td style="width:300px;"><center><b>' . $akun["name"] . '</b></center></td>
</tr>
</tbody>
</table> 



</body>
</html>';
$mpdf->WriteHTML($isi);
$mpdf->Output();
