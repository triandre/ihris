<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $title; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dosen') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Tacking Ajuan</a></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <?php if ($this->session->flashdata('gagal_store')) { ?>
                    <div class="alert alert-danger col-md-12">
                        <?= $this->session->flashdata('gagal_store') ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php } ?>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="example1" class="table table-bordered table-striped table-sm">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Jenis Ajuan</th>
                                <th>Ket</th>
                                <th>Tanggal Ajuan</th>
                                <th>Umur Ajuan (Hari)</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            foreach ($pdd as $row) : ?>
                                <tr>
                                    <td style="width:4%"><?= $no++; ?></td>
                                    <td style="width:30%;color:blue ;"><?= $row['jenis_ajuan']; ?></td>
                                    <td style="width:40%;color:red ;"><?= $row['komentar']; ?></td>
                                    <td style="width:10%"><?= date("d M Y", strtotime($row['tgl_ajuan'])); ?></td>
                                    <td>
                                        <?php
                                        $tgl1 = date("Y-m-d", strtotime($row['tgl_ajuan']));
                                        $tanggal1 = strtotime($tgl1);
                                        $tgl2 = date('Y-m-d');
                                        $tanggal2 = strtotime($tgl2);
                                        $perbedaan = $tanggal2 - $tanggal1;
                                        $hari = $perbedaan / 60 / 60 / 24;
                                        echo "$hari Hari";
                                        ?>
                                    </td>
                                    <td style="width:5%">
                                        <?php
                                        if ($row['sts'] == 1) {
                                            echo '<span class="badge badge-info">Diajukan</span> ';
                                        } elseif ($row['sts'] == 2) {
                                            echo '<span class="badge badge-success">Disetujui</span>';
                                        } elseif ($row['sts'] == 3) {
                                            echo '<span class="badge badge-danger">Ditolak</span>';
                                        }
                                        ?>
                                    </td>
                                    <td style="width:5%">
                                        <a href="<?= base_url('perbaikiData/' . $row['reff']) ?>" class="btn btn-warning btn-sm" title="Perbaiki Data">
                                            <i class="fas fa-edit"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">

            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->

</div>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
    $(function() {
        $("#example1").DataTable({
            "language": {
                "sSearch": "Cari"
            }
        });
    });
</script>