<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $title; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dosen') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Penunjang</a></li>
                        <li class="breadcrumb-item active">Anggota Profesi</li>
                        <li class="breadcrumb-item active">Update Anggota Profesi</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>
    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title"><?= $title; ?></h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-danger col-md-8 alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?= validation_errors(); ?>
                    </div>
                <?php endif ?>


                <form class="form-horizontal" action="<?= base_url('aprof/editAprofGo'); ?>" enctype="multipart/form-data" autocomplete="off" method="post">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <input hidden class="form-control" type="text" id="id_af" name="id_af" value="<?= $d['id_af']; ?>" readonly required>
                                <tbody>
                                    <tr>
                                        <td>Kategori Kegiatan <span style="color:red;">*</span> </td>
                                        <td>
                                            <select id="kategori_kgt " name="kategori_kgt" class="form-control selectx" required>
                                                <option value="<?= $d['kategori_kgt']; ?>"><?= $d['kategori_kgt']; ?></option>
                                                <option value="Menjadi anggota organisasi profesi Tingkat internasional sebagai pengurus">Menjadi anggota organisasi profesi Tingkat internasional sebagai pengurus</option>
                                                <option value="Menjadi anggota organisasi profesi Tingkat internasional sebagai anggota atas permintaan">Menjadi anggota organisasi profesi Tingkat internasional sebagai anggota atas permintaan</option>
                                                <option value="Menjadi anggota organisasi profesi Tingkat internasional sebagai anggota">Menjadi anggota organisasi profesi Tingkat internasional sebagai anggota</option>
                                                <option value="Menjadi anggota organisasi profesi Tingkat nasional sebagai pengurus">Menjadi anggota organisasi profesi Tingkat nasional sebagai pengurus</option>
                                                <option value="Menjadi anggota organisasi profesi Tingkat nasional sebagai anggota atas permintaan">Menjadi anggota organisasi profesi Tingkat nasional sebagai anggota atas permintaan</option>
                                                <option value="Menjadi anggota organisasi profesi Tingkat nasional sebagai anggota">Menjadi anggota organisasi profesi Tingkat nasional sebagai anggota</option>
                                                <option value=" Keanggotaan dalam organisasi profesi dosen Tingkat nasional sebagai pengurus aktif"> Keanggotaan dalam organisasi profesi dosen Tingkat nasional sebagai pengurus aktif</option>
                                                <option value=" Keanggotaan dalam organisasi profesi dosen Tingkat nasional sebagai anggota aktif"> Keanggotaan dalam organisasi profesi dosen Tingkat nasional sebagai anggota aktif</option>
                                                <option value=" Keanggotaan dalam organisasi profesi dosen Tingkat nasional provinsi/kabupaten/kota sebagai pengurus aktif"> Keanggotaan dalam organisasi profesi dosen Tingkat nasional provinsi/kabupaten/kota sebagai pengurus aktif</option>
                                                <option value=" Keanggotaan dalam organisasi profesi dosen Tingkat nasional provinsi/kabupaten/kota sebagai anggota aktif"> Keanggotaan dalam organisasi profesi dosen Tingkat nasional provinsi/kabupaten/kota sebagai anggota aktif</option>


                                            </select>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Nama Organisasi <span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="text" id="nama_org" name="nama_org" value="<?= $d['nama_org']; ?>" required> </td>
                                    </tr>

                                    <tr>
                                        <td>Peran <span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="text" id="peran" name="peran" value="<?= $d['peran']; ?>" required> </td>
                                    </tr>

                                    <tr>
                                        <td>Mulai Keanggotaan <span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="date" id="mulai_anggota" name="mulai_anggota" value="<?= $d['mulai_anggota']; ?>" required> </td>
                                    </tr>
                                    <tr>
                                        <td>Selesai Keanggotaan <span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="date" id="selesai_anggota" name="selesai_anggota" value="<?= $d['selesai_anggota']; ?>" required> </td>
                                    </tr>

                                    <tr>
                                        <td>Instansi <span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="text" id="instansi" name="instansi" value="<?= $d['instansi']; ?>" required> </td>
                                    </tr>

                                    <tr>
                                        <td>Upload Dokumen<br />
                                            <small><i>(Maksimal total ukuran file dalam sekali proses upload : <br> <b><u>5 MB, Format Pdf (Ijazah, SK dan Transkip Jadikan 1 File)</u></b>)</i></small>
                                        </td>
                                        <td><input type="file" class="form-control" name="file" id="file" accept="application/pdf" required></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <a href="<?= base_url('aprof') ?>">
                            <a href="javascript:history.back()" class="btn btn-danger"> <i class="fa fa-backward"></i> Kembali</a>
                        </a>
                        <button type="submit" class="btn btn-primary"> <i class="fa fa-save"></i> Simpan</button>
                    </div>
                    <!-- /.card-footer -->
                </form>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">

            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->





</div>
<!-- /.content-wrapper -->
<!-- Select2 -->
<script src="<?= base_url() ?>assets/vendor/backend/plugins/select2/js/select2.full.min.js"></script>
<script src="<?= base_url() ?>assets/vendor//js/rupiah.js"></script>
<script src="<?= base_url() ?>assets/vendor//js/duit.js"></script>
<script>
    $(document).ready(function() {
        $('.selectx').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $(document).ready(function() {
        $('.selecty').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $(document).ready(function() {
        $('.selectz').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $('#id_user').change(function() {
        var id_user = $(this).val();
        $.ajax({
            url: "<?= site_url('get-user'); ?>",
            method: "POST",
            data: {
                id_user: id_user
            },
            async: true,
            dataType: 'json',
            success: function(data) {

                var jbt = '';

                var i;
                for (i = 0; i < data.length; i++) {
                    jbt = data[i].jabatan;
                }
                document.getElementById("jabatan").value = jbt;
            }
        });
        return false;
    });


    $(document).ready(function() {

        $('#id_kampus').change(function() {
            var id_kampus = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-tempat'); ?>",
                method: "POST",
                data: {
                    id_kampus: id_kampus
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_gedung + '>' + data[i].gedung + '</option>';
                    }
                    $('#id_gedung').html(html);

                }
            });
            return false;
        });

        $('.selectw').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });



    $(document).ready(function() {

        $('#id_gedung').change(function() {
            var id_gedung = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-gedung'); ?>",
                method: "POST",
                data: {
                    id_gedung: id_gedung
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_lantai + '>' + data[i].lantai + '</option>';
                    }
                    $('#id_lantai').html(html);

                }
            });
            return false;
        });

        $('.selectw').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });

    $(document).ready(function() {

        $('#id_lantai').change(function() {
            var id_lantai = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-ruangan'); ?>",
                method: "POST",
                data: {
                    id_lantai: id_lantai
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_ruangan + '>' + data[i].ruangan + '</option>';
                    }
                    $('#id_ruangan').html(html);

                }
            });
            return false;
        });

        $('.selecta').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });
</script>