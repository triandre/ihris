<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Detail Anggota Profesi</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('home') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Penunjang</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url('aset_berwujud') ?>">Anggota Profesi</a></li>
                        <li class="breadcrumb-item active">Detail</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Detail Anggota Profesi</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">

                <table class="table table-striped" id="users">
                    <tbody>
                        <tr>
                            <td width="100px">NIDN</td>
                            <td width="50px">:</td>
                            <td><?= $d['nidn'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Kategori Kegiatan</td>
                            <td width="50px">:</td>
                            <td><?= $d['kategori_kgt']; ?> </td>
                        </tr>
                        <tr>
                            <td width="200px">Nama Organisasi</td>
                            <td width="50px">:</td>
                            <td><?= $d['nama_org'] ?></td>
                        </tr>
                        <tr>
                            <td width="200px">Peran</td>
                            <td width="50px">:</td>
                            <td><?= $d['peran'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Mulai Keanggotaan</td>
                            <td width="50px">:</td>
                            <td><?= date('d F Y', strtotime($d['mulai_anggota'])) ?> </td>
                        </tr>
                        <tr>
                            <td width="100px">Selesai Keanggotaan</td>
                            <td width="50px">:</td>
                            <td><?= date('d F Y', strtotime($d['selesai_anggota'])) ?> </td>
                        </tr>
                        <tr>
                            <td width="100px">Instansi</td>
                            <td width="50px">:</td>
                            <td><?= $d['instansi'] ?></td>
                        </tr>

                        <tr>
                            <td width="100px">Dokumen</td>
                            <td width="50px">:</td>
                            <td> <a href="<?= base_url('archive/aprof/'); ?><?= $d['file'] ?>" target="_blank"> Lihat Data</a> </td>
                        </tr>

                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <a href="<?= base_url('aprof') ?>">
                    <button type="button" class="btn btn-danger"><i class="fa fa-backward"></i> Kembali</button>
                </a>
            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->