<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Detail Penghargaan</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('home') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Penunjang</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url('aset_berwujud') ?>">Penghargaan</a></li>
                        <li class="breadcrumb-item active">Detail</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Detail Penghargaan</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">

                <table class="table table-striped" id="users">
                    <tbody>
                        <tr>
                            <td width="100px">NIDN</td>
                            <td width="50px">:</td>
                            <td><?= $d['nidn'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Kategori Kegiatan</td>
                            <td width="50px">:</td>
                            <td><?= $d['kategori_penghargaan']; ?> </td>
                        </tr>
                        <tr>
                            <td width="200px">Tingkat Penghargaan</td>
                            <td width="50px">:</td>
                            <td><?= $d['tingkat_penghargaan']; ?></td>
                        </tr>
                        <tr>
                            <td width="200px">Jenis Penghargaan</td>
                            <td width="50px">:</td>
                            <td><?= $d['jenis_penghargaan']; ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Nama Penghargaan</td>
                            <td width="50px">:</td>
                            <td><?= $d['nama_penghargaan']; ?> </td>
                        </tr>
                        <tr>
                            <td width="100px">Tahun</td>
                            <td width="50px">:</td>
                            <td><?= $d['tahun']; ?> </td>
                        </tr>
                        <tr>
                            <td width="100px">Instansi</td>
                            <td width="50px">:</td>
                            <td><?= $d['instansi'] ?></td>
                        </tr>

                        <tr>
                            <td width="100px">Dokumen</td>
                            <td width="50px">:</td>
                            <td> <a href="<?= base_url('archive/penghargaan/'); ?><?= $d['file'] ?>" target="_blank"> Lihat Data</a> </td>
                        </tr>

                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <a href="<?= base_url('penghargaan') ?>">
                    <button type="button" class="btn btn-danger"><i class="fa fa-backward"></i> Kembali</button>
                </a>
            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->