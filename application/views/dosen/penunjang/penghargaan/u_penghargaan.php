<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $title; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dosen') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Penunjang</a></li>
                        <li class="breadcrumb-item active">Anggota Profesi</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>
    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title"><?= $title; ?></h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-danger col-md-8 alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?= validation_errors(); ?>
                    </div>
                <?php endif ?>


                <form class="form-horizontal" action="<?= base_url('penghargaan/editPenghargaanGo'); ?>" enctype="multipart/form-data" autocomplete="off" method="post">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tbody>
                                    <input hidden class="form-control" type="text" id="id_penghargaan" name="id_penghargaan" required value="<?= $d['id_penghargaan']; ?>" readonly>
                                    <tr>
                                        <td>Kategori kegiatan <span style="color:red;">*</span> </td>
                                        <td>
                                            <select id="kategori_penghargaan" name="kategori_penghargaan" class="form-control selectx" required>
                                                <option value="<?= $d['kategori_penghargaan']; ?>"><?= $d['kategori_penghargaan']; ?></option>
                                                <option value="Mendapat penghargaan/tanda jasa - Penghargaan/tanda jasa Satyalancana Karya Satya 30 tahun">Mendapat penghargaan/tanda jasa - Penghargaan/tanda jasa Satyalancana Karya Satya 30 tahun</option>
                                                <option value="Mendapat penghargaan/tanda jasa - Penghargaan/tanda jasa Satyalancana Karya Satya 20 tahun">Mendapat penghargaan/tanda jasa - Penghargaan/tanda jasa Satyalancana Karya Satya 20 tahun</option>
                                                <option value="Mendapat penghargaan/tanda jasa - Penghargaan/tanda jasa Satyalancana Karya Satya 10 tahun">Mendapat penghargaan/tanda jasa - Penghargaan/tanda jasa Satyalancana Karya Satya 10 tahun</option>
                                                <option value="Mendapat penghargaan/tanda jasa - Penghargaan lainnya tingkat internasional">Mendapat penghargaan/tanda jasa - Penghargaan lainnya tingkat internasional</option>
                                                <option value="Mendapat penghargaan/tanda jasa - Penghargaan lainnya tingkat nasional">Mendapat penghargaan/tanda jasa - Penghargaan lainnya tingkat nasional</option>
                                                <option value="Mendapat penghargaan/tanda jasa - Penghargaan lainnya tingkat provinsi/lokal">Mendapat penghargaan/tanda jasa - Penghargaan lainnya tingkat provinsi/lokal</option>
                                                <option value="Mempunyai prestasi di bidang olahraga/humaniora - Tingkat internasional">Mempunyai prestasi di bidang olahraga/humaniora - Tingkat internasional</option>
                                                <option value="Mempunyai prestasi di bidang olahraga/humaniora - Tingkat nasional">Mempunyai prestasi di bidang olahraga/humaniora - Tingkat nasional</option>
                                                <option value="Mempunyai prestasi di bidang olahraga/humaniora - Tingkat daerah/lokal">Mempunyai prestasi di bidang olahraga/humaniora - Tingkat daerah/lokal</option>
                                            </select>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Tingkat Penghargaan <span style="color:red;">*</span> </td>
                                        <td>
                                            <select id="tingkat_penghargaan" name="tingkat_penghargaan" class="form-control selectx" required>
                                                <option value="<?= $d['tingkat_penghargaan']; ?>"><?= $d['tingkat_penghargaan']; ?></option>
                                                <option value="Sekolah">Sekolah</option>
                                                <option value="Kecamatan">Kecamatan</option>
                                                <option value="Kab/Kota">Kab/Kota</option>
                                                <option value="Provinsi">Provinsi</option>
                                                <option value="Naional">Naional</option>
                                                <option value="Internasional">Internasional</option>
                                                <option value="Lainnya">Lainnya</option>

                                            </select>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Jenis Penghargaan <span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="text" id="jenis_penghargaan" name="jenis_penghargaan" required value="<?= $d['jenis_penghargaan']; ?>"> </td>
                                    </tr>

                                    <tr>
                                        <td>Nama Penghargaan <span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="text" id="nama_penghargaan" name="nama_penghargaan" required value="<?= $d['nama_penghargaan']; ?>"> </td>
                                    </tr>

                                    <tr>
                                        <td>Tahun <span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="text" id="tahun" name="tahun" required value="<?= $d['tahun']; ?>"> </td>
                                    </tr>

                                    <tr>
                                        <td>Instansi Pemberi <span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="text" id="instansi" name="instansi" required value="<?= $d['instansi']; ?>"> </td>
                                    </tr>

                                    <tr>
                                        <td>Upload Dokumen<br />
                                            <small><i>(Maksimal total ukuran file dalam sekali proses upload : <br> <b><u>5 MB, Format Pdf (SK Pendukung Jadikan 1 File)</u></b>)</i></small>
                                        </td>
                                        <td><input type="file" class="form-control" name="file" id="file" accept="application/pdf" required></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <a href="<?= base_url('aprof') ?>">
                            <a href="javascript:history.back()" class="btn btn-danger"> <i class="fa fa-backward"></i> Kembali</a>
                        </a>
                        <button type="submit" class="btn btn-primary"> <i class="fa fa-save"></i> Simpan</button>
                    </div>
                    <!-- /.card-footer -->
                </form>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">

            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->





</div>
<!-- /.content-wrapper -->
<!-- Select2 -->
<script src="<?= base_url() ?>assets/vendor/backend/plugins/select2/js/select2.full.min.js"></script>
<script src="<?= base_url() ?>assets/vendor//js/rupiah.js"></script>
<script src="<?= base_url() ?>assets/vendor//js/duit.js"></script>
<script>
    $(document).ready(function() {
        $('.selectx').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $(document).ready(function() {
        $('.selecty').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $(document).ready(function() {
        $('.selectz').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $('#id_user').change(function() {
        var id_user = $(this).val();
        $.ajax({
            url: "<?= site_url('get-user'); ?>",
            method: "POST",
            data: {
                id_user: id_user
            },
            async: true,
            dataType: 'json',
            success: function(data) {

                var jbt = '';

                var i;
                for (i = 0; i < data.length; i++) {
                    jbt = data[i].jabatan;
                }
                document.getElementById("jabatan").value = jbt;
            }
        });
        return false;
    });


    $(document).ready(function() {

        $('#id_kampus').change(function() {
            var id_kampus = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-tempat'); ?>",
                method: "POST",
                data: {
                    id_kampus: id_kampus
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_gedung + '>' + data[i].gedung + '</option>';
                    }
                    $('#id_gedung').html(html);

                }
            });
            return false;
        });

        $('.selectw').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });



    $(document).ready(function() {

        $('#id_gedung').change(function() {
            var id_gedung = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-gedung'); ?>",
                method: "POST",
                data: {
                    id_gedung: id_gedung
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_lantai + '>' + data[i].lantai + '</option>';
                    }
                    $('#id_lantai').html(html);

                }
            });
            return false;
        });

        $('.selectw').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });

    $(document).ready(function() {

        $('#id_lantai').change(function() {
            var id_lantai = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-ruangan'); ?>",
                method: "POST",
                data: {
                    id_lantai: id_lantai
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_ruangan + '>' + data[i].ruangan + '</option>';
                    }
                    $('#id_ruangan').html(html);

                }
            });
            return false;
        });

        $('.selecta').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });
</script>