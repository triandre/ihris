<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dosen') ?>">Profil</a></li>
                        <li class="breadcrumb-item"><a href="#">Data Pribadi</a></li>
                        <li class="breadcrumb-item active">Keluarga</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>
    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title">Form Ajuan Data Keluarga</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-danger col-md-8 alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?= validation_errors(); ?>
                    </div>
                <?php endif ?>

                <form class="form-horizontal" action="<?= base_url('keluarga/update'); ?>" enctype="multipart/form-data" autocomplete="off" method="post">
                    <div class="card-body">
                        <table class="table table-striped">
                            <tbody>
                                <tr>
                                    <th></th>
                                    <th>Data Keluarga Lama</th>
                                    <th>Data Keluarga Baru</th>
                                </tr>
                                <tr>
                                    <td>Status Perkawinan</td>
                                    <td><?= $d['status_perkawinan']; ?></td>
                                    <td>
                                        <select id="status_perkawinan" name="status_perkawinan" class="form-control selectx" required>
                                            <option selected disabled value="">Pilih</option>
                                            <option value="Lajang">Lajang</option>
                                            <option value="Kawin">Kawin</option>
                                            <option value="Cerai">Cerai</option>

                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Nama Suami/Istri</td>
                                    <td><?= $d['nama_pasangan']; ?></td>
                                    <td>
                                        <input class="form-control" type="text" id="nama_pasangan" name="nama_pasangan" required>
                                    </td>
                                </tr>
                                <tr>
                                    <td>NIK Suami/Istri</td>
                                    <td><?= $d['nik']; ?></td>
                                    <td>
                                        <input class="form-control" type="text" id="nik" name="nik" required>
                                    </td>
                                </tr>

                                <tr>
                                    <td>NIP Suami/Istri</td>
                                    <td><?= $d['nip']; ?></td>
                                    <td>
                                        <input class="form-control" type="text" id="nip" name="nip">
                                    </td>
                                </tr>

                                <tr>
                                    <td>Pekerjaan Suami/Istri</td>
                                    <td><?= $d['pekerjaan_pasangan']; ?></td>
                                    <td>
                                        <input class="form-control" type="text" id="pekerjaan_pasangan" name="pekerjaan_pasangan" required>
                                    </td>
                                </tr>

                                <tr>
                                    <td>Terhitung Tanggal PNS Suami/Istri</td>
                                    <td><?= $d['tgl_pns']; ?></td>
                                    <td>
                                        <input class="form-control" type="date" id="tgl_pns" name="tgl_pns">
                                    </td>
                                </tr>


                                <tr>
                                    <td>Upload Dokumen Pendukung</td>
                                    <td> <a href="<?= base_url('archive/keluarga/'); ?><?= $d['file']; ?>" target="_blank" rel="noopener noreferrer">File Lama </a></td>
                                    <td><input type="file" class="form-control" name="file" id="file" accept="application/pdf" required></td>
                                </tr>
                            </tbody>
                        </table>


                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <a href="<?= base_url('ukeluarga') ?>">
                            <a href="javascript:history.back()" class="btn btn-danger">Kembali</a>
                        </a>
                        <button type="submit" class="btn btn-info">Simpan</button>
                    </div>
                    <!-- /.card-footer -->
                </form>


            </div>
            <!-- /.card-body -->
            <div class="card-footer">

            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->


    <section class="content">

        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title">
                    Riwayat Ajuan Perubahan Data Profil
                </h3>
            </div>
            <div class="card-body">
                <ul class="nav nav-tabs" id="custom-content-below-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="custom-content-below-home-tab" data-toggle="pill" href="#custom-content-below-home" role="tab" aria-controls="custom-content-below-home" aria-selected="true"><span class="badge badge-default"><?= $nDraft; ?> </span> Draft</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="custom-content-below-profile-tab" data-toggle="pill" href="#custom-content-below-profile" role="tab" aria-controls="custom-content-below-profile" aria-selected="false"><span class="badge badge-primary"><?= $nDiajukan; ?> </span> Diajukan</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="custom-content-below-settings-tab" data-toggle="pill" href="#custom-content-below-setujui" role="tab" aria-controls="custom-content-below-settings" aria-selected="false"><span class="badge badge-success"><?= $nDisetujui; ?> </span> Disetujui</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="custom-content-below-messages-tab" data-toggle="pill" href="#custom-content-below-messages" role="tab" aria-controls="custom-content-below-messages" aria-selected="false"><span class="badge badge-danger"><?= $nDitolak; ?> </span> Ditolak</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="custom-content-below-settings-tab" data-toggle="pill" href="#custom-content-below-settings" role="tab" aria-controls="custom-content-below-settings" aria-selected="false"><span class="badge badge-warning"><?= $nDitangguhkan; ?> </span> Ditangguhkan</a>
                    </li>

                </ul>
                <div class="tab-content" id="custom-content-below-tabContent">
                    <!-- DRAFT -->
                    <div class="tab-pane fade show active" id="custom-content-below-home" role="tabpanel" aria-labelledby="custom-content-below-home-tab">
                        <br><br>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr class="active">
                                        <td class="tcenter" style="vertical-align: middle;"><b>Tanggal Dibuat</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Keterangan Perubahan</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Status</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Aksi</b></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <?php $no = 1;
                                        foreach ($draft as $row) : ?>
                                            <td><?= $row['tgl_ajuan']; ?></td>
                                            <td><?= $row['jenis_ajuan']; ?></td>
                                            <td><span class="badge badge-default">Belum Dikirim</span></td>
                                            <td><a href="<?= base_url('keluarga/detail/'); ?><?= $row['reff']; ?>" title="Detail"><i class="fa fa-information"></i> </a>
                                            </td>
                                    </tr>
                                <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <!-- DIAJUKAN -->
                    <div class="tab-pane fade" id="custom-content-below-profile" role="tabpanel" aria-labelledby="custom-content-below-profile-tab">
                        <br><br>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr class="active">
                                        <td class="tcenter" style="vertical-align: middle;"><b>Tanggal Diajukan</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Keterangan Perubahan</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Status</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Aksi</b></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <?php $no = 1;
                                        foreach ($diajukan as $row2) : ?>
                                            <td><?= date("d M Y", strtotime($row2['tgl_ajuan'])); ?></td>
                                            <td><?= $row2['jenis_ajuan']; ?></td>
                                            <td><span class="badge badge-primary">Diajukan</span></td>
                                            <td><a href="<?= base_url('keluarga/detail/' . $row2['reff']) ?>" title="Detail" class="btn btn-primary btn-sm"><i class="fa fa-info"></i> </a>
                                            </td>
                                    </tr>
                                <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div>


                    <div class="tab-pane fade" id="custom-content-below-setujui" role="tabpanel" aria-labelledby="custom-content-below-profile-tab">
                        <br><br>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr class="active">
                                        <td class="tcenter" style="vertical-align: middle;"><b>Tanggal Diajukan</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Tanggal Disetujui</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Keterangan Perubahan</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Status</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Aksi</b></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <?php $no = 1;
                                        foreach ($disetujui as $row3) : ?>
                                            <td><?= date("d M Y", strtotime($row3['tgl_ajuan'])); ?></td>
                                            <td><?= date("d M Y", strtotime($row3['tgl_verifikasi'])); ?></td>
                                            <td><?= $row3['jenis_ajuan']; ?></td>
                                            <td><span class="badge badge-success">Disetujui</span></td>
                                            <td><a href="<?= base_url('keluarga/detail/' . $row3['reff']) ?>" title="Detail" class="btn btn-primary"><i class="fa fa-info"></i> </a>
                                            </td>
                                    </tr>
                                <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div>


                    <div class="tab-pane fade" id="custom-content-below-messages" role="tabpanel" aria-labelledby="custom-content-below-messages-tab">
                        <br><br>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr class="active">
                                        <td class="tcenter" style="vertical-align: middle;"><b>Tanggal Diajukan</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Tanggal Ditolak</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Keterangan Penolakan</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Status</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Aksi</b></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <?php $no = 1;
                                        foreach ($ditolak as $row4) : ?>
                                            <td><?= date("d M Y", strtotime($row4['tgl_ajuan'])); ?></td>
                                            <td><?= date("d M Y", strtotime($row4['tgl_verifikasi'])); ?></td>
                                            <td><?= $row4['komentar']; ?></td>
                                            <td><span class="badge badge-warning">Ditolak</span></td>
                                            <td><a href="<?= base_url('keluarga/detail/'); ?><?= $row4['reff']; ?>" title="Detail" class="btn btn-primary"><i class="fa fa-info"></i> </a>
                                            </td>
                                    </tr>
                                <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div>


                    <div class="tab-pane fade" id="custom-content-below-settings" role="tabpanel" aria-labelledby="custom-content-below-settings-tab">
                        <br><br>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr class="active">
                                        <td class="tcenter" style="vertical-align: middle;"><b>Tanggal Diajukan</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Tanggal Ditangguhkan</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Keterangan Penangguhan</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Status</b></td>
                                        <td class="tcenter" style="vertical-align: middle;"><b>Aksi</b></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <?php $no = 1;
                                        foreach ($ditangguhkan as $row5) : ?>
                                            <td><?= date("d M Y", strtotime($row5['tgl_ajuan'])); ?></td>
                                            <td><?= date("d M Y", strtotime($row5['tgl_verifikasi'])); ?></td>
                                            <td><?= $row5['komentar']; ?></td>
                                            <td><span class="badge badge-danger">Ditangguhkan</span></td>
                                            <td><a href="<?= base_url('keluarga/detail/'); ?><?= $row5['reff']; ?>" title="Detail" class="btn btn-primary"><i class="fa fa-info"></i> </a>
                                            </td>
                                    </tr>
                                <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div>


                </div>
            </div>




        </div>
        <!-- /.card -->
</div>
</section>
<!-- /.content -->





</div>
<!-- /.content-wrapper -->
<!-- Select2 -->
<script src="<?= base_url() ?>assets/vendor/backend/plugins/select2/js/select2.full.min.js"></script>
<script src="<?= base_url() ?>assets/vendor//js/rupiah.js"></script>
<script src="<?= base_url() ?>assets/vendor//js/duit.js"></script>
<script>
    $(document).ready(function() {
        $('.selectx').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $(document).ready(function() {
        $('.selecty').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $(document).ready(function() {
        $('.selectz').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $('#id_user').change(function() {
        var id_user = $(this).val();
        $.ajax({
            url: "<?= site_url('get-user'); ?>",
            method: "POST",
            data: {
                id_user: id_user
            },
            async: true,
            dataType: 'json',
            success: function(data) {

                var jbt = '';

                var i;
                for (i = 0; i < data.length; i++) {
                    jbt = data[i].jabatan;
                }
                document.getElementById("jabatan").value = jbt;
            }
        });
        return false;
    });


    $(document).ready(function() {

        $('#id_kampus').change(function() {
            var id_kampus = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-tempat'); ?>",
                method: "POST",
                data: {
                    id_kampus: id_kampus
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_gedung + '>' + data[i].gedung + '</option>';
                    }
                    $('#id_gedung').html(html);

                }
            });
            return false;
        });

        $('.selectw').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });



    $(document).ready(function() {

        $('#id_gedung').change(function() {
            var id_gedung = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-gedung'); ?>",
                method: "POST",
                data: {
                    id_gedung: id_gedung
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_lantai + '>' + data[i].lantai + '</option>';
                    }
                    $('#id_lantai').html(html);

                }
            });
            return false;
        });

        $('.selectw').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });

    $(document).ready(function() {

        $('#id_lantai').change(function() {
            var id_lantai = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-ruangan'); ?>",
                method: "POST",
                data: {
                    id_lantai: id_lantai
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_ruangan + '>' + data[i].ruangan + '</option>';
                    }
                    $('#id_ruangan').html(html);

                }
            });
            return false;
        });

        $('.selecta').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });
</script>