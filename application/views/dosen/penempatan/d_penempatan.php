<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $title; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dosen') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Penempatan</a></li>
                        <li class="breadcrumb-item active">Detail Penempatan</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Detail</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-striped" id="users">
                    <tbody>
                        <tr>
                            <td width="100px">NIDN</td>
                            <td width="50px">:</td>
                            <td><?= $d['nidn'] ?></td>
                        </tr>
                        <tr>
                            <td width="200px">Nama</td>
                            <td width="50px">:</td>
                            <td><?= $d['name'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Unit Kerja</td>
                            <td width="50px">:</td>
                            <td><?= $d['nickname_unit']; ?> </td>
                        </tr>

                        <tr>
                            <td width="100px">Nomor SK Penugasan</td>
                            <td width="50px">:</td>
                            <td><?= $d['no_sk'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Tanggal Mulai</td>
                            <td width="50px">:</td>
                            <td><?= $d['tgl_mulai'] ?></td>
                        </tr>


                        <tr>
                            <td width="100px">Dokumen</td>
                            <td width="50px">:</td>
                            <td> <a href="<?= base_url('archive/penempatan/'); ?><?= $d['file'] ?>" target="_blank"> Lihat Data</a> </td>
                        </tr>
                    </tbody>
                </table>

            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <a href="javascript:history.back()" class="btn btn-danger">Kembali</a>
            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->