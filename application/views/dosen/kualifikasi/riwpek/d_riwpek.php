<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Detail Data Riwayat Pekerjaan</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('home') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Profil</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url('aset_berwujud') ?>">Riwayat Pekerjaan</a></li>
                        <li class="breadcrumb-item active">Detail Data Riwayat Pekerjaan</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">

                <table class="table table-striped" id="users">
                    <tbody>
                        <tr>
                            <td width="100px">NIDN</td>
                            <td width="50px">:</td>
                            <td><?= $d['nidn'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Bidang Usaha</td>
                            <td width="50px">:</td>
                            <td><?= $d['bidang_usaha']; ?> </td>
                        </tr>
                        <tr>
                            <td width="200px">Jenis Pekerjaan</td>
                            <td width="50px">:</td>
                            <td><?= $d['jenis_pekerjaan'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Jabatan</td>
                            <td width="50px">:</td>
                            <td><?= $d['jabatan'] ?></td>
                        </tr>

                        <tr>
                            <td width="100px">Instansi </td>
                            <td width="50px">:</td>
                            <td><?= $d['instansi']; ?></td>
                        </tr>

                        <tr>
                            <td width="100px">Devisi</td>
                            <td width="50px">:</td>
                            <td><?= $d['devisi'] ?></td>
                        </tr>

                        <tr>
                            <td width="100px">Deskripsi </td>
                            <td width="50px">:</td>
                            <td><?= $d['deskripsi'] ?></td>
                        </tr>


                        <tr>
                            <td width="100px">Area Pekerjaan </td>
                            <td width="50px">:</td>
                            <td><?= $d['area_pekerjaan'] ?></td>
                        </tr>

                        <tr>
                            <td width="100px">Mulai Bekerja </td>
                            <td width="50px">:</td>
                            <td><?= date('d F Y', strtotime($d['mulai_bekerja'])) ?> </td>
                        </tr>

                        <tr>
                            <td width="100px">Selesai Bekerja </td>
                            <td width="50px">:</td>
                            <td><?= date('d F Y', strtotime($d['selesai_bekerja'])) ?> </td>
                        </tr>

                        <tr>
                            <td width="100px">Dokumen</td>
                            <td width="50px">:</td>
                            <td> <a href="<?= base_url('archive/riwpek/'); ?><?= $d['file'] ?>" target="_blank"> Lihat Data</a> </td>
                        </tr>

                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <a href="javascript:history.back()" class="btn btn-danger"> <i class="fa fa-backward"></i> Kembali</a>
            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->