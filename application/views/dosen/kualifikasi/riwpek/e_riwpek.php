<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $title; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dosen') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Kualifikasi</a></li>
                        <li class="breadcrumb-item active">Update Riwayat Pekerjaan</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>
    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title"><?= $title; ?></h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-danger col-md-8 alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?= validation_errors(); ?>
                    </div>
                <?php endif ?>


                <form class="form-horizontal" action="<?= base_url('updateRiwpek'); ?>" enctype="multipart/form-data" autocomplete="off" method="post">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tbody>
                                    <input hidden class="form-control" type="text" id="id_riwpek" name="id_riwpek" value="<?= $d['id_riwpek']; ?>" readonly required>
                                    <tr>
                                        <td>Bidang Usaha <span style="color:red;">*</span> </td>
                                        <td>
                                            <select id="bidang_usaha" name="bidang_usaha" class="form-control selectx" required>
                                                <option value="<?= $d['bidang_usaha']; ?>"><?= $d['bidang_usaha']; ?></option>
                                                <option value="A. Pertanian, Kehutanan dan Perikanan">A. Pertanian, Kehutanan dan Perikanan</option>
                                                <option value="B. Pertambangan dan Penggalian">B. Pertambangan dan Penggalian</option>
                                                <option value="C. Industri Pengolahan">C. Industri Pengolahan</option>
                                                <option value="D. Pengadaan Listrik, Gas, Uap/Air Dan Udara Dingin">D. Pengadaan Listrik, Gas, Uap/Air Dan Udara Dingin</option>
                                                <option value="E. Pengelolaan Air, Pengelolaan Air Limbah dan Daur Ulang Sampah dan Aktivitas Remediasi">E. Pengelolaan Air, Pengelolaan Air Limbah dan Daur Ulang Sampah dan Aktivitas Remediasi</option>
                                                <option value="F. Kontruksi">F. Kontruksi</option>
                                                <option value="G. Perdagangan dan Pergudangan">G. Perdagangan dan Pergudangan</option>
                                                <option value="H. Pengangkutan dan Pergudangan">H. Pengangkutan dan Pergudangan</option>
                                                <option value="I. Penyediaan Akomodasi dan Penyediaan Makan Minum">I. Penyediaan Akomodasi dan Penyediaan Makan Minum</option>
                                                <option value="J. Informasi dan Komunikasi">J. Informasi dan Komunikasi</option>
                                                <option value="I. Real Estat">I. Real Estat</option>
                                                <option value="M. Aktivitas Profesional, Ilmiah dan Teknis">M. Aktivitas Profesional, Ilmiah dan Teknis</option>
                                                <option value="N. Aktivitas Penyewaan dan Sewa Guna Usaha Tanpa Hak Opsi, Ketenagakerjaan, Agen Perjalanan dan Penunjang Usaha Lainnya">N. Aktivitas Penyewaan dan Sewa Guna Usaha Tanpa Hak Opsi, Ketenagakerjaan, Agen Perjalanan dan Penunjang Usaha Lainnya</option>
                                                <option value="O. Administrasi Pemerintah, Pertahanan dan Jaminan Sosial Wajib">O. Administrasi Pemerintah, Pertahanan dan Jaminan Sosial Wajib</option>
                                                <option value="P. Pendidikan">P. Pendidikan</option>
                                                <option value="Q. Aktivitas Kesehatan Manusia dan Aktivitas Sosial">Q. Aktivitas Kesehatan Manusia dan Aktivitas Sosial</option>
                                                <option value="R. Kesenian, Hiburan dan Rekreasi">R. Kesenian, Hiburan dan Rekreasi</option>
                                                <option value="S. Aktivitas Jasa Lainnya">S. Aktivitas Jasa Lainnya</option>
                                                <option value="T. Aktivitas Rumah Tangga Sebagai Pemberi Kerja; Aktivitas Yang Menghasilkan Barang dan Jasa Oleh Rumah Tangga yang Digunakan untuk Memenuhi Kebutuhan Sendiri">T. Aktivitas Rumah Tangga Sebagai Pemberi Kerja; Aktivitas Yang Menghasilkan Barang dan Jasa Oleh Rumah Tangga yang Digunakan untuk Memenuhi Kebutuhan Sendiri</option>
                                                <option value="U. Aktivitas Badan Internasional dan Badan Ekstra Internasional Lainnya">U. Aktivitas Badan Internasional dan Badan Ekstra Internasional Lainnya</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Jenis Pekerjaan <span style="color:red;">*</span> </td>
                                        <td>
                                            <select id="jenis_pekerjaan" name="jenis_pekerjaan" class="form-control selectx" required>
                                                <option value="<?= $d['jenis_pekerjaan']; ?>"><?= $d['jenis_pekerjaan']; ?></option>
                                                <option value="Peneliti">Peneliti</option>
                                                <option value="Tim Ahli/Konsultan">Tim Ahli / Konsultan</option>
                                                <option value="Magang">Magang</option>
                                                <option value="Tenaga Pengajar / Instruktur / Fasiltator">Tenaga Pengajar / Instruktur / Fasiltator</option>
                                                <option value="Pimpinan / Manajerial">Pimpinan / Manajerial</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Jabatan <span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="text" id="jabatan" name="jabatan" value="<?= $d['jabatan']; ?>" required> </td>
                                    </tr>

                                    <tr>
                                        <td>Instansi <span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="text" id="instansi" name="instansi" value="<?= $d['instansi']; ?>" required> </td>
                                    </tr>

                                    <tr>
                                        <td>Devisi <span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="text" id="devisi" name="devisi" value="<?= $d['devisi']; ?>" required> </td>
                                    </tr>

                                    <tr>
                                        <td>Deskripsi<span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="text" id="deskripsi" name="deskripsi" value="<?= $d['deskripsi']; ?>" required> </td>
                                    </tr>


                                    <tr>
                                        <td>Mulai Bekerja <span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="date" id="mulai_bekerja" name="mulai_bekerja" value="<?= $d['mulai_bekerja']; ?>" required> </td>
                                    </tr>

                                    <tr>
                                        <td>Selesai Bekerja <span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="date" id="selesai_bekerja" name="selesai_bekerja" value="<?= $d['selesai_bekerja']; ?>" required> </td>
                                    </tr>


                                    <tr>
                                        <td>Area Pekerjaan <span style="color:red;">*</span> </td>
                                        <td> <select id="area_pekerjaan" name="area_pekerjaan" class="form-control selectx" required>
                                                <option value="<?= $d['area_pekerjaan']; ?>"><?= $d['area_pekerjaan']; ?></option>
                                                <option value="Dalam Negri">Dalam Negri</option>
                                                <option value="Luar Negri">Luar Negri</option>

                                            </select>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Upload Dokumen<br />
                                            <small><i>(Maksimal total ukuran file dalam sekali proses upload : <br> <b><u>5 MB, Format Pdf (Ijazah, SK dan Transkip Jadikan 1 File)</u></b>)</i></small>
                                        </td>
                                        <td><input type="file" class="form-control" name="file" id="file" accept="application/pdf" required></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>


                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <a href="<?= base_url('aset_wujud') ?>">
                            <a href="javascript:history.back()" class="btn btn-danger"> <i class="fa fa-backward"></i> Kembali</a>
                        </a>
                        <button type="submit" class="btn btn-primary"> <i class="fa fa-save"></i> Simpan</button>
                    </div>
                    <!-- /.card-footer -->
                </form>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">

            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->





</div>
<!-- /.content-wrapper -->
<!-- Select2 -->
<script src="<?= base_url() ?>assets/vendor/backend/plugins/select2/js/select2.full.min.js"></script>
<script src="<?= base_url() ?>assets/vendor//js/rupiah.js"></script>
<script src="<?= base_url() ?>assets/vendor//js/duit.js"></script>
<script>
    $(document).ready(function() {
        $('.selectx').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $(document).ready(function() {
        $('.selecty').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $(document).ready(function() {
        $('.selectz').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $('#id_user').change(function() {
        var id_user = $(this).val();
        $.ajax({
            url: "<?= site_url('get-user'); ?>",
            method: "POST",
            data: {
                id_user: id_user
            },
            async: true,
            dataType: 'json',
            success: function(data) {

                var jbt = '';

                var i;
                for (i = 0; i < data.length; i++) {
                    jbt = data[i].jabatan;
                }
                document.getElementById("jabatan").value = jbt;
            }
        });
        return false;
    });


    $(document).ready(function() {

        $('#id_kampus').change(function() {
            var id_kampus = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-tempat'); ?>",
                method: "POST",
                data: {
                    id_kampus: id_kampus
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_gedung + '>' + data[i].gedung + '</option>';
                    }
                    $('#id_gedung').html(html);

                }
            });
            return false;
        });

        $('.selectw').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });



    $(document).ready(function() {

        $('#id_gedung').change(function() {
            var id_gedung = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-gedung'); ?>",
                method: "POST",
                data: {
                    id_gedung: id_gedung
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_lantai + '>' + data[i].lantai + '</option>';
                    }
                    $('#id_lantai').html(html);

                }
            });
            return false;
        });

        $('.selectw').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });

    $(document).ready(function() {

        $('#id_lantai').change(function() {
            var id_lantai = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-ruangan'); ?>",
                method: "POST",
                data: {
                    id_lantai: id_lantai
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_ruangan + '>' + data[i].ruangan + '</option>';
                    }
                    $('#id_ruangan').html(html);

                }
            });
            return false;
        });

        $('.selecta').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });
</script>