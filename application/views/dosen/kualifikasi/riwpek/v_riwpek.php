<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $title; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dosen') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">profil</a></li>
                        <li class="breadcrumb-item active">Riwayat Pekerjaan</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <div class=""></div>
    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <?php if ($this->session->flashdata('gagal_store')) { ?>
                    <div class="alert alert-danger col-md-12">
                        <?= $this->session->flashdata('gagal_store') ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php } ?>


                <h3 class="card-title">
                    <a href="<?= base_url('createRiwpek'); ?>" class="btn btn bg-gradient-primary"><i class="fa fa-plus"></i> Tambah</a>


                </h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <div class="alert alert-info">
                    Menu ini untuk mencatat riwayat pekerjaan dosen baik sebelum maupun setelah menjadi dosen. <br>
                    Data ini akan digunakan untuk melihat indikator jumlah dosen yang telah berpengalaman di Industri. <br>
                    Selain itu data ini juga akan dijadikan salah satu indikator penilai pada program Klasterisasi Perguruan Tinggi.
                </div>
                <div class="table-responsive">
                    <table id="example1" class="table table-bordered table-striped table-sm">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Nama Pekerjaan</th>
                                <th>Jabatan</th>
                                <th>Rincian Pekerjaan</th>
                                <th>instansi</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            foreach ($riwpek as $row) : ?>
                                <tr>
                                    <td><?= $no++; ?></td>
                                    <td><?= $row['bidang_usaha']; ?></td>
                                    <td><?= $row['jabatan']; ?></td>
                                    <td><?= $row['deskripsi']; ?></td>
                                    <td><?= $row['instansi']; ?></td>
                                    <td>
                                        <a href="<?= base_url('editRiwpek/' . $row['id_riwpek']) ?>" class="btn btn-success btn-sm" title="Update Data">
                                            <i class="fas fa-edit"></i>
                                        </a>
                                        <a href="<?= base_url('detailRiwpek/' . $row['id_riwpek']) ?>" class="btn btn-info btn-sm" title="Detail Data">
                                            <i class="fas fa-info"></i>
                                        </a>

                                        <a href="<?= base_url('hapusRiwpek/' . $row['id_riwpek']) ?>" class="btn btn-danger btn-sm tombol-hapus" title="Hapus Data">
                                            <i class="fas fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">

            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->

</div>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
    $(function() {
        $("#example1").DataTable({
            "language": {
                "sSearch": "Cari"
            }
        });
    });
</script>