<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $title; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dosen') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Kualifikasi</a></li>
                        <li class="breadcrumb-item active">Pendidikan Formal</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>
    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title"><?= $title; ?></h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-danger col-md-8 alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?= validation_errors(); ?>
                    </div>
                <?php endif ?>


                <form class="form-horizontal" action="<?= base_url('savePenfor'); ?>" enctype="multipart/form-data" autocomplete="off" method="post">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td>Perguruan Tinggi <span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="text" id="perguruan_tinggi" name="perguruan_tinggi" required> </td>
                                    </tr>
                                    <tr>
                                        <td>Jenjang Studi <span style="color:red;">*</span> </td>
                                        <td>
                                            <select id="jenjang" name="jenjang" class="form-control selectx" required>
                                                <option selected disabled value="">Pilih</option>
                                                <option value="SMA/Sederajat">SMA/Sederajat</option>
                                                <option value="D1">D1</option>
                                                <option value="D2">D2</option>
                                                <option value="D3">D3</option>
                                                <option value="D4">D4</option>
                                                <option value="S1">S1</option>
                                                <option value="Profesi">Profesi</option>
                                                <option value="Sp-1">Sp-1</option>
                                                <option value="S2">S2</option>
                                                <option value="S2 Terapan">S2 Terapan</option>
                                                <option value="Sp-2">Sp-2</option>
                                                <option value="S3">S3</option>
                                                <option value="S3 Terapan">S3 Terapan</option>

                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Program Studi <span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="text" id="program_studi" name="program_studi" required> </td>
                                    </tr>

                                    <tr>
                                        <td>Gelar Akademik <span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="text" id="gelar_akademik" name="gelar_akademik" required> </td>
                                    </tr>

                                    <tr>
                                        <td>Bidang Studi <span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="text" id="bidang_studi" name="bidang_studi" required> </td>
                                    </tr>

                                    <tr>
                                        <td>Tahun Masuk <span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="text" id="tahun_masuk" name="tahun_masuk" required> </td>
                                    </tr>


                                    <tr>
                                        <td>Tahun Lulus <span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="text" id="tahun_lulus" name="tahun_lulus" required> </td>
                                    </tr>

                                    <tr>
                                        <td>Tanggal Kelulusan <span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="date" id="tgl_kelulusan" name="tgl_kelulusan" required> </td>
                                    </tr>


                                    <tr>
                                        <td>Nomor Induk <span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="text" id="nomor_induk" name="nomor_induk" required> </td>
                                    </tr>


                                    <tr>
                                        <td>Jumlah Semester Tempuh <span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="number" id="jumlah_semester_tempuh" name="jumlah_semester_tempuh" required> </td>
                                    </tr>

                                    <tr>
                                        <td>Jumlah Kelulusan <span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="number" id="jumlah_kelulusan" name="jumlah_kelulusan" required> </td>
                                    </tr>


                                    <tr>
                                        <td>IPK Kelulusan</td>
                                        <td><input class="form-control" type="text" id="ipk_kelulusan" name="ipk_kelulusan">
                                            <span><i>Masukkan angka menggunakan tanda titik.</i> <span style="color:red;">Contoh: 3.55</span></span>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Nomor SK Penyetaraan</td>
                                        <td><input class="form-control" type="text" id="no_sk_penyetaraan" name="no_sk_penyetaraan"> </td>
                                    </tr>


                                    <tr>
                                        <td>Tanggal SK Penyetaraan</td>
                                        <td><input class="form-control" type="date" id="tgl_sk_penyetaraan" name="tgl_sk_penyetaraan"> </td>
                                    </tr>


                                    <tr>
                                        <td>Nomor Ijazah</td>
                                        <td><input class="form-control" type="text" id="nomor_ijazah" name="nomor_ijazah" required> </td>
                                    </tr>

                                    <tr>
                                        <td>Judul Skripsi/Tesis/Disertasi </td>
                                        <td><input class="form-control" type="text" id="judul" name="judul" required> </td>
                                    </tr>

                                    <tr>
                                        <td>Upload Dokumen<br />
                                            <small><i>(Maksimal total ukuran file dalam sekali proses upload : <br> <b><u>5 MB, Format Pdf (Ijazah, SK dan Transkip Jadikan 1 File)</u></b>)</i></small>
                                        </td>
                                        <td><input type="file" class="form-control" name="file" id="file" accept="application/pdf" required></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>


                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <a href="<?= base_url('aset_wujud') ?>">
                            <a href="javascript:history.back()" class="btn btn-danger"> <i class="fa fa-backward"></i> Kembali</a>
                        </a>
                        <button type="submit" class="btn btn-primary"> <i class="fa fa-save"></i> Simpan</button>
                    </div>
                    <!-- /.card-footer -->
                </form>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">

            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->





</div>
<!-- /.content-wrapper -->
<!-- Select2 -->
<script src="<?= base_url() ?>assets/vendor/backend/plugins/select2/js/select2.full.min.js"></script>
<script src="<?= base_url() ?>assets/vendor//js/rupiah.js"></script>
<script src="<?= base_url() ?>assets/vendor//js/duit.js"></script>
<script>
    $(document).ready(function() {
        $('.selectx').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $(document).ready(function() {
        $('.selecty').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $(document).ready(function() {
        $('.selectz').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $('#id_user').change(function() {
        var id_user = $(this).val();
        $.ajax({
            url: "<?= site_url('get-user'); ?>",
            method: "POST",
            data: {
                id_user: id_user
            },
            async: true,
            dataType: 'json',
            success: function(data) {

                var jbt = '';

                var i;
                for (i = 0; i < data.length; i++) {
                    jbt = data[i].jabatan;
                }
                document.getElementById("jabatan").value = jbt;
            }
        });
        return false;
    });


    $(document).ready(function() {

        $('#id_kampus').change(function() {
            var id_kampus = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-tempat'); ?>",
                method: "POST",
                data: {
                    id_kampus: id_kampus
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_gedung + '>' + data[i].gedung + '</option>';
                    }
                    $('#id_gedung').html(html);

                }
            });
            return false;
        });

        $('.selectw').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });



    $(document).ready(function() {

        $('#id_gedung').change(function() {
            var id_gedung = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-gedung'); ?>",
                method: "POST",
                data: {
                    id_gedung: id_gedung
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_lantai + '>' + data[i].lantai + '</option>';
                    }
                    $('#id_lantai').html(html);

                }
            });
            return false;
        });

        $('.selectw').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });

    $(document).ready(function() {

        $('#id_lantai').change(function() {
            var id_lantai = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-ruangan'); ?>",
                method: "POST",
                data: {
                    id_lantai: id_lantai
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_ruangan + '>' + data[i].ruangan + '</option>';
                    }
                    $('#id_ruangan').html(html);

                }
            });
            return false;
        });

        $('.selecta').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });
</script>