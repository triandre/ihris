<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $title; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dosen') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Kualifikasi</a></li>
                        <li class="breadcrumb-item active">Diklat Pekerti/AA</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>
    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title"><?= $title; ?></h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-danger col-md-8 alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?= validation_errors(); ?>
                    </div>
                <?php endif ?>


                <form class="form-horizontal" action="<?= base_url('saveDiklat'); ?>" enctype="multipart/form-data" autocomplete="off" method="post">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td>Jenis Diklat <span style="color:red;">*</span> </td>
                                        <td>
                                            <select id="jenis_diklat" name="jenis_diklat" class="form-control selectx" required>
                                                <option selected disabled value="">Pilih</option>
                                                <option value="Pekerti">Pekerti</option>
                                                <option value="Applied Approach">Applied Approach</option>
                                            </select>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Kategori Kegiatan <span style="color:red;">*</span> </td>
                                        <td>
                                            <select id="kategori_kegiatan" name="kategori_kegiatan" class="form-control selectx" required>
                                                <option selected disabled value="">Pilih</option>
                                                <option value="Lamanya 10-30 Jam">Lamanya 10-30 Jam</option>
                                                <option value="Lamanya 31-80 Jam">Lamanya 31-80 Jam</option>
                                                <option value="Lamanya 81-160 Jam">Lamanya 81-160 Jam</option>
                                                <option value="Lamanya 161-480 Jam">Lamanya 161-480 Jam</option>
                                                <option value="Lamanya 481-640 Jam">Lamanya 181-640 Jam</option>
                                                <option value="Lamanya 641-960 Jam">Lamanya 641-960 Jam</option>
                                                <option value="Lamanya Lebih Dari 960 Jam">Lamanya Lebih Dari 960 Jam</option>

                                            </select>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Tingkat Diklat </td>
                                        <td>
                                            <select id="tingkat_kegiatan" name="tingkat_kegiatan" class="form-control selectx">
                                                <option selected disabled value="">Pilih</option>
                                                <option value="Lokal">Lokal</option>
                                                <option value="Regional">Regional</option>
                                                <option value="Nasional">Nasional</option>
                                                <option value="internasional">internasional</option>
                                            </select>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Nama Diklat <span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="text" id="nama_diklat" name="nama_diklat" required> </td>
                                    </tr>


                                    <tr>
                                        <td>Penyelenggara <span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="text" id="penyelenggara" name="penyelenggara" required> </td>
                                    </tr>

                                    <tr>
                                        <td>Peran </td>
                                        <td><input class="form-control" type="text" id="peran" name="peran"> </td>
                                    </tr>

                                    <tr>
                                        <td>Jumlah Jam</td>
                                        <td><input class="form-control" type="text" id="jumlah_jam" name="jumlah_jam"> </td>
                                    </tr>

                                    <tr>
                                        <td>No. Sertifikat <span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="text" id="no_sertifikat" name="no_sertifikat" required> </td>
                                    </tr>


                                    <tr>
                                        <td>Tanggal Sertifikat <span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="date" id="tgl_sertifikat" name="tgl_sertifikat" required> </td>
                                    </tr>

                                    <tr>
                                        <td>Tahun Penyelenggara <span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="number" id="tahun_penyelenggara" name="tahun_penyelenggara" required> </td>
                                    </tr>


                                    <tr>
                                        <td>Tempat</td>
                                        <td><input class="form-control" type="text" id="tempat" name="tempat"> </td>
                                    </tr>

                                    <tr>
                                        <td>Tanggal Mulai<span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="date" id="tgl_mulai" name="tgl_mulai" required> </td>
                                    </tr>


                                    <tr>
                                        <td>Tanggal Selesai<span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="date" id="tgl_selesai" name="tgl_selesai" required>

                                        </td>
                                    </tr>

                                    <tr>
                                        <td>No. SK Penugasan</td>
                                        <td><input class="form-control" type="text" id="no_sk_penugasan" name="no_sk_penugasan"> </td>
                                    </tr>


                                    <tr>
                                        <td>Tanggal SK Penugasan</td>
                                        <td><input class="form-control" type="date" id="tgl_sk_penugasan" name="tgl_sk_penugasan"> </td>
                                    </tr>


                                    <tr>
                                        <td>Upload Dokumen<br />
                                            <small><i>(Maksimal total ukuran file dalam sekali proses upload : <br> <b><u>5 MB, Format Pdf (Ijazah, SK dan Transkip Jadikan 1 File)</u></b>)</i></small>
                                        </td>
                                        <td><input type="file" class="form-control" name="file" id="file" accept="application/pdf" required></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>


                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <a href="<?= base_url('aset_wujud') ?>">
                            <a href="javascript:history.back()" class="btn btn-danger"> <i class="fa fa-backward"></i> Kembali</a>
                        </a>
                        <button type="submit" class="btn btn-primary"> <i class="fa fa-save"></i> Simpan</button>
                    </div>
                    <!-- /.card-footer -->
                </form>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">

            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->





</div>
<!-- /.content-wrapper -->
<!-- Select2 -->
<script src="<?= base_url() ?>assets/vendor/backend/plugins/select2/js/select2.full.min.js"></script>
<script src="<?= base_url() ?>assets/vendor//js/rupiah.js"></script>
<script src="<?= base_url() ?>assets/vendor//js/duit.js"></script>
<script>
    $(document).ready(function() {
        $('.selectx').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $(document).ready(function() {
        $('.selecty').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $(document).ready(function() {
        $('.selectz').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $('#id_user').change(function() {
        var id_user = $(this).val();
        $.ajax({
            url: "<?= site_url('get-user'); ?>",
            method: "POST",
            data: {
                id_user: id_user
            },
            async: true,
            dataType: 'json',
            success: function(data) {

                var jbt = '';

                var i;
                for (i = 0; i < data.length; i++) {
                    jbt = data[i].jabatan;
                }
                document.getElementById("jabatan").value = jbt;
            }
        });
        return false;
    });


    $(document).ready(function() {

        $('#id_kampus').change(function() {
            var id_kampus = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-tempat'); ?>",
                method: "POST",
                data: {
                    id_kampus: id_kampus
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_gedung + '>' + data[i].gedung + '</option>';
                    }
                    $('#id_gedung').html(html);

                }
            });
            return false;
        });

        $('.selectw').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });



    $(document).ready(function() {

        $('#id_gedung').change(function() {
            var id_gedung = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-gedung'); ?>",
                method: "POST",
                data: {
                    id_gedung: id_gedung
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_lantai + '>' + data[i].lantai + '</option>';
                    }
                    $('#id_lantai').html(html);

                }
            });
            return false;
        });

        $('.selectw').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });

    $(document).ready(function() {

        $('#id_lantai').change(function() {
            var id_lantai = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-ruangan'); ?>",
                method: "POST",
                data: {
                    id_lantai: id_lantai
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_ruangan + '>' + data[i].ruangan + '</option>';
                    }
                    $('#id_ruangan').html(html);

                }
            });
            return false;
        });

        $('.selecta').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });
</script>