<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $title; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dosen') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">profil</a></li>
                        <li class="breadcrumb-item active">Diklat</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <?php if ($this->session->flashdata('gagal_store')) { ?>
                    <div class="alert alert-danger col-md-12">
                        <?= $this->session->flashdata('gagal_store') ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php } ?>


                <h3 class="card-title">
                    <div class="btn-group">
                        <button type="button" class="btn btn bg-gradient-primary dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-plus"></i> Tambah
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="<?= base_url('createDiklat'); ?>">Pekerti/AA</a>
                            <a class="dropdown-item" href="<?= base_url('createDiklatNon'); ?>">Non Pekerti/AA</a>
                        </div>
                    </div>

                    <a href="<?= base_url('riwayatDiklat'); ?>" class="btn btn bg-gradient-info"> Riwayat Ajuan </a>
                </h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="example1" class="table table-bordered table-striped table-sm">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Nama Diklat</th>
                                <th>Jenis Diklat</th>
                                <th>Penyelenggara</th>
                                <th>Tahun</th>
                                <th>status </th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            foreach ($diklat as $row) : ?>
                                <tr>
                                    <td><?= $no++; ?></td>
                                    <td><?= $row['nama_diklat']; ?></td>
                                    <td><?= $row['jenis_diklat']; ?></td>
                                    <td><?= $row['penyelenggara']; ?></td>
                                    <td><?= $row['tahun_penyelenggara']; ?></td>
                                    <td><?php
                                        if ($row['sts'] == 2) {
                                            echo '<span class="badge badge-info">Proses Verifikasi</span> ';
                                        } elseif ($row['sts'] == 3) {
                                            echo '<span class="badge badge-success">Disetujui</span>';
                                        } elseif ($row['sts'] == 4) {
                                            echo '<span class="badge badge-danger">Tolak</span>';
                                        } elseif ($row['sts'] == 5) {
                                            echo '<span class="badge badge-warning">Ditangguhkan</span>';
                                        }  ?></i></td>
                                    <td>

                                        <a href="<?= base_url('detailDiklat/' . $row['id_diklat']) ?>" class="btn btn-info btn-sm" title="Detail Data">
                                            <i class="fas fa-info"></i>
                                        </a>

                                        <a href="<?= base_url('hapusDiklat/' . $row['id_diklat']) ?>" class="btn btn-danger btn-sm tombol-hapus" title="Hapus Data">
                                            <i class="fas fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">

            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->

</div>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
    $(function() {
        $("#example1").DataTable({
            "language": {
                "sSearch": "Cari"
            }
        });
    });
</script>