<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $title; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dosen') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Pel. Penelitian</a></li>
                        <li class="breadcrumb-item active">Penelitian</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>
    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title"><?= $title; ?></h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-danger col-md-8 alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?= validation_errors(); ?>
                    </div>
                <?php endif ?>


                <form class="form-horizontal" enctype="multipart/form-data" autocomplete="off" method="post">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tbody>
                                    <input hidden class="form-control" type="text" id="reff_penelitian" name="reff_penelitian" value="<?= $d['reff_penelitian']; ?>" readonly required>
                                    <tr>
                                        <td>Kategori Kegiatan</td>
                                        <td> <?= $d['kategori_kgt']; ?> </td>
                                    </tr>

                                    <tr>
                                        <td>Judul Kegiatan</td>
                                        <td><?= $d['judul_kgt']; ?></td>
                                    </tr>

                                    <tr>
                                        <td>Afiliasi</td>
                                        <td><?= $d['afiliasi']; ?></td>
                                    </tr>


                                    <tr>
                                        <td>Kelompok Bidang</td>
                                        <td><?= $d['kelompok']; ?></td>
                                    </tr>

                                    <tr>
                                        <td>Litabmas Sebelum</td>
                                        <td><?= $d['litabmas_sebelum']; ?></td>
                                    </tr>

                                    <tr>
                                        <td>Jenis Skim</td>
                                        <td><?= $d['jenis_skim']; ?> </td>
                                    </tr>
                                    <tr>
                                        <td>Lokasi Kegiatan</td>
                                        <td><?= $d['lokasi']; ?></td>
                                    </tr>

                                    <tr>
                                        <td>Tahun Usulan</td>
                                        <td><?= $d['tahun_usulan']; ?> </td>
                                    </tr>

                                    <tr>
                                        <td>Tahun Kegiatan</td>
                                        <td><?= $d['tahun_kegiatan']; ?></td>
                                    </tr>

                                    <tr>
                                        <td>Tahun pelaksanaan</td>
                                        <td><?= $d['tahun_pelaksanaan']; ?></td>
                                    </tr>

                                    <tr>
                                        <td>Lama Kegiatan</td>
                                        <td><?= $d['lama_kgt']; ?></td>
                                    </tr>

                                    <tr>
                                        <td>Tahun pelaksanaan Ke</td>
                                        <td><?= $d['tahun_pelaksanaan_ke']; ?></td>
                                    </tr>


                                    <tr>
                                        <td>Dana Dari Dikti (Rp.)</td>
                                        <td><?= $d['dana_dikti']; ?></td>
                                    </tr>

                                    <tr>
                                        <td>Dana Dari Perguruan Tinggi (Rp.)</td>
                                        <td><?= $d['dana_pt']; ?></td>
                                    </tr>

                                    <tr>
                                        <td>Dana Dari Instansi Lain (Rp.)</td>
                                        <td><?= $d['dana_pt_lain']; ?></td>
                                    </tr>

                                    <tr>
                                        <td>In Kind</td>
                                        <td><?= $d['in_kind']; ?></td>
                                    </tr>


                                    <tr>
                                        <td>No. SK Penugasan</td>
                                        <td><?= $d['no_sk']; ?></td>
                                    </tr>


                                    <tr>
                                        <td>Tanggal SK Penugasan</td>
                                        <td><?= $d['tgl_sk']; ?></td>
                                    </tr>


                                    <tr>
                                        <td>Upload Dokumen Pendukung </td>
                                        <td><a href="<?= base_url('archive/penelitian/'); ?><?= $d['file'] ?>" target="_blank"> Lihat Data</a></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>


                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <a href="<?= base_url('penelitian') ?>">
                            <a href="javascript:history.back()" class="btn btn-danger"> <i class="fa fa-backward"></i> Kembali</a>
                        </a>

                    </div>
                    <!-- /.card-footer -->
                </form>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">

            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->





</div>
<!-- /.content-wrapper -->
<!-- Select2 -->
<script src="<?= base_url() ?>assets/vendor/backend/plugins/select2/js/select2.full.min.js"></script>
<script src="<?= base_url() ?>assets/js/rupiah.js"></script>
<script src="<?= base_url() ?>assets/js/duit.js"></script>
<script src="<?= base_url() ?>assets/js/format.js"></script>
<script>
    $(document).ready(function() {
        $('.selectx').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $(document).ready(function() {
        $('.selecty').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $(document).ready(function() {
        $('.selectz').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $('#id_user').change(function() {
        var id_user = $(this).val();
        $.ajax({
            url: "<?= site_url('get-user'); ?>",
            method: "POST",
            data: {
                id_user: id_user
            },
            async: true,
            dataType: 'json',
            success: function(data) {

                var jbt = '';

                var i;
                for (i = 0; i < data.length; i++) {
                    jbt = data[i].jabatan;
                }
                document.getElementById("jabatan").value = jbt;
            }
        });
        return false;
    });


    $(document).ready(function() {

        $('#id_kampus').change(function() {
            var id_kampus = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-tempat'); ?>",
                method: "POST",
                data: {
                    id_kampus: id_kampus
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_gedung + '>' + data[i].gedung + '</option>';
                    }
                    $('#id_gedung').html(html);

                }
            });
            return false;
        });

        $('.selectw').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });



    $(document).ready(function() {

        $('#id_gedung').change(function() {
            var id_gedung = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-gedung'); ?>",
                method: "POST",
                data: {
                    id_gedung: id_gedung
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_lantai + '>' + data[i].lantai + '</option>';
                    }
                    $('#id_lantai').html(html);

                }
            });
            return false;
        });

        $('.selectw').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });

    $(document).ready(function() {

        $('#id_lantai').change(function() {
            var id_lantai = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-ruangan'); ?>",
                method: "POST",
                data: {
                    id_lantai: id_lantai
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_ruangan + '>' + data[i].ruangan + '</option>';
                    }
                    $('#id_ruangan').html(html);

                }
            });
            return false;
        });

        $('.selecta').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });
</script>