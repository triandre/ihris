<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $title; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dosen') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Pel. Penelitian</a></li>
                        <li class="breadcrumb-item active">Penelitian</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>
    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title"><?= $title; ?></h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-danger col-md-8 alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?= validation_errors(); ?>
                    </div>
                <?php endif ?>


                <form class="form-horizontal" action="<?= base_url('penelitian/perbaikanPenelitianGo'); ?>" enctype="multipart/form-data" autocomplete="off" method="post">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tbody>
                                    <input hidden class="form-control" type="text" id="reff_penelitian" name="reff_penelitian" value="<?= $d['reff_penelitian']; ?>" readonly required>
                                    <tr>
                                        <td>Kategori Kegiatan <span style="color:red;">*</span> </td>
                                        <td>
                                            <select id="kategori_kgt" name="kategori_kgt" class="form-control selectx" required>
                                                <option value="<?= $d['kategori_kgt']; ?>"><?= $d['kategori_kgt']; ?></option>
                                                <option value="Melaksanakan aktivitas penelitian - Sebagai ketua">Melaksanakan aktivitas penelitian - Sebagai ketua</option>
                                                <option value="Melaksanakan aktivitas penelitian - Sebagai anggota">Melaksanakan aktivitas penelitian - Sebagai anggota</option>
                                                <option value="Hasil penelitian atau pemikiran atau kerjasama industri termasuk penelitian penugasan dari kementerian atau LPNK yang tidak dipublikasikan (tersimpan dalam perpustakaan) yang dilakukan secara melembaga">Hasil penelitian atau pemikiran atau kerjasama industri termasuk penelitian penugasan dari kementerian atau LPNK yang tidak dipublikasikan (tersimpan dalam perpustakaan) yang dilakukan secara melembaga</option>
                                            </select>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Judul Kegiatan <span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="text" id="judul_kgt" name="judul_kgt" required value="<?= $d['judul_kgt']; ?>"> </td>
                                    </tr>

                                    <tr>
                                        <td>Afiliasi <span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="text" id="afiliasi" name="afiliasi" required value="<?= $d['afiliasi']; ?>"> </td>
                                    </tr>


                                    <tr>
                                        <td>Kelompok Bidang <span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="text" id="kelompok" name="kelompok" required value="<?= $d['kelompok']; ?>"> </td>
                                    </tr>

                                    <tr>
                                        <td>Litabmas Sebelum <span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="text" id="litabmas_sebelum" name="litabmas_sebelum" required value="<?= $d['litabmas_sebelum']; ?>"> </td>
                                    </tr>

                                    <tr>
                                        <td>Jenis Skim <span style="color:red;">*</span> </td>
                                        <td>
                                            <select id="jenis_skim" name="jenis_skim" class="form-control selectx" required>
                                                <option value="<?= $d['jenis_skim']; ?>"><?= $d['jenis_skim']; ?></option>
                                                <option value="Biomedik">Biomedik</option>
                                                <option value="Hibah HI-LINK">Hibah HI-LINK</option>
                                                <option value="Ipteks">Ipteks</option>
                                                <option value="Iptek Bagi Inovasi Kreativitas Kampus">Iptek Bagi Inovasi Kreativitas Kampus</option>
                                                <option value="Iptek Bagi Kewirausahaan">Iptek Bagi Kewirausahaan</option>
                                                <option value="Iptek Bagi Masyarakat">Iptek Bagi Masyarakat</option>
                                                <option value="Iptek Vagi Produk Ekspor">Iptek Vagi Produk Ekspor</option>
                                                <option value="Iptek Bagi Wilayah">Iptek Bagi Wilayah</option>
                                                <option value="Iptek Bagi Wilaya Antara PT-CSR / PT-PEMDA-CSR">Iptek Bagi Wilaya Antara PT-CSR / PT-PEMDA-CSR</option>
                                                <option value="KKN Pembelajaran Pemberdayaan Masyarakat">KKN Pembelajaran Pemberdayaan Masyarakat</option>
                                                <option value="Mobil Listrik Nasional">Mobil Listrik Nasional</option>
                                                <option value="MP3EI">MP3EI</option>
                                                <option value="Pendidikan Magister Doktor Sarjana Unggul">Pendidikan Magister Doktor Sarjana Unggul</option>
                                                <option value="Penelitian Desertasi Doktor">Penelitian Desertasi Doktor</option>
                                                <option value="Penelitian Dosen Pemula">Penelitian Dosen Pemula</option>
                                                <option value="Penelitian Fundamental">Penelitian Fundamental</option>
                                                <option value="Penelitian Hibah Bersaing">Penelitian Hibah Bersaing</option>
                                                <option value="Penelitian Kerja sama Perguruan Tinggi">Penelitian Kerja sama Perguruan Tinggi</option>
                                                <option value="Penelitian Kompetensi">Penelitian Kompetensi</option>
                                                <option value="Penelitian Strategis Nasional">Penelitian Strategis Nasional</option>
                                                <option value="Penelitian Tim Pascasarjana">Penelitian Tim Pascasarjana</option>
                                                <option value="Penelitian Unggulan Perguruan Tinggi">Penelitian Unggulan Perguruan Tinggi</option>
                                                <option value="Penelitian Unggulan Startegis Nasional">Penelitian Unggulan Startegis Nasional</option>
                                                <option value="Riset Andalan Perguruan Tinggi">Riset Andalan Perguruan Tinggi</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Lokasi Kegiatan <span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="text" id="lokasi" name="lokasi" required value="<?= $d['lokasi']; ?>"> </td>
                                    </tr>

                                    <tr>
                                        <td>Tahun Usulan <span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="text" id="tahun_usulan" name="tahun_usulan" required value="<?= $d['tahun_usulan']; ?>"> </td>
                                    </tr>

                                    <tr>
                                        <td>Tahun Kegiatan <span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="text" id="tahun_kegiatan" name="tahun_kegiatan" required value="<?= $d['tahun_kegiatan']; ?>"> </td>
                                    </tr>

                                    <tr>
                                        <td>Tahun pelaksanaan <span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="text" id="tahun_pelaksanaan" name="tahun_pelaksanaan" required value="<?= $d['tahun_pelaksanaan']; ?>"> </td>
                                    </tr>

                                    <tr>
                                        <td>Lama Kegiatan <span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="text" id="lama_kgt" name="lama_kgt" required value="<?= $d['lama_kgt']; ?>"> </td>
                                    </tr>

                                    <tr>
                                        <td>Tahun pelaksanaan Ke <span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="text" id="tahun_pelaksanaan_ke" name="tahun_pelaksanaan_ke" required value="<?= $d['tahun_pelaksanaan_ke']; ?>"> </td>
                                    </tr>


                                    <tr>
                                        <td>Dana Dari Dikti (Rp.) <span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="text" id="duit" name="dana_dikti" required value="<?= $d['dana_dikti']; ?>"> </td>
                                    </tr>

                                    <tr>
                                        <td>Dana Dari Perguruan Tinggi (Rp.) <span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="text" id="uang" name="dana_pt" required value="<?= $d['dana_pt']; ?>"> </td>
                                    </tr>

                                    <tr>
                                        <td>Dana Dari Instansi Lain (Rp.) <span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="text" id="rupiah" name="dana_pt_lain" required value="<?= $d['dana_pt_lain']; ?>"> </td>
                                    </tr>

                                    <tr>
                                        <td>In Kind <span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="text" id="in_kind" name="in_kind" required value="<?= $d['in_kind']; ?>"> </td>
                                    </tr>


                                    <tr>
                                        <td>No. SK Penugasan</td>
                                        <td><input class="form-control" type="text" id="no_sk" name="no_sk" value="<?= $d['no_sk']; ?>"> </td>
                                    </tr>


                                    <tr>
                                        <td>Tanggal SK Penugasan</td>
                                        <td><input class="form-control" type="date" id="tgl_sk" name="tgl_sk" value="<?= $d['tgl_sk']; ?>"> </td>
                                    </tr>


                                    <tr>
                                        <td>Upload Dokumen Pendukung </td>
                                        <td><input type="file" class="form-control" name="file" id="file" accept="application/pdf" required></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>


                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <a href="<?= base_url('aset_wujud') ?>">
                            <a href="javascript:history.back()" class="btn btn-danger"> <i class="fa fa-backward"></i> Kembali</a>
                        </a>
                        <button type="submit" class="btn btn-primary"> <i class="fa fa-save"></i> Simpan</button>
                    </div>
                    <!-- /.card-footer -->
                </form>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">

            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->





</div>
<!-- /.content-wrapper -->
<!-- Select2 -->
<script src="<?= base_url() ?>assets/vendor/backend/plugins/select2/js/select2.full.min.js"></script>
<script src="<?= base_url() ?>assets/js/rupiah.js"></script>
<script src="<?= base_url() ?>assets/js/duit.js"></script>
<script src="<?= base_url() ?>assets/js/format.js"></script>
<script>
    $(document).ready(function() {
        $('.selectx').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $(document).ready(function() {
        $('.selecty').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $(document).ready(function() {
        $('.selectz').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $('#id_user').change(function() {
        var id_user = $(this).val();
        $.ajax({
            url: "<?= site_url('get-user'); ?>",
            method: "POST",
            data: {
                id_user: id_user
            },
            async: true,
            dataType: 'json',
            success: function(data) {

                var jbt = '';

                var i;
                for (i = 0; i < data.length; i++) {
                    jbt = data[i].jabatan;
                }
                document.getElementById("jabatan").value = jbt;
            }
        });
        return false;
    });


    $(document).ready(function() {

        $('#id_kampus').change(function() {
            var id_kampus = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-tempat'); ?>",
                method: "POST",
                data: {
                    id_kampus: id_kampus
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_gedung + '>' + data[i].gedung + '</option>';
                    }
                    $('#id_gedung').html(html);

                }
            });
            return false;
        });

        $('.selectw').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });



    $(document).ready(function() {

        $('#id_gedung').change(function() {
            var id_gedung = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-gedung'); ?>",
                method: "POST",
                data: {
                    id_gedung: id_gedung
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_lantai + '>' + data[i].lantai + '</option>';
                    }
                    $('#id_lantai').html(html);

                }
            });
            return false;
        });

        $('.selectw').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });

    $(document).ready(function() {

        $('#id_lantai').change(function() {
            var id_lantai = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-ruangan'); ?>",
                method: "POST",
                data: {
                    id_lantai: id_lantai
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_ruangan + '>' + data[i].ruangan + '</option>';
                    }
                    $('#id_ruangan').html(html);

                }
            });
            return false;
        });

        $('.selecta').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });
</script>