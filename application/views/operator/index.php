<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Dashboard</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('') ?>">Dashboard</a></li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>

    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <?php if ($this->session->flashdata('gagal_store')) { ?>
                    <div class="alert alert-danger col-md-12">
                        <?= $this->session->flashdata('gagal_store') ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php } ?>
                <h3 class="card-title">
                    <!-- <a href="<?= base_url('createPengajaran'); ?>" class="btn btn-block bg-gradient-primary"><i class="fa fa-plus"></i> Tambah</a> -->
                </h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <div class="entry-header" style='box-sizing: border-box; margin: 0px 0px 30px; padding: 0px; border: 0px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-weight: 400; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: "Open Sans", Helvetica, Arial, sans-serif; vertical-align: baseline; color: rgb(58, 58, 58); letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;'>
                    <h1 class="jeg_post_title" style="box-sizing: border-box; margin: 0px 0px 0.4em; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: 3em; line-height: 1.15; font-family: Lato, Helvetica, Arial, sans-serif; vertical-align: baseline; color: rgb(17, 17, 17); text-rendering: optimizelegibility; letter-spacing: -0.04em;">Visi dan Misi</h1>
                </div>
                <div class="entry-content " style='box-sizing: border-box; margin: 0px 0px 30px; padding: 0px; border: 0px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-weight: 400; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: "Open Sans", Helvetica, Arial, sans-serif; vertical-align: baseline; position: relative; color: rgb(58, 58, 58); letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;'>
                    <div class="content-inner" style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline;">
                        <p style="box-sizing: border-box; margin: 0px 0px 1.25em; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; text-rendering: optimizelegibility; color: rgb(38, 38, 38); text-align: justify;">Perguruan tinggi pada hakekatnya merupakan lembaga yang berfungsi untuk melestarikan, mengembangkan, menyebarluaskan, dan menggali ilmu pengetahuan dan teknologi. Selain itu perguruan tinggi juga berfungsi mengembangkan kualitas sumberdaya manusia dan menghasilkan jasa-jasa. Dalam era globalisasi, informasi, dan saling ketergantungan sebagaimana yang telah, sedang, dan akan berlangsung, peran perguruan tinggi menjadi semakin penting. Dalam era tersebut keunggulan suatu bangsa tidak lagi ditentukan oleh kekayaan sumberdaya alam yang dimilikinya, tetapi lebih ditentukan oleh kualitas sumberdaya manusia, penguasaan informasi, serta penguasaan ilmu pengetahuan dan teknologi.</p>
                        <p style="box-sizing: border-box; margin: 0px 0px 1.25em; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; text-rendering: optimizelegibility; color: rgb(38, 38, 38); text-align: justify;">Berkaitan dengan persoalan di atas, eksistensi Universitas Muhammadiyah Sumatera Utara kedepan ditentukan oleh kemampuannya untuk memenuhi tuntutan kebutuhan-kebutuhan tersebut. Untuk memenuhi tuntutan-tuntutan tersebut, Universitas Muhammadiyah Sumatera Utara perlu secara terus-menerus mempertinggi daya saing dan daya juang guna mencapai keunggulan kompetitif berkelanjutan berdasarkan landasan filosofi dan pemikiran di atas, Universitas Muhammadiyah Sumatera Utara merumuskan visi, misi dan tujuan penyelenggaraan dan pengembangan sebagai berikut.</p>
                        <p style="box-sizing: border-box; margin: 0px 0px 1.25em; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; text-rendering: optimizelegibility; color: rgb(38, 38, 38);"><strong style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">Visi</strong></p>
                        <p style="box-sizing: border-box; margin: 0px 0px 1.25em; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; text-rendering: optimizelegibility; color: rgb(38, 38, 38);">Menjadi Perguruan Tinggi yang unggul dalam membangun peradaban bangsa dengan mengembangkan ilmu pengetahuan, teknologi dan Sumber Daya manusia berdasarkan Al-Islam dan Kemuhammadiyahan</p>
                        <p style="box-sizing: border-box; margin: 0px 0px 1.25em; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; text-rendering: optimizelegibility; color: rgb(38, 38, 38);"><strong style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">Misi</strong></p>
                        <p style="box-sizing: border-box; margin: 0px 0px 1.25em; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; text-rendering: optimizelegibility; color: rgb(38, 38, 38);">Untuk mewujudkan visinya, Universitas Muhammadiyah Sumatera Utara memiliki misi sebagai berikut:</p>
                        <ol>
                            <li style="box-sizing: border-box; margin-top: 0px; margin-right: 0px; margin-bottom: 1.25em; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; text-rendering: optimizelegibility; color: rgb(38, 38, 38);">Menyelenggarakan pendidikan dan pengajaran berdasarkan Al-Islam dan Kemuhammadiyahan.</li>
                            <li style="box-sizing: border-box; margin-top: 0px; margin-right: 0px; margin-bottom: 1.25em; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; text-rendering: optimizelegibility; color: rgb(38, 38, 38);">Menyelenggarakan penelitian, pengembangan ilmu pengetahuan dan teknologi berdasarkan Al-Islam dan Kemuhammadiyahan.</li>
                            <li style="box-sizing: border-box; margin-top: 0px; margin-right: 0px; margin-bottom: 1.25em; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; text-rendering: optimizelegibility; color: rgb(38, 38, 38);">Melakukan pengabdian kepada masyarakat melalui pemberdayaan dan pengembangan kehidupan masyarakat berdasarkan Al-Islam dan Kemuhammadiyahan.</li>
                        </ol>
                        <p style="box-sizing: border-box; margin: 0px 0px 1.25em; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; text-rendering: optimizelegibility; color: rgb(38, 38, 38);"><strong style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">Tujuan</strong></p>
                        <p style="box-sizing: border-box; margin: 0px 0px 1.25em; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; text-rendering: optimizelegibility; color: rgb(38, 38, 38);">Untuk mewujudkan visi dan misi, tujuan yang ingin dicapai adalah:</p>
                        <ol>
                            <li style="box-sizing: border-box; margin-top: 0px; margin-right: 0px; margin-bottom: 1.25em; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; text-rendering: optimizelegibility; color: rgb(38, 38, 38);">Menghasilkan lulusan yang professional, kreatif, inovatif, mandiri dan bertanggungjawab.</li>
                            <li style="box-sizing: border-box; margin-top: 0px; margin-right: 0px; margin-bottom: 1.25em; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; text-rendering: optimizelegibility; color: rgb(38, 38, 38);">Mewujudkan manajemen perguruan tinggi yang efektif, efisien, transparan, akuntabel dan sustainabel.</li>
                            <li style="box-sizing: border-box; margin-top: 0px; margin-right: 0px; margin-bottom: 1.25em; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; text-rendering: optimizelegibility; color: rgb(38, 38, 38);">Menghasilkan sumber daya manusia yang handal dibidang penelitian.</li>
                            <li style="box-sizing: border-box; margin-top: 0px; margin-right: 0px; margin-bottom: 1.25em; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; text-rendering: optimizelegibility; color: rgb(38, 38, 38);">&nbsp;Menghasilkan karya ilmiah berskala nasional dan internasional yang bermanfaat untuk pengembangan ilmu pengetahuan dan teknologi.</li>
                            <li style="box-sizing: border-box; margin-top: 0px; margin-right: 0px; margin-bottom: 1.25em; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; text-rendering: optimizelegibility; color: rgb(38, 38, 38);">&nbsp;Mewujudkan jaringan kerjasama dengan berbagai institusi nasional maupun internasional.</li>
                            <li style="box-sizing: border-box; margin-top: 0px; margin-right: 0px; margin-bottom: 1.25em; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; text-rendering: optimizelegibility; color: rgb(38, 38, 38);">Membantu mewujudkan masyarakat yang berkualitas dan mandiri.</li>
                        </ol>
                        <p style="box-sizing: border-box; margin: 0px 0px 1.25em; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; text-rendering: optimizelegibility; color: rgb(38, 38, 38);"><strong style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: 700; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline;">Tugas dan Fungsi</strong></p>
                        <ol>
                            <li style="box-sizing: border-box; margin-top: 0px; margin-right: 0px; margin-bottom: 1.25em; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; text-rendering: optimizelegibility; color: rgb(38, 38, 38);">Universitas bertugas menyelenggarakan pembinaan ketaqwaan dan keimanan kepada Allah Swt, pendidikan dan pengajaran, penelitian dan pengabdian kepada masyarakat serta mengembangkan ilmu pengetahuan dan teknologi menurut tuntunan Islam.</li>
                            <li style="box-sizing: border-box; margin-top: 0px; margin-right: 0px; margin-bottom: 1.25em; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; text-rendering: optimizelegibility; color: rgb(38, 38, 38);">Universitas berfungsi mengelola sumber daya pendidikan yang mencakup pembinaan keimanan dan ketaqwaan kepada Allah swt, pendidikan dan pengajaran, penelitian, pengabdian kepada masyarakat, serta menyusun dan melaksanakan kebijaksanaan teknis akademis yang tunduk dan bertanggung jawab kepada Majelis Dikti sesuai dengan ketentuan yang berlaku.</li>
                        </ol>
                    </div>
                </div>


            </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">

        </div>
        <!-- /.card-footer-->
</div>
<!-- /.card -->

</section>
<!-- /.content -->

</div>