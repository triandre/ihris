<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2/css/select2.min.css">
<link rel="stylesheet"
    href="<?= base_url() ?>assets/vendor/backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $title; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Report Presensi</a></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>
    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title"><?= $title; ?></h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip"
                        title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <?php if ($this->session->flashdata('gagal_store')) { ?>

                <?= $this->session->flashdata('gagal_store') ?>


                <?php } ?>

                <form action="<?= base_url('presensi/c_invateDosen'); ?>" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <div class="form-group row">

                            <div class="col-sm-8">
                                <label>Cari Dosen</label>
                                <select id="nidn" name="nidn" class="form-control selectx" required>
                                    <option selected disabled value="">Pilih</option>
                                    <?php foreach ($dosen as $row2) : ?>
                                    <option value="<?= $row2['nidn']; ?>"><?= $row2['nidn']; ?> - <?= $row2['name']; ?>, <?= $row2['gelar']; ?>
                                    </option>
                                    <?php endforeach ?>
                                </select>
                                <?= form_error('nidn', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Tambah
                        Dosen!</button>
                </form>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->


</div>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/select2/js/select2.full.min.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'excel', 'pdf'
        ]
    });
});


$(document).ready(function() {
    $('.selectx').select2({
        placeholder: "Pilih..",
        allowClear: true,
        theme: 'bootstrap4'
    });
});
</script>