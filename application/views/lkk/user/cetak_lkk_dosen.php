<?php

$mpdf = new \Mpdf\Mpdf([
    'mode' => 'utf-8',
    'format' => [210, 330],
    'orientation' => 'P'
]);

$isi =
    '<!DOCTYPE html>
<html>
<head>
    <title>Surat Pengantar </title>
    <style>@page {
        margin-top: 10px;
       }</style>
</head>

<body>

<h4 align="center">SURAT PERMOHONAN <br> BANTUAN SOSIAL DAN KESEHATAN <br> LEMBAGA KESEJAHTERAAN KARYAWAN <br> UNIVERSITAS MUHAMMADIYAH SUMATERA UTARA</h4>
<hr>
<p align="right">Medan, ' . date("d F Y") . '</p>
<p> Kepada Yth. <br> Bapak Ketua LKK UMSU <br> di <br> Medan</p>
<img src="assets/images/ass.png" style="width:35%">
<p>Yang bertanda tangan di bawah ini <i>(Pimpinan Unit Kerja)</i>:</p>
<table>
<tbody>
<tr> 
<td>Nama</td>
<td>:</td>
<td>.......................................</td>
</tr>
<tr> 
<td>Jabatan</td>
<td>:</td>
<td>.......................................</td>
</tr>

</tbody>
</table>
<p>Memohon Kepada Bapak untuk dapat kiranya memberikan bantuan sosial dan kesehatan Atas: <br>
<table>
<tbody>
<tr> 
<td>NPP</td>
<td>:</td>
<td>' . $d["npp"] . '</td>
</tr>
<tr> 
<td>Nama</td>
<td>:</td>
<td>' . $d["name"] . '</td>
</tr>
<tr>
<td>Status Kerja</td>
<td>:</td>
<td>Dosen</td>
</tr>
</tbody>
</table>
<br>
</p>
<table style="border: 1px solid; width: 100%; border-collapse: collapse;">
<tbody>
<tr> 
<th style="border: 1px solid;">No</th>
<th style="border: 1px solid;">Jenis Bantuan Dan Sosial</th>
<th style="border: 1px solid;">Rp.</th>
<th style="border: 1px solid;">Terbilang</th>
</tr>
<tr> 
<td align="center" style="border: 1px solid;">1</td>
<td align="center" style="border: 1px solid;">' . $d["sts_keluarga"] . '</td>
<td align="center" style="border: 1px solid;">' . number_format($d["nominal"]) . '</td>
<td align="center" style="border: 1px solid;">' . strtoupper(terbilang($d["nominal"])) . ' RUPIAH</td>
</tr>

</tr>
</tbody>
</table>
<p>Demikian permohonan ini diperbuat atas bantuan dan kebijaksanaan bapak diucapkan terimakasih. Akhirnya semoga selamatkan kita semua. Amiin</p>
<p align="center">
<img src="assets/images/wass.png" style="width:35%">
</p>
<table style="width:100%">
<tbody>
<tr> 
<td align="center">Diketahui oleh:<br>Pengurus LKK UMSU<br><img src="assets/ttd/ttd_aswin.png" style="width:130px;"><br> <b>Dr. Aswin Bancin, S.E., M.Pd</b></td>
<td align="center">Pemohon  <br> Pimpinan Unit Kerja <br><br><br><br>.....................................</td>
</tr>
</tbody>
</table>
<table style="width:100%">
<tbody>
<tr> 
<td align="center">Disetujui oleh <br> Ketua LKK <br><img src="assets/ttd/wr2.png" style="width:130px;"><br> <b>Prof. Dr. Akrim, M.Pd</b></td>
</tr>
</tbody>
</table>
<hr>
<b>Catatan Ketua LKK : ' . $d["catatan_pimpinan"] . ' </b>
<hr>
<p>* Coret yang tidak perlu <br><b>CP LKK: MUHARANI - HP: 0812-644-5822</b></p>
</body>
</html>';
$mpdf->WriteHTML($isi);
$mpdf->Output();
