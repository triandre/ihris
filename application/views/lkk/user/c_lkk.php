<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $title; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('tendik') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Permohonan</a></li>
                        <li class="breadcrumb-item active">Bantuan LKK</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>

    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title">Permohonan Bantuan Sosial & Kesehatan</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-danger col-md-12 alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?= validation_errors(); ?>
                    </div>
                <?php endif ?>


                <form class="form-horizontal" action="<?= base_url('lkk/createBantuan'); ?>" enctype="multipart/form-data" autocomplete="off" method="post">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tbody>

                                    <tr>
                                        <td>Email <span style="color:red;">*</span> </td>
                                        <td><input readonly class="form-control" type="text" id="email" name="email" value="<?php echo $this->session->userdata('email'); ?>" required readonly>
                                            <?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?></td>
                                    </tr>

                                    <tr>
                                        <td>Jenis Bantuan <span style="color:red;">*</span> </td>
                                        <td>
                                            <select class="form-control selectz" id="bantuan_id" name="bantuan_id" required>
                                                <option selected disabled value="">Pilih</option>
                                                <?php foreach ($bansos as $row3) : ?>
                                                    <option value="<?= $row3['id_bantuan']; ?>"><?= $row3['jenis_bantuan']; ?></option>
                                                <?php endforeach ?>
                                            </select>
                                            <?= form_error('bantuan_id', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Nama Anggota/Keluarga <span style="color:red;">*</span> </td>
                                        <td><input class="form-control" type="text" id="nama_keluarga" name="nama_keluarga" value="<?= set_value('nama_keluarga'); ?>" required>
                                            <?= form_error('nama_keluarga', '<small class="text-danger pl-3">', '</small>'); ?></td>
                                    </tr>

                                    <tr>
                                        <td>Status Keluarga <span style="color:red;">*</span> </td>
                                        <td>
                                            <select class="form-control selectz" id="sts_keluarga" name="sts_keluarga" required>
                                                <option selected disabled value="">Pilih</option>
                                                <option value="Anggota Sakit">Anggota Sakit</option>
                                                <option value="Istri/Suami Sakit">Istri/Suami Sakit</option>
                                                <option value="Anak Kandung Sakit">Anak Kandung</option>
                                                <option value="Santunan Duka (Anggota Meninggal)">Santunan Duka (Anggota Meninggal)</option>
                                                <option value="Santunan Duka (Suami/istri,Anak,Orangtua,Mertua)">Santunan Duka (Suami/istri,Anak,Orangtua,Mertua)</option>
                                                <option value="Santunan Duka (Anggota Pensiunan Meninggal)">Santunan Duka (Anggota Pensiunan Meninggal)</option>
                                            </select>
                                            <?= form_error('sts_keluarga', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </td>
                                    </tr>


                                    <tr>
                                        <td>Jenis Kelamin <span style="color:red;">*</span> </td>
                                        <td>
                                            <select class="form-control selectz" id="jk_keluarga" name="jk_keluarga" required>
                                                <option selected disabled value="">Pilih</option>
                                                <option value="Laki-laki">Laki-laki</option>
                                                <option value="Perempuan">Perempuan</option>

                                            </select>
                                            <?= form_error('jk_keluarga', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </td>
                                    </tr>



                                    <tr>
                                        <td>Tanggal (Sakit/Melahirkan/Meninggal) <span style="color:red;">*</span> </td>
                                        <td><input type="date" class="form-control" id="tgl_h" name="tgl_h" value="<?= set_value('tgl_h'); ?>" required>
                                            <?= form_error('tgl_h', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </td>
                                    </tr>


                                    <tr>
                                        <td>Lampiran Surat (Sakit/Melahirkan/Kematian)<span style="color:red;">*</span> </td>
                                        <td><input type="file" class="form-control" id="file_lkk" name="file_lkk" required>
                                            <?= form_error('file_lkk', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>


                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <a href="javascript:history.back()" class="btn btn-danger"> <i class="fa fa-backward"></i> Kembali</a>
                        <button type="submit" class="btn btn-primary"> <i class="fa fa-save"></i> Simpan</button>
                    </div>
                    <!-- /.card-footer -->
                </form>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">

            </div>
            <!-- /.card-footer-->
        </div>

    </section>
    <!-- /.content -->

</div>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
    $(function() {
        $("#example1").DataTable({
            "language": {
                "sSearch": "Cari"
            }
        });
    });
</script>