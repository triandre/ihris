<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $title; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('tendik') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Permohonan</a></li>
                        <li class="breadcrumb-item active">Laporan Bantuan LKK</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">

            <!-- Default box -->
            <div class="card">
                <div class="card-header">
                    <?php if ($this->session->flashdata('gagal_store')) { ?>
                        <div class="alert alert-danger col-md-12">
                            <?= $this->session->flashdata('gagal_store') ?>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <?php } ?>

                    <?= form_error('username', '<div class="alert alert-danger" role="alert">', '</div>') ?>
                    <?= form_error('password', '<div class="alert alert-danger" role="alert">', '</div>') ?>


                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fas fa-minus"></i></button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                            <i class="fas fa-times"></i></button>
                    </div>
                </div>
                <div class="card-body">
                    <form class="form-horizontal" action="<?= base_url('verif_lkk/laporanGo'); ?>" method="post">
                        <div class="card-body">
                            <div class="table-responsive">
                                <div class="form-group">
                                    <label for="pwd">Dari Tanggal:</label>
                                    <div class="col-md-4">
                                        <input type="datetime-local" class="form-control" id="start" name="start" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="pwd">Sampai Tanggal:</label>
                                    <div class="col-md-4">
                                        <input type="datetime-local" class="form-control" id="end" name="end" required>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary"> <i class="fa fa-save"></i> Cetak</button>

                    </form>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">

                </div>
                <!-- /.card-footer-->
            </div>
            <!-- /.card -->

    </section>
    <!-- /.content -->

</div>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
    $(function() {
        $("#example1").DataTable({
            "language": {
                "sSearch": "Cari"
            }
        });
    });
</script>