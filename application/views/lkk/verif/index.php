<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Dashboard</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('pimpinan1') ?>">Dashboard</a></li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <?php
    $tahun = date('Y');
    $bansos_proses = $this->db->query("SELECT * FROM occ_bantuan_lkk where aktif_lkk='1' AND tahun='$tahun' AND status_lkk BETWEEN 1 AND 2");
    $bansos_disetujui = $this->db->query("SELECT * FROM occ_bantuan_lkk where status_lkk='3' AND aktif_lkk='1' AND tahun='$tahun'");
    $bansos_ditolak = $this->db->query("SELECT * FROM occ_bantuan_lkk where status_lkk='4' AND aktif_lkk='1' AND tahun='$tahun'");
    $bansos_cair = $this->db->query("SELECT * FROM occ_bantuan_lkk where status_lkk='3' AND sts_ambil='1' AND aktif_lkk='1' AND tahun='$tahun'");
    ?>
    <!-- /.content-header -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3><?php echo $bansos_proses->num_rows(); ?></h3>

                            <p>Permohonan Masuk/Proses</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="<?= base_url('verif_lkk/vmasuk'); ?>" class="small-box-footer">Lihat Data!<i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-success">
                        <div class="inner">
                            <h3><?php echo $bansos_disetujui->num_rows(); ?></h3>

                            <p>Permohonan Disetujui</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="<?= base_url('verif_lkk/vdisetujui'); ?>" class="small-box-footer">Lihat Data!<i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-danger">
                        <div class="inner">
                            <h3><?php echo $bansos_ditolak->num_rows(); ?></h3>

                            <p>Permohonan Ditolak</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="<?= base_url('verif_lkk/vditolak'); ?>" class="small-box-footer">Lihat Data!<i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-warning">
                        <div class="inner">
                            <h3><?php echo $bansos_cair->num_rows(); ?></h3>

                            <p>Permohonan Pencairan</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="<?= base_url('verif_lkk/vdisetujui'); ?>" class="small-box-footer">Lihat Data!<i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>

            </div>

        </div>
    </section>
    <!-- Main content -->

</div>
<!-- /.content-wrapper -->
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
    $(function() {
        $("#example1").DataTable({
            "language": {
                "sSearch": "Cari"
            },
            pageLength: 5,
        });
    });
</script>