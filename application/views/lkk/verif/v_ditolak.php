<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $title; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('tendik') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Permohonan</a></li>
                        <li class="breadcrumb-item active">Bantuan LKK</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">

            <!-- Default box -->
            <div class="card">
                <div class="card-header">
                    <?php if ($this->session->flashdata('gagal_store')) { ?>
                        <div class="alert alert-danger col-md-12">
                            <?= $this->session->flashdata('gagal_store') ?>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <?php } ?>

                    <?= form_error('username', '<div class="alert alert-danger" role="alert">', '</div>') ?>
                    <?= form_error('password', '<div class="alert alert-danger" role="alert">', '</div>') ?>


                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fas fa-minus"></i></button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                            <i class="fas fa-times"></i></button>
                    </div>
                </div>
                <div class="card-body">
                    <?= $this->session->flashdata('message'); ?>
                    <div class="table-responsive">
                        <table id="example1" class="table table-bordered table-striped table-sm">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Permohonan Bantuan LKK</th>
                                    <th>Jenis Bantuan</th>
                                    <th>Status</th>

                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1;
                                foreach ($bansos as $bans) : ?>
                                    <tr>
                                        <td><?= $i; ?></td>
                                        <td>
                                            <a href="<?= base_url('detail/pegawai/'); ?><?= $bans['nidn']; ?>"> <?= $bans['name']; ?></a>
                                            <br>
                                            <b>Nama Keluarga : <?= $bans['nama_keluarga']; ?> </b>
                                            <br>
                                            <b>Status Keluarga : <?= $bans['sts_keluarga']; ?> - <?= $bans['jk_keluarga']; ?> </b>
                                            <br>
                                            <b>Lampiran : <a href="<?= base_url('lkk/'); ?><?= $bans['file_lkk']; ?>" target="_blank"> Lihat </a></b>
                                            <br>
                                            Tanggal : <?= date('d F Y', strtotime($bans['tgl_h'])); ?>
                                            <br>
                                            <?php if ($bans['status_lkk'] == '4') { ?>
                                                Catatan Validator : <?= $bans['catatan_verif']; ?>
                                            <?php } ?>

                                        </td>
                                        <td><?= $bans['sts_keluarga']; ?></td>
                                        <td> <?php
                                                if ($bans['status_lkk'] == 1) {
                                                    echo '<span class="badge badge-primary">Proses LKK</span>';
                                                } elseif ($bans['status_lkk'] == 2) {
                                                    echo '<span class="badge badge-info">Proses Pimpinan</span>';
                                                } elseif ($bans['status_lkk'] == 3 and $bans['sts_ambil'] == 1) {
                                                    echo '<span class="badge badge-warning">Permohonan Disetujui</span>';
                                                } elseif ($bans['status_lkk'] == 3 and $bans['sts_ambil'] == 0) {
                                                    echo '<span class="badge badge-success">Bantuan Sudah Diterima</span>';
                                                } elseif ($bans['status_lkk'] == 4) {
                                                    echo '<span class="badge badge-danger">Permohonan Ditolak</span>';
                                                }
                                                ?></td>




                                    </tr>

                                    <?php $i++; ?>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">

                </div>
                <!-- /.card-footer-->
            </div>
            <!-- /.card -->

    </section>
    <!-- /.content -->

</div>
<?php $i = 1;
foreach ($bansos as $bans) : ?>
    <!-- The Modal -->
    <div class="modal fade" id="myModal<?php echo $bans['reff_lkk'] ?>" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Tolak Permohonan</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <form action="<?= base_url('verif_lkk/tolakBantuan') ?>" method="post">
                        <label>REFF ID</label>
                        <input class="form-control" type="text" id="reff_lkk" name="reff_lkk" value="<?= $bans['reff_lkk']; ?>" required readonly>
                        <label>Catatan Penolakan</label>
                        <textarea class="form-control" type="text" id="catatan_verif" name="catatan_verif" rows="5" required></textarea>

                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                    <button type="submit" class="btn btn-success"> <i class="fa fa-save"></i> Kirim</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <?php $i++; ?>
<?php endforeach; ?>

<?php $i = 1;
foreach ($bansos as $bans) : ?>
    <!-- The Modal -->
    <div class="modal fade" id="myProses<?php echo $bans['reff_lkk'] ?>" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Proses Permohonan</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <form action="<?= base_url('verif_lkk/prosesBantuan') ?>" method="post">
                        <label>REFF ID</label>
                        <input class="form-control" type="text" id="reff_lkk" name="reff_lkk" value="<?= $bans['reff_lkk']; ?>" required readonly>
                        <label>Catatan Proses Ke Pimpinan</label>
                        <textarea class="form-control" type="text" id="catatan_pimpinan" name="catatan_pimpinan" rows="5" required></textarea>

                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                    <button type="submit" class="btn btn-success"> <i class="fa fa-save"></i> Kirim</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <?php $i++; ?>
<?php endforeach; ?>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
    $(function() {
        $("#example1").DataTable({
            "language": {
                "sSearch": "Cari"
            }
        });
    });
</script>