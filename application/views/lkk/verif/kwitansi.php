<?php

$mpdf = new \Mpdf\Mpdf([
  'mode' => 'utf-8',
  'format' => [210, 140],
  'orientation' => 'P'
]);

$isi =
  '<!DOCTYPE html>
<html>
<head>
    <title>FAKTUR PENERIMAAN </title>
    <style>@page {
        margin-top: 10px;
        margin-bottom: 5px;
        margin-left: 50px;
        margin-right: 25px;
       }</style>

       <style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid;
  text-align: left;
}


</style>
</head>

<body>

<table>
<tbody>
<tr>
<th style="width:30px;border:0"><img src="assets/images/logo/icon.png" style="width:80px"></th>
<th style="border:0">

<h4>LEMBAGA KEUANGAN MIKRO SYARIAH <br> BAITUL MAAL WAT TAMWIL <br> LEMBAGA KESEJAHTERAAN KARYAWAN</h4>
<small>UNIVERSITAS MUHAMMADIYAH SUMATERA UTARA</small>
</th>

</tbody>
</table> 
<hr>

<table style="font-size:14px;">
<tbody>

<tr>
<td style="border:0; width:50%">

<table style="font-size:14px;">
<tbody>
<tr>
<td style="border:0; width:150px">Dibayarkan Kepada</td>
<td style="border:0;width:10px">:</td>
<td style="border:0">' . $d["name"] . '</td>
</tr>

<tr>
<td style="border:0;">Bantuan</td>
<td style="border:0">:</td>
<td style="border:0"> ' . $d["sts_keluarga"] . '</td>
</tr>

</tbody>
</table> 

</td>

<td align="center" style="border:0; width:50%">
<h3>KWITANSI BUKTI PENERIMAAN</h3>
<h1>' . date("Ym") . '' . sprintf("%05d", $d['id_lkk']) . '</h1>
</td>
</tr>

</tbody>
</table> 

<table style="width:100%;font-size:14px;">
<tbody>
<tr>
<td style="text-align: center; width:5%"><strong>No</strong></td>
<td style="text-align: center; width:60%"><strong>Rincian</strong></td>
<td style="text-align: center; width:30%"><strong>Jumlah</strong></td>
</tr>
<tr>
<td style="text-align: center;">1.</td>
<td> Bantuan Sosial & Kesehatan A/N ' . $d["nama_keluarga"] . ' - ' . $d["jk_keluarga"] . ' <br>  ' . $d["sts_keluarga"] . ' Dari  ' . $d["name"] . '</td>
<td style="text-align: right;">' . number_format($d["nominal"], 2, ",", ".") . '</td>
</tr>
<tr>
<th style="text-align:right" colspan="2">Jumlah Rp.</th>
<th style="text-align: right;">' . number_format($d["nominal"], 2, ",", ".") . '</th>
</tr>
</tbody>
</table>';

$bawah = '
<table style="width:100%;font-size:14px;">
<tbody>
<tr>
<td style="width:10%"><i>Terbilang : </i></td>
<td style="background-color: #dddddd; width:90%" colspan="3"><small><b>#' . strtoupper(terbilang($d["nominal"])) . '  RUPIAH#</small></b></td>
</tr>
</tbody>

</table> 
<br>
<table style="width:100%;font-size:14px;">
<tbody>
<tr>
<th style="text-align: center; width:500px" colspan="3"><strong>Mengetahui Dan Menyetujui</strong></th>

</tr>
<tr>
<td style="text-align: center;width:25%"><small>Disetujui oleh:<br></small></td>
<td style="text-align: center;width:25%"><small>Dibukukan oleh:<br></small></td>
<td style="text-align: center;width:25%"><small>Diterima oleh:<br></small></td>
</tr>

<tr>
<td><center><small><br><br><br>Prof. Dr. Akrim, M.Pd</small></center></td>
<td><center><small><br><br><br>Muharani</small></center></td> 
<td><center><small>Medan, ' . date('d F Y') . ' <br> 
Diterima Oleh: <br><br>
' . $d["name"] . '</small></center></td>
</tr>

</tbody>
</table>

<table style="width:100%;font-size:14px;">
<tbody>
<tr>
<td style="border:0"><small><i>Hal:1</i></small></td>
<td style="text-align: right;border:0"><small><i>' . date("Y-m-d h:i:s") . '</i></small></td>

</tr>
</tbody>
</table>
</body>
</html>';
$mpdf->WriteHTML($isi);
$mpdf->setFooter($bawah);
$mpdf->Output();
