<?php

$mpdf = new \Mpdf\Mpdf([
    'mode' => 'utf-8',
    'format' => [210, 297],
    'orientation' => 'P'
]);

$isi =
    '<!DOCTYPE html>
<html>
<head>
    <title>FAKTUR PENERIMAAN </title>
    <style>@page {
        margin-top: 10px;
        margin-bottom: 5px;
        margin-left: 50px;
        margin-right: 25px;
       }</style>

       <style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid;
  text-align: left;
}


</style>
</head>

<body>

<table>
<tbody>
<tr>
<th style="width:30px;border:0"><img src="assets/images/logo/icon.png" style="width:80px"></th>
<th style="border:0">

<h4>LEMBAGA KEUANGAN MIKRO SYARIAH <br> BAITUL MAAL WAT TAMWIL <br> LEMBAGA KESEJAHTERAAN KARYAWAN</h4>
<small>UNIVERSITAS MUHAMMADIYAH SUMATERA UTARA</small>
</th>
</tbody>
</table> 
<hr>
<h3>Laporan Bantuan Sosial Dan Kesehatan Kepagawaian UMSU <br> Tanggal: ' . date('Y-m-d', strtotime($dari)) . ' S/d ' . date('Y-m-d', strtotime($sampai)) . '</h3>

<table style="width:100%;font-size:14px;">

<thead>
<tr>
<td style="text-align: center; width:5%"><strong>No</strong></td>
<td style="text-align: center; width:15%"><strong>Nama Pegawai</strong></td>
<td style="text-align: center; width:15%"><strong>Kepegawaian</strong></td>
<td style="text-align: center; width:20%"><strong>Nama Keluarga</strong></td>
<td style="text-align: center; width:30%"><strong>Jenis Bantuan</strong></td>
<td style="text-align: center; width:15%"><strong>Nominal</strong></td>
</tr>
</thead>
<tbody>';
$i = 1;
foreach ($bansos as $gs) :

    $isi .= '
<tr>
<td align="center">' . $i . '</td>
<td>' . $gs["name"] . '</td>';
    if ($gs["role_id"] == 2) {
        $isi .= ' <td>Dosen</td> ';
    } elseif ($gs["role_id"] == 1) {
        $isi .= ' <td>Tendik</td> ';
    }
    $isi .= ' <td>' . $gs["nama_keluarga"] . '</td>
<td>' . $gs["sts_keluarga"] . '</td>
<td align="right">' . number_format($gs['nominal']) . '</td>
</tr>';
    $i++;
endforeach;

$isi .= '
<tr>
<td colspan="5"><b>TOTAL KESELURUHAN</b></td>
<td align="right"><b>' . number_format($total["totalSemua"]) . '</b></td>
</tr>
</tbody>
</table>
<br>
<br>
<br>
<table style="border:0">
<tbody>

<tr>
<th style="width:50%; border:0"></th>
<td style="border:0" align="center">
Ketua LKK<br>
<br><br><br><br>
<b><u>Prof. Dr. Akrim, M.Pd</u></b><br>
NIDN. 0122127902

</td>
</tr>
</tbody>
</table>
</body>
</html>';
$mpdf->WriteHTML($isi);
$mpdf->Output();
