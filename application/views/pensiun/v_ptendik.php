<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $title; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Data Pegawai</a></li>
                        <li class="breadcrumb-item active">Masa Pensiun</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>
    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title"><?= $title; ?></h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">

                <form action="<?= base_url('masaPensiun/search_pegawai') ?>" method="post">
                    <div class="row">
                        <div class="col-8">
                            <select id="status_pegawai" name="status_pegawai" class="form-control selecta" required>
                                <option value="">- Pilih --</option>
                                <option value="1">Tendik</option>
                                <option value="2">Dosen</option>
                            </select>
                        </div>

                        <div class="col">
                            <button type="submit" class="btn btn-block btn-outline-primary">Cari</button>
                        </div>
                        <div class="col">
                            <button type="reset" class="btn btn-block btn-outline-danger">Reset</button>
                        </div>
                    </div>
                </form>

            </div>
            <!-- /.card-footer-->
        </div>
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title"><?= $title; ?></h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-danger col-md-8 alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?= validation_errors(); ?>
                    </div>
                <?php endif ?>
                <button type="button" class="btn btn-danger mt-4" disabled>
                    <i class="fa fa-print" aria-hidden="true"></i> Print
                </button>
                <button type="button" class="btn btn-success mt-4" disabled>
                    <i class="fa fa-file" aria-hidden="true"></i> Export Excel
                </button>
                <br><br>
                <div class="table-responsive">
                    <table id="example1" class="table table-bordered table-striped table-sm">
                        <thead>
                            <tr>
                                <th>NO.</th>
                                <th>Nama</th>
                                <th>Unit Kerja</th>
                                <th>Tanggal Lahir</th>
                                <th>Usia</th>
                                <th>#</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            <?php foreach ($view as $v) : ?>
                                <tr>
                                    <td align="center"><?= $i; ?></td>
                                    <td><?= $v['name']; ?></td>
                                    <td><?= $v['nickname_unit']; ?></td>
                                    <td><?= date('d F Y',  strtotime($v['tgl_lahir'])); ?> </td>
                                    <td><?php $lahir    = new DateTime($v['tgl_lahir']);
                                        $today        = new DateTime();
                                        $umur = $today->diff($lahir);
                                        echo $umur->y;
                                        echo " Tahun ";
                                        ?></td>
                                    <td></td>
                                </tr>
                                <?php $i++; ?>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


        <!-- /.card -->
    </section>
    <!-- /.content -->





</div>
<!-- /.content-wrapper -->
<!-- Select2 -->
<script src="<?= base_url() ?>assets/vendor/backend/plugins/select2/js/select2.full.min.js"></script>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/select2/js/select2.full.min.js"></script>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
    $(function() {
        $("#example1").DataTable({
            "language": {
                "sSearch": "Cari"
            }
        });
    });
</script>
<script>
    $(document).ready(function() {
        $('.selecta').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });
</script>