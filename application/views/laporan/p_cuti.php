<?php

$mpdf = new \Mpdf\Mpdf([
    'mode' => 'utf-8',
    'format' => [210, 330],
    'orientation' => 'L'
]);

$isi = '<!DOCTYPE html>
<html>
<head>
    <title>Laporan Cuti </title>
</head>
<style>
table {
  font-family: arial, sans-serif;
  font-size: 10px;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>

<body>
<h1 align="center">Laporan Cuti Kepegawaian</h1>
<p>Dari Tanggal :' . date('d F Y', strtotime($tgl_mulai)) . ' s/d ' . date('d F Y', strtotime($tgl_selesai)) . '</p>
<table>
<thead>
<tr>
<th>No</th>
<th>NAMA</th>
<th>JENIS</th>
<th>CUTI DI AMBIL</th>
<th>ALASAN</th>
<th>TGL CUTI</th>
<th>TGL SELESAI</th>
<th>TGL MASUK</th>
</tr>
</thead>
<tbody>';

$i = 1;
foreach ($lap_cuti as $gs) :
    $isi .= '<tr>
<td>' . $i . '</td>
<td>' . $gs["name"] . '</td>
<td>' . $gs["jenisCuti"] . '</td>
<td>' . $gs["cutiDiambil"] . '</td>
<td>' . $gs["alasan"] . '</td>
<td>' . $gs["tglCuti"] . '</td>
<td>' . $gs["tglSelesaiCuti"] . '</td>
<td>' . $gs["tglMasuk"] . '</td>
</tr>';

    $i++;
endforeach;
$isi .= '
</tbody>
</table> 
</body>
</html>';

$mpdf->WriteHTML($isi);
$mpdf->Output();
