<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url() ?>src/backend/plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="<?= base_url() ?>src/backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Laporan</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('home') ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Laporan</a></li>
                        <li class="breadcrumb-item active">Izin Sakit</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">
                    Laporan Izin Sakit
                </h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i>
                    </button>
                </div>
            </div>
            <div class="card-body">
                <form action="<?= base_url('laporan/sakitGo') ?>" method="post">
                    <div class="row">
                        <div class="col">
                            <label for="">Tanggal Mulai</label>
                        </div>
                        <div class="col-4">
                            <input type="date" id="date_start" name="date_start" class="form-control" placeholder="Tanggal Mulai" required>
                        </div>
                        <div class="col">
                            <label for="">Tanggal Selesai</label>
                        </div>
                        <div class="col-4">
                            <input type="date" id="date_end" name="date_end" class="form-control" placeholder="Tanggal Selesai" required>
                        </div>
                        <div class="col">
                            <button type="submit" class="btn btn-block btn-outline-primary">Print</button>
                        </div>
                        <div class="col">
                            <button class="btn btn-block btn-outline-danger">Reset</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
</div>
<script src="<?= base_url() ?>src/backend/plugins/select2/js/select2.full.min.js"></script>
<script>
    $('.selecta').select2({
        placeholder: "Pilih..",
        allowClear: true,
        theme: 'bootstrap4'
    });
</script>

<script src="<?= base_url() ?>src/backend/plugins/select2/js/select2.full.min.js"></script>
<script>
    $('.selecta').select2({
        placeholder: "Pilih..",
        allowClear: true,
        theme: 'bootstrap4'
    });
</script>