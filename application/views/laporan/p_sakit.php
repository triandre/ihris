<?php

$mpdf = new \Mpdf\Mpdf([
    'mode' => 'utf-8',
    'format' => [210, 330],
    'orientation' => 'L'
]);

$isi = '<!DOCTYPE html>
<html>
<head>
    <title>Laporan Cuti </title>
</head>
<style>
table {
  font-family: arial, sans-serif;
  font-size: 10px;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>

<body>
<h1 align="center">Laporan Izin Sakit Kepegawaian</h1>
<p>Dari Tanggal :' . date('d F Y', strtotime($tgl_mulai)) . ' s/d ' . date('d F Y', strtotime($tgl_selesai)) . '</p>
<table>
<thead>
<tr>
<th>No</th>
<th>NAMA</th>
<th>TIDAK HADIR</th>
<th>ALASAN</th>
<th>DARI TGL </th>
<th>SAMPAI TGL</th>
<th>TGL MASUK</th>
</tr>
</thead>
<tbody>';

$i = 1;
foreach ($lap_cuti as $gs) :
    $isi .= '<tr>
<td>' . $i . '</td>
<td>' . $gs["name"] . '</td>
<td>' . $gs["tidakHadir"] . '</td>
<td>' . $gs["ket"] . '</td>
<td>' . $gs["dariTanggal"] . '</td>
<td>' . $gs["sampaiTanggal"] . '</td>
<td>' . $gs["tglMasuk"] . '</td>
</tr>';

    $i++;
endforeach;
$isi .= '
</tbody>
</table> 
</body>
</html>';

$mpdf->WriteHTML($isi);
$mpdf->Output();
