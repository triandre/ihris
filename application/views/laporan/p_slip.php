<?php

$mpdf = new \Mpdf\Mpdf([
  'mode' => 'utf-8',
  'format' => [210, 297],
  'orientation' => 'P'
]);

$isi =
  '<!DOCTYPE html>
<html>
<head>
    <title>SLIP THR KEPEGAWAIAN </title>
    <style>@page {
        margin-top: 10px;
        margin-bottom: 5px;
        margin-left: 50px;
        margin-right: 25px;
       }</style>

       <style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid;
  text-align: left;
}


</style>
</head>

<body>
<table>
<tbody>
<tr>
<th style="width:30px;border:0"><img src="assets/images/icon.png" style="width:80px"></th>
<th style="border:0">
<h2>UNIVERSITAS MUHAMMADIYAH <br> SUMATERA UTARA </h2>
</th>
<th style="border:0" align="right">
<h3><u>SLIP THR KEPEGAWAIAN</u></h3>
</th>
</tr>
</tbody>
</table> 
<hr>

<table>
<tbody>
<tr>
<th style="border:0">
<table style="border:0">
<tbody>
<tr>
<th style="border:0">NAMA</th>
<th style="border:0">:</th>
<th style="border:0">' . $d["nama"] . '</th>
</tr>
<tr>
<th style="border:0">UNIT KERJA</th>
<th style="border:0">:</th>
<th style="border:0">' . $d["unit_kerja"] . '</th>
</tr>
<tr>
<th style="border:0">JABATAN</th>
<th style="border:0">:</th>
<th style="border:0">' . $d['jabatan'] . '</th>
</tr>
<tr>
<th style="border:0">GOLONGAN</th>
<th style="border:0">:</th>
<th style="border:0">' . $d['gol'] . '</th>
</tr>
<tr>
<th style="border:0">JAFUNG</th>
<th style="border:0">:</th>
<th style="border:0">' . $d['jafung'] . '</th>
</tr>
</tbody>
</table>


<th style="border:0">
<table style="border:0">
<tbody>
<tr>
<th style="border:0">STATUS KEPEGAWAIAN</th>
<th style="border:0">:</th>
<th style="border:0">' . $d['tugas'] . '</th>
</tr>
<tr>
<th style="border:0">KRITERIA</th> 
<th style="border:0">:</th>
<th style="border:0">' . $d['kriteria'] . '</th>
</tr>
<tr>
<th style="border:0">TAHUN</th>
<th style="border:0">:</th>
<th style="border:0">1445 H</th>
</tr>
<tr>
<th style="border:0">MASA KERJA</th>
<th style="border:0">:</th>
<th style="border:0">' . $d['masa_kerja'] . ' Tahun</th>
</tr> 
<tr>
<th style="border:0">GOL INFASING</th>
<th style="border:0">:</th>
<th style="border:0">' . $d['gol_inpasing'] . ' Tahun</th>
</tr> 
</tbody>
</table>
</th>
</tr>
</tbody>
</table> 
<h2 align="center"><u>RINCIAN</u></h2>

<table>
<tbody>

<tr>
<th align="center" style="border:1">PENGHASILAN</th>
<th align="center" style="border:1">POTONGAN</th>
</tr>

<tr>
<th align="center" style="border:1">
<table style="border:0">
<tbody>

<tr>
<td style="border:0"></td>
<td style="border:0">GAJI POKOK</td>
<td style="border:0">Rp.</td>
<td style="border:0" align="right">' . number_format($d['gaji_pokok']) . '</td>
</tr>

<tr>
<td style="border:0"></td>
<td style="border:0">GAJI KHUSUS</td>
<td style="border:0">Rp.</td>
<td style="border:0" align="right">' . number_format($d['gaji_tamba']) . '</td>
</tr>

<tr>
<td style="border:0"></td>
<td style="border:0"></td>
<td style="border:0"></td>
<td style="border:0" align="right"></td>
</tr>

<tr>
<td style="border:0" colspan="3"><b><u>TUNJANGAN</u></b></td>
</tr>

<tr>
<td style="border:0">1.</td>
<td style="border:0">JABATAN</td>
<td style="border:0">Rp.</td>
<td style="border:0" align="right">' . number_format($d['tun_jabatan']) . '</td>
</tr>

<tr>
<td style="border:0">2.</td>
<td style="border:0">INSENTIF JAB.</td>
<td style="border:0">Rp.</td>
<td style="border:0" align="right">' . number_format($d['tun_insen']) . '</td>
</tr>


<tr>
<td style="border:0">3.</td>
<td style="border:0">STRUKTUR</td>
<td style="border:0">Rp.</td>
<td style="border:0" align="right">' . number_format($d['tun_struk']) . '</td>
</tr>

<tr>
<td style="border:0">4.</td>
<td style="border:0">U. MAKAN</td>
<td style="border:0">Rp.</td>
<td style="border:0" align="right">' . number_format($d['tun_uang']) . '</td>
</tr>


<tr>
<td style="border:0">5.</td>
<td style="border:0">FUNGSIONAL</td>
<td style="border:0">Rp.</td>
<td style="border:0" align="right">' . number_format($d['dsn_gaji_fungsional']) . '</td>
</tr>

<tr>
<td style="border:0">6.</td>
<td style="border:0">KEAKTIFAN</td>
<td style="border:0">Rp.</td>
<td style="border:0" align="right">' . number_format($d['dsn_gaji_keaktifan']) . '</td>
</tr>


<tr>
<td style="border:0">7.</td>
<td style="border:0">JURUSAN</td>
<td style="border:0">Rp.</td>
<td style="border:0" align="right">' . number_format($d['dsn_gaji_jurusan']) . '</td>
</tr>


<tr>
<td style="border:0">8.</td>
<td style="border:0">LEMBAGA</td>
<td style="border:0">Rp.</td>
<td style="border:0" align="right">' . number_format($d['dsn_gaji_lembaga1'] + $d['dsn_gaji_lembaga2']) . '</td>
</tr>


<tr>
<td style="border:0">9.</td>
<td style="border:0">LABORATORIUM</td>
<td style="border:0">Rp.</td>
<td style="border:0" align="right">' . number_format($d['gaji_lab']) . '</td>
</tr>

<tr>
<td style="border:0">10.</td>
<td style="border:0">INSENTIF GAJI</td>
<td style="border:0">Rp.</td>
<td style="border:0" align="right">' . number_format($d['insentif_gaji']) . '</td>
</tr>

<tr>
<td style="border:0">11.</td>
<td style="border:0">TRANS. JABATAN</td>
<td style="border:0">Rp.</td>
<td style="border:0" align="right">' . number_format($d['trans_jabatan']) . '</td>
</tr>







</tbody>
</table>

</th>
<th align="center" style="border:1">
<table style="border:0">
<tbody>

<tr>
<td style="border:0"></td>
<td style="border:0"></td>
<td style="border:0"></td>
<td style="border:0" align="right"></td>
</tr>

<tr>
<td style="border:0" colspan="3"></td>
</tr>


<tr>
<td style="border:0">1.</td>
<td style="border:0">PPH</td>
<td style="border:0">Rp.</td>
<td style="border:0" align="right">0</td>
</tr>

<tr>
<td style="border:0">2.</td>
<td style="border:0">DANA PENSIUN</td>
<td style="border:0">Rp.</td>
<td style="border:0" align="right">0</td>
</tr>

<tr>
<td style="border:0">3.</td>
<td style="border:0">SIMPANAN LKK</td>
<td style="border:0">Rp.</td>
<td style="border:0" align="right">0</td>
</tr>

<tr>
<td style="border:0">4.</td>
<td style="border:0">CICILAN LKK</td>
<td style="border:0">Rp.</td>
<td style="border:0" align="right">0</td>
</tr>

<tr>
<td style="border:0">5.</td>
<td style="border:0">DAKWA</td>
<td style="border:0">Rp.</td>
<td style="border:0" align="right">0</td>
</tr>

<tr>
<td style="border:0">6.</td>
<td style="border:0">ZIS</td>
<td style="border:0">Rp.</td>
<td style="border:0" align="right">0</td>
</tr>

<tr>
<td style="border:0">7.</td>
<td style="border:0">QURBAN</td>
<td style="border:0">Rp.</td>
<td style="border:0" align="right">0</td>
</tr>

<tr>
<td style="border:0">8.</td>
<td style="border:0">JHT</td>
<td style="border:0">Rp.</td>
<td style="border:0" align="right">0</td>
</tr>

<tr>
<td style="border:0">9.</td>
<td style="border:0">LKD</td>
<td style="border:0">Rp.</td>
<td style="border:0" align="right">0</td>
</tr>

<tr>
<td style="border:0">10</td>
<td style="border:0">SPP</td>
<td style="border:0">Rp.</td>
<td style="border:0" align="right">0</td>
</tr>

<tr>
<td style="border:0">11.</td>
<td style="border:0">S.D</td>
<td style="border:0">Rp.</td>
<td style="border:0" align="right">0</td>
</tr>

<tr>
<td style="border:0">12. </td>
<td style="border:0">LAIN-LAIN</td>
<td style="border:0">Rp.</td>
<td style="border:0" align="right">0</td>
</tr>

</tbody>
</table>

</th>
</tr>

<tr>
<th align="center" style="border:1">
<table style="border:0">
<tbody>

<tr>
<th style="border:0">JUMLAH PENDAPATAN</th>
<th style="border:0">Rp.</th>
<th style="border:0" align="right">' . number_format($tot['tot_Total_pokok']) . '</th>
</tr>

</tbody>
</table>
</th>

<th align="center" style="border:1">
<table style="border:0">
<tbody>
<tr>
<th style="border:0">JUMLAH POTONGAN</th>
<th style="border:0">Rp.</th>
<th style="border:0" align="right">0</th>
</tr>

</tbody>
</table>
</th>
</tr>


<tr>
<th align="center" style="border:0"> </th>

<th align="center" style="border:1">
<table style="border:0">
<tbody>

<tr>
<th style="border:0">PERHITUNGAN</th>
<th style="border:0">' . $d["kelipatan"] . '</th>
<th style="border:0">X</th>
<th style="border:0" align="right">PENDAPATAN</th>
</tr>
</tbody>
</table>
</th>
</tr>

<tr>
<th align="center" style="border:0"> </th>

<th align="center" style="border:1">
<table style="border:0">
<tbody>

<tr>
<th style="border:0">THR DI TERIMA</th>
<th style="border:0">Rp.</th>
<th style="border:0" align="right">' . number_format($tot['tot_Jumlah_pendapatan']) . '</th>
</tr>
</tbody>
</table>
</th>
</tr>

</tbody>
</table>
<br><br>

<table style="border:0">
<tbody>

<tr>
<th style="width:50%; border:0"></th>
<td style="border:0" align="center">
A.n Rektor<br>
Wakil Rektor II<br>
<img src="assets/images/ttd.png" style="width:250px"><br>
<b><u>Prof. Dr. Akrim, M.Pd</u></b><br>
NIDN. 0122127902

</td>
</tr>
</tbody>
</table>
</body>
</html>';
$mpdf->WriteHTML($isi);
$mpdf->setFooter($bawah);
$mpdf->Output();
