<!DOCTYPE html>
<html>

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title>Login System</title>
  <link rel="shortcut icon" href="<?= base_url('assets/') ?>images/px.png" type="image/icon">
  <link href="<?= base_url('assets/'); ?>css/bootstrap.min.css" rel="stylesheet">
  <link href="<?= base_url('assets/'); ?>font-awesome/css/font-awesome.css" rel="stylesheet">

  <link href="<?= base_url('assets/'); ?>css/animate.css" rel="stylesheet">
  <link href="<?= base_url('assets/'); ?>css/style.css" rel="stylesheet">

</head>

<body class="gray-bg">
  <?php
  if (null !== $this->session->userdata('logged')) {
    redirect(site_url('home'));
  }
  ?>

  <div class="loginColumns animated fadeInDown">
    <?php if ($this->session->flashdata('gagal_login')) { ?>
      <div class="alert alert-danger">
        <?= $this->session->flashdata('gagal_login') ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    <?php } ?>
    <br>
    <div class="row">

      <div class="col-md-6">


        <img src="<?= base_url('assets/images/log22.png'); ?>" style="width:100%;" />

      </div>
      <div class="col-md-6">
        <?= $this->session->flashdata('message'); ?>
        <div class="ibox-content">
          <form class="m-t" action="<?= base_url('login'); ?>" method="post" autocomplete="off">
            <div class="form-group">
              <input type="email" id="email" name="email" class="form-control" placeholder="Email">
              <?php echo form_error('email'); ?>
            </div>
            <div class="form-group">
              <input type="password" id="password" name="password" class="form-control" placeholder="Password">
              <?php echo form_error('password'); ?>
            </div>
            <button type="submit" class="btn btn-primary block full-width m-b">Login</button>

            <a href="<?= base_url('forgot_password'); ?>">
              <small>Forgot password?</small>
            </a>

            <p class="text-muted text-center">
              <small>Do not have an account?</small>
            </p>
            <a class="btn btn-sm btn-white btn-block" href="#">Create an account</a>
          </form>
          <p class="m-t">
            <small>Sistem Integrasi SDM UMSU &copy; 2022</small>
          </p>
        </div>
      </div>
    </div>
    <hr />

  </div>

</body>

</html>