<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?= $title; ?></title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <script src="https://kit.fontawesome.com/64d58efce2.js" crossorigin="anonymous"></script>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="Bantuan dan insentif publikasi dosen Universitas Muhammadiyah Sumatera Utara">
    <meta name="keywords" content="simpublis, publis umsu, publis, lp2m, lp2m umsu, umsu">
    <meta name="author" content="Tri Andre Anu">
    <link rel="shortcut icon" type="image/icon" href="<?= base_url('assets/vendor/img/logo.png'); ?>">

    <link href="<?= base_url('assets/static/'); ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= base_url('assets/static/'); ?>font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?= base_url('assets/static/'); ?>css/animate.css" rel="stylesheet">
    <link href="<?= base_url('assets/static/'); ?>css/style.css" rel="stylesheet">
    <link href="<?= base_url('assets/static/'); ?>css/login.css" rel="stylesheet">
</head>

<body class="gray-bg">
    <?php
    if (null !== $this->session->userdata('logged')) {
        redirect(site_url('login'));
    }
    ?>
    <div class="middle-box animated fadeInDown">
        <div class="abs-bg"></div>

        <div class="content loginscreen">
            <div class="text-center">
                <div>
                    <img src="<?= base_url('assets/images/kop.png'); ?>" class="logo" />
                </div>
                <h3>IHRIS UMSU &copy; BETA &trade;</h3>
                <p style="font-size: small;"><b>Integrated Human Resources Information System </b></p>
                <?php if ($this->session->flashdata('gagal_login')) { ?>
                    <div class="alert alert-danger">
                        <?= $this->session->flashdata('gagal_login') ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php } ?>


                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <p><b> Informasi! </b> Bagi Akun yang Tidak Terdaftar Mohon Hubungi Admin WhatsApp: 082365653858</p>
                </div>
                <?= $this->session->flashdata('message'); ?>

                <!--  <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <p><b> WEB UNDER MAINTENANCE! </b> <br>Saat ini, Sistem Sedang Melakukan Pemeliharaan Berkala.</p>
                </div>
                <?= $this->session->flashdata('message'); ?>
            </div> -->
            </div>

            <form method="post" action="<?= base_url('login'); ?>" class="m-t" role="form">
                <div class="form-group ">
                    <label class="control-label">Email</label>
                    <input type="text" name="email" value="<?= set_value('email'); ?>" class="form-control" placeholder="Tulis email anda..." required>
                </div>
                <div class="form-group  ">
                    <label class="control-label">Password</label>
                    <input type="password" name="password" id="password" class="form-control" placeholder="Tulis password anda..." required>
                </div>

                <button type="submit" class="btn btn-primary block full-width m-b noborder-radius"><b>LOGIN</b></button>

                <table class="table">
                    <tbody>
                        <tr>
                            <td>
                                <div style="text-align: left;">
                                    <label class="control-label">
                                        <h5><a href="<?= base_url('forgotPassword'); ?>">Lupa password?</a></h5>
                                    </label>
                                </div>
                            </td>
                            <td>
                                <div style="text-align: right;">
                                    <label class="control-label">
                                        <h5>Belum memiliki akun?<a href="#"> Daftar di sini.</a></h5>
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div style="text-align: left;">
                                    <h4><a href="<?= base_url('panduan_new_revisi1.pdf') ?>" target="_blank"><i class="fa fa-download"></i> Unduh Panduan</a></h4>
                            </td>
        </div>
        <td>
            <div style="text-align: right;">
                <span style="font-size: 0.7em">
                    <i class="fa fa-home"></i>
                </span>
                <span style="font-size: 0.75em">
                    <a href="<?= base_url('login'); ?>" rel="noopener noreferrer">Halaman Utama</a>
                </span>
            </div>
        </td>
        </tr>
        </tbody>
        </table>
        </form>

        <p class="m-t text-center">
            <small>
                <a href="https://umsu.ac.id" target="_blank"> <b>Universitas Muhammadiyah Sumatera Utara</b></a>
            </small>
        </p>
    </div>

    <!-- Mainly scripts -->
    <script src="<?= base_url('assets/static/'); ?>js/jquery-2.1.1.js"></script>
    <script src="<?= base_url('assets/static/'); ?>js/bootstrap.min.js"></script>

</html>