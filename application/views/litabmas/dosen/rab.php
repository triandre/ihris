<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Usulan Baru</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dosen') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Litabmas</a></li>
                        <li class="breadcrumb-item active">Usulan Baru</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>
    <!-- Main content -->
    <section class="content">
        <div class="card">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('litabmas/tahap2'); ?>"><b>1. Identitas Usulan</b></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('litabmas/anggota_penelitian'); ?>"><b>2. Anggota Penelitian</b></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('litabmas/sub_usul'); ?>"><b>3. Substansi Usulan</b></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="<?= base_url('litabmas/rab'); ?>"><b>4. RAB</b></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('litabmas/dok_pendukung'); ?>"><b>5. Dokumen Pendukung</b></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('litabmas/kirim_usul'); ?>"><b>6. Kirim Usulan</b></a>
                </li>
            </ul>


            <div class="container-fluid tab-pane active"><br>
                <div class="card">
                    <div class="card-header" style="background-color:#007BFF ">
                        <h3 class="card-title" style="color: white;">Rancana Anggaran Belanja</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fas fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="alert alert-info">
                            <strong><i class="fa fa-info"></i></strong> Besaran Maksimal Dana Hibah Skeman Penelitian Dosen Pemula (Non Koloratif) adalah Rp. 10.000.000 / Tahun
                        </div>
                        <div class="alert alert-info">
                            <strong><i class="fa fa-info"></i></strong>Total Dana Direncanakan: Rp. 10.000.000.
                        </div>
                        <br>
                        <h5>RAB</h5>
                        <hr>
                        <?= $this->session->flashdata('message'); ?>
                        <div class="form-group row">
                            <a href="" data-toggle="modal" data-target="#tambahRab" type="button" class="btn btn-danger pull-right"><i class="fa fa-plus"></i> Tambah Data</a>
                        </div>
                        <div class="table-responsive">
                            <table id="example1" class="table table-bordered table-striped table-sm">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Komponen</th>
                                        <th>Item</th>
                                        <th>Satuan</th>
                                        <th>Volume</th>
                                        <th>Harga Satuan</th>
                                        <th>Total</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    <?php foreach ($viw as $rb) : ?>
                                        <td><?= $i; ?></td>
                                        <td><?= $rb['komponen']; ?></td>
                                        <td><?= $rb['item']; ?></td>
                                        <td><?= $rb['satuan']; ?></td>
                                        <td><?= $rb['volum']; ?></td>
                                        <td><?= $rb['harga_satuan']; ?></td>
                                        <td><?= $rb['total']; ?></td>
                                        <td></td>
                                        <?php $i++; ?>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>


</div>
</section>
<!-- /.content -->

<!-- The Modal -->
<div class="modal" id="tambahRab">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Tambah RAB</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form action="<?= base_url('litabmas/tambahRab'); ?>" method="post" enctype="multipart/form-data">
                    <input hidden class="form_control" id="reff_ltb" name="reff_ltb" value="<?= $d['reff_ltb'] ?>" readonly style="width:100%;" required>
                    <div class="form-group">
                        <label for="uname">Tahun Ke:</label>
                        <select class="form-control" id="tahun" name="tahun" style="width:100%;" required>
                            <option selected disabled value="">Pilih</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="uname">Kelompok:</label>
                        <select class="form-control" id="tahun" name="tahun" style="width:100%;" required>
                            <option selected disabled value="">Pilih</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="uname">Komponen:</label>
                        <select class="form-control" id="tahun" name="tahun" style="width:100%;" required>
                            <option selected disabled value="">Pilih</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="uname">Item:</label>
                        <select class="form-control" id="tahun" name="tahun" style="width:100%;" required>
                            <option selected disabled value="">Pilih</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="uname">Satuan:</label>
                        <select class="form-control" id="tahun" name="tahun" style="width:100%;" required>
                            <option selected disabled value="">Pilih</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="uname">volum:</label>
                        <input type="number" class="form-control" id="tahun" name="tahun" style="width:100%;" required>
                    </div>

                    <div class="form-group">
                        <label for="uname">Harga Satuan:</label>
                        <input type="number" class="form-control" id="tahun" name="tahun" style="width:100%;" required>
                    </div>


                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                        <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button>
                    </div>
                </form>

            </div>
        </div>
    </div>

</div>
<!-- /.content-wrapper -->
<!-- Select2 -->
<script src="<?= base_url() ?>assets/vendor/backend/plugins/select2/js/select2.full.min.js"></script>
<script>
    $(document).ready(function() {
        $('input[type="checkbox"]').on('change', function() {
            $('input[type="checkbox"]').not(this).prop('checked', false);
        });
    });
</script>