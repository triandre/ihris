<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Usulan Baru</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dosen') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Litabmas</a></li>
                        <li class="breadcrumb-item active">Usulan Baru</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>
    <!-- Main content -->
    <section class="content">
        <form action="<?= base_url('litabmas/save_tahap2'); ?>" method="post" enctype="multipart/form-data">
            <div class="card">
                <div class="card-header" style="background-color:#007BFF ">
                    <h3 class="card-title" style="color: white;">Persyaratan Skema Penelitian</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fas fa-minus"></i></button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <table class="table" style="overflow-x:auto;">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>Nama Skema</th>
                                    <th>Kualifikasi</th>
                                    <th>NIDN</th>
                                    <th>Sinta ID</th>
                                    <th>Scopus ID</th>
                                    <th>Sedang Studi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1; ?>
                                <?php foreach ($skema as $ba) : ?>
                                    <tr>
                                        <td><?= $i; ?></td>
                                        <td><?= $ba['nm_skema']; ?></td>
                                        <td><?= $ba['kualifikasi']; ?></td>
                                        <td><?= $ba['p_nidn']; ?></td>
                                        <td><?= $ba['p_sinta']; ?></td>
                                        <td><?= $ba['p_scopus']; ?></td>
                                        <td><?= $ba['sedang_studi']; ?></td>
                                    </tr>
                                    <?php $i++; ?>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>


                <div class="card-footer">

                </div>
            </div>

            <div class="card">
                <div class="card-header" style="background-color:#007BFF ">
                    <h3 class="card-title" style="color: white;">Skema Penelitian Yang dapat anda pilih.</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fas fa-minus"></i></button>
                    </div>
                </div>
                <div class="card-body">

                    <div class="form-group row">
                        <div class="col-sm-12">
                            <select class="form-control" id="id_skema" name="id_skema" style="width:100%;" required>
                                <option selected disabled value="">Pilih</option>
                                <?php foreach ($skema as $row4) : ?>
                                    <option value="<?= $row4['id_skema']; ?>"><?= $row4['nm_skema']; ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <input hidden id="id_periode" name="id_periode" value="<?= $periode['id_periode']; ?>" required readonly>

                </div>
            </div>

            <div class="card">
                <div class="card-header" style="background-color:#007BFF ">
                    <h3 class="card-title" style="color: white;">Periode Penelitian.</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fas fa-minus"></i></button>
                    </div>
                </div>
                <div class="card-body">

                    <div class="row">
                        <table>
                            <tbody>
                                <tr>
                                    <td>Periode</td>
                                    <th style="width: 50px;"></th>
                                    <th><?= $periode['periode']; ?></th>
                                </tr>
                                <tr>
                                    <td>Buka</td>
                                    <th></th>
                                    <th><?= date('d F Y', strtotime($periode['buka'])); ?></th>
                                </tr>
                                <tr>
                                    <td>Tutup</td>
                                    <th></th>
                                    <th><?= date('d F Y', strtotime($periode['tutup'])); ?></th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <hr>
                    <div class="form-group row">
                        <div class="col-sm-6">
                            <label>Tahun Usulan</label>
                            <select class="form-control" id="tahun_usulan" name="tahun_usulan" required>
                                <option selected disabled value="">Pilih</option>
                                <option value="2024">2024</option>
                                <option value="2025">2025</option>
                                <option value="2026">2026</option>
                                <option value="2027">2027</option>
                                <option value="2028">2028</option>
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <label>Pelaksanaan</label>
                            <select class="form-control" id="tahun_pelaksanaan" name="tahun_pelaksanaan" required>
                                <option selected disabled value="">Pilih</option>
                                <option value="2024">2024</option>
                                <option value="2025">2025</option>
                                <option value="2026">2026</option>
                                <option value="2027">2027</option>
                                <option value="2028">2028</option>
                            </select>
                        </div>
                    </div>

                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary float-right"> <i class="fa fa-save"></i> Registrasi Now</button>
                </div>
            </div>
        </form>

    </section>
    <!-- /.content -->





</div>
<!-- /.content-wrapper -->
<!-- Select2 -->
<script src="<?= base_url() ?>assets/vendor/backend/plugins/select2/js/select2.full.min.js"></script>