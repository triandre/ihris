<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Usulan Baru</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dosen') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Litabmas</a></li>
                        <li class="breadcrumb-item active">Usulan Baru</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>
    <!-- Main content -->
    <section class="content">
        <div class="card">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" href="<?= base_url('litabmas/tahap2'); ?>"><b>1. Identitas Usulan</b></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('litabmas/anggota_penelitian'); ?>"><b>2. Anggota Penelitian</b></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="<?= base_url('litabmas/sub_usul'); ?>"><b>3. Substansi Usulan</b></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('litabmas/rab'); ?>"><b>4. RAB</b></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('litabmas/dok_pendukung'); ?>"><b>5. Dokumen Pendukung</b></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('litabmas/kirim_usul'); ?>"><b>6. Kirim Usulan</b></a>
                </li>
            </ul>


            <div class="container-fluid tab-pane active"><br>
                <div class="card">
                    <div class="card-header" style="background-color:#007BFF ">
                        <h3 class="card-title" style="color: white;">Subtansi Usulan</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fas fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="<?= base_url('litabmas/uploadDokUsulan'); ?>" method="post" enctype="multipart/form-data">
                            <div class="form-group row">
                                <input hidden class="form_control" id="reff_ltb" name="reff_ltb" value="<?= $d['reff_ltb'] ?>" readonly style="width:100%;" required>
                                <div class="col-sm-4">
                                    <table class="table table-bordered">
                                        <tbody>
                                            <tr>
                                                <td>Dokumen Usulan</td>
                                            </tr>
                                            <tr>
                                                <?php if ($d['file_substansi'] == null) { ?>
                                                    <td>Dokumen Usulan Belum Ada</td>
                                                <?php } else { ?>
                                                    <td><a href="<?= base_url('arc_litabmas/'); ?><?= $d['file_substansi'] ?>" target="_blank"><img src="<?= base_url('assets/litabmas/pdf.png') ?>" alt=""></a></td>
                                                <?php } ?>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-sm-8">
                                    <input type="file" class="form-control" id="file_substansi" name="file_substansi" accept="application/pdf" required>
                                    <br>
                                    <button type="submit" class="btn btn-primary"> <i class="fa fa-arrow-circle-right"></i> Simpan/Update</button>
                                </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>

        <div class="container-fluid tab-pane active"><br>
            <div class="card">
                <div class="card-header" style="background-color:#007BFF ">
                    <h3 class="card-title" style="color: white;">Subtansi Usulan Penelitian</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fas fa-minus"></i></button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <div class="alert alert-info" role="info">
                                Luaran Dan Target Capaian
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <?php if ($lur == null) { ?>
                                <i>Belum ada Luaran Wajib..</i>
                            <?php } else { ?>
                                <div class="card">
                                    <div class="card-body"><b><?= $lur['luaran'] ?></b>
                                        <a href="<?= base_url('litabmas/hapus_luaranTC/') ?><?= $lur['id_luaran']; ?>" class="btn btn-danger btn-sm float-right tombol-hapus"> <i class="fa fa-trash"></i> </a>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <?php if ($lur == null) { ?>
                            <a href="" data-toggle="modal" data-target="#luaranTC" type="button" class="btn btn-danger pull-right"><i class="fa fa-plus"></i> Data Baru</a>
                        <?php } ?>
                    </div>
                    <hr>

                    <div class="form-group row">
                        <div class="col-sm-12">
                            <div class="alert alert-info" role="info">
                                Luaran Tambahan
                            </div>
                        </div>
                        <?php if ($lurTam == null) { ?>
                            <i>Belum ada Luaran Wajib..</i>
                        <?php } else { ?>
                            <div class="container-fluid">
                                <?php foreach ($lurTam as $lt) : ?>
                                    <div class="card">
                                        <div class="card-body"><b>Luaran Tambahan : <?= $lt['keterangan']; ?></b> (Tahun Ke <?= $lt['tahun']; ?>)
                                            <a href="<?= base_url('litabmas/hapus_luaranTambahan/') ?><?= $lt['id_luaran_tambahan']; ?>" class="btn btn-danger btn-sm float-right tombol-hapus"> <i class="fa fa-trash"></i> </a>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            <?php } ?>
                            </div>
                            <div class="form-group row">
                                <a href="" data-toggle="modal" data-target="#luaranTambahan" type="button" class="btn btn-danger pull-right"><i class="fa fa-plus"></i> Data Baru</a>
                            </div>
                            <hr>

                            <hr>
                    </div>
                </div>
            </div>
        </div>



</div>
</section>
<!-- /.content -->

<!-- The Modal -->
<div class="modal" id="luaranTC">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Luaran Dan Capaian Target</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form action="<?= base_url('litabmas/luaranTC'); ?>" method="post" enctype="multipart/form-data">
                    <input hidden class="form_control" id="reff_ltb" name="reff_ltb" value="<?= $d['reff_ltb'] ?>" readonly style="width:100%;" required>
                    <select class="form-control" id="luaran" name="luaran" style="width:100%;" required>
                        <option selected disabled value="">Pilih</option>
                        <option value="Tahun Ke 1 - Proceeding terindeks SCOPUS">Tahun Ke 1 - Proceeding terindeks SCOPUS</option>
                        <option value="Tahun Ke 1 - Publikasi (Minimal SINTA 3)">Tahun Ke 1 - Publikasi (Minimal SINTA 3)</option>
                    </select>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button>
            </div>
            </form>

        </div>
    </div>
</div>


<!-- The Modal -->
<div class="modal" id="luaranTambahan">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Luaran Tambahan</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form action="<?= base_url('litabmas/luaranTambahan'); ?>" method="post" enctype="multipart/form-data">
                    <input hidden class="form_control" id="reff_ltb" name="reff_ltb" value="<?= $d['reff_ltb'] ?>" readonly style="width:100%;" required>
                    <label for="email">luaran Tambahan:</label>
                    <select class="form-control" id="keterangan" name="keterangan" style="width:100%;" required>
                        <option selected disabled value="">Pilih</option>
                        <option value="Buku">Buku</option>
                        <option value="Kekayaan Intelektual Lalk Industri">Kekayaan Intelektual Lalk Industri</option>
                        <option value="Kekayaan Intelektual">Kekayaan Intelektual</option>
                        <option value="Prosiding Terindeks SCOPUS/WOS">Prosiding Terindeks SCOPUS/WOS</option>
                        <option value="Publikasi Jurnal Internasional Bereputasi (Minimal SCOPUS Q2)">Publikasi Jurnal Internasional Bereputasi (Minimal SCOPUS Q2)</option>
                        <option value="Publikasi Jurnal Internasional Bereputasi (SCOPUS)">Publikasi Jurnal Internasional Bereputasi (SCOPUS)</option>
                        <option value="Publikasi Jurnal Internasional Bereputasi (SCOPUS)/WOS">Publikasi Jurnal Internasional Bereputasi (SCOPUS)/WOS</option>
                        <option value="Publikasi Jurnal Internasional Terindeks SCOPUS">Publikasi Jurnal Internasional Terindeks SCOPUS</option>
                        <option value="Uji Coba Produk">Uji Coba Produk</option>
                        <option value="Feasibility Study">Feasibility Study</option>
                    </select>

                    <label for="email">Tahun Ke:</label>
                    <select class="form-control" id="tahun" name="tahun" style="width:100%;" required>
                        <option selected disabled value="">Pilih</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="2">2</option>

                    </select>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button>
            </div>
            </form>

        </div>
    </div>
</div>



</div>
<!-- /.content-wrapper -->
<!-- Select2 -->
<script src="<?= base_url() ?>assets/vendor/backend/plugins/select2/js/select2.full.min.js"></script>
<script>
    $(document).ready(function() {
        $('input[type="checkbox"]').on('change', function() {
            $('input[type="checkbox"]').not(this).prop('checked', false);
        });
    });
</script>