<?php
if ($cek_tahap == null) {
    redirect('litabmas/tahap1');
} elseif ($cek_tahap['sts_tahap'] == 1) {
    redirect('litabmas/tahap2');
} elseif ($cek_tahap['sts_tahap'] == 2) {
    redirect('litabmas/tahap3');
} elseif ($cek_tahap['sts_tahap'] == 3) {
    redirect('litabmas/tahap4');
} elseif ($cek_tahap['sts_tahap'] == 4) {
    redirect('litabmas/tahap5');
}
