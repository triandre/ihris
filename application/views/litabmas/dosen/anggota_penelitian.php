<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Usulan Baru</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dosen') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Litabmas</a></li>
                        <li class="breadcrumb-item active">Usulan Baru</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>
    <!-- Main content -->
    <section class="content">
        <div class="card">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('litabmas/tahap2'); ?>"><b>1. Identitas Usulan</b></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="<?= base_url('litabmas/anggota_penelitian'); ?>"><b>2. Anggota Penelitian</b></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('litabmas/sub_usul'); ?>"><b>3. Substansi Usulan</b></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('litabmas/rab'); ?>"><b>4. RAB</b></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('litabmas/dok_pendukung'); ?>"><b>5. Dokumen Pendukung</b></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('litabmas/kirim_usul'); ?>"><b>6. Kirim Usulan</b></a>
                </li>
            </ul>


            <div class="container-fluid tab-pane active"><br>
                <div class="container-fluid tab-pane active">
                    <div class="card">
                        <div class="card-header" style="background-color:#007BFF ">
                            <h3 class="card-title" style="color: white;">Anggota Penelitian</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                    <i class="fas fa-minus"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <div class="alert alert-info" role="info">
                                        Identitas Pengusul Dosen Pembimbing
                                    </div>
                                </div>
                                <div class="container-fluid">
                                    Data:
                                    <?php $i = 1; ?>
                                    <?php foreach ($pbb as $ba) : ?>
                                        <div class="card">
                                            <div class="card-body"><b><?= $ba['name']; ?></b> (<?= $ba['nidn']; ?>)
                                                <br> <b> Bidang Ilmu : </b> <?= $ba['bidang_ilmu']; ?>
                                                <br> <b> Peran : </b> <?= $ba['peran_pembimbing']; ?> || <b> Tugas : </b> <?= $ba['tugas_pembimbing']; ?>
                                                <br> <?php
                                                        if ($ba['sts_pembimbing'] == 1) {
                                                            echo '<span class="badge badge-primary">Menunggu Persetujuan</span>';
                                                        } elseif ($ba['sts_pembimbing'] == 2) {
                                                            echo '<span class="badge badge-success">Disetujui</span>';
                                                        } elseif ($ba['sts_pembimbing'] == 3) {
                                                            echo '<span class="badge badge-danger">Ditolak</span>';
                                                        }
                                                        ?>

                                                <a href="<?= base_url('litabmas/hapus_dospem/') ?><?= $ba['id_pembimbing']; ?>" class="btn btn-danger btn-sm float-right tombol-hapus"> <i class="fa fa-trash"></i> </a>

                                            </div>
                                        </div>
                                        <?php $i++; ?>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                            <div class="form-group row">
                                <a href="" data-toggle="modal" data-target="#myModal" type="button" class="btn btn-danger pull-right"><i class="fa fa-plus"></i> Data Baru</a>
                            </div>
                            <hr>

                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <div class="alert alert-info" role="info">
                                        Identitas Pengusul Anggota Penelitian Dosen UMSU
                                    </div>
                                </div>
                                <div class="container-fluid">
                                    Data:
                                    <?php $i = 1; ?>
                                    <?php foreach ($agt as $ag) : ?>
                                        <div class="card">
                                            <div class="card-body"><b><?= $ag['name']; ?></b> (<?= $ag['nidn']; ?>)
                                                <br> <b> Bidang Ilmu : </b> <?= $ag['bidang_ilmu']; ?>
                                                <br> <b> Peran : </b> <?= $ag['peran_anggota']; ?> || <b> Tugas : </b> <?= $ag['tugas_anggota']; ?>
                                                <br> <?php
                                                        if ($ag['sts_anggota'] == 1) {
                                                            echo '<span class="badge badge-primary">Menunggu Persetujuan</span>';
                                                        } elseif ($ag['sts_anggota'] == 2) {
                                                            echo '<span class="badge badge-success">Disetujui</span>';
                                                        } elseif ($ag['sts_anggota'] == 3) {
                                                            echo '<span class="badge badge-danger">Ditolak</span>';
                                                        }
                                                        ?>
                                                <a href="<?= base_url('litabmas/hapus_anggotaPen/') ?><?= $ag['id_anggota']; ?>" class="btn btn-danger btn-sm float-right tombol-hapus"> <i class="fa fa-trash"></i> </a>
                                            </div>
                                        </div>
                                        <?php $i++; ?>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                            <div class="form-group row">
                                <a href="" data-toggle="modal" data-target="#myModal2" type="button" class="btn btn-danger pull-right"><i class="fa fa-plus"></i> Data Baru</a>
                            </div>
                            <hr>

                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <div class="alert alert-info" role="info">
                                        Identitas Pengusul Anggota Mahasiswa UMSU
                                    </div>
                                </div>
                                <div class="container-fluid">
                                    Data:
                                    <?php $i = 1; ?>
                                    <?php foreach ($mhs as $mh) : ?>
                                        <div class="card">
                                            <div class="card-body"><b><?= $mh['nama_mahasiswa']; ?></b> (<?= $mh['npm']; ?>)
                                                <br> <b> Bidang Ilmu : </b> <?= $mh['prodi_mahasiswa']; ?>
                                                <br> <b> Peran : </b> <?= $mh['peran_mahasiswa']; ?> || <b> Tugas : </b> <?= $mh['tugas_mahasiswa']; ?>
                                                <br> <?php
                                                        if ($mh['sts_mahasiswa'] == 1) {
                                                            echo '<span class="badge badge-primary">Menunggu Persetujuan</span>';
                                                        } elseif ($mh['sts_mahasiswa'] == 2) {
                                                            echo '<span class="badge badge-success">Disetujui</span>';
                                                        } elseif ($mh['sts_mahasiswa'] == 3) {
                                                            echo '<span class="badge badge-danger">Ditolak</span>';
                                                        }
                                                        ?>

                                                <a href="<?= base_url('litabmas/hapus_anggotaMahasiswa/') ?><?= $mh['id_mahasiswa']; ?>" class="btn btn-danger btn-sm float-right tombol-hapus"> <i class="fa fa-trash"></i> </a>
                                            </div>
                                        </div>
                                        <?php $i++; ?>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                            <div class="form-group row">
                                <a href="" data-toggle="modal" data-target="#myModal3" type="button" class="btn btn-danger pull-right"><i class="fa fa-plus"></i> Data Baru</a>
                            </div>
                            <hr>
                        </div>
                    </div>
                </div>
            </div>


        </div>
</div>
</section>
<!-- /.content -->


<!-- The Modal -->
<div class="modal" id="myModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Pilih Dosen Pembimbing</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form action="<?= base_url('litabmas/dospem'); ?>" method="post" enctype="multipart/form-data">
                    <input hidden class="form_control" id="reff_ltb" name="reff_ltb" value="<?= $d['reff_ltb'] ?>" readonly style="width:100%;" required>
                    <div class="form-group">
                        <label for="email">Dosen Pembimbing:</label>
                        <select class="form_control selecty" id="email_pembimbing" name="email_pembimbing" style="width:100%;" required>
                            <option selected disabled value="">Pilih</option>
                            <?php foreach ($pembimbing as $row5) : ?>
                                <option value="<?= $row5['email']; ?>"> <?= $row5['name']; ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="email">Peran:</label>
                        <input class="form_control" id="peran" name="peran" style="width:100%;" required>
                    </div>

                    <div class="form-group">
                        <label for="email">Tugas Dalam Penelitian:</label>
                        <input type="text" class="form_control" id="tugas" name="tugas" style="width:100%;" required>
                    </div>

            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button>
            </div>
            </form>

        </div>
    </div>
</div>

<!-- The Modal -->
<div class="modal" id="myModal2">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Pilih Anggota Penelitian</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form action="<?= base_url('litabmas/anggotaPen'); ?>" method="post" enctype="multipart/form-data">
                    <input hidden class="form_control" id="reff_ltb" name="reff_ltb" value="<?= $d['reff_ltb'] ?>" readonly style="width:100%;" required>
                    <div class="form-group">
                        <label for="email">Anggota Penelitian:</label>
                        <select class="form_control selecty" id="email_anggota" name="email_anggota" style="width:100%;" required>
                            <option selected disabled value="">Pilih</option>
                            <?php foreach ($pembimbing as $row5) : ?>
                                <option value="<?= $row5['email']; ?>"> <?= $row5['name']; ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="email">Peran:</label>
                        <input class="form_control" id="peran" name="peran" style="width:100%;" required>
                    </div>

                    <div class="form-group">
                        <label for="email">Tugas Dalam Penelitian:</label>
                        <input type="text" class="form_control" id="tugas" name="tugas" style="width:100%;" required>
                    </div>

            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button>
            </div>
            </form>

        </div>
    </div>
</div>


<!-- The Modal -->
<div class="modal" id="myModal3">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Pilih Anggota Penelitian Mahasiswa</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form action="<?= base_url('litabmas/anggotaMahasiswa'); ?>" method="post" enctype="multipart/form-data">
                    <input hidden class="form_control" id="reff_ltb" name="reff_ltb" value="<?= $d['reff_ltb'] ?>" readonly style="width:100%;" required>
                    <div class="form-group">
                        <label for="email">NPM:</label>
                        <input class="form_control" id="npm" name="npm" style="width:100%;" required>

                        <div class="form-group">
                            <label for="email">Nama Mahasiswa:</label>
                            <input class="form_control" id="nama_mahasiswa" name="nama_mahasiswa" style="width:100%;" required>
                        </div>

                        <div class="form-group">
                            <label for="email">Program Studi:</label>
                            <input class="form_control" id="prodi" name="prodi" style="width:100%;" required>
                        </div>
                        <div class="form-group">
                            <label for="email">Peran:</label>
                            <input class="form_control" id="peran" name="peran" style="width:100%;" required>
                        </div>

                        <div class="form-group">
                            <label for="email">Tugas Dalam Penelitian:</label>
                            <input type="text" class="form_control" id="tugas" name="tugas" style="width:100%;" required>
                        </div>

                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                        <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button>
                    </div>
                </form>

            </div>
        </div>
    </div>



</div>
<!-- /.content-wrapper -->
<!-- Select2 -->
<script src="<?= base_url() ?>assets/vendor/backend/plugins/select2/js/select2.full.min.js"></script>
<script>
    $(document).ready(function() {
        $('.selectx').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $(document).ready(function() {
        $('.selecty').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $(document).ready(function() {
        $('.selectz').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $(document).ready(function() {
        $('.select1').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $(document).ready(function() {
        $('.select5').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });


    $(document).ready(function() {
        $('.select3').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });
</script>