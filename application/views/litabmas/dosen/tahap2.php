<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Usulan Baru</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dosen') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Litabmas</a></li>
                        <li class="breadcrumb-item active">Usulan Baru</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>
    <!-- Main content -->
    <section class="content">
        <div class="card">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" href="<?= base_url('litabmas/tahap2'); ?>"><b>1. Identitas Usulan</b></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('litabmas/anggota_penelitian'); ?>"><b>2. Anggota Penelitian</b></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('litabmas/sub_usul'); ?>"><b>3. Substansi Usulan</b></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('litabmas/rab'); ?>"><b>4. RAB</b></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('litabmas/dok_pendukung'); ?>"><b>5. Dokumen Pendukung</b></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('litabmas/kirim_usul'); ?>"><b>6. Kirim Usulan</b></a>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div class="container-fluid tab-pane active"><br>
                    <div class="card">
                        <div class="card-header" style="background-color:#007BFF ">
                            <h3 class="card-title" style="color: white;">Identitas Usulan Penelitian</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                    <i class="fas fa-minus"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <form method="post" action="<?= base_url('litabmas/updateTahap2') ?>" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label for="comment">Judul:</label>
                                    <textarea class="form-control" rows="3" id="judul" name="judul" value="<?= $d['judul']; ?>" style="width:100%;"><?= $d['judul']; ?></textarea>
                                </div>

                        </div>
                    </div>

                    <div class="container-fluid tab-pane active"><br>
                        <div class="card">
                            <div class="card-header" style="background-color:#007BFF ">
                                <h3 class="card-title" style="color: white;">Pemilihan Skema Usulan</h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                        <i class="fas fa-minus"></i></button>
                                </div>
                            </div>
                            <div class="card-body">

                                <div class="form-group row">
                                    <input hidden type="text" name="reff_ltb" id="reff_ltb" value="<?= $d['reff_ltb']; ?>" required readonly>
                                    <label for="inputPassword" class="col-sm-2 col-form-label">Skema Penelitian</label>
                                    <div class="col-sm-10">
                                        <select class="form-control selectx" id="id_skema" name="id_skema" style="width:100%;">
                                            <option selected disabled value="">Pilih</option>
                                            <?php foreach ($skema as $row4) : ?>
                                                <option value="<?= $row4['id_skema']; ?>" <?= ($d['id_skema'] == $row4['id_skema']) ? 'selected' : ''; ?>><?= $row4['nm_skema']; ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-2 col-form-label">Jenis Kolaborasi</label>
                                    <div class="col-sm-10">
                                        <select class="form_control selecty" id="jenis_kolaborasi_id" name="jenis_kolaborasi_id" style="width:100%;" required>
                                            <option selected disabled value="">Pilih</option>
                                            <?php foreach ($kolaborasi as $row5) : ?>
                                                <option value="<?= $row5['id_jenis_kolaborasi']; ?>" <?= ($d['jenis_kolaborasi_id'] == $row5['id_jenis_kolaborasi']) ? 'selected' : ''; ?>><?= $row5['jenis_kolaborasi']; ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-2 col-form-label">Rumpun Ilmu</label>
                                    <div class="col-sm-10">
                                        <select class="form_control selectz" id="rumpun_ilmu_id" name="rumpun_ilmu_id" style="width:100%;" required>
                                            <option selected disabled value="">Pilih</option>
                                            <?php foreach ($rumpun as $row6) : ?>
                                                <option value="<?= $row6['id_rumpun_ilmu']; ?>" <?= ($d['rumpun_ilmu_id'] == $row6['id_rumpun_ilmu']) ? 'selected' : ''; ?>><?= $row6['rumpun_ilmu']; ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-2 col-form-label">Bidang Fokus Penelitian</label>
                                    <div class="col-sm-10">
                                        <select class="form_control select1" id="bfp_id" name="bfp_id" style="width:100%;" required>
                                            <option selected disabled value="">Pilih</option>
                                            <?php foreach ($bidang as $row7) : ?>
                                                <option value="<?= $row7['id_bfp']; ?>" <?= ($d['bfp_id'] == $row7['id_bfp']) ? 'selected' : ''; ?>><?= $row7['bidang_fp']; ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-2 col-form-label">Tema Penelitian</label>
                                    <div class="col-sm-10">
                                        <select class="form_control select5" id="tema_ltb_id" name="tema_ltb_id" style="width:100%;" required>
                                            <option selected disabled value="">Pilih</option>
                                            <?php foreach ($tema as $row8) : ?>
                                                <option value="<?= $row8['id_tema_ltb']; ?>" <?= ($d['tema_ltb_id'] == $row8['id_tema_ltb']) ? 'selected' : ''; ?>><?= $row8['tema']; ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-2 col-form-label">Topik Penelitian</label>
                                    <div class="col-sm-10">
                                        <select class="form_control select3" id="topik_ltb_id" name="topik_ltb_id" style="width:100%;" required>
                                            <option selected disabled value="">Pilih</option>
                                            <?php foreach ($topik as $row9) : ?>
                                                <option value="<?= $row9['id_topik_ltb']; ?>" <?= ($d['topik_ltb_id'] == $row9['id_topik_ltb']) ? 'selected' : ''; ?>><?= $row9['topik_ltb']; ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-2 col-form-label">Lama Kegiatan</label>
                                    <div class="col-sm-10">
                                        <select class="custom-select mr-sm-2" id="lama_kegiatan" name="lama_kegiatan" style="width:100%;" required>
                                            <option value="<?= $d['lama_kegiatan'] ?>"><?= $d['lama_kegiatan'] ?></option>
                                            <option value="1 Tahun">1 Tahun</option>
                                            <option value="2 Tahun">2 Tahun</option>
                                            <option value="3 Tahun">3 Tahun</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary float-right"> <i class="fa fa-save"></i> Update/Save</button>
                    </div>
                    </form>
                </div>
            </div>

        </div>
</div>
</section>
<!-- /.content -->





</div>
<!-- /.content-wrapper -->
<!-- Select2 -->
<script src="<?= base_url() ?>assets/vendor/backend/plugins/select2/js/select2.full.min.js"></script>
<script>
    $(document).ready(function() {
        $('.selectx').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $(document).ready(function() {
        $('.selecty').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $(document).ready(function() {
        $('.selectz').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $(document).ready(function() {
        $('.select1').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $(document).ready(function() {
        $('.select5').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });


    $(document).ready(function() {
        $('.select3').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });
</script>