<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Usulan Baru</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dosen') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Litabmas</a></li>
                        <li class="breadcrumb-item active">Usulan Baru</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>
    <!-- Main content -->
    <section class="content">
        <div class="card">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" href="<?= base_url('litabmas/tahap2'); ?>"><b>1. Identitas Usulan</b></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('litabmas/anggota_penelitian'); ?>"><b>2. Anggota Penelitian</b></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('litabmas/sub_usul'); ?>"><b>3. Substansi Usulan</b></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('litabmas/rab'); ?>"><b>4. RAB</b></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('litabmas/dok_pendukung'); ?>"><b>5. Dokumen Pendukung</b></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('litabmas/kirim_usul'); ?>"><b>6. Kirim Usulan</b></a>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div class="container-fluid tab-pane active"><br>
                    <div class="card">
                        <div class="card-header" style="background-color:#007BFF ">
                            <h3 class="card-title" style="color: white;">Identitas Usulan Penelitian</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                    <i class="fas fa-minus"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="comment">Judul:</label>
                                <textarea class="form-control" rows="3" id="judul" name="judul" value="<?= set_value('judul'); ?>" style="width:100%;"><?= set_value('judul'); ?></textarea>
                            </div>

                        </div>
                    </div>

                    <div class="container-fluid tab-pane active"><br>
                        <div class="card">
                            <div class="card-header" style="background-color:#007BFF ">
                                <h3 class="card-title" style="color: white;">Pemilihan Skema Usulan</h3>
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                        <i class="fas fa-minus"></i></button>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-2 col-form-label">Skema Penelitian</label>
                                    <div class="col-sm-10">
                                        <select class="form-control" id="id_skema" name="id_skema" style="width:100%;">
                                            <option selected disabled value="">Pilih</option>
                                            <?php foreach ($skema as $row4) : ?>
                                                <option value="<?= $row4['id_skema']; ?>"><?= $row4['nm_skema']; ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-2 col-form-label">Jenis Kolaborasi</label>
                                    <div class="col-sm-10">
                                        <select class="custom-select mr-sm-2" id="jenis_colaborasi" name="jenis_colaborasi">
                                            <option selected>Choose...</option>
                                            <option value="1">One</option>
                                            <option value="2">Two</option>
                                            <option value="3">Three</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-2 col-form-label">Rumpun Ilmu</label>
                                    <div class="col-sm-10">
                                        <select class="custom-select mr-sm-2" id="rumpun_ilmu" name="rumpun_ilmu">
                                            <option selected>Choose...</option>
                                            <option value="1">One</option>
                                            <option value="2">Two</option>
                                            <option value="3">Three</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-2 col-form-label">Bidang Fokus Penelitian</label>
                                    <div class="col-sm-10">
                                        <select class="custom-select mr-sm-2" id="bidang_fokus_penelitian" name="bidang_fokus_penelitian">
                                            <option selected>Choose...</option>
                                            <option value="1">One</option>
                                            <option value="2">Two</option>
                                            <option value="3">Three</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-2 col-form-label">Tema Penelitian</label>
                                    <div class="col-sm-10">
                                        <select class="custom-select mr-sm-2" id="tema_penelitian" name="tema_penelitian">
                                            <option selected>Choose...</option>
                                            <option value="1">One</option>
                                            <option value="2">Two</option>
                                            <option value="3">Three</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-2 col-form-label">Topik Penelitian</label>
                                    <div class="col-sm-10">
                                        <select class="custom-select mr-sm-2" id="topik_penelitian" name="topik_penelitian">
                                            <option selected>Choose...</option>
                                            <option value="1">One</option>
                                            <option value="2">Two</option>
                                            <option value="3">Three</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-2 col-form-label">Lama Kegiatan</label>
                                    <div class="col-sm-10">
                                        <select class="custom-select mr-sm-2" id="lama_kegiatan" name="lama_kegiatan">
                                            <option selected disabled value="">Pilih</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="card-footer">
                        <a href="<?= base_url('litabmas/tahap3') ?>" type="button" class="btn btn-primary float-right"> <i class="fa fa-save"></i> Simpan</a>
                    </div>
                </div>
            </div>

        </div>
</div>
</section>
<!-- /.content -->





</div>
<!-- /.content-wrapper -->
<!-- Select2 -->
<script src="<?= base_url() ?>assets/vendor/backend/plugins/select2/js/select2.full.min.js"></script>