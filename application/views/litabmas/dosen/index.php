<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Usulan Baru</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dosen') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Litabmas</a></li>
                        <li class="breadcrumb-item active">Usulan Baru</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <?php
    $email = $this->session->userdata('email');
    $usulan = $this->db->query("SELECT * FROM ltb_litabmas where email='$email'");
    ?>

    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>
    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title"></h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>

                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-6">H-INDEX: 0 </div>
                    <div class="col-sm-6">USULAN BARU:<a href="<?= base_url('litabmas/cek'); ?>"> <b> <?php echo $usulan->num_rows(); ?></b></a></div>
                </div>
            </div>
            <div class="card-footer">

            </div>
        </div>


        <div class="card">
            <div class="card-header" style="background-color:#007BFF ">
                <h3 class="card-title" style="color: white;">Identitas Pengusul Ketua</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <table>
                        <tbody>
                            <tr>
                                <td>Nama</td>
                                <th style="width: 50px;"></th>
                                <th><?= $akun['name']; ?></th>
                            </tr>
                            <tr>
                                <td>NIDN</td>
                                <th></th>
                                <th><?= $akun['nidn']; ?></th>
                            </tr>
                            <tr>
                                <td>Unit Kerja</td>
                                <th></th>
                                <th><?= $penempatan['unit']; ?></th>
                            </tr>
                            <tr>
                                <td>Jabatan Akademik</td>
                                <th></th>
                                <th><?= $jafung['jafung']; ?> (<?= $jafung['skor']; ?>)</th>
                            </tr>
                            <tr>
                                <td>Kualifikasi</td>
                                <th></th>
                                <th><?= $penfor['jenjang']; ?> - (<?= $penfor['program_studi']; ?>)</th>
                            </tr>

                            <tr>
                                <td>Email</td>
                                <th></th>
                                <th><?= $akun['email']; ?></th>
                            </tr>
                        </tbody>
                        </tbody>
                    </table>
                </div>
            </div>


            <div class="card-footer">

            </div>
        </div>

        <div class="card">
            <div class="card-header" style="background-color:#007BFF ">
                <h3 class="card-title" style="color: white;">Status Pengusul</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <table>
                        <tbody>
                            <tr>
                                <td>Sinta ID</td>
                                <th style="width: 50px;"></th>
                                <th><?= $lain['sinta_id']; ?></th>
                            </tr>
                            <tr>
                                <td>Status Pegawai</td>
                                <th></th>
                                <th><?php if ($akun['is_active'] == 1) { ?>
                                        Aktif
                                    <?php } else { ?>
                                        Tidak Aktif
                                    <?php } ?></th>
                            </tr>
                            <tr>
                                <td>Program Studi</td>
                                <th></th>
                                <th><?= $penfor['program_studi']; ?></th>
                            </tr>
                            <tr>
                                <td>Scopus ID</td>
                                <th></th>
                                <th><?= $lain['scopus_id']; ?></th>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>


            <div class="card-footer">

            </div>
        </div>

        <div class="card">
            <div class="card-header" style="background-color:#007BFF ">
                <h3 class="card-title" style="color: white;">Template Konten Dokumen Usulan</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <table>
                        <tbody>
                            <tr>
                                <td><img src="<?= base_url('assets/litabmas/word.png') ?>" alt=""></td>
                                <th>Template Konten Dokumen Usulan <br><a href="<?= base_url('assets/litabmas') ?>"> Download</a></th>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer">
                <a href="<?= base_url('litabmas/cek') ?>" type="button" class="btn btn-primary float-right"> <i class="fa fa-arrow-circle-right"></i> Lanjutkan</a>
            </div>
        </div>

    </section>
    <!-- /.content -->





</div>
<!-- /.content-wrapper -->
<!-- Select2 -->
<script src="<?= base_url() ?>assets/vendor/backend/plugins/select2/js/select2.full.min.js"></script>