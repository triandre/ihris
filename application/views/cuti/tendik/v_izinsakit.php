<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $title; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('tendik') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Permohonan</a></li>
                        <li class="breadcrumb-item active">Cuti</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <?php
    $email = $this->session->userdata('email');
    $permohonan = $this->db->query("SELECT * FROM occ_permohonan_cuti where email='$email'");

    ?>


    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <?php if ($this->session->flashdata('gagal_store')) { ?>
                    <div class="alert alert-danger col-md-12">
                        <?= $this->session->flashdata('gagal_store') ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php } ?>

                <?= form_error('username', '<div class="alert alert-danger" role="alert">', '</div>') ?>
                <?= form_error('password', '<div class="alert alert-danger" role="alert">', '</div>') ?>

                <h3 class="card-title">
                    <a href="<?= base_url('tendik/izinsakit/create'); ?>" class="btn btn-block bg-gradient-primary"><i class="fa fa-plus"></i> Tambah</a>
                </h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="example1" class="table table-bordered table-striped table-sm">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Izin Sakit</th>
                                <th>Tanggal Permohonan</th>
                                <th> <i class="fa fa-cog"></i> </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1;
                            foreach ($isku as $prm) : ?>
                                <tr>
                                    <td><?= $i; ?></td>
                                    <td><?= $prm['name']; ?> <br>
                                        <b>Tidak Bekerja : <?= $prm['tidakHadir']; ?> Hari | Dari Tanggal : <?= date("d F Y", strtotime($prm['dariTanggal'])); ?>
                                            <br>Sampai Tanggal:<?= date("d F Y", strtotime($prm['sampaiTanggal'])); ?> | Tanggal Masuk Kerja :<?= date("d F Y", strtotime($prm['tglMasuk'])); ?> </b>
                                        <br><small> Keterangan : <?= $prm['ket']; ?> </small>
                                        <br> Status :
                                        <?php
                                        if ($prm['sts'] == 1) {
                                            echo '<span class="badge badge-info">Permohonan Sedang Diproses</span>';
                                        } elseif ($prm['sts'] == 2) {
                                            echo '<span class="badge badge-success">Permohonan Diverifikasi</span>';
                                        } else {
                                            echo '<span class="badge badge-danger">Permohonan Ditolak</span>';
                                        }
                                        ?>
                                        <br>
                                        <?php
                                        if ($prm['sts'] == '3') { ?>
                                            <p class="text-danger"> Pesaan Penolakan : <?= $prm['pesan']; ?></p>
                                        <?php } ?>
                                        <a href="<?= base_url('assets/scan/'); ?><?= $prm['scan'];  ?>" target="_blank"><span class="badge badge-danger">*Lampiran</span></a>


                                    </td>
                                    <td> <?= date("d F Y", strtotime($prm['tglPengajuan'])); ?></td>
                                    <td>
                                        <div class="dropdown">
                                            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
                                                <i class="fa fa-cog"></i> <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <?php
                                                if ($prm['sts'] == '1') { ?>
                                                    <li><a href="<?= base_url('tendik/izinsakit/ubahIzinSakit/'); ?><?= $prm['id_is']; ?>" class="dropdown-item">Ubah</a></li>
                                                <?php } ?>

                                                <?php
                                                if ($prm['sts'] == '3') { ?>
                                                    <li><a href="<?= base_url('tendik/izinsakit/ubahIzinSakit/'); ?><?= $prm['id_is']; ?>" class="dropdown-item">Perbaikan</a></li>
                                                <?php } ?>

                                            </ul>
                                        </div>

                                    </td>

                                </tr>

                                <?php $i++; ?>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">

            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->

</div>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
    $(function() {
        $("#example1").DataTable({
            "language": {
                "sSearch": "Cari"
            }
        });
    });
</script>