<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $title; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('tendik') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Permohonan</a></li>
                        <li class="breadcrumb-item active">Cuti</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <?php
    $email = $this->session->userdata('email');
    $permohonan = $this->db->query("SELECT * FROM occ_permohonan_cuti where email='$email'");

    ?>


    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <?php if ($this->session->flashdata('gagal_store')) { ?>
                    <div class="alert alert-danger col-md-12">
                        <?= $this->session->flashdata('gagal_store') ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php } ?>

                <?= form_error('username', '<div class="alert alert-danger" role="alert">', '</div>') ?>
                <?= form_error('password', '<div class="alert alert-danger" role="alert">', '</div>') ?>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">

                <form class="form-horizontal">
                    <div class="card-body">
                        <div class="table-responsive">
                            <div class="form-group">
                                <label for="pwd">Pilih Form Cuti:</label>
                                <select class="form-control show-tick" name="form" onchange="location = this.value;">
                                    <option selected disabled value="">Pilih Form</option>
                                    <option value="<?= base_url('tendik/cuti/umum'); ?>">Form Cuti Umum</option>
                                    <option value="<?= base_url('tendik/cuti/umroh'); ?>">Form Cuti Umrah</option>
                                    <option value="<?= base_url('tendik/cuti/hamil'); ?>">Form Cuti Hamil</option>
                                    <option value="<?= base_url('tendik/cuti/sakit'); ?>">Form Izin Sakit</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
        <!-- Default box -->
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title">Permohonan Cuti Umum</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-danger col-md-12 alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?= validation_errors(); ?>
                    </div>
                <?php endif ?>


                <form class="form-horizontal" action="<?= base_url('tendik/cuti/umum'); ?>" enctype="multipart/form-data" autocomplete="off" method="post">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tbody>

                                    <tr>
                                        <td>Email <span style="color:red;">*</span> </td>
                                        <td><input readonly class="form-control" type="text" id="email" name="email" value="<?php echo $this->session->userdata('email'); ?>" required readonly>
                                            <?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?></td>
                                    </tr>

                                    <tr>
                                        <td>Lamanya Cuti <span style="color:red;">*</span> </td>
                                        <td>
                                            <select class="form-control show-tick" id="cutiDiambil" name="cutiDiambil">
                                                <option selected disabled value="">Pilih Form Cuti</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                            </select>
                                            <?= form_error('cutiDiambil', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </td>
                                    </tr>


                                    <tr>
                                        <td>Hak Cuti <span style="color:red;">*</span> </td>
                                        <td><input type="number" class="form-control" id="stokCuti" name="stokCuti" value="<?= $hak['cutiUmum']; ?>" readonly>
                                            <?= form_error('stokCuti', '<small class="text-danger pl-3">', '</small>'); ?> </td>
                                    </tr>

                                    <tr>
                                        <td>Tanggal Mulai Cuti <span style="color:red;">*</span> </td>
                                        <td><input type="date" class="form-control" id="tglCuti" name="tglCuti" value="<?= set_value('tglCuti'); ?>">
                                            <?= form_error('tglCuti', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Tanggal Selesai Cuti <span style="color:red;">*</span> </td>
                                        <td><input type="date" class="form-control" id="tglSelesaiCuti" name="tglSelesaiCuti" value="<?= set_value('tglSelesaiCuti'); ?>">
                                            <?= form_error('tglSelesaiCuti', '<small class="text-danger pl-3">', '</small>'); ?> </td>
                                    </tr>

                                    <tr>
                                        <td>Tanggal Masuk Kerja <span style="color:red;">*</span> </td>
                                        <td><input type="date" class="form-control" id="tglMasuk" name="tglMasuk" value="<?= set_value('tglMasuk'); ?>">
                                            <?= form_error('tglMasuk', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Surat Izin Pimpinan Biro/fakultas/ Lembaga/Unit/Pusat<span style="color:red;">*</span> </td>
                                        <td><input type="file" class="form-control" id="scan" name="scan" accept="image/gif, image/jpeg, image/png">
                                            <?= form_error('scan', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Catatan<span style="color:red;">*</span> </td>
                                        <td><textarea class="form-control" type="text" id="alasan" name="alasan" rows="5"></textarea> </td>
                                    </tr>


                                </tbody>
                            </table>
                        </div>


                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <a href="javascript:history.back()" class="btn btn-danger"> <i class="fa fa-backward"></i> Kembali</a>
                        <button type="submit" class="btn btn-primary"> <i class="fa fa-save"></i> Simpan</button>
                    </div>
                    <!-- /.card-footer -->
                </form>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">

            </div>
            <!-- /.card-footer-->
        </div>

    </section>
    <!-- /.content -->

</div>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
    $(function() {
        $("#example1").DataTable({
            "language": {
                "sSearch": "Cari"
            }
        });
    });
</script>