<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $title; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('tendik') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Permohonan</a></li>
                        <li class="breadcrumb-item active">Cuti</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>

    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <?php if ($this->session->flashdata('gagal_store')) { ?>
                    <div class="alert alert-danger col-md-12">
                        <?= $this->session->flashdata('gagal_store') ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php } ?>

                <?= form_error('username', '<div class="alert alert-danger" role="alert">', '</div>') ?>
                <?= form_error('password', '<div class="alert alert-danger" role="alert">', '</div>') ?>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="example1" class="table table-bordered table-striped table-sm">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Permohonan Cuti</th>
                                <th>Aksi</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1;
                            foreach ($cutiku as $cutku) : ?>
                                <tr>
                                    <td><?= $i; ?></td>
                                    <td> <a href="<?= base_url('profil/viewUser/'); ?><?= $cutku['email']; ?>"> <?= $cutku['name']; ?> </a> - <span class="badge badge-primary">Jenis Cuti: <?= $cutku['jenisCuti']; ?> </span> <br>
                                        <b>Lamanya Cuti : <?= $cutku['cutiDiambil']; ?> Hari | Tanggal Mulai Cuti : <?= date("d F Y", strtotime($cutku['tglCuti'])); ?>
                                            <br>Tanggal Selesai Cuti :<?= date("d F Y", strtotime($cutku['tglSelesaiCuti'])); ?> | Tanggal Masuk Kerja :<?= date("d F Y", strtotime($cutku['tglMasuk'])); ?> </b>
                                        <br><small> Alasan : <?= $cutku['alasan']; ?> </small>
                                        <br> Status :
                                        <?php
                                        if ($cutku['sts'] == 1) {
                                            echo '<span class="badge badge-primary">Permohonan Terkirim</span>';
                                        } elseif ($cutku['sts'] == 2) {
                                            echo '<span class="badge badge-info">Permohonan Sedang Diproses</span>';
                                        } elseif ($cutku['sts'] == 3) {
                                            echo '<span class="badge badge-success">Permohonan Disetujui</span>';
                                        } elseif ($cutku['sts'] == 4) {
                                            echo '<span class="badge badge-warning">Permohonan Ditolak</span>';
                                        } elseif ($cutku['sts'] == 5) {
                                            echo '<span class="badge badge-danger">Membatalkan Permohonan</span>';
                                        }
                                        ?>
                                        <br>
                                        <?php
                                        if ($cutku['sts'] == '4') { ?>
                                            <p class="text-danger"> Pesaan Penolakan : <?= $cutku['pesan']; ?></p>
                                        <?php } ?>
                                        <a href="<?= base_url('assets/scan/'); ?><?= $cutku['scan'];  ?>" target="_blank"><span class="badge badge-danger">*Lampiran</span></a>
                                    </td>

                                    <td>

                                        <div class="dropdown">
                                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                                <i class="fa fa-cog"></i>
                                            </button>
                                            <div class="dropdown-menu">
                                                <a href="<?= base_url('muser/detail/' . $cutku['email']) ?>" class="dropdown-item">Detail Profil</a>
                                                <?php if ($cutku['sts'] == '2') { ?>
                                                    <a href="<?= base_url('pimpinan/cutiSetujui/' . $cutku['id_cuti']) ?>" class="dropdown-item">Setuju</a>
                                                    <a href="<?= base_url('pimpinan/cutiTolak/' . $cutku['id_cuti'] . '/' . $cutku['email']) ?>" class="dropdown-item">Tolak</a>
                                                <?php } ?>
                                            </div>
                                        </div>

                                    </td>

                                </tr>

                                <?php $i++; ?>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">

            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->

</div>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
    $(function() {
        $("#example1").DataTable({
            "language": {
                "sSearch": "Cari"
            }
        });
    });
</script>