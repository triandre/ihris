<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $title; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dosen') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Permohonan Cuti</a></li>
                        <li class="breadcrumb-item active">Penolakan</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>
    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title"><?= $title; ?></h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-danger col-md-8 alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?= validation_errors(); ?>
                    </div>
                <?php endif ?>


                <form class="form-horizontal" action="<?= base_url('admin/cutiTolakGo'); ?>" enctype="multipart/form-data" autocomplete="off" method="post">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tbody>

                                    <tr>
                                        <td>Reff. ID Cuti <span style="color:red;">*</span> </td>
                                        <td><input readonly class="form-control" type="text" id="id_cuti" name="id_cuti" value="<?= $d['id_cuti']; ?>" required readonly> </td>
                                    </tr>

                                    <tr>
                                        <td>Email <span style="color:red;">*</span> </td>
                                        <td><input type="text" class="form-control" id="email" name="email" value="<?= $d['email']; ?>" readonly>
                                            <?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?> </td>
                                    </tr>

                                    <tr>
                                        <td>Jenis Cuti <span style="color:red;">*</span> </td>
                                        <td><input type="text" class="form-control" id="jenisCuti" name="jenisCuti" value="<?= $d['jenisCuti']; ?>" readonly>
                                            <?= form_error('jenisCuti', '<small class="text-danger pl-3">', '</small>'); ?> </td>
                                    </tr>

                                    <tr>
                                        <td>Hak Cuti <span style="color:red;">*</span> </td>
                                        <td><input type="number" class="form-control" id="stokCuti" name="stokCuti" value="<?= $hak['cutiUmum']; ?>" readonly>
                                            <?= form_error('stokCuti', '<small class="text-danger pl-3">', '</small>'); ?> </td>
                                    </tr>

                                    <tr>
                                        <td>Cuti Di Ambil <span style="color:red;">*</span> </td>
                                        <td><input type="number" class="form-control" id="cutiDiambil" name="cutiDiambil" value="<?= $d['cutiDiambil']; ?>" readonly>
                                            <?= form_error('cutiDiambil', '<small class="text-danger pl-3">', '</small>'); ?> </td>
                                    </tr>

                                    <tr>
                                        <td>Tanggal Mulai Cuti <span style="color:red;">*</span> </td>
                                        <td><input type="date" class="form-control" id="tglCuti" name="tglCuti" value="<?= $d['tglCuti']; ?>" readonly>
                                            <?= form_error('tglCuti', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Tanggal Selesai Cuti <span style="color:red;">*</span> </td>
                                        <td><input type="date" class="form-control" id="tglSelesaiCuti" name="tglSelesaiCuti" value="<?= $d['tglCuti']; ?>" readonly>
                                            <?= form_error('tglSelesaiCuti', '<small class="text-danger pl-3">', '</small>'); ?> </td>
                                    </tr>

                                    <tr>
                                        <td>Tanggal Masuk Kerja <span style="color:red;">*</span> </td>
                                        <td><input type="date" class="form-control" id="tglMasuk" name="tglMasuk" value="<?= $d['tglMasuk']; ?>" readonly>
                                            <?= form_error('tglMasuk', '<small class="text-danger pl-3">', '</small>'); ?>
                                        </td>
                                    </tr>


                                    <tr>
                                        <td>Catatan Untuk Pemohon <span style="color:red;">*</span> </td>
                                        <td><textarea class="form-control" type="text" id="pesan" name="pesan" rows="5" required></textarea> </td>
                                    </tr>


                                </tbody>
                            </table>
                        </div>


                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">

                        <a href="javascript:history.back()" class="btn btn-danger"> <i class="fa fa-backward"></i> Kembali</a>

                        <button type="submit" class="btn btn-primary"> <i class="fa fa-save"></i> Kirim</button>
                    </div>
                    <!-- /.card-footer -->
                </form>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">

            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->





</div>
<!-- /.content-wrapper -->
<!-- Select2 -->
<script src="<?= base_url() ?>assets/vendor/backend/plugins/select2/js/select2.full.min.js"></script>
<script src="<?= base_url() ?>assets/js/rupiah.js"></script>
<script src="<?= base_url() ?>assets/js/duit.js"></script>