<link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.3.5/css/buttons.dataTables.min.css">



<link rel="stylesheet" href="https://cdn.datatables.net/1.13.3/css/jquery.dataTables.min.css">



<!-- Content Wrapper. Contains page content -->



<div class="content-wrapper">



    <!-- Content Header (Page header) -->



    <section class="content-header">



        <div class="container-fluid">



            <div class="row mb-2">



                <div class="col-sm-6">



                    <h1><?= $title; ?></h1>



                </div>



                <div class="col-sm-6">



                    <ol class="breadcrumb float-sm-right">



                        <li class="breadcrumb-item">Dashboard</a></li>



                        <li class="breadcrumb-item"><a href="#">Report Presensi</a></li>



                    </ol>



                </div>



            </div>



        </div><!-- /.container-fluid -->



    </section>











    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>



    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>



    <!-- Main content -->



    <section class="content">







        <!-- Default box -->



        <div class="card card-primary card-outline">



            <div class="card-header">



                <h3 class="card-title"><?= $title; ?></h3>







                <div class="card-tools">



                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"

                        title="Collapse">



                        <i class="fas fa-minus"></i></button>



                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip"

                        title="Remove">



                        <i class="fas fa-times"></i></button>



                </div>



            </div>



            <div class="card-body">



                <?php if (validation_errors()) : ?>



                <div class="alert alert-danger col-md-8 alert-dismissible">



                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>



                    <?= validation_errors(); ?>



                </div>



                <?php endif ?>







                <form action="<?= base_url('adminkeu/v_lapPresensi'); ?>" method="post" enctype="multipart/form-data">



                    <div class="form-group">



                        <div class="form-group row">







                            <div class="col-sm-4">



                                <label>Bulan</label>



                                <select id="bulan" name="bulan" class="form-control selectx" required>



                                    <option selected disabled value="">Pilih</option>



                                    <option value="01">Januari</option>



                                    <option value="02">Februari</option>



                                    <option value="03">Maret</option>



                                    <option value="04">April</option>



                                    <option value="05">Mei</option>



                                    <option value="06">Juni</option>



                                    <option value="07">Juli</option>



                                    <option value="08">Agustus</option>



                                    <option value="09">September</option>



                                    <option value="10">Oktober</option>



                                    <option value="11">November</option>



                                    <option value="12">Desember</option>



                                </select>



                                <?= form_error('bulan', '<small class="text-danger pl-3">', '</small>'); ?>



                            </div>



                            <div class="col-sm-4">



                                <label>tahun</label>



                                <select style="color: black;" class="form-control" id="tahun" name="tahun" required>



                                    <option selected disabled value="">Pilih</option>



                                    <option value="2023">2023</option>



                                    <option value="2024">2024</option>



                                    <option value="2025">2025</option>



                                </select>



                                <?= form_error('batch', '<small class="text-danger pl-3">', '</small>'); ?>



                            </div>







                        </div>



                    </div>



                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Lihat Data!</button>



                </form>



            </div>



            <!-- /.card-body -->



        </div>



        <!-- /.card -->







        <div class="card card-primary card-outline">



            <div class="card-header">



                <div class="card-tools">



                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"

                        title="Collapse">



                        <i class="fas fa-minus"></i></button>



                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip"

                        title="Remove">



                        <i class="fas fa-times"></i></button>



                </div>



            </div>



            <div class="card-body">



                <table id="example" class="table table-bordered table-striped table-sm">



                    <thead>



                        <tr>

                            <th>No.</th>

                            <th>Payroll</th>

                            <th>NIDN</th>

                            <th>Nama</th>

                            <th>Total Penerimaan</th>

                            <th>P.SD</th>

                            <th>P PPH</th>

                            <th>ZIS</th>

                            <th>Jumlah Potongan</th>

                            <th>Total Bersih</th>

                        </tr>



                    </thead>



                    <tbody>



                        <?php



                        $no = 1;



                        foreach ($v as $row) : ?>



                        <tr>



                            <td><?= $no++; ?></td>

                            <td><?= $row['payrol']; ?></td>

                            <td><?= $row['nidn']; ?></td>

                            <td><?= $row['name']; ?>, <?= $row['gelar']; ?></td>

                            <td><?= number_format($row['jumlah_penerimaan']); ?></td>

                            <td><?= number_format($row['sarana_dakwah']); ?></td>

                            <td><?php $a =  $row['pph_psl'];

                                    $b = $row['jumlah_penerimaan'];

                                    $c = $b * $a; ?>

                                <?= number_format($c); ?>

                            </td>

                            <td>

                                <?php $z =  $row['p_zis'];

                                    $y = $row['jumlah_penerimaan'];

                                    $w = $z * $y; ?>

                                <?= number_format($w); ?>

                            </td>



                            <td>

                                <?php

                                    $sd =  $row['sarana_dakwah'];

                                    $pphpsl = $c;

                                    $pzis = $w;

                                    $jpot = $sd + $pphpsl + $pzis; ?>

                                <?= number_format($jpot); ?>

                            </td>

                            <td>

                                <?php

                                    $tb = $row['jumlah_penerimaan'] - $jpot;

                                    ?>

                                <b><?= number_format($tb); ?></b>



                            </td>



                        </tr>



                        <?php endforeach ?>



                    </tbody>



                </table>



            </div>



            <!-- /.card-body -->



        </div>



        <!-- /.card -->



    </section>



    <!-- /.content -->







</div>



<script src="https://code.jquery.com/jquery-3.5.1.js"></script>



<script src="https://cdn.datatables.net/1.13.3/js/jquery.dataTables.min.js"></script>



<script src="https://cdn.datatables.net/buttons/2.3.5/js/dataTables.buttons.min.js"></script>



<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>



<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>



<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>



<script src="https://cdn.datatables.net/buttons/2.3.5/js/buttons.html5.min.js"></script>



<script src="https://cdn.datatables.net/buttons/2.3.5/js/buttons.print.min.js"></script>



<script>

$(document).ready(function() {



    $('#example').DataTable({



        dom: 'Bfrtip',



        buttons: [



            'excel', 'pdf'



        ]



    });



});

</script>