<link rel="stylesheet"

    href="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/css/dataTables.bootstrap4.css">



<div class="content-wrapper">



    <!-- Content Header (Page header) -->



    <section class="content-header">



        <div class="container-fluid">



            <div class="row mb-2">



                <div class="col-sm-6">



                    <h1><?= $title; ?></h1>



                </div>



                <div class="col-sm-6">



                    <ol class="breadcrumb float-sm-right">



                        <li class="breadcrumb-item"><a href="<?= base_url('dosen') ?>">Dashboard</a></li>



                        <li class="breadcrumb-item active"><a href="#">Kehadiran Dosen</a></li>



                    </ol>



                </div>



            </div>



        </div><!-- /.container-fluid -->



    </section>







    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>



    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>







    <!-- Main content -->



    <section class="content">







        <!-- Default box -->



        <div class="card">



            <div class="card-header">



                <?php



                if ($this->session->flashdata('gagal_store')) { ?>



                <div class="alert alert-danger col-md-12">



                    <?= $this->session->flashdata('gagal_store') ?>



                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">



                        <span aria-hidden="true">&times;</span>



                    </button>



                </div>



                <?php } ?>







                <?= form_error('username', '<div class="alert alert-danger" role="alert">', '</div>') ?>



                <?= form_error('password', '<div class="alert alert-danger" role="alert">', '</div>') ?>



                <h3 class="card-title">







                </h3>







                <div class="card-tools">



                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"

                        title="Collapse">



                        <i class="fas fa-minus"></i></button>



                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip"

                        title="Remove">



                        <i class="fas fa-times"></i></button>



                </div>



            </div>



            <div class="card-body">

                <div class="table-responsive">

                    <table id="example1" class="table table-bordered table-striped table-sm">

                        <thead>

                            <tr>

                                <th>No.</th>

                                <th>Nama</th>

                                <th>Matakuliah</th>

                                <th>Kelas</th>

                                <th>Sks</th>

                                <th>Kehadiran</th>

                                <th>Trsanport</th>

                                <th>Honor</th>

                                <th>Aksi</th>

                            </tr>

                        </thead>

                        <tbody>

                            <?php

                            $no = 1;

                            foreach ($v as $row) : ?>

                            <tr>

                                <td><?= $no++; ?></td>

                                <td><?= $row['name']; ?>, <?= $row['gelar']; ?></td>

                                <td><?= $row['matakuliah']; ?></td>

                                <td><?= $row['kelas']; ?> - <?= $row['uraian']; ?></td>

                                <td><?= $row['sks_keu']; ?></td>

                                <td><?= $row['jumlah_kehadiran_keu']; ?></td>

                                <td><?= number_format($row['transport']); ?></td>

                                <td><?php 

                                $ssks=$row['sks_keu']; 

                                $sjk = $row['jumlah_kehadiran_keu'];  

                                $starif=$row['tarif']; 

                                $strans = $row['transport'] * $row['jumlah_kehadiran'];  

                                $sjum = $ssks * $sjk * $starif;

                                $stot = $sjum + $strans;

                                ?>

                                    <?= number_format($stot); ?>

                                </td>

                                <td>

                                    <div class="dropdown">

                                        <a href="" class="btn btn-danger btn-sm" title="Edit" data-toggle="modal"

                                            data-target="#myModal<?php echo $row['id_reff']; ?>">

                                            <i class="fas fa-edit"></i>

                                        </a>

                                    </div>

                                </td>

                            </tr>

                            <!-- The Modal -->

                            <div class="modal" id="myModal<?php echo $row['id_reff']; ?>" tabindex="-1" role="dialog"

                                aria-labelledby="editModalLabel" aria-hidden="true">

                                <div class="modal-dialog modal-lg">

                                    <div class="modal-content">



                                        <!-- Modal Header -->

                                        <div class="modal-header">

                                            <h4 class="modal-title">Ubah SKS Dan Kehadiran</h4>

                                            <button type="button" class="close" data-dismiss="modal">&times;</button>

                                        </div>



                                        <!-- Modal body -->

                                        <div class="modal-body">

                                            <form action="<?= base_url('adminkeu/editSksKehadiran'); ?>" method="post">

                                                <div class="form-group">

                                                    <input type="hidden" class="form-control"

                                                        id="id_reff<?php echo $row['id_reff']; ?>" name="id_reff"

                                                        value="<?php echo $row['id_reff']; ?>" required readonly>

                                                </div>

                                                <div class="form-group">

                                                    <input type="hidden" class="form-control"

                                                        id="nidn<?php echo $row['id_reff']; ?>" name="nidn"

                                                        value="<?php echo $row['nidn']; ?>" required readonly>

                                                </div>

                                                <div class="form-group">

                                                    <label for="email">Nama Dosen:</label>

                                                    <input type="text" class="form-control"

                                                        id="name<?php echo $row['id_reff']; ?>" name="name"

                                                        value="<?php echo $row['name']; ?>" required readonly>

                                                </div>

                                                <div class="form-group">

                                                    <label for="email">Matakuliah:</label>

                                                    <input type="text" class="form-control"

                                                        id="matakuliah<?php echo $row['id_reff']; ?>" name="matakuliah"

                                                        value="<?php echo $row['matakuliah']; ?>" required readonly>

                                                </div>

                                                <div class="form-group">

                                                    <label for="email">SKS:</label>

                                                    <input type="text" class="form-control"

                                                        id="sks_keu<?php echo $row['id_reff']; ?>" name="sks_keu"

                                                        value="<?php echo $row['sks_keu']; ?>">

                                                </div>

                                                <div class="form-group">

                                                    <label for="email">Kehadiran:</label>

                                                    <input type="text" class="form-control"

                                                        id="jumlah_kehadiran_keu<?php echo $row['id_reff']; ?>"

                                                        name="jumlah_kehadiran_keu"

                                                        value="<?php echo $row['jumlah_kehadiran_keu']; ?>">

                                                </div>



                                        </div>



                                        <!-- Modal footer -->

                                        <div class="modal-footer">

                                            <button type="submit" class="btn btn-success">Update</button>

                                            <button type="button" class="btn btn-danger"

                                                data-dismiss="modal">Close</button>

                                        </div>

                                        </form>

                                    </div>

                                </div>

                            </div>

                            <?php endforeach ?>

                        </tbody>

                        <tfoot>

                            <th colspan="4">

                                <center>T O T A L<center>

                            </th>

                            <th><?= $jsks['jsks']; ?></th>

                            <th><?= $hadir['jkehadiran']; ?></th>

                            <th><?= $transport['jtransport']; ?></th>

                            <th colspan="2"> <?= number_format($tarif['honor']); ?> </th>



                        </tfoot>

                    </table>

                    <b>Catatan:</b><br>

                    <p><b>Beban SKS : <?= $d['beban_sks']; ?>

                            <br>

                            TARIF PER SKS : <?= number_format($d['tarif']); ?> </b></p>



                    <a href="<?= base_url('adminkeu/listPresensi_fai'); ?>" class="btn btn-danger"> <i

                            class="fa fa-backward"></i>

                        Kembali</a>



                </div>

            </div>

    </section>

    <!-- /.content -->





</div>

<script src=" <?= base_url() ?>assets/vendor/backend/plugins/datatables/jquery.dataTables.js">

</script>

<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>

<script>

$(function() {

    $("#example1").DataTable({

        "language": {

            "sSearch": "Cari"

        }

    });

});

</script>

<script>

$(document).ready(function() {

    <?php foreach ($v as $row): ?>

    $('#myModal<?php echo $row['id_reff']; ?>').on('show.bs.modal', function(e) {

        $('#id_reff<?php echo $row['id_reff']; ?>').val('<?php echo $row['id_reff']; ?>');

        $('#nidn<?php echo $row['id_reff']; ?>').val('<?php echo $row['nidn']; ?>');

        $('#name<?php echo $row['id_reff']; ?>').val('<?php echo $row['name']; ?>');

        $('#matakuliah<?php echo $row['id_reff']; ?>').val('<?php echo $row['matakuliah']; ?>');

        $('#sks_keu<?php echo $row['id_reff']; ?>').val('<?php echo $row['sks_keu']; ?>');

        $('#jumlah_kehadiran<?php echo $row['id_reff']; ?>').val(

            '<?php echo $row['jumlah_kehadiran']; ?>');

    });

    <?php endforeach; ?>

});

</script>