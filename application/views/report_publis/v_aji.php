<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $title; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Tacking</a></li>
                        <li class="breadcrumb-item active">UI. Publikasi Jurnal</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>

    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title"><?= $title; ?></h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-danger col-md-8 alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?= validation_errors(); ?>
                    </div>
                <?php endif ?>

                <form action="<?= base_url('reportpublis/cariData'); ?>" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <div class="form-group row">

                            <div class="col-sm-4">
                                <label>Jenis Insentif</label>
                                <select id="jenis_insentif" name="jenis_insentif" class="form-control selectx" required>
                                    <option selected disabled value="">Pilih</option>
                                    <?php foreach ($jenis as $row2) : ?>
                                        <option value="<?= $row2['id']; ?>"><?= $row2['jenis']; ?> </option>
                                    <?php endforeach ?>
                                </select>
                                <?= form_error('jenis_insentif', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                            <div class="col-sm-4">
                                <label>Batch</label>
                                <select style="color: black;" class="form-control" id="batch" name="batch" required>
                                    <option selected disabled value="">Pilih</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
<option value="7">7</option>
 <option value="8">8</option>
 <option value="10">9</option>
 <option value="10">10</option>

                                </select>
                                <?= form_error('batch', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-print"></i> Lihat Data!</button>
                </form>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <?php if ($this->session->flashdata('gagal_store')) { ?>
                    <div class="alert alert-danger col-md-12">
                        <?= $this->session->flashdata('gagal_store') ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php } ?>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <?= $this->session->flashdata('message'); ?>
                <a href="<?= base_url('reportpublis/pdf_Aji/') . $this->input->post('batch') ?>" target="_blank" class="btn btn-danger">
                    <i class="fa fa-file-pdf" aria-hidden="true"></i> Convert To PDF
                </a>
                <a href="<?= base_url('reportpublis/excel_Aji/') . $this->input->post('batch') ?>" class="btn btn-success">
                    <i class="fa fa-file-excel" aria-hidden="true"></i> Export Excel
                </a>
                <br><br>
                <div class="table-responsive">
                    <table id="example1" class="table table-bordered table-striped table-sm">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Permohonan Insentif Artikel Dijurnal</th>
                                <th>Insentif</th>
                                <th class="text-center">Batch</th>
                                <th class="text-center" style="width:5%;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            <?php foreach ($v as $aj) : ?>
                                <tr>
                                    <td style="width:5%;" class="text-center" scope="row"><?= $i; ?></td>
                                    <td style="width:70%;"><small>Tanggal permohonan, <?= date("d F Y", strtotime($aj['date_created'])) ?></small><br>
                                        <?= $aj['judul']; ?> - <?= $aj['nama_jurnal']; ?> <br>
                                        <small><?= $aj['kategori_jurnal']; ?></small> <br>
                                        <small><?= $aj['lembaga_pengindeks']; ?> - <?= $aj['eissn']; ?> <br>
                                            <small>Status : <?php
                                                            if ($aj['sts'] == 1) {
                                                                echo '<span class="badge badge-info">Proses LP2M</span> ';
                                                            } elseif ($aj['sts'] == 2) {
                                                                echo '<span class="badge badge-primary">Proses Wakil Rektor I</span>';
                                                            } elseif ($aj['sts'] == 3) {
                                                                echo '<span class="badge badge-warning">Proses Wakil Rektor II</span>';
                                                            } elseif ($aj['sts'] == 4) {
                                                                echo '<span class="badge badge-success">Di Terima</span>';
                                                            } elseif ($aj['sts'] == 6) {
                                                                echo '<span class="badge badge-danger">Berkas DiTolak</span>';
                                                            }
                                                            ?> </small> <br>
                                            <?php
                                            if ($akun['role_id'] >= '3') { ?>
                                                Standart Pagu Anggaran Lembaga Pengindeks: <b><?= $aj['lembaga_pengindeks']; ?> = Rp. <?= number_format($aj['pagu']);  ?> </b> <br>
                                                Insentif Diajukan LP2M = <b> Rp. <?= number_format($aj['insentif']); ?> </b> <br>

                                                <a href="<?= base_url('user/detail/'); ?><?= $aj['email']; ?>"><i>Pemohon:<?= $aj['email']; ?></i></a>
                                            <?php } ?>
                                        </small>
                                        <br>
                                        <small> <?php
                                                if ($aj['sts'] == '6') { ?>
                                                Catatan : <?= $aj['ket']; ?>
                                            <?php } ?></small>
                                    </td>
                                    <td style="width:5%;" class="text-center"> <?= number_format($aj['insentif_disetujui']); ?></td>
                                    <td style="width:5%;" class="text-center"> <?= $aj['batch']; ?></td>
                                    <td style="width:10%;" class="text-center">
                                        <div class="dropdown">
                                            <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
                                                <i class="fa fa-cog"></i>
                                            </button>
                                            <div class="dropdown-menu">
                                                <a href="<?= base_url('vaji/detail/'); ?><?= $aj['id']; ?>" class="dropdown-item">Detail</a>
                                                <a href="<?= base_url('vaji/history/'); ?><?= $aj['email']; ?>" class="dropdown-item">History</a>
                                            </div>
                                        </div>

                                    </td>


                                </tr>
                                <?php $i++; ?>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">

            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->

</div>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
    $(function() {
        $("#example1").DataTable({
            "language": {
                "sSearch": "Cari"
            }
        });
    });
</script>