<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Detail Usulan Insentif Artikel Di Jurnal</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('home') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Permohonan Insentif</a></li>
                        <li class="breadcrumb-item">Usulan Insentif Artikel Di Jurnal</a></li>
                        <li class="breadcrumb-item active">Detail</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Detail Usulan</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <h5>A. IDENTITAS ARTIKEL</h5>

                <table class="table table-striped" id="users">
                    <tbody>
                        <tr>
                            <td width="100px">Email</td>
                            <td width="50px">:</td>
                            <td><?= $d['email'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Judul Artikel</td>
                            <td width="50px">:</td>
                            <td><?= $d['judul']; ?> </td>
                        </tr>
                        <tr>
                            <td width="200px">Nama Penulis</td>
                            <td width="50px">:</td>
                            <td><?= $d['nama_penulis'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Volume/Nomor/Tahun</td>
                            <td width="50px">:</td>
                            <td><?= $d['vnt'] ?></td>
                        </tr>

                        <tr>
                            <td width="100px">Kontribusi</td>
                            <td width="50px">:</td>
                            <td><?= $d['kontribusi'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Nama Jurnal</td>
                            <td width="50px">:</td>
                            <td><?= $d['nama_jurnal'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">EISSN</td>
                            <td width="50px">:</td>
                            <td><?= $d['eissn'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Kategori Jurnal</td>
                            <td width="50px">:</td>
                            <td><?= $d['kategori_jurnal'] ?></td>
                        </tr>

                        <tr>
                            <td width="100px">Lembaga Pengindeks</td>
                            <td width="50px">:</td>
                            <td><?= $d['lembaga_pengindeks'] ?></td>
                        </tr>


                        <tr>
                            <td width="100px">Kategori Jurnal</td>
                            <td width="50px">:</td>
                            <td><?= $d['kategori_jurnal'] ?></td>
                        </tr>


                        <tr>
                            <td width="100px">Peringkat Jurnal</td>
                            <td width="50px">:</td>
                            <td><?= $d['peringkat_jurnal'] ?></td>
                        </tr>

                        <tr>
                            <td width="100px">URL Jurnal</td>
                            <td width="50px">:</td>
                            <td><a href="<?= $d['url_jurnal']; ?>" target="_blank"><?= $d['url_jurnal']; ?></a></td>
                        </tr>

                    </tbody>
                </table>
                <hr>

                <h5>B. Faktor dampak/Impact Factor (IF)/SJR Jurnal (Untuk jurnal bereputasi Scopus)</h5>

                <table class="table table-striped" id="users">
                    <tbody>
                        <tr>
                            <td width="500px">URL Impact Factor/SJR</td>
                            <td width="50px">:</td>
                            <td><?= $d['url_sjr'] ?></td>
                        </tr>
                        <tr>
                            <td width="500px">Nilai Impact Faktor/SJR:</td>
                            <td width="50px">:</td>
                            <td><?= $d['nilai_sjr']; ?> </td>
                        </tr>

                    </tbody>
                </table>
                <hr>
                <h5>C. Identitas Penelitian yang Menghasilkan artikel</h5>
                <table class="table table-striped" id="users">
                    <tbody>
                        <tr>
                            <td width="300px">Nama Program</td>
                            <td width="50px">:</td>
                            <td><?= $d['nama_program'] ?></td>
                        </tr>
                        <tr>
                            <td width="300px">Nomor Kontrak</td>
                            <td width="50px">:</td>
                            <td><?= $d['nomor_kontrak']; ?> </td>
                        </tr>
                        <tr>
                            <td width="300px">NApakah Artikel yang diterbitkan Direview</td>
                            <td width="50px">:</td>
                            <td><?= $d['pertanyaan1']; ?> </td>
                        </tr>
                        <tr>
                            <td width="300px">Dokumen Artikel</td>
                            <td width="50px">:</td>
                            <td><a href="<?= base_url('file/'); ?><?= $d['upload_artikel']; ?>" target="_blank"><i class="fa fa-download"></i></a> </td>
                        </tr>
                        <tr>
                            <td width="300px">Dokumen Bukti Artikel</td>
                            <td width="50px">:</td>
                            <td><a href="<?= base_url('file/'); ?><?= $d['upload_bukti_review']; ?>" target="_blank"><i class="fa fa-download"></i></a> </td>
                        </tr>



                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <a href="javascript:history.back()" type="button" class="btn bg-gradient-danger"><i class="fa fa-backward"></i> Kembali</a>
            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->