<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h5><?= $title; ?></h5>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dosen') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Permohonan Insentif</a></li>
                        <li class="breadcrumb-item active">UB. Biaya Publikasi</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <?php if ($this->session->flashdata('gagal_store')) { ?>
                    <div class="alert alert-danger col-md-12">
                        <?= $this->session->flashdata('gagal_store') ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php } ?>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <?= $this->session->flashdata('message'); ?>
                <div class="table-responsive">
                    <table class="table table-bordered" id="example1" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Permohonan Bantuan Biaya Publikasi</th>
                                <th>File Permohonan</th>
                                <?php if ($akun['role_id'] == '4') { ?>
                                    <th>Catatan Validator</th>
                                <?php } ?>
                                <?php if ($akun['role_id'] == '5') { ?>
                                    <th>Catatan WR I</th>
                                <?php } ?>
                                <th>#</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            <?php foreach ($v as $aj) : ?>
                                <tr>
                                    <th style="width:5%;" scope="row"><?= $i; ?></th>
                                    <td style="width:60%;"><small>Tanggal Permohonan, <?= date("d F Y", strtotime($aj['date_created'])) ?></small>
                                        <br>
                                        <?= $aj['judul_artikel']; ?> - <?= $aj['nama_jurnal']; ?>
                                        <br>
                                        <small>Lembaga Pengindeks : <?= $aj['lembaga_pengindeks']; ?></small>
                                        <br>
                                        <small><i>ISSN : <?= $aj['issn']; ?></i></small>
                                        <br>
                                        <small><i>Status : <?php
                                                            if ($aj['sts'] == 1) {
                                                                echo '<span class="badge badge-info">Proses LP2M</span> ';
                                                            } elseif ($aj['sts'] == 2) {
                                                                echo '<span class="badge badge-primary">Proses Wakil Rektor I</span>';
                                                            } elseif ($aj['sts'] == 3) {
                                                                echo '<span class="badge badge-warning">Proses Wakil Rektor II</span>';
                                                            } elseif ($aj['sts'] == 4) {
                                                                echo '<span class="badge badge-success">Di Terima</span>';
                                                            } elseif ($aj['sts'] == 6) {
                                                                echo '<span class="badge badge-danger">Berkas DiTolak</span>';
                                                            }  ?></i></small>

                                        <br>
                                        <small>Biaya Publikasi Ditulis: Rp. <?= number_format($aj['biaya_publikasi']); ?></small>
                                        <br>
                                        <?php
                                        if ($akun['role_id'] >= '3') { ?>
                                            <small>Standart Pagu Anggaran Lembaga Pengindeks: <b><?= $aj['lembaga_pengindeks']; ?> = Rp. <?= number_format($aj['pagu']);  ?> </b> <br>
                                                Insentif Diajukan= <b> Rp. <?= number_format($aj['insentif']); ?> </b>
                                            </small> <br>
                                            <a href="<?= base_url('user/detail/'); ?><?= $aj['email']; ?>"><small><i>Pemohon:<?= $aj['email']; ?></i></small></a>
                                        <?php } ?>
                                        <br>
                                        <?php
                                        if ($aj['sts'] == '6') { ?>
                                            <small style="color:red;">Catatan : <?= $aj['ket']; ?></small>
                                        <?php } ?>

                                    <td style="width:5%;"><a href="<?= base_url('file/') ?><?= $aj['upload_permohonan']; ?>" target="_blank"><i class="fa fa-download"></i> Download </td>


                                    <?php
                                    if ($akun['role_id'] == '4') { ?>
                                        <td> <?= $aj['ket']; ?> </td>
                                    <?php } ?>
                                    <?php
                                    if ($akun['role_id'] == '5') { ?>
                                        <td> <?= $aj['catatan1']; ?> </td>
                                    <?php } ?>
                                    <td style="width:5%;">
                                        <div class="dropdown">
                                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                                <i class="fa fa-cog"></i>
                                            </button>
                                            <div class="dropdown-menu">
                                                <a href="<?= base_url('vbbp/detail/'); ?><?= $aj['id']; ?>" class="dropdown-item">Lihat Detail</a>



                                                <?php
                                                if ($akun['role_id'] == '3') { ?>
                                                    <?php
                                                    if ($aj['sts'] == '1') { ?>
                                                        <a href="<?= base_url('vbbp/setujui/'); ?><?= $aj['id']; ?>" class="dropdown-item">Setuju Berkas</i></a>
                                                        <a href="<?= base_url('vbbp/tolak/'); ?><?= $aj['id']; ?>" class="dropdown-item">Tolak Berkas</i></a>
                                                        <a href="<?= base_url('vbbp/history/'); ?><?= $aj['email']; ?>" class="dropdown-item">History</a>
                                                    <?php } ?>
                                                <?php } ?>

                                                <?php
                                                if ($akun['role_id'] == '4') { ?>
                                                    <?php
                                                    if ($aj['sts'] == '2') { ?>
                                                        <a href="<?= base_url('pimpinan1/vbbp/fee/'); ?><?= $aj['id']; ?>" class="dropdown-item">Setuju</i></a>
                                                        <a href="<?= base_url('pimpinan1/vbbp/tolak/'); ?><?= $aj['id']; ?>" class="dropdown-item">Tolak</i></a>
                                                        <a href="<?= base_url('vbbp/history/'); ?><?= $aj['email']; ?>" class="dropdown-item">History</a>
                                                    <?php } ?>
                                                <?php } ?>

                                                <?php
                                                if ($akun['role_id'] == '5') { ?>
                                                    <?php
                                                    if ($aj['sts'] == '3') { ?>
                                                        <a href="<?= base_url('pimpinan2/vbbp/fee/'); ?><?= $aj['id']; ?>" class="dropdown-item">Setuju</i></a>
                                                        <a href="<?= base_url('pimpinan2/vbbp/tolak/'); ?><?= $aj['id']; ?>" class="dropdown-item">Tolak</i></a>
                                                        <a href="<?= base_url('vbbp/history/'); ?><?= $aj['email']; ?>" class="dropdown-item">History</a>
                                                    <?php } ?>
                                                <?php } ?>

                                            </div>
                                        </div>


                                    </td>
                                </tr>
                                <?php $i++; ?>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- /.card-body -->

</div>
<!-- /.card -->

</section>
<!-- /.content -->

</div>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
    $(function() {
        $("#example1").DataTable({
            "language": {
                "sSearch": "Cari"
            }
        });
    });
</script>