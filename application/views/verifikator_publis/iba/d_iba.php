<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Detail Usulan Insentif Buku Ajar</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('home') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Permohonan Insentif</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url('aset_berwujud') ?>">Usulan Insentif Buku Ajar</a></li>
                        <li class="breadcrumb-item active">Detail</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Detail Usulan</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-striped" id="users">
                    <tbody>
                        <tr>
                            <td width="100px">Email</td>
                            <td width="50px">:</td>
                            <td><?= $d['email'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Jenis Buku Ajar</td>
                            <td width="50px">:</td>
                            <td><?= $d['jenis_buku']; ?> </td>
                        </tr>
                        <tr>
                            <td width="200px">Judul Buku</td>
                            <td width="50px">:</td>
                            <td><?= $d['judul_buku'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Kontribusi Pengusul</td>
                            <td width="50px">:</td>
                            <td><?= $d['kontribusi_pengusul'] ?></td>
                        </tr>

                        <tr>
                            <td width="100px">Penerbit</td>
                            <td width="50px">:</td>
                            <td><?= $d['penerbit'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Kategori Buku Ajar</td>
                            <td width="50px">:</td>
                            <td><?= $d['kategori_ba'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Tahun Buku Terbit</td>
                            <td width="50px">:</td>
                            <td><?= $d['tahun_terbit'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">ISBN/E-ISBN</td>
                            <td width="50px">:</td>
                            <td><?= $d['isbn'] ?></td>
                        </tr>

                        <tr>
                            <td width="100px">URL Website Penerbit</td>
                            <td width="50px">:</td>
                            <td><?= $d['web_penerbit'] ?></td>
                        </tr>


                        <tr>
                            <td width="100px">Apakah Data Buku Sudah Diinput di SINTA Pengusul</td>
                            <td width="50px">:</td>
                            <td><?= $d['pertanyaan1'] ?></td>
                        </tr>



                        <tr>
                            <td width="100px">Mata Kuliah Yang Diampu</td>
                            <td width="50px">:</td>
                            <td><?= $d['pertanyaan2'] ?></td>
                        </tr>

                        <tr>
                            <td width="100px">MBuku tersebut digunakan Untuk Mata Kuliah</td>
                            <td width="50px">:</td>
                            <td><?= $d['pertanyaan3'] ?></td>
                        </tr>

                        <tr>
                            <td width="100px">Apakah Buku tersebut bersedia di Jual melalui UMSU Press</td>
                            <td width="50px">:</td>
                            <td><?= $d['pertanyaan4'] ?></td>
                        </tr>

                        <tr>
                            <td width="100px">Harga Jual Buku Per Eks</td>
                            <td width="50px">:</td>
                            <td><?= $d['harga_jual'] ?></td>
                        </tr>



                        <tr>
                            <td width="100px">Dokumen Pendukung</td>
                            <td width="50px">:</td>
                            <td><a href="<?= base_url('file/') ?><?= $d['upload_cover']; ?>" target="_blank"><i class="fa fa-download"></i></td>
                        </tr>
                        <tr>
                            <td width="100px">File Buku Ajar</td>
                            <td width="50px">:</td>
                            <td><a href="<?= base_url('file/') ?><?= $d['upload_buku']; ?>" target="_blank"><i class="fa fa-download"></i></td>
                        </tr>
                    </tbody>
                </table>

            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <a href="javascript:history.back()">
                    <button type="button" class="btn btn-danger"><i class="fa fa-backward"></i> Kembali</button>
                </a>
            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->