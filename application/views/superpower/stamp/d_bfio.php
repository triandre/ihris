<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h3>Detail Usulan Bantuan Biaya Publikasi</h3>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('home') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Permohonan Insentif</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url('aset_berwujud') ?>">Usulan Bantuan Biaya Publikasi</a></li>
                        <li class="breadcrumb-item active">Detail</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Detail Usulan</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-striped" id="users">
                    <tbody>
                        <tr>
                            <td width="100px">Email</td>
                            <td width="50px">:</td>
                            <td><?= $d['email'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Kegiatan Seminar</td>
                            <td width="50px">:</td>
                            <td><?= $d['kgt']; ?> </td>
                        </tr>
                        <tr>
                            <td width="200px">Biaya Pendaftaran</td>
                            <td width="50px">:</td>
                            <td> Rp. <?= number_format($d['biaya_pendaftaran']); ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Biaya Kuota Internet/Transport</td>
                            <td width="50px">:</td>
                            <td> Rp. <?= number_format($d['insentif_tambahan']); ?></td>
                        </tr>

                        <tr>
                            <td width="100px">Judul Artikel</td>
                            <td width="50px">:</td>
                            <td><?= $d['judul_artikel'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Nama Forum Ilmiah</td>
                            <td width="50px">:</td>
                            <td><?= $d['nama_fi'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Jenis Forum Ilmiah</td>
                            <td width="50px">:</td>
                            <td><?= $d['jenis_fi'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Website Forum Ilmiah</td>
                            <td width="50px">:</td>
                            <td><?= $d['web_fi'] ?></td>
                        </tr>

                        <tr>
                            <td width="100px">Lokasi</td>
                            <td width="50px">:</td>
                            <td><?= $d['lokasi'] ?></td>
                        </tr>

                        <tr>
                            <td width="100px">Tanggal Penyelenggara</td>
                            <td width="50px">:</td>
                            <td><?= $d['tgl_penyelenggara'] ?></td>
                        </tr>

                        <tr>
                            <td width="100px">Dokumen Pendukung</td>
                            <td width="50px">:</td>
                            <td><a href="<?= base_url('file/') ?><?= $d['upload_permohonan']; ?>" target="_blank"><i class="fa fa-download"></i></td>
                        </tr>

                    </tbody>
                </table>

            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <a href="javascript:history.back()" type="button" class="btn bg-gradient-danger"><i class="fa fa-backward"></i> Kembali</a>
            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->