<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Dashboard</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('superpower') ?>">Dashboard</a></li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <?php
    $jd = $this->db->query("SELECT * FROM occ_user where role_id='2'");
    $jt = $this->db->query("SELECT * FROM occ_user where role_id='1'");
    $bjd = $this->db->query("SELECT * FROM occ_user where role_id='2' AND is_active=0");
    $bjt = $this->db->query("SELECT * FROM occ_user where role_id='1' AND is_active=0");
    ?>
    <!-- /.content-header -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3><?php echo $jd->num_rows(); ?></h3>

                            <p>Jumlah Dosen</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person"></i>
                        </div>
                        <a href="<?= base_url('seluruhDosen') ?>" class="small-box-footer">Lihat Data!<i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-success">
                        <div class="inner">
                            <h3><?php echo $jt->num_rows(); ?></h3>

                            <p>Jumlah Tendik</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person"></i>
                        </div>
                        <a href="<?= base_url('seluruhTendik') ?>" class="small-box-footer">Lihat Data!<i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>

                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-warning">
                        <div class="inner">
                            <h3><?php echo $bjd->num_rows(); ?></h3>

                            <p>Jumlah Dosen Berhenti</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person"></i>
                        </div>
                        <a href="<?= base_url('seluruhDosen') ?>" class="small-box-footer">Lihat Data!<i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-danger">
                        <div class="inner">
                            <h3><?php echo $bjt->num_rows(); ?></h3>

                            <p>Jumlah Tendik Berhenti</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person"></i>
                        </div>
                        <a href="<?= base_url('seluruhTendik') ?>" class="small-box-footer">Lihat Data!<i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>


            </div>

        </div>
    </section>
    <!-- Main content -->

</div>
<!-- /.content-wrapper -->
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
    $(function() {
        $("#example1").DataTable({
            "language": {
                "sSearch": "Cari"
            },
            pageLength: 5,
        });
    });
</script>