<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">Beranda</li>
                        <li class="breadcrumb-item">Ajuan Pdd</li>
                        <li class="breadcrumb-item">Data Pribadi</li>
                        <li class="breadcrumb-item active">Validasi</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>
    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title">Detail Ajuan Data Kependudukan</h3>

                <div class="card-tools">
                    <a href="javascript:history.back()" class="btn btn-info"><i class="fa fa-backward"></i> Kembali</a>
                </div>
            </div>
            <div class="card-body">
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-danger col-md-8 alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?= validation_errors(); ?>
                    </div>
                <?php endif ?>

                <form class="form-horizontal">
                    <div class="card-body">
                        <table class="table table-striped">
                            <tbody>
                                <tr>
                                    <th></th>
                                    <th>Data Kepegawaian Lama</th>
                                    <th>Data Kepegawaian Baru</th>
                                </tr>
                                <tr>
                                    <td>Program Studi</td>
                                    <td><?= $d['prodi']; ?></td>
                                    <td><?= $dr['prodi']; ?></td>
                                </tr>

                                <td>Status Kepegawaian</td>
                                <td><?= $d['status_pegawai']; ?></td>
                                <td><?= $dr['status_pegawai']; ?></td>
                                </tr>
                                <tr>
                                    <td>Status Keaktifan</td>
                                    <td><?= $d['status_keaktifan']; ?></td>
                                    <td><?= $dr['status_keaktifan']; ?></td>
                                </tr>
                                <tr>
                                    <td>No. Sk CPNS (Khusus PNS)</td>
                                    <td><?= $d['no_sk_pns']; ?></td>
                                    <td><?= $dr['no_sk_pns']; ?></td>
                                </tr>
                                <tr>
                                    <td>SK CPNS TMT</td>
                                    <td><?= $d['no_sk_pns']; ?></td>
                                    <td><?= $dr['no_sk_pns']; ?></td>
                                </tr>
                                <tr>
                                    <td>No. SK TMMD</td>
                                    <td><?= $d['no_sk_tmmd']; ?></td>
                                    <td><?= $dr['no_sk_tmmd']; ?></td>
                                </tr>
                                <tr>
                                    <td>TGL. SK TMMD</td>
                                    <td><?= $d['tgl_sk_tmmd']; ?></td>
                                    <td><?= $dr['tgl_sk_tmmd']; ?></td>
                                </tr>
                                <tr>
                                    <td>Pangkat</td>
                                    <td><?= $d['pangkat']; ?></td>
                                    <td><?= $dr['pangkat']; ?></td>
                                </tr>
                                <tr>
                                    <td>Golongan</td>
                                    <td><?= $d['golongan']; ?></td>
                                    <td><?= $dr['golongan']; ?></td>
                                </tr>

                                <tr>
                                    <td>Sumber Gaji</td>
                                    <td><?= $d['sumber_gaji']; ?></td>
                                    <td><?= $dr['sumber_gaji']; ?></td>
                                </tr>

                                <tr>
                                    <td>Upload Dokumen Pendukung</td>
                                    <td> <a href="<?= base_url('archive/kepegawaian/'); ?><?= $d['file']; ?>" target="_blank" rel="noopener noreferrer">File Lama </a></td>
                                    <td> <a href="<?= base_url('archive/kepegawaian/'); ?><?= $dr['file']; ?>" target="_blank" rel="noopener noreferrer">File Baru </a></td>
                                </tr>
                            </tbody>
                        </table>


                    </div>
                    <!-- /.card-body -->
                    <?php if ($this->session->userdata('role_id') == '7' and $dr['sts'] == 1) { ?>
                        <div class="card-footer">
                            <a href="<?= base_url('ajuan_pdd/setujuiDataKepegawaian/'); ?><?= $dr['reff_kepegawaian']; ?>" class="btn btn-success"><i class="fa fa-check"></i> Setujui</a>
                            <button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-danger"><i class="fa fa-times"></i> Tolak</button>
                        </div>
                        <!-- /.card-footer -->
                    <?php } ?>
                </form>


            </div>
            <!-- /.card-body -->
            <div class="card-footer">

            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->
    <div class="modal fade" id="myModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Keterangan Penolakan</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <form method="post" action="<?= base_url('ajuan_pdd/tolakDataKepegawaian'); ?>">
                        <div class="form-group">
                            <label for="usr">Reff:</label>
                            <input type="text" class="form-control" id="reff_kepegawaian" name="reff_kepegawaian" value="<?= $dr['reff_kepegawaian']; ?>" readonly>
                        </div>

                        <div class="form-group">
                            <label for="usr">Keterangan:</label>
                            <textarea type="text" class="form-control" id="komentar" name="komentar" rows="8" required></textarea>
                        </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Submit</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                </div>
                </form>

            </div>
        </div>
    </div>
</div>
<!-- /.content-wrapper -->
<!-- Select2 -->
<script src="<?= base_url() ?>assets/vendor/backend/plugins/select2/js/select2.full.min.js"></script>
<script src="<?= base_url() ?>assets/vendor//js/rupiah.js"></script>
<script src="<?= base_url() ?>assets/vendor//js/duit.js"></script>
<script>
    $(document).ready(function() {
        $('.selectx').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $(document).ready(function() {
        $('.selecty').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $(document).ready(function() {
        $('.selectz').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });
    });

    $('#id_user').change(function() {
        var id_user = $(this).val();
        $.ajax({
            url: "<?= site_url('get-user'); ?>",
            method: "POST",
            data: {
                id_user: id_user
            },
            async: true,
            dataType: 'json',
            success: function(data) {

                var jbt = '';

                var i;
                for (i = 0; i < data.length; i++) {
                    jbt = data[i].jabatan;
                }
                document.getElementById("jabatan").value = jbt;
            }
        });
        return false;
    });


    $(document).ready(function() {

        $('#id_kampus').change(function() {
            var id_kampus = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-tempat'); ?>",
                method: "POST",
                data: {
                    id_kampus: id_kampus
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_gedung + '>' + data[i].gedung + '</option>';
                    }
                    $('#id_gedung').html(html);

                }
            });
            return false;
        });

        $('.selectw').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });



    $(document).ready(function() {

        $('#id_gedung').change(function() {
            var id_gedung = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-gedung'); ?>",
                method: "POST",
                data: {
                    id_gedung: id_gedung
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_lantai + '>' + data[i].lantai + '</option>';
                    }
                    $('#id_lantai').html(html);

                }
            });
            return false;
        });

        $('.selectw').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });

    $(document).ready(function() {

        $('#id_lantai').change(function() {
            var id_lantai = $(this).val();
            $.ajax({
                url: "<?php echo site_url('get-sub-ruangan'); ?>",
                method: "POST",
                data: {
                    id_lantai: id_lantai
                },
                async: true,
                dataType: 'json',
                success: function(data) {

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<option value=' + data[i].id_ruangan + '>' + data[i].ruangan + '</option>';
                    }
                    $('#id_ruangan').html(html);

                }
            });
            return false;
        });

        $('.selecta').select2({
            placeholder: "Pilih..",
            allowClear: true,
            theme: 'bootstrap4'
        });

    });
</script>