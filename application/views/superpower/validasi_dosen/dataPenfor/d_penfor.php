<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Detail Data Pendidikan Formal</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('home') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Profil</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url('aset_berwujud') ?>">Pendidikan Formal</a></li>
                        <li class="breadcrumb-item active">Detail Data Pendidikan Formal</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Detail Ajuan Data Pendidikan Formal</h3>
                <div class="card-tools">
                    <a href="javascript:history.back()" class="btn btn-info"><i class="fa fa-backward"></i> Kembali</a>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-striped" id="users">
                    <tbody>
                        <tr>
                            <td width="100px">Reff</td>
                            <td width="50px">:</td>
                            <td><?= $d['reff_penfor'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">NIDN</td>
                            <td width="50px">:</td>
                            <td><?= $d['nidn'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Jenjang Pendidikan</td>
                            <td width="50px">:</td>
                            <td><?= $d['jenjang']; ?> </td>
                        </tr>
                        <tr>
                            <td width="200px">Perguruan Tinggi</td>
                            <td width="50px">:</td>
                            <td><?= $d['perguruan_tinggi'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Gelar Akademik</td>
                            <td width="50px">:</td>
                            <td><?= $d['gelar_akademik'] ?></td>
                        </tr>

                        <tr>
                            <td width="100px">Bidang Studi </td>
                            <td width="50px">:</td>
                            <td><?= $d['bidang_studi']; ?></td>
                        </tr>

                        <tr>
                            <td width="100px">Program Studi</td>
                            <td width="50px">:</td>
                            <td><?= $d['program_studi'] ?></td>
                        </tr>

                        <tr>
                            <td width="100px">Tahun Masuk </td>
                            <td width="50px">:</td>
                            <td><?= $d['tahun_masuk'] ?></td>
                        </tr>


                        <tr>
                            <td width="100px">Tahun Lulus </td>
                            <td width="50px">:</td>
                            <td><?= $d['tahun_lulus'] ?></td>
                        </tr>

                        <tr>
                            <td width="100px">Tanggal Kelulusan </td>
                            <td width="50px">:</td>
                            <td><?= date('d F Y', strtotime($d['tgl_kelulusan'])) ?> </td>
                        </tr>
                        <tr>
                            <td width="100px">Nomor Induk </td>
                            <td width="50px">:</td>
                            <td><?= $d['nomor_induk'] ?></td>
                        </tr>

                        <tr>
                            <td width="100px">Jumlah Semester Tempuh </td>
                            <td width="50px">:</td>
                            <td><?= $d['jumlah_semester_tempuh'] ?></td>
                        </tr>

                        <tr>
                            <td width="100px">IPK Kelulusan </td>
                            <td width="50px">:</td>
                            <td><?= $d['ipk_kelulusan'] ?></td>
                        </tr>

                        <tr>
                            <td width="100px">Nomor SK Penyetaraan </td>
                            <td width="50px">:</td>
                            <td><?= $d['no_sk_penyetaraan'] ?></td>
                        </tr>

                        <tr>
                            <td width="100px">Tanggal SK Penyetaraan </td>
                            <td width="50px">:</td>
                            <td><?= $d['tgl_sk_penyetaraan'] ?></td>
                        </tr>

                        <tr>
                            <td width="100px">Nomor Ijazah </td>
                            <td width="50px">:</td>
                            <td><?= $d['nomor_ijazah'] ?></td>
                        </tr>

                        <tr>
                            <td width="100px">Judul </td>
                            <td width="50px">:</td>
                            <td><?= $d['judul'] ?></td>
                        </tr>

                        <tr>
                            <td width="100px">Dokumen</td>
                            <td width="50px">:</td>
                            <td> <a href="<?= base_url('archive/penfor/'); ?><?= $d['file'] ?>" target="_blank"> Lihat Data</a> </td>
                        </tr>

                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
            <?php if ($this->session->userdata('role_id') == '7' and $d['sts'] == 1) { ?>
                <div class="card-footer">
                    <a href="<?= base_url('ajuan_pdd/setujuiPenfor/'); ?><?= $d['reff_penfor']; ?>" class="btn btn-success"><i class="fa fa-check"></i> Setujui</a>
                    <button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-danger"><i class="fa fa-times"></i> Tolak</button>
                </div>
            <?php } ?>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <div class="modal fade" id="myModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Keterangan Penolakan</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <form method="post" action="<?= base_url('ajuan_pdd/tolakDataPenfor'); ?>">
                        <div class="form-group">
                            <label for="usr">Reff:</label>
                            <input type="text" class="form-control" id="reff_penfor" name="reff_penfor" value="<?= $d['reff_penfor']; ?>" readonly>
                        </div>

                        <div class="form-group">
                            <label for="usr">Keterangan:</label>
                            <textarea type="text" class="form-control" id="komentar" name="komentar" rows="8" required></textarea>
                        </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Submit</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                </div>
                </form>

            </div>
        </div>
    </div>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->