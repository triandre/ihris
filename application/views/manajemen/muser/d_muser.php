<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Detail Manajemen User</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('home') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Manajemen Pengguna</a></li>
                        <li class="breadcrumb-item active">Detail</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Detail Pengguna </h3>
                <div class="card-tools">
                    <a href="javascript:history.back()" class="btn btn-info"><i class="fa fa-backward"></i> Kembali</a>
                </div>

            </div>
            <div class="card-body">

                <table class="table table-striped" id="users">
                    <tbody>
                        <tr>
                            <td width="100px">NIDN</td>
                            <td width="50px">:</td>
                            <td><?= $d['nidn'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">NPP</td>
                            <td width="50px">:</td>
                            <td><?= $d['npp'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Email</td>
                            <td width="50px">:</td>
                            <td><?= $d['email'] ?></td>
                        </tr>
                        <tr>
                            <td width="200px">Nama</td>
                            <td width="50px">:</td>
                            <td><?= $d['name'] ?></td>
                        </tr>
                        <tr>
                            <td width="100px">Status</td>
                            <td width="50px">:</td>
                            <td> <?php
                                    if ($d['role_id'] == 1) {
                                        echo '<span class="badge badge-info">Tenaga Kependidikan</span> ';
                                    } elseif ($d['role_id'] == 2) {
                                        echo '<span class="badge badge-success">Dosen</span>';
                                    } elseif ($d['role_id'] == 3) {
                                        echo '<span class="badge badge-warning">Validasi LPPM</span>';
                                    } elseif ($d['role_id'] == 4) {
                                        echo '<span class="badge badge-danger">Wakil Rektor 1</span>';
                                    } elseif ($d['role_id'] == 5) {
                                        echo '<span class="badge badge-primary">Wakil Rektor 2</span>';
                                    } elseif ($d['role_id'] == 6) {
                                        echo '<span class="badge badge-default">Baum</span>';
                                    }
                                    ?></td>
                        </tr>

                        <tr>
                            <td width="100px">Dokumen <i>(Lampiran)</i></td>
                            <td width="50px">:</td>
                            <td> <a href="<?= base_url('archive/arsip/'); ?><?= $d['file'] ?>" target="_blank"> Lihat Data</a> </td>
                        </tr>

                    </tbody>
                </table>
            </div>

            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
    <div class="modal fade" id="myModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Keterangan Penolakan</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <form method="post" action="<?= base_url('ajuan_pdd/tolakJafung'); ?>">
                        <div class="form-group">
                            <label for="usr">Reff:</label>
                            <input type="text" class="form-control" id="reff_jafung" name="reff_jafung" value="<?= $d['reff_jafung']; ?>" readonly>
                        </div>

                        <div class="form-group">
                            <label for="usr">Keterangan:</label>
                            <textarea type="text" class="form-control" id="komentar" name="komentar" rows="8" required></textarea>
                        </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Submit</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                </div>
                </form>

            </div>
        </div>
    </div>
</div>
<!-- /.content-wrapper -->