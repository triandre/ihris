<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $title; ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= base_url('dosen') ?>">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Manajemen</a></li>
                        <li class="breadcrumb-item active">Pengguna</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
    <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <?php if ($this->session->flashdata('gagal_store')) { ?>
                    <div class="alert alert-danger col-md-12">
                        <?= $this->session->flashdata('gagal_store') ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php } ?>

                <?= form_error('username', '<div class="alert alert-danger" role="alert">', '</div>') ?>
                <?= form_error('password', '<div class="alert alert-danger" role="alert">', '</div>') ?>

                <h3 class="card-title">
                    <a href="<?= base_url('muser/new'); ?>" class="btn btn-block bg-gradient-primary"><i class="fa fa-plus"></i> Tambah</a>
                </h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fas fa-times"></i></button>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="example1" class="table table-bordered table-striped table-sm">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>NIDN</th>
                                <th>NPP</th>
                                <th>Email</th>
                                <th>Nama</th>
                                <th>Akses</th>
                                <th>Keaktifan</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            foreach ($v as $row) : ?>
                                <tr>
                                    <td><?= $no++; ?></td>
                                    <td><?= $row['nidn']; ?></td>
                                    <td><?= $row['npp']; ?></td>
                                    <td><?= $row['email']; ?></td>
                                    <td><?= $row['name']; ?></td>
                                    <td>
                                        <?php
                                        if ($row['role_id'] == 1) {
                                            echo '<span class="badge badge-info">Tenaga Kependidikan</span> ';
                                        } elseif ($row['role_id'] == 2) {
                                            echo '<span class="badge badge-success">Dosen</span>';
                                        } elseif ($row['role_id'] == 3) {
                                            echo '<span class="badge badge-warning">Validasi LPPM</span>';
                                        } elseif ($row['role_id'] == 4) {
                                            echo '<span class="badge badge-danger">Wakil Rektor 1</span>';
                                        } elseif ($row['role_id'] == 5) {
                                            echo '<span class="badge badge-primary">Wakil Rektor 2</span>';
                                        } elseif ($row['role_id'] == 6) {
                                            echo '<span class="badge badge-default">Baum</span>';
                                        } elseif ($row['role_id'] == 10) {
                                            echo '<span class="badge badge-default">Dosen Tidak Tetap</span>';
                                        }
                                        ?></td>
                                    <td> <?php
                                            if ($row['is_active'] == 1) {
                                                echo '<span class="badge badge-success">Aktif</span> ';
                                            } else {
                                                echo '<span class="badge badge-danger">Tidak Aktif</span>';
                                            }
                                            ?></td>
                                    <td>

                                        <div class="btn-group">
                                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                                <i class="fa fa-cog"></i>
                                            </button>
                                            <div class="dropdown-menu">
                                                <a href="<?= base_url('muser/detail/' . $row['id']) ?>" class="dropdown-item">Detail</a>
                                                <a href="<?= base_url('muser/edit/' . $row['id']) ?>" class="dropdown-item">Edit</a>
                                                <a href="<?= base_url('muser/reset/' . $row['id']) ?>" class="dropdown-item tombol-hapus">Reset Password</a>
                                                <?php if ($row['is_active'] == 0) { ?>
                                                    <a href="<?= base_url('muser/on/' . $row['id']) ?>" class="dropdown-item">Aktif</a>
                                                <?php } else { ?>
                                                    <a href="<?= base_url('muser/off/' . $row['id']) ?>" class="dropdown-item">Tidak Aktif</a>
                                                <?php } ?>
                                            </div>
                                        </div>

                                    </td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">

            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->

</div>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>assets/vendor/backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
    $(function() {
        $("#example1").DataTable({
            "language": {
                "sSearch": "Cari"
            }
        });
    });
</script>