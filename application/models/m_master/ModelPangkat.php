<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ModelPangkat extends CI_Model
{
    public function getPangkat()
    {
        $this->db->select('*');
        $this->db->from('mm_pangkat');
        $query = $this->db->get();
        return $query->result_array();
    }
}

/* End of file ModelName.php */
