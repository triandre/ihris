<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ModelMasterJafung extends CI_Model
{
    public function getMasterJafung()
    {
        $this->db->select('*');
        $this->db->from('mm_jafung');
        $query = $this->db->get();
        return $query->result_array();
    }
}

/* End of file ModelName.php */
