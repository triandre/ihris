<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ModelDashboard extends CI_Model
{
    public function getPendidikanTerakhir()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_pendidikan_formal');
        $this->db->where('nidn', $nidn);
        $this->db->where('sts', 2);
        $this->db->where('active', 1);
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getKesejahteraan()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_kesejahteraan');
        $this->db->where('nidn', $nidn);
        $this->db->where('aktif', 1);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getPenelitian()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_penelitian');
        $this->db->where('nidn', $nidn);
        $this->db->where('aktif', 1);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getViSci()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_vs');
        $this->db->where('nidn', $nidn);
        $this->db->where('aktif', 1);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getTunjangan()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_tunjangan');
        $this->db->where('nidn', $nidn);
        $this->db->where('aktif', 1);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getBukuAjar()
    {
        $email = $this->session->userdata('email');
        $this->db->select('*');
        $this->db->from('ins_iba');
        $this->db->where('email', $email);
        $this->db->where('aktif', 1);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getPembicara()
    {
        $email = $this->session->userdata('email');
        $this->db->select('*');
        $this->db->from('ins_ks');
        $this->db->where('email', $email);
        $this->db->where('aktif', 1);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getNotif()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_pdd');
        $this->db->where('nidn', $nidn);
        $this->db->where('sts', 3);
        $query = $this->db->get();
        return $query->num_rows();
    }
}
