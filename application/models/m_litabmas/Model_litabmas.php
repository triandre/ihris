<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_litabmas extends CI_Model
{

    public function getPenempatan()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_penempatan');
        $this->db->where('nidn', $nidn);
        $this->db->join('mm_unit', 'occ_penempatan.nickname_unit = mm_unit.nickname', 'left');
        $this->db->order_by('occ_penempatan.id_penempatan', 'desc'); // Gantilah 'nama_kolom_urutan' dengan nama kolom yang ingin diurutkan
        $this->db->limit(1); // Batasan hasil hanya 1 baris
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getJafung()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_jafung');
        $this->db->where('nidn', $nidn);
        $this->db->join('mm_jafung', 'occ_jafung.id_jfungsional = mm_jafung.id_jfungsional', 'left');
        $this->db->order_by('occ_jafung.id_jafung', 'desc'); // Gantilah 'nama_kolom_urutan' dengan nama kolom yang ingin diurutkan
        $this->db->limit(1); // Batasan hasil hanya 1 baris
        $query = $this->db->get();
        return $query->row_array();
    }
    public function getPenfor()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_pendidikan_formal');
        $this->db->where('nidn', $nidn);
        $this->db->order_by('occ_pendidikan_formal.id_penfor', 'desc'); // Gantilah 'nama_kolom_urutan' dengan nama kolom yang ingin diurutkan
        $this->db->limit(1); // Batasan hasil hanya 1 baris
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getLain()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_lain');
        $this->db->where('nidn', $nidn);
        $this->db->order_by('occ_lain.id_lain', 'desc'); // Gantilah 'nama_kolom_urutan' dengan nama kolom yang ingin diurutkan
        $this->db->limit(1); // Batasan hasil hanya 1 baris
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getSkema()
    {
        $this->db->select('*');
        $this->db->from('mm_skema_penelitian');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getPeriode()
    {
        $this->db->select('*');
        $this->db->from('mm_periode');
        $this->db->where('aktif_periode', 1);
        $this->db->order_by('id_periode', 'desc'); // Gantilah 'nama_kolom_urutan' dengan nama kolom yang ingin diurutkan
        $this->db->limit(1); // Batasan hasil hanya 1 baris
        $query = $this->db->get();
        return $query->row_array();
    }

    public function cekLtb()
    {
        $email = $this->session->userdata('email');
        $this->db->select('*');
        $this->db->from('ltb_litabmas');
        $this->db->where('email', $email);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getlitabmas()
    {
        $email = $this->session->userdata('email');
        $this->db->select('*');
        $this->db->from('ltb_litabmas');
        $this->db->where('email', $email);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getKolaborasi()
    {
        $this->db->select('*');
        $this->db->from('ltb_kolaborasi');
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getRumpun()
    {
        $this->db->select('*');
        $this->db->from('ltb_rumpun_ilmu');
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getBidang()
    {
        $this->db->select('*');
        $this->db->from('ltb_bidang_fp');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getTema()
    {
        $this->db->select('*');
        $this->db->from('ltb_tema');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getTopik()
    {
        $this->db->select('*');
        $this->db->from('ltb_topik');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getPembimbing()
    {
        $this->db->select('*');
        $this->db->from('occ_user');
        $this->db->where('role_id', 2);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getPbb()
    {
        $email = $this->session->userdata('email');
        $this->db->select('*');
        $this->db->from('ltb_pembimbing a');
        $this->db->join('occ_user b', 'b.email = a.email_pembimbing', 'left');
        $this->db->join('occ_bidang_ilmu c', 'c.nidn = b.nidn', 'left');
        $this->db->where('a.email_created', $email);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getAnggota()
    {
        $email = $this->session->userdata('email');
        $this->db->select('*');
        $this->db->from('ltb_anggota a');
        $this->db->join('occ_user b', 'b.email = a.email_anggota', 'left');
        $this->db->join('occ_bidang_ilmu c', 'c.nidn = b.nidn', 'left');
        $this->db->where('a.email_created', $email);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getMahasiswa()
    {
        $email = $this->session->userdata('email');
        $this->db->select('*');
        $this->db->from('ltb_mahasiswa');
        $this->db->where('email_created', $email);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getlur()
    {
        $email = $this->session->userdata('email');
        $this->db->select('*');
        $this->db->from('ltb_luaran');
        $this->db->where('email_created', $email);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getlurTambah()
    {
        $email = $this->session->userdata('email');
        $this->db->select('*');
        $this->db->from('ltb_luaran_tambahan');
        $this->db->where('email_created', $email);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getRab()
    {
        $email = $this->session->userdata('email');
        $this->db->select('*');
        $this->db->from('ltb_rab');
        $this->db->where('email_created', $email);
        $query = $this->db->get();
        return $query->result_array();
    }
}
