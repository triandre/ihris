<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ModelPensiun extends CI_Model
{
    public function viewData($status_pegawai)
    {

        $this->db->select('*');
        $this->db->from('occ_user a');
        $this->db->join('occ_profil b', 'b.nidn=a.nidn', 'left');
        $this->db->join('occ_penempatan c', 'c.nidn=a.nidn', 'left');
        $this->db->where('a.role_id', $status_pegawai);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function update_user($email, $data_user)
    {
        $this->db->where(array('email' => $email));
        $res = $this->db->update('occ_user', $data_user);
        return $res;
    }
}

/* End of file ModelName.php */
