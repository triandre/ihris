<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_dataPegawai extends CI_Model
{
    public function getDosen()
    {

        $this->db->select('*');
        $this->db->from('occ_user');
        $this->db->where('role_id', 2);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getTendik()
    {

        $this->db->select('*');
        $this->db->from('occ_user');
        $this->db->where('role_id', 1);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getBiodata($nidn)
    {
        $this->db->select('*');
        $this->db->from('occ_profil');
        $this->db->where('nidn', $nidn);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getKependudukan($nidn)
    {
        $this->db->select('*');
        $this->db->from('occ_kependudukan');
        $this->db->where('nidn', $nidn);
        $this->db->where('sts', 2);
        $this->db->order_by('id_kependudukan', "desc");
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getUser($nidn)
    {
        $this->db->select('*');
        $this->db->from('occ_user');
        $this->db->where('nidn', $nidn);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getAlamat($nidn)
    {
        $this->db->select('*');
        $this->db->from('occ_alamat');
        $this->db->where('nidn', $nidn);
        $this->db->where('sts', 2);
        $this->db->order_by('id_alamat', "desc");
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getKepegawaian($nidn)
    {
        $this->db->select('*');
        $this->db->from('occ_kepegawaian');
        $this->db->where('nidn', $nidn);
        $this->db->where('sts', 2);
        $this->db->order_by('id_kepegawaian', "desc");
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getPenempatan($nidn)
    {
        $this->db->select('*');
        $this->db->from('occ_penempatan');
        $this->db->where('nidn', $nidn);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getKemuhammadiyahan($nidn)
    {
        $this->db->select('*');
        $this->db->from('occ_muhammadiyah');
        $this->db->where('nidn', $nidn);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getLainlain($nidn)
    {
        $this->db->select('*');
        $this->db->from('occ_lain');
        $this->db->where('nidn', $nidn);
        $this->db->where('sts', 2);
        $this->db->order_by('id_lain', "desc");
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }
}

/* End of file ModelName.php */
