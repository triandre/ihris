<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_valDataJafung extends CI_Model
{

    public function viewDiajukan()
    {
        $kode = "ADJF";
        $viewDiajukan = "SELECT occ_pdd.reff, 
        occ_user.nidn,
        occ_user.name,
        occ_pdd.tgl_ajuan,
        occ_pdd.sts,
        occ_pdd.kode_ajuan,
        occ_pdd.jenis_ajuan
        FROM occ_pdd
        LEFT JOIN occ_user ON occ_user.nidn = occ_pdd.nidn 
        WHERE occ_pdd.sts=1 AND occ_pdd.kode_ajuan='$kode'";
        return $this->db->query($viewDiajukan)->result_array();
    }

    public function viewDitolak()
    {
        $kode = "ADJF";
        $viewDiajukan = "SELECT occ_pdd.reff, 
        occ_user.nidn,
        occ_user.name,
        occ_pdd.tgl_ajuan,
        occ_pdd.sts,
        occ_pdd.kode_ajuan,
        occ_pdd.tgl_verifikasi,
        occ_pdd.komentar,
        occ_pdd.jenis_ajuan
        FROM occ_pdd
        LEFT JOIN occ_user ON occ_user.nidn = occ_pdd.nidn 
        WHERE occ_pdd.sts=3 AND occ_pdd.kode_ajuan='$kode'";
        return $this->db->query($viewDiajukan)->result_array();
    }

    public function viewDisetujui()
    {
        $kode = "ADJF";
        $viewDiajukan = "SELECT occ_pdd.reff, 
        occ_user.nidn,
        occ_user.name,
        occ_pdd.tgl_ajuan,
        occ_pdd.sts,
        occ_pdd.kode_ajuan,
        occ_pdd.tgl_verifikasi,
        occ_pdd.komentar,
        occ_pdd.jenis_ajuan
        FROM occ_pdd
        LEFT JOIN occ_user ON occ_user.nidn = occ_pdd.nidn 
        WHERE occ_pdd.sts=2 AND occ_pdd.kode_ajuan='$kode'";
        return $this->db->query($viewDiajukan)->result_array();
    }

    public function detailJafung($reff)
    {
        $this->db->select('*');
        $this->db->from('occ_jafung a');
        $this->db->join('mm_jafung b', 'b.id_jfungsional = a.id_jfungsional');
        $this->db->where('reff_jafung', $reff);
        $this->db->where('active', 1);
        $query = $this->db->get();
        return $query->row_array();
    }
}

/* End of file ModelName.php */
