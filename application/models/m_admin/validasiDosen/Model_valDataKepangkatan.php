<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_valDataKepangkatan extends CI_Model
{

    public function viewDiajukan()
    {
        $kode = "ADI";
        $viewDiajukan = "SELECT occ_pdd.reff, 
        occ_user.nidn,
        occ_user.name,
        occ_pdd.tgl_ajuan,
        occ_pdd.sts,
        occ_pdd.kode_ajuan,
        occ_pdd.jenis_ajuan
        FROM occ_pdd
        LEFT JOIN occ_user ON occ_user.nidn = occ_pdd.nidn 
        WHERE occ_pdd.sts=1 AND occ_pdd.kode_ajuan='$kode'";
        return $this->db->query($viewDiajukan)->result_array();
    }

    public function viewDitolak()
    {
        $kode = "ADI";
        $viewDiajukan = "SELECT occ_pdd.reff, 
        occ_user.nidn,
        occ_user.name,
        occ_pdd.tgl_ajuan,
        occ_pdd.sts,
        occ_pdd.kode_ajuan,
        occ_pdd.tgl_verifikasi,
        occ_pdd.komentar,
        occ_pdd.jenis_ajuan
        FROM occ_pdd
        LEFT JOIN occ_user ON occ_user.nidn = occ_pdd.nidn 
        WHERE occ_pdd.sts=3 AND occ_pdd.kode_ajuan='$kode'";
        return $this->db->query($viewDiajukan)->result_array();
    }

    public function viewDisetujui()
    {
        $kode = "ADI";
        $viewDiajukan = "SELECT occ_pdd.reff, 
        occ_user.nidn,
        occ_user.name,
        occ_pdd.tgl_ajuan,
        occ_pdd.sts,
        occ_pdd.kode_ajuan,
        occ_pdd.tgl_verifikasi,
        occ_pdd.komentar,
        occ_pdd.jenis_ajuan
        FROM occ_pdd
        LEFT JOIN occ_user ON occ_user.nidn = occ_pdd.nidn 
        WHERE occ_pdd.sts=2 AND occ_pdd.kode_ajuan='$kode'";
        return $this->db->query($viewDiajukan)->result_array();
    }

    public function detailKepangkatan($reff)
    {
        $this->db->select('*');
        $this->db->from('occ_impassing a');
        $this->db->join('mm_pangkat b', 'b.id_pangkat = a.id_pangkat');
        $this->db->where('reff_impassing', $reff);
        $this->db->where('active', 1);
        $query = $this->db->get();
        return $query->row_array();
    }
}

/* End of file ModelName.php */
