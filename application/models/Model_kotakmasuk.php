<?php
defined('BASEPATH') or exit('No direct script access allowed');

use GuzzleHttp\Client;


class Model_kotakmasuk extends CI_Model
{
    private $_client;
    public function __construct()
    {
        $this->_client = new Client([
            'base_uri' => 'https://api.umsu.ac.id/Administrasi/'
        ]);
    }
    public function getKotakmasuk()
    {
        $email = $this->session->userdata('email');
        $response = $this->_client->request(
            'POST',
            'penerima',
            [
                'form_params' => [
                    'email' => $email
                ]
            ]
        );
        $result = json_decode($response->getBody()->getContents(), true);
        return $result['data'];
    }
}

/* End of file ModelName.php */
