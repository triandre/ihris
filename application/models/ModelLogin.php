<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ModelLogin extends CI_Model
{

	function login($email, $password2)
	{
		$this->db->where('email', $email);
		$this->db->where('password', $password2);
		$query =  $this->db->get_where('occ_user',  ['is_active' => 1]);
		return $query->num_rows();
	}

	function data_login($email, $password2)
	{
		$this->db->where('email', $email);
		$this->db->where('password', $password2);
		return $this->db->get_where('occ_user',  ['is_active' => 1])->row();
	}
}

/* End of file ModelLogin.php */
/* Location: ./application/models/ModelLogin.php */