<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_lkk extends CI_Model
{
    public function getView()
    {
        $email = $this->session->userdata('email');
        $this->db->select('*');
        $this->db->from('occ_bantuan_lkk a');
        $this->db->join('occ_user b', 'b.email = a.email');
        $this->db->where('a.email', $email);
        $this->db->where('a.aktif_lkk', 1);
        $this->db->order_by('id_lkk', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getDetail($reff_lkk)
    {
        $this->db->select('*');
        $this->db->from('occ_bantuan_lkk a');
        $this->db->join('occ_user b', 'b.email = a.email');
        $this->db->where('a.reff_lkk', $reff_lkk);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getView_masuk()
    {
        $this->db->select('*');
        $this->db->from('occ_bantuan_lkk a');
        $this->db->join('occ_user b', 'b.email = a.email');
        $this->db->where('a.aktif_lkk', 1);
        $this->db->where('a.sts_ambil', 1);
        $this->db->where('a.status_lkk', 1);
        $this->db->order_by('id_lkk', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }


    public function getView_proses()
    {
        $this->db->select('*');
        $this->db->from('occ_bantuan_lkk a');
        $this->db->join('occ_user b', 'b.email = a.email');
        $this->db->where('a.aktif_lkk', 1);
        $this->db->where('a.sts_ambil', 1);
        $this->db->where('a.status_lkk', 2);
        $this->db->order_by('id_lkk', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getView_disetujui()
    {
        $this->db->select('*');
        $this->db->from('occ_bantuan_lkk a');
        $this->db->join('occ_user b', 'b.email = a.email');
        $this->db->where('a.aktif_lkk', 1);
        $this->db->where('a.sts_ambil', 1);
        $this->db->where('a.status_lkk', 3);
        $this->db->order_by('id_lkk', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getView_ditolak()
    {
        $this->db->select('*');
        $this->db->from('occ_bantuan_lkk a');
        $this->db->join('occ_user b', 'b.email = a.email');
        $this->db->where('a.aktif_lkk', 1);
        $this->db->where('a.sts_ambil', 1);
        $this->db->where('a.status_lkk', 4);
        $this->db->order_by('id_lkk', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getBansos()
    {

        $this->db->select('*');
        $this->db->from('mm_jenis_bantuan');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getView_masuk_pimpinan()
    {
        $this->db->select('*');
        $this->db->from('occ_bantuan_lkk a');
        $this->db->join('occ_user b', 'b.email = a.email');
        $this->db->where('a.aktif_lkk', 1);
        $this->db->where('a.sts_ambil', 1);
        $this->db->where('a.status_lkk', 2);
        $this->db->order_by('id_lkk', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getView_diterima()
    {
        $this->db->select('*');
        $this->db->from('occ_bantuan_lkk a');
        $this->db->join('occ_user b', 'b.email = a.email');
        $this->db->where('a.aktif_lkk', 1);
        $this->db->where('a.sts_ambil', 0);
        $this->db->where('a.status_lkk', 3);
        $this->db->order_by('id_lkk', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getlaporan($start, $end)
    {

        $bansos = "SELECT * FROM occ_bantuan_lkk 
        LEFT JOIN occ_user ON occ_user.email = occ_bantuan_lkk.email
        where occ_bantuan_lkk.aktif_lkk=1 AND occ_bantuan_lkk.sts_ambil=0 AND occ_bantuan_lkk.status_lkk=3 AND occ_bantuan_lkk.date_penerimaan BETWEEN '$start' AND '$end'";
        return $this->db->query($bansos)->result_array();
    }

    public function getJumlah($start, $end)
    {

        $bansos = "SELECT SUM(nominal) as totalSemua FROM occ_bantuan_lkk 
        where aktif_lkk=1 AND sts_ambil=0 AND status_lkk=3 AND date_penerimaan BETWEEN '$start' AND '$end'";
        return $this->db->query($bansos)->row_array();
    }
}

/* End of file ModelName.php */