<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_mprod extends CI_Model
{

    public function getview()
    {
        $this->db->select('*');
        $this->db->from('mm_prodi a');
        $this->db->join('mm_fakultas b', 'b.kode_fakultas = a.kode_fakultas');
        $this->db->where('a.aktif', 1);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function save_prod($data)
    {
        $query = $this->db->insert('mm_prodi', $data);
        return $query;
    }

    public function detail_prod($id_prodi)
    {
        $this->db->select('*');
        $this->db->from('mm_prodi');
        $this->db->where('id_prodi', $id_prodi);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function edit_fak($id_prodi, $data)
    {
        $this->db->where(array('id_prodi' => $id_prodi));
        $res = $this->db->update('mm_fakultas', $data);
        return $res;
    }
}

/* End of file ModelName.php */
