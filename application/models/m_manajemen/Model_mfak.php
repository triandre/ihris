<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_mfak extends CI_Model
{
    public function getview()
    {

        $this->db->select('*');
        $this->db->from('mm_fakultas');
        $this->db->where('aktif', 1);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function save_fak($data)
    {
        $query = $this->db->insert('mm_fakultas', $data);
        return $query;
    }

    public function detail_fak($id)
    {
        $this->db->select('*');
        $this->db->from('mm_fakultas');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function edit_fak($id, $data)
    {
        $this->db->where(array('id' => $id));
        $res = $this->db->update('mm_fakultas', $data);
        return $res;
    }
}

/* End of file ModelName.php */
