<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_munit extends CI_Model
{
    public function getview()
    {

        $this->db->select('*');
        $this->db->from('mm_unit');
        $this->db->where('aktif', 1);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function save_unit($data)
    {
        $query = $this->db->insert('mm_unit', $data);
        return $query;
    }

    public function detail_unit($id_unit)
    {
        $this->db->select('*');
        $this->db->from('mm_unit');
        $this->db->where('id_unit', $id_unit);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function edit_unit($id_unit, $data)
    {
        $this->db->where(array('id_unit' => $id_unit));
        $res = $this->db->update('mm_unit', $data);
        return $res;
    }
}

/* End of file ModelName.php */
