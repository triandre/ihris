<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_cuti extends CI_Model
{
    public function getView()
    {
        $email = $this->session->userdata('email');
        $this->db->select('*');
        $this->db->from('occ_permohonan_cuti a');
        $this->db->join('occ_user b', 'b.email = a.email');
        $this->db->where('a.email', $email);
        $this->db->order_by('id_cuti', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }


    public function getLaporanCuti($date_start, $date_end)
    {

        $lapCuti = "SELECT * FROM occ_permohonan_cuti LEFT JOIN occ_user ON occ_user.email = occ_permohonan_cuti.email
        WHERE occ_permohonan_cuti.sts=3 AND occ_permohonan_cuti.tglCuti BETWEEN '$date_start' AND '$date_end' ORDER BY occ_user.name ASC";
        return $this->db->query($lapCuti)->result_array();
    }

    public function getLaporanSakit($date_start, $date_end)
    {
        $lapCuti = "SELECT * FROM occ_izin_sakit LEFT JOIN occ_user ON occ_user.email = occ_izin_sakit.email
        WHERE occ_izin_sakit.sts=2 AND occ_izin_sakit.dariTanggal BETWEEN '$date_start' AND '$date_end' ORDER BY occ_user.name ASC";
        return $this->db->query($lapCuti)->result_array();
    }

    function cutiku()
    {

        $email = $this->session->userdata('email');
        $this->db->select('*');
        $this->db->from('mm_hak_cuti a');
        $this->db->join('occ_user b', 'b.email = a.email');
        $this->db->where('a.email', $email);
        $query = $this->db->get();
        return $query->row_array();
    }
    public function getDetail()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_alamat');
        $this->db->where('nidn', $nidn);
        $this->db->where('aktif', 1);
        $this->db->where('sts', 2);
        $this->db->order_by('id_alamat', "desc");
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    function detailCuti($id_cuti)
    {
        $this->db->select('*');
        $this->db->from('occ_permohonan_cuti');
        $this->db->where('id_cuti', $id_cuti);
        $query = $this->db->get();
        return $query->row_array();
    }

    function detail_izinsakit($id_is)
    {
        $this->db->select('*');
        $this->db->from('occ_izin_sakit a');
        $this->db->join('occ_user b', 'b.email = a.email');
        $this->db->where('a.id_is', $id_is);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getView_izinsakit()
    {
        $email = $this->session->userdata('email');
        $this->db->select('*');
        $this->db->from('occ_izin_sakit a');
        $this->db->join('occ_user b', 'b.email = a.email');
        $this->db->where('a.email', $email);
        $this->db->order_by('id_is', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getizinsakit()
    {
        $email = $this->session->userdata('email');
        $this->db->select('*');
        $this->db->from('occ_izin_sakit');
        $this->db->where('email', $email);
        $this->db->order_by('id_is', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }
}

/* End of file ModelName.php */
