<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_stamp extends CI_Model
{
    public function getpj()
    {

        $this->db->select('*');
        $this->db->from('ins_pj');
        $this->db->where('sts', 4);
        $this->db->where('stempel', 1);
        $this->db->where('batch', 6);
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getbbp()
    {

        $this->db->select('*');
        $this->db->from('ins_bbp ');
        $this->db->where('sts', 4);
        $this->db->where('stempel', 1);
        $this->db->where('batch', 6);
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getubpjs()
    {
        $this->db->select('*');
        $this->db->from('ins_ubpjs ');
        $this->db->where('sts', 4);
        $this->db->where('stempel', 1);
        $this->db->where('batch', 6);
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getbfio()
    {

        $this->db->select('*');
        $this->db->from('ins_bfio ');
        $this->db->where('sts', 4);
        $this->db->where('stempel', 1);
        $this->db->where('batch', 6);
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getbhki()
    {

        $this->db->select('*');
        $this->db->from('ins_bhki ');
        $this->db->where('sts', 4);
        $this->db->where('stempel', 1);
        $this->db->where('batch', 6);
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }
}

/* End of file ModelName.php */
