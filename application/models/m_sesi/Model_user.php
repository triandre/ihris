<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_user extends CI_Model
{
    public function getUser()
    {
        $email = $this->session->userdata('email');
        $this->db->select('*');
        $this->db->from('occ_user');
        $this->db->where('email', $email);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function update_user($email, $data_user)
    {
        $this->db->where(array('email' => $email));
        $res = $this->db->update('occ_user', $data_user);
        return $res;
    }
}

/* End of file ModelName.php */
