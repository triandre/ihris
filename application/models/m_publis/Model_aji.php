<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_aji extends CI_Model
{
    public function getAji()
    {
        $email = $this->session->userdata('email');
        $this->db->select('*');
        $this->db->from('ins_aji ');
        $this->db->where('email', $email);
        $this->db->where('aktif', 1);
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }


    public function getDetail($id)
    {
        $this->db->select('*');
        $this->db->from('ins_aji');
        $this->db->where('id', $id);
        $this->db->where('aktif', 1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getKategori()
    {
        $this->db->select('*');
        $this->db->from('mm_kategori_jurnal');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getKontribusi()
    {
        $this->db->select('*');
        $this->db->from('mm_kontribusi');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getLembaga()
    {
        $this->db->select('*');
        $this->db->from('mm_lembaga_pengindeks');
        $query = $this->db->get();
        return $query->result_array();
    }
  public function getLembaga2()
    {
        $this->db->select('*');
        $this->db->from('mm_lembaga_pengindeks');
        $this->db->where('std_is', 1);
        $query = $this->db->get();
        return $query->result_array();
    }
}

/* End of file ModelName.php */
