<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_laporan extends CI_Model
{
    public function getAji()
    {
        $this->db->select('*');
        $this->db->from('ins_aji ');
        $this->db->where('aktif', 1);
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getPj($batch)
    {
        $this->db->select('*');
        $this->db->from('ins_pj');
        $this->db->where('aktif', 1);
        $this->db->where('batch', $batch);
        $this->db->where('sts', 4);
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function total_inspj($batch)
    {
        $total = "SELECT SUM(insentif_disetujui) AS total FROM ins_pj where sts=4 AND ins_pj.batch='$batch'";
        return $this->db->query($total)->row_array();
    }
    public function insiks($batch)
    {
        $this->db->select('*');
        $this->db->from('ins_ks');
        $this->db->where('aktif', 1);
        $this->db->where('batch', $batch);
        $this->db->where('sts', 4);
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function total_insiks($batch)
    {
        $total = "SELECT SUM(insentif_disetujui) AS total FROM ins_ks where sts=4 AND ins_ks.batch='$batch'";
        return $this->db->query($total)->row_array();
    }
    public function insbfio($batch)
    {
        $this->db->select('*');
        $this->db->from('ins_bfio');
        $this->db->where('aktif', 1);
        $this->db->where('batch', $batch);
        $this->db->where('sts', 4);
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }
    public function total_insbfio($batch)
    {
        $total = "SELECT SUM(insentif_disetujui) AS total FROM ins_bfio where sts=4 AND ins_bfio.batch='$batch'";
        return $this->db->query($total)->row_array();
    }

    public function insiba($batch)
    {
        $this->db->select('*');
        $this->db->from('ins_iba');
        $this->db->where('aktif', 1);
        $this->db->where('batch', $batch);
        $this->db->where('sts', 4);
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }


    public function total_insiba($batch)
    {
        $total = "SELECT SUM(insentif_disetujui) AS total FROM ins_iba where sts=4 AND ins_iba.batch='$batch'";
        return $this->db->query($total)->row_array();
    }

    public function insbbp($batch)
    {
        $this->db->select('*');
        $this->db->from('ins_bbp');
        $this->db->where('aktif', 1);
        $this->db->where('batch', $batch);
        $this->db->where('sts', 4);
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }
    public function total_insbpp($batch)
    {
        $total = "SELECT SUM(biaya_disetujui) AS total FROM ins_bbp where sts=4 AND ins_bbp.batch='$batch'";
        return $this->db->query($total)->row_array();
    }

    public function insifip($batch)
    {
        $this->db->select('*');
        $this->db->from('ins_ifip');
        $this->db->where('aktif', 1);
        $this->db->where('batch', $batch);
        $this->db->where('sts', 4);
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function total_insifip($batch)
    {
        $total = "SELECT SUM(insentif_disetujui) AS total FROM ins_ifip where sts=4 AND ins_ifip.batch='$batch'";
        return $this->db->query($total)->row_array();
    }

    public function inshki($batch)
    {
        $this->db->select('*');
        $this->db->from('ins_hki');
        $this->db->where('aktif', 1);
        $this->db->where('batch', $batch);
        $this->db->where('sts', 4);
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function total_inshki($batch)
    {
        $total = "SELECT SUM(insentif_disetujui) AS total FROM ins_hki where sts=4 AND ins_hki.batch='$batch'";
        return $this->db->query($total)->row_array();
    }

    public function insaji($batch)
    {
        $this->db->select('*');
        $this->db->from('ins_aji');
        $this->db->where('aktif', 1);
        $this->db->where('batch', $batch);
        $this->db->where('sts', 4);
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }
    public function total_insaji($batch)
    {
        $total = "SELECT SUM(insentif_disetujui) AS total FROM ins_aji where sts=4 AND ins_aji.batch='$batch'";
        return $this->db->query($total)->row_array();
    }

    public function insubpjs($batch)
    {
        $this->db->select('*');
        $this->db->from('ins_ubpjs');
        $this->db->where('aktif', 1);
        $this->db->where('batch', $batch);
        $this->db->where('sts', 4);
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }
    public function total_insubpjs($batch)
    {
        $total = "SELECT SUM(biaya_disetujui) AS total FROM ins_ubpjs where sts=4 AND ins_ubpjs.batch='$batch'";
        return $this->db->query($total)->row_array();
    }

    public function insuipjs($batch)
    {
        $this->db->select('*');
        $this->db->from('ins_uipjs');
        $this->db->where('aktif', 1);
        $this->db->where('batch', $batch);
        $this->db->where('sts', 4);
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }
    public function total_insuipjs($batch)
    {
        $total = "SELECT SUM(insentif_disetujui) AS total FROM ins_uipjs where sts=4 AND ins_uipjs.batch='$batch'";
        return $this->db->query($total)->row_array();
    }

    public function insbhki($batch)
    {
        $this->db->select('*');
        $this->db->from('ins_bhki');
        $this->db->where('aktif', 1);
        $this->db->where('batch', $batch);
        $this->db->where('sts', 4);
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }
    public function total_bhki($batch)
    {
        $total = "SELECT SUM(insentif_disetujui) AS total FROM ins_bhki where sts=4 AND ins_bhki.batch='$batch'";
        return $this->db->query($total)->row_array();
    }
}

/* End of file ModelName.php */
