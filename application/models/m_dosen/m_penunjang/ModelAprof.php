<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ModelAprof extends CI_Model
{
    public function getView()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_aprof');
        $this->db->where('nidn', $nidn);
        $this->db->where('aktif', 1);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function storeAprof($data)
    {
        $query = $this->db->insert('occ_aprof', $data);
        return $query;
    }

    public function detailAprof($id_af)
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_aprof ');
        $query = $this->db->get();
        return $query->row_array();
    }

    public function updateAprof($id_af, $data)
    {
        $this->db->where(array('id_af' => $id_af));
        $res = $this->db->update('occ_aprof', $data);
        return $res;
    }
}

/* End of file ModelName.php */
