<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ModelPenghargaan extends CI_Model
{
    public function getView()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_penghargaan');
        $this->db->where('nidn', $nidn);
        $this->db->where('aktif', 1);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function storePenghargaan($data)
    {
        $query = $this->db->insert('occ_penghargaan', $data);
        return $query;
    }

    public function detailPenghargaan($id_penghargaan)
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_penghargaan');
        $this->db->where('id_penghargaan', $id_penghargaan);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function updatePenghargaan($id_penghargaan, $data)
    {
        $this->db->where(array('id_penghargaan' => $id_penghargaan));
        $res = $this->db->update('occ_penghargaan', $data);
        return $res;
    }
}

/* End of file ModelName.php */
