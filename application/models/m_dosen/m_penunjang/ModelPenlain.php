<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ModelPenlain extends CI_Model
{
    public function getView()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_penunjang_lain');
        $this->db->where('aktif', 1);
        $this->db->where('nidn', $nidn);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function storePenlain($data)
    {
        $query = $this->db->insert('occ_penunjang_lain', $data);
        return $query;
    }

    public function detailPenlain($id_pl)
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_penunjang_lain');
        $this->db->where('id_pl', $id_pl);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function updatePenlain($id_pl, $data)
    {
        $this->db->where(array('id_pl' => $id_pl));
        $res = $this->db->update('occ_penunjang_lain', $data);
        return $res;
    }
}

/* End of file ModelName.php */
