<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_kepegawaian extends CI_Model
{
    public function getView()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_kepegawaian a');
        $this->db->join('master_prodi b', 'b.kode_prodi = a.kode_prodi');
        $this->db->where('a.nidn', $nidn);
        $this->db->where('a.aktif', 1);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getProdi()
    {

        $this->db->select('*');
        $this->db->from('master_prodi');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getDetail()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_kepegawaian a');
        $this->db->join('master_prodi b', 'b.kode_prodi = a.kode_prodi');
        $this->db->where('a.nidn', $nidn);
        $this->db->where('a.aktif', 1);
        $this->db->where('a.sts', 2);
        $this->db->order_by('a.id_kepegawaian', "desc");
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function storeKepegawaian($data)
    {
        $query = $this->db->insert('occ_kepegawaian', $data);
        return $query;
    }



    public function viewDraft()
    {
        $kode = "ADKPG";
        $nidn = $this->session->userdata('nidn');
        $queryDraft = "SELECT * FROM occ_pdd WHERE sts=0 AND kode_ajuan='$kode' AND nidn='$nidn'";
        return $this->db->query($queryDraft)->result_array();
    }

    public function viewDiajukan()
    {
        $kode = "ADKPG";
        $nidn = $this->session->userdata('nidn');
        $viewDiajukan = "SELECT * FROM occ_pdd WHERE sts=1 AND kode_ajuan='$kode' AND nidn='$nidn'";
        return $this->db->query($viewDiajukan)->result_array();
    }

    public function viewDisetujui()
    {
        $kode = "ADKPG";
        $nidn = $this->session->userdata('nidn');
        $viewDisetujui = "SELECT * FROM occ_pdd WHERE sts=2 AND kode_ajuan='$kode' AND nidn='$nidn'";
        return $this->db->query($viewDisetujui)->result_array();
    }

    public function viewTolak()
    {
        $kode = "ADKPG";
        $nidn = $this->session->userdata('nidn');
        $viewTolak = "SELECT * FROM occ_pdd WHERE sts=3 AND kode_ajuan='$kode' AND nidn='$nidn'";
        return $this->db->query($viewTolak)->result_array();
    }

    public function viewDitangguhkan()
    {
        $kode = "ADKPG";
        $nidn = $this->session->userdata('nidn');
        $viewDitangguhkan = "SELECT * FROM occ_pdd WHERE sts=4 AND kode_ajuan='$kode' AND nidn='$nidn'";
        return $this->db->query($viewDitangguhkan)->result_array();
    }

    public function numsDraft()
    {
        $kode = "ADKPG";
        $nidn = $this->session->userdata('nidn');
        $queryDraft = "SELECT * FROM occ_pdd WHERE sts=0 AND kode_ajuan='$kode' AND nidn='$nidn'";
        return $this->db->query($queryDraft)->num_rows();
    }

    public function numsDiajukan()
    {
        $kode = "ADKPG";
        $nidn = $this->session->userdata('nidn');
        $viewDiajukan = "SELECT * FROM occ_pdd WHERE sts=1 AND kode_ajuan='$kode' AND nidn='$nidn'";
        return $this->db->query($viewDiajukan)->num_rows();
    }

    public function numsDisetujui()
    {
        $kode = "ADKPG";
        $nidn = $this->session->userdata('nidn');
        $viewDisetujui = "SELECT * FROM occ_pdd WHERE sts=2 AND kode_ajuan='$kode' AND nidn='$nidn'";
        return $this->db->query($viewDisetujui)->num_rows();
    }

    public function numsTolak()
    {
        $kode = "ADKPG";
        $nidn = $this->session->userdata('nidn');
        $viewTolak = "SELECT * FROM occ_pdd WHERE sts=3 AND kode_ajuan='$kode' AND nidn='$nidn'";
        return $this->db->query($viewTolak)->num_rows();
    }

    public function numsDitangguhkan()
    {
        $kode = "ADKPG";
        $nidn = $this->session->userdata('nidn');
        $viewDitangguhkan = "SELECT * FROM occ_pdd WHERE sts=4 AND kode_ajuan='$kode' AND nidn='$nidn'";
        return $this->db->query($viewDitangguhkan)->num_rows();
    }

    public function detailRiwayatKepegawaian($reff)
    {
        $this->db->select('*');
        $this->db->from('occ_kepegawaian a');
        $this->db->join('master_prodi b', 'b.kode_prodi = a.kode_prodi');
        $this->db->where('a.aktif', 1);
        $this->db->where('a.reff_kepegawaian', $reff);
        $query = $this->db->get();
        return $query->row_array();
    }
}

/* End of file ModelName.php */
