<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_muhammadiyah extends CI_Model
{
    public function getView()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_muhammadiyah');
        $this->db->where('nidn', $nidn);
        $this->db->where('aktif', 1);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getDetail()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_muhammadiyah');
        $this->db->where('nidn', $nidn);
        $this->db->where('aktif', 1);
        $this->db->where('sts', 2);
        $this->db->order_by('id_km', "desc");
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function storeMuhammadiyah($data)
    {
        $query = $this->db->insert('occ_muhammadiyah', $data);
        return $query;
    }



    public function viewDraft()
    {
        $kode = "ADKTAM";
        $nidn = $this->session->userdata('nidn');
        $queryDraft = "SELECT * FROM occ_pdd WHERE sts=0 AND kode_ajuan='$kode' AND nidn='$nidn'";
        return $this->db->query($queryDraft)->result_array();
    }

    public function viewDiajukan()
    {
        $kode = "ADKTAM";
        $nidn = $this->session->userdata('nidn');
        $viewDiajukan = "SELECT * FROM occ_pdd WHERE sts=1 AND kode_ajuan='$kode' AND nidn='$nidn'";
        return $this->db->query($viewDiajukan)->result_array();
    }

    public function viewDisetujui()
    {
        $kode = "ADKTAM";
        $nidn = $this->session->userdata('nidn');
        $viewDisetujui = "SELECT * FROM occ_pdd WHERE sts=2 AND kode_ajuan='$kode' AND nidn='$nidn'";
        return $this->db->query($viewDisetujui)->result_array();
    }

    public function viewTolak()
    {
        $kode = "ADKTAM";
        $nidn = $this->session->userdata('nidn');
        $viewTolak = "SELECT * FROM occ_pdd WHERE sts=3 AND kode_ajuan='$kode' AND nidn='$nidn'";
        return $this->db->query($viewTolak)->result_array();
    }

    public function viewDitangguhkan()
    {
        $kode = "ADKTAM";
        $nidn = $this->session->userdata('nidn');
        $viewDitangguhkan = "SELECT * FROM occ_pdd WHERE sts=4 AND kode_ajuan='$kode' AND nidn='$nidn'";
        return $this->db->query($viewDitangguhkan)->result_array();
    }

    public function numsDraft()
    {
        $kode = "ADKTAM";
        $nidn = $this->session->userdata('nidn');
        $queryDraft = "SELECT * FROM occ_pdd WHERE sts=0 AND kode_ajuan='$kode' AND nidn='$nidn'";
        return $this->db->query($queryDraft)->num_rows();
    }

    public function numsDiajukan()
    {
        $kode = "ADKTAM";
        $nidn = $this->session->userdata('nidn');
        $viewDiajukan = "SELECT * FROM occ_pdd WHERE sts=1 AND kode_ajuan='$kode' AND nidn='$nidn'";
        return $this->db->query($viewDiajukan)->num_rows();
    }

    public function numsDisetujui()
    {
        $kode = "ADKTAM";
        $nidn = $this->session->userdata('nidn');
        $viewDisetujui = "SELECT * FROM occ_pdd WHERE sts=2 AND kode_ajuan='$kode' AND nidn='$nidn'";
        return $this->db->query($viewDisetujui)->num_rows();
    }

    public function numsTolak()
    {
        $kode = "ADKTAM";
        $nidn = $this->session->userdata('nidn');
        $viewTolak = "SELECT * FROM occ_pdd WHERE sts=3 AND kode_ajuan='$kode' AND nidn='$nidn'";
        return $this->db->query($viewTolak)->num_rows();
    }

    public function numsDitangguhkan()
    {
        $kode = "ADKTAM";
        $nidn = $this->session->userdata('nidn');
        $viewDitangguhkan = "SELECT * FROM occ_pdd WHERE sts=4 AND kode_ajuan='$kode' AND nidn='$nidn'";
        return $this->db->query($viewDitangguhkan)->num_rows();
    }

    public function detailRiwayatMuhammadiyah($reff)
    {
        $this->db->select('*');
        $this->db->from('occ_muhammadiyah');
        $this->db->where('reff_km', $reff);
        $query = $this->db->get();
        return $query->row_array();
    }
}

/* End of file ModelName.php */
