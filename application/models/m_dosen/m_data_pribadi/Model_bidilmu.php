<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_bidilmu extends CI_Model
{
    public function getView()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_bidang_ilmu');
        $this->db->where('nidn', $nidn);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getDetail()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_bidang_ilmu');
        $this->db->where('nidn', $nidn);
        $this->db->order_by('id_bid', "desc");
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function storeBidilmu($data)
    {
        $query = $this->db->insert('occ_bidang_ilmu', $data);
        return $query;
    }
}

/* End of file ModelName.php */
