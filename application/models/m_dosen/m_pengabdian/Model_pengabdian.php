<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_pengabdian extends CI_Model
{
    public function getView()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_tugas_tambahan');
        $this->db->where('nidn', $nidn);
        $this->db->where('aktif', 1);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function storeTugtam($data)
    {
        $query = $this->db->insert('occ_tugas_tambahan', $data);
        return $query;
    }

    public function getDetail($reff_tugtam)
    {
        $this->db->select('*');
        $this->db->from('occ_tugas_tambahan');
        $this->db->where('reff_tugtam', $reff_tugtam);
        $this->db->where('aktif', 1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function updateTugtam($reff_tugtam, $data)
    {
        $this->db->where(array('reff_tugtam' => $reff_tugtam));
        $res = $this->db->update('occ_tugas_tambahan', $data);
        return $res;
    }
}

/* End of file ModelName.php */
