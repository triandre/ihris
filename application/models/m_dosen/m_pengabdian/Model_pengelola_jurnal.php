<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_pengelola_jurnal extends CI_Model
{
    public function getView()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_pengelola_jurnal');
        $this->db->where('nidn', $nidn);
        $this->db->where('aktif', 1);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function storePengelola_jurnal($data)
    {
        $query = $this->db->insert('occ_pengelola_jurnal', $data);
        return $query;
    }

    public function getDetail($reff_pengjur)
    {
        $this->db->select('*');
        $this->db->from('occ_pengelola_jurnal');
        $this->db->where('reff_pengjur', $reff_pengjur);
        $this->db->where('aktif', 1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function updatePengelola_jurnal($reff_pengjur, $data)
    {
        $this->db->where(array('reff_pengjur' => $reff_pengjur));
        $res = $this->db->update('occ_pengelola_jurnal', $data);
        return $res;
    }
}

/* End of file ModelName.php */
