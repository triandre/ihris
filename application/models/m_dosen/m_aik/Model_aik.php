<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_aik extends CI_Model
{
    public function getView()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_aik a');
        $this->db->join('mm_aik b', 'b.id_kriteria = a.id_kriteria');
        $this->db->where('nidn', $nidn);
        $this->db->where('a.aktif', 1);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getKriteria()
    {

        $this->db->select('*');
        $this->db->from('mm_aik');
        $this->db->where('aktif', 1);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function storeAik($data)
    {
        $query = $this->db->insert('occ_aik', $data);
        return $query;
    }

    public function detailAik($reff)
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_aik a');
        $this->db->join('mm_aik b', 'b.id_kriteria = a.id_kriteria');
        $this->db->where('nidn', $nidn);
        $this->db->where('a.aktif', 1);
        $this->db->where('a.reff', $reff);
        $query = $this->db->get();
        return $query->row_array();
    }


    public function editAik($reff)
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_aik');
        $this->db->where('reff', $reff);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function updateAik($reff, $data)
    {
        $this->db->where(array('reff' => $reff));
        $res = $this->db->update('occ_aik', $data);
        return $res;
    }
}

/* End of file ModelName.php */
