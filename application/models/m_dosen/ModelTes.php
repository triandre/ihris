<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ModelTes extends CI_Model
{
    public function getTes()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_tes');
        $this->db->where('nidn', $nidn);
        $this->db->where('active', 1);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function storeTes($data)
    {
        $query = $this->db->insert('occ_tes', $data);
        return $query;
    }

    public function detailTes($id_tes)
    {
        $this->db->select('*');
        $this->db->from('occ_tes');
        $this->db->where('id_tes', $id_tes);
        $this->db->where('active', 1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function detailRiwTes($reff)
    {
        $this->db->select('*');
        $this->db->from('occ_tes');
        $this->db->where('reff_tes', $reff);
        $this->db->where('active', 1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function viewDraft()
    {
        $kode = "ADTS";
        $nidn = $this->session->userdata('nidn');
        $queryDraft = "SELECT * FROM occ_pdd WHERE sts=1 AND kode_ajuan='$kode' AND nidn='$nidn'";
        return $this->db->query($queryDraft)->result_array();
    }

    public function viewDiajukan()
    {
        $kode = "ADTS";
        $nidn = $this->session->userdata('nidn');
        $viewDiajukan = "SELECT * FROM occ_pdd WHERE sts=2 AND kode_ajuan='$kode' AND nidn='$nidn'";
        return $this->db->query($viewDiajukan)->result_array();
    }

    public function viewDisetujui()
    {
        $kode = "ADTS";
        $nidn = $this->session->userdata('nidn');
        $viewDisetujui = "SELECT * FROM occ_pdd WHERE sts=3 AND kode_ajuan='$kode' AND nidn='$nidn'";
        return $this->db->query($viewDisetujui)->result_array();
    }

    public function viewTolak()
    {
        $kode = "ADTS";
        $nidn = $this->session->userdata('nidn');
        $viewTolak = "SELECT * FROM occ_pdd WHERE sts=4 AND kode_ajuan='$kode' AND nidn='$nidn'";
        return $this->db->query($viewTolak)->result_array();
    }

    public function viewDitangguhkan()
    {
        $kode = "ADTS";
        $nidn = $this->session->userdata('nidn');
        $viewDitangguhkan = "SELECT * FROM occ_pdd WHERE sts=5 AND kode_ajuan='$kode' AND nidn='$nidn'";
        return $this->db->query($viewDitangguhkan)->result_array();
    }

    public function numsDraft()
    {
        $kode = "ADTS";
        $nidn = $this->session->userdata('nidn');
        $queryDraft = "SELECT * FROM occ_pdd WHERE sts=1 AND kode_ajuan='$kode' AND nidn='$nidn'";
        return $this->db->query($queryDraft)->num_rows();
    }

    public function numsDiajukan()
    {
        $kode = "ADTS";
        $nidn = $this->session->userdata('nidn');
        $viewDiajukan = "SELECT * FROM occ_pdd WHERE sts=2 AND kode_ajuan='$kode' AND nidn='$nidn'";
        return $this->db->query($viewDiajukan)->num_rows();
    }

    public function numsDisetujui()
    {
        $kode = "ADTS";
        $nidn = $this->session->userdata('nidn');
        $viewDisetujui = "SELECT * FROM occ_pdd WHERE sts=3 AND kode_ajuan='$kode' AND nidn='$nidn'";
        return $this->db->query($viewDisetujui)->num_rows();
    }

    public function numsTolak()
    {
        $kode = "ADTS";
        $nidn = $this->session->userdata('nidn');
        $viewTolak = "SELECT * FROM occ_pdd WHERE sts=4 AND kode_ajuan='$kode' AND nidn='$nidn'";
        return $this->db->query($viewTolak)->num_rows();
    }

    public function numsDitangguhkan()
    {
        $kode = "ADTS";
        $nidn = $this->session->userdata('nidn');
        $viewDitangguhkan = "SELECT * FROM occ_pdd WHERE sts=5 AND kode_ajuan='$kode' AND nidn='$nidn'";
        return $this->db->query($viewDitangguhkan)->num_rows();
    }
}

/* End of file ModelName.php */
