<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ModelPenfor extends CI_Model
{
    public function getPenfor()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_pendidikan_formal');
        $this->db->where('nidn', $nidn);
        $this->db->where('active', 1);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function storePenfor($data)
    {
        $query = $this->db->insert('occ_pendidikan_formal', $data);
        return $query;
    }

    public function detailPenfor($id_penfor)
    {
        $this->db->select('*');
        $this->db->from('occ_pendidikan_formal');
        $this->db->where('id_penfor', $id_penfor);
        $this->db->where('active', 1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function updateImpassing($id_impassing, $data)
    {
        $this->db->where(array('id_impassing' => $id_impassing));
        $res = $this->db->update('occ_impassing', $data);
        return $res;
    }

    public function viewDraft()
    {
        $kode = "ADPF";
        $nidn = $this->session->userdata('nidn');
        $queryDraft = "SELECT * FROM occ_pdd WHERE sts=0 AND kode_ajuan='$kode' AND nidn='$nidn'";
        return $this->db->query($queryDraft)->result_array();
    }

    public function viewDiajukan()
    {
        $kode = "ADPF";
        $nidn = $this->session->userdata('nidn');
        $viewDiajukan = "SELECT * FROM occ_pdd WHERE sts=1 AND kode_ajuan='$kode' AND nidn='$nidn'";
        return $this->db->query($viewDiajukan)->result_array();
    }

    public function viewDisetujui()
    {
        $kode = "ADPF";
        $nidn = $this->session->userdata('nidn');
        $viewDisetujui = "SELECT * FROM occ_pdd WHERE sts=2 AND kode_ajuan='$kode' AND nidn='$nidn'";
        return $this->db->query($viewDisetujui)->result_array();
    }

    public function viewTolak()
    {
        $kode = "ADPF";
        $nidn = $this->session->userdata('nidn');
        $viewTolak = "SELECT * FROM occ_pdd WHERE sts=3 AND kode_ajuan='$kode' AND nidn='$nidn'";
        return $this->db->query($viewTolak)->result_array();
    }

    public function viewDitangguhkan()
    {
        $kode = "ADPF";
        $nidn = $this->session->userdata('nidn');
        $viewDitangguhkan = "SELECT * FROM occ_pdd WHERE sts=4 AND kode_ajuan='$kode' AND nidn='$nidn'";
        return $this->db->query($viewDitangguhkan)->result_array();
    }

    public function numsDraft()
    {
        $kode = "ADPF";
        $nidn = $this->session->userdata('nidn');
        $queryDraft = "SELECT * FROM occ_pdd WHERE sts=0 AND kode_ajuan='$kode' AND nidn='$nidn'";
        return $this->db->query($queryDraft)->num_rows();
    }

    public function numsDiajukan()
    {
        $kode = "ADPF";
        $nidn = $this->session->userdata('nidn');
        $viewDiajukan = "SELECT * FROM occ_pdd WHERE sts=1 AND kode_ajuan='$kode' AND nidn='$nidn'";
        return $this->db->query($viewDiajukan)->num_rows();
    }

    public function numsDisetujui()
    {
        $kode = "ADPF";
        $nidn = $this->session->userdata('nidn');
        $viewDisetujui = "SELECT * FROM occ_pdd WHERE sts=2 AND kode_ajuan='$kode' AND nidn='$nidn'";
        return $this->db->query($viewDisetujui)->num_rows();
    }

    public function numsTolak()
    {
        $kode = "ADPF";
        $nidn = $this->session->userdata('nidn');
        $viewTolak = "SELECT * FROM occ_pdd WHERE sts=3 AND kode_ajuan='$kode' AND nidn='$nidn'";
        return $this->db->query($viewTolak)->num_rows();
    }

    public function numsDitangguhkan()
    {
        $kode = "ADPF";
        $nidn = $this->session->userdata('nidn');
        $viewDitangguhkan = "SELECT * FROM occ_pdd WHERE sts=4 AND kode_ajuan='$kode' AND nidn='$nidn'";
        return $this->db->query($viewDitangguhkan)->num_rows();
    }

    public function detailPenforRiwayat($reff)
    {
        $this->db->select('*');
        $this->db->from('occ_pendidikan_formal');
        $this->db->where('reff_penfor', $reff);
        $this->db->where('active', 1);
        $query = $this->db->get();
        return $query->row_array();
    }
}

/* End of file ModelName.php */
