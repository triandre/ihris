<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_Pubkar extends CI_Model
{
    public function getView()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_pubkar');
        $this->db->where('nidn', $nidn);
        $this->db->where('aktif', 1);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function storePenelitian($data)
    {
        $query = $this->db->insert('occ_pubkar', $data);
        return $query;
    }

    public function getDetail($reff_pubkar)
    {
        $this->db->select('*');
        $this->db->from('occ_pubkar');
        $this->db->where('reff_pubkar', $reff_pubkar);
        $this->db->where('aktif', 1);
        $query = $this->db->get();
        return $query->row_array();
    }
    public function updatePenelitian($reff_pubkar, $data)
    {
        $this->db->where(array('reff_pubkar' => $reff_pubkar));
        $res = $this->db->update('occ_pubkar', $data);
        return $res;
    }
}

/* End of file ModelName.php */
