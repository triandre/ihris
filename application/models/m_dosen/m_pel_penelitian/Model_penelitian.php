<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_Penelitian extends CI_Model
{
    public function getView()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_penelitian');
        $this->db->where('nidn', $nidn);
        $this->db->where('aktif', 1);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function storePenelitian($data)
    {
        $query = $this->db->insert('occ_penelitian', $data);
        return $query;
    }

    public function getDetail($reff_penelitian)
    {
        $this->db->select('*');
        $this->db->from('occ_penelitian');
        $this->db->where('reff_penelitian', $reff_penelitian);
        $this->db->where('aktif', 1);
        $query = $this->db->get();
        return $query->row_array();
    }
    public function updatePenelitian($reff_penelitian, $data)
    {
        $this->db->where(array('reff_penelitian' => $reff_penelitian));
        $res = $this->db->update('occ_penelitian', $data);
        return $res;
    }
}

/* End of file ModelName.php */
