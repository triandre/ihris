<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ModelKesejahteraan extends CI_Model
{
    public function getView()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_kesejahteraan');
        $this->db->where('aktif', 1);
        $this->db->where('nidn', $nidn);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function storeKesejahteraan($data)
    {
        $query = $this->db->insert('occ_kesejahteraan', $data);
        return $query;
    }

    public function detailKesejahteraan($id_kesejahteraan)
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_kesejahteraan ');
        $this->db->where('id_kesejahteraan', $id_kesejahteraan);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function updateKesejahteraan($id_kesejahteraan, $data)
    {
        $this->db->where(array('id_kesejahteraan' => $id_kesejahteraan));
        $res = $this->db->update('occ_kesejahteraan', $data);
        return $res;
    }
}

/* End of file ModelName.php */
