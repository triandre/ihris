<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ModelTunjangan extends CI_Model
{
    public function getView()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_tunjangan');
        $this->db->where('aktif', 1);
        $this->db->where('nidn', $nidn);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function storeTunjangan($data)
    {
        $query = $this->db->insert('occ_tunjangan', $data);
        return $query;
    }

    public function detailTunjangan($id_tunjangan)
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_tunjangan');
        $this->db->where('id_tunjangan', $id_tunjangan);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function updateTunjangan($id_tunjangan, $data)
    {
        $this->db->where(array('id_tunjangan' => $id_tunjangan));
        $res = $this->db->update('occ_tunjangan', $data);
        return $res;
    }
}

/* End of file ModelName.php */
