<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ModelDosen extends CI_Model
{
    public function getProfil()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_profil');
        $this->db->where('nidn', $nidn);
        $this->db->where('sts', 2);
        $this->db->order_by('reff_profil', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getUser()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_user');
        $this->db->where('nidn', $nidn);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getDetailProfil()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_profil');
        $this->db->where('nidn', $nidn);
        $this->db->where('sts', 2);
        $this->db->order_by('id_profil', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getDetailUser()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_user');
        $this->db->where('nidn', $nidn);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getDetailKdd()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_kependudukan');
        $this->db->where('nidn', $nidn);
        $this->db->where('sts', 2);
        $this->db->order_by('id_kependudukan', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getDetailKlg()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_keluarga');
        $this->db->where('nidn', $nidn);
        $this->db->where('sts', 2);
        $this->db->order_by('id_keluarga', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getDetailKtam()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_muhammadiyah');
        $this->db->where('nidn', $nidn);
        $this->db->where('sts', 2);
        $this->db->order_by('id_km', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getDetailBid()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_bidang_ilmu');
        $this->db->where('nidn', $nidn);
        $this->db->order_by('id_bid', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getDetailAlm()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_alamat');
        $this->db->where('nidn', $nidn);
        $this->db->where('sts', 2);
        $this->db->order_by('id_alamat', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getDetaiKpg()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_kepegawaian a');
        $this->db->join('master_prodi b', 'b.kode_prodi = a.kode_prodi');
        $this->db->where('a.nidn', $nidn);
        $this->db->where('a.aktif', 1);
        $this->db->where('a.sts', 2);
        $this->db->order_by('a.id_kepegawaian', "desc");
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getDetailLln()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_lain');
        $this->db->where('nidn', $nidn);
        $this->db->where('sts', 2);
        $this->db->order_by('id_lain', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }


    public function getProfilBaru($reff)
    {

        $this->db->select('*');
        $this->db->from('occ_biodata');
        $this->db->where('reff_bio', $reff);
        $query = $this->db->get();
        return $query->row_array();
    }


    public function storeProfil($data)
    {
        $query = $this->db->insert('occ_biodata', $data);
        return $query;
    }
    public function storePdd($pdd)
    {
        $queryPdd = $this->db->insert('occ_pdd', $pdd);
        return $queryPdd;
    }


    public function viewDiajukan()
    {
        $kode = "ADPROFIL";
        $nidn = $this->session->userdata('nidn');
        $viewDiajukan = "SELECT * FROM occ_pdd WHERE sts=1 AND kode_ajuan='$kode' AND nidn='$nidn'";
        return $this->db->query($viewDiajukan)->result_array();
    }

    public function viewDisetujui()
    {
        $kode = "ADPROFIL";
        $nidn = $this->session->userdata('nidn');
        $viewDisetujui = "SELECT * FROM occ_pdd WHERE sts=2 AND kode_ajuan='$kode' AND nidn='$nidn'";
        return $this->db->query($viewDisetujui)->result_array();
    }

    public function viewTolak()
    {
        $kode = "ADPROFIL";
        $nidn = $this->session->userdata('nidn');
        $viewTolak = "SELECT * FROM occ_pdd WHERE sts=3 AND kode_ajuan='$kode' AND nidn='$nidn'";
        return $this->db->query($viewTolak)->result_array();
    }




    public function numsDiajukan()
    {
        $kode = "ADPROFIL";
        $nidn = $this->session->userdata('nidn');
        $viewDiajukan = "SELECT * FROM occ_pdd WHERE sts=1 AND kode_ajuan='$kode' AND nidn='$nidn'";
        return $this->db->query($viewDiajukan)->num_rows();
    }

    public function numsDisetujui()
    {
        $kode = "ADPROFIL";
        $nidn = $this->session->userdata('nidn');
        $viewDisetujui = "SELECT * FROM occ_pdd WHERE sts=2 AND kode_ajuan='$kode' AND nidn='$nidn'";
        return $this->db->query($viewDisetujui)->num_rows();
    }

    public function numsTolak()
    {
        $kode = "ADPROFIL";
        $nidn = $this->session->userdata('nidn');
        $viewTolak = "SELECT * FROM occ_pdd WHERE sts=3 AND kode_ajuan='$kode' AND nidn='$nidn'";
        return $this->db->query($viewTolak)->num_rows();
    }
}

/* End of file ModelName.php */
