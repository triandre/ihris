<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ModelDosenDetail extends CI_Model
{
    public function getProfil($email)
    {

        $this->db->select('*');
        $this->db->from('occ_user u');
        $this->db->left('occ_biodata b', 'b.nidn = u.nidn', 'left');
        $this->db->where('u.nidn', $email);
        $this->db->where('b. sts', 2);
        $this->db->order_by('b. id_bio', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getUser($email)
    {
        $this->db->select('*');
        $this->db->from('occ_user');
        $this->db->where('email', $email);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getDetailProfil($email)
    {
        $this->db->select('*');
        $this->db->from('occ_user u');
        $this->db->join('occ_biodata b', 'b.nidn = u.nidn', 'left');
        $this->db->where('u.email', $email);
        $this->db->where('b.sts', 2);
        $this->db->order_by('b.id_bio', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getDetailKdd($email)
    {
        $this->db->select('*');
        $this->db->from('occ_user u');
        $this->db->join('occ_kependudukan k', 'k.nidn = u.nidn', 'left');
        $this->db->where('u.email', $email);
        $this->db->where('k.sts', 2);
        $this->db->order_by('k.id_kependudukan', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getDetailKlg($email)
    {

        $this->db->select('*');
        $this->db->from('occ_user u');
        $this->db->join('occ_keluarga k', 'k.nidn = u.nidn', 'left');
        $this->db->where('u.email', $email);
        $this->db->where('k.sts', 2);
        $this->db->order_by('k.id_keluarga', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getDetailKtam($email)
    {
        $this->db->select('*');
        $this->db->from('occ_user u');
        $this->db->join('occ_muhammadiyah m', 'm.nidn = u.nidn', 'left');
        $this->db->where('u.email', $email);
        $this->db->where('m.sts', 2);
        $this->db->order_by('m.id_km', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getDetailBid($email)
    {
        $this->db->select('*');
        $this->db->from('occ_user u');
        $this->db->join('occ_bidang_ilmu bi', 'bi.nidn = u.nidn', 'left');
        $this->db->where('u.email', $email);
        $this->db->order_by('bi.id_bid', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getDetailAlm($email)
    {
        $this->db->select('*');
        $this->db->from('occ_user u');
        $this->db->join('occ_alamat a', 'a.nidn = u.nidn', 'left');
        $this->db->from('occ_alamat');
        $this->db->where('u.email', $email);
        $this->db->where('a.sts', 2);
        $this->db->order_by('a.id_alamat', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getDetaiKpg($email)
    {
        $this->db->select('*');
        $this->db->from('occ_user u');
        $this->db->join('occ_kepegawaian k', 'k.nidn = u.nidn', 'left');
        $this->db->join('master_prodi b', 'b.kode_prodi = k.kode_prodi');
        $this->db->where('u.email', $email);
        $this->db->where('k.aktif', 1);
        $this->db->where('k.sts', 2);
        $this->db->order_by('k.id_kepegawaian', "desc");
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getDetailLln($email)
    {

        $this->db->select('*');
        $this->db->from('occ_user u');
        $this->db->join('occ_lain l', 'l.nidn = u.nidn', 'left');
        $this->db->where('u.email', $email);
        $this->db->where('l.sts', 2);
        $this->db->order_by('l.id_lain', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }
}

/* End of file ModelName.php */
