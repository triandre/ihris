<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_detasering extends CI_Model
{
    public function getDetasering()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_detasering');
        $this->db->where('nidn', $nidn);
        $this->db->where('aktif', 1);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function storeDetasering($data)
    {
        $query = $this->db->insert('occ_detasering', $data);
        return $query;
    }

    public function getDetail($reff)
    {
        $this->db->select('*');
        $this->db->from('occ_detasering');
        $this->db->where('reff', $reff);
        $this->db->where('aktif', 1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function updateDetasering($reff, $data)
    {
        $this->db->where(array('reff' => $reff));
        $res = $this->db->update('occ_detasering', $data);
        return $res;
    }
}

/* End of file ModelName.php */
