<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_oril extends CI_Model
{
    public function getView()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_oril');
        $this->db->where('nidn', $nidn);
        $this->db->where('aktif', 1);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function storeOril($data)
    {
        $query = $this->db->insert('occ_oril', $data);
        return $query;
    }

    public function getDetail($reff_oril)
    {
        $this->db->select('*');
        $this->db->from('occ_oril');
        $this->db->where('reff_oril', $reff_oril);
        $this->db->where('aktif', 1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function updateOril($reff_oril, $data)
    {
        $this->db->where(array('reff_oril' => $reff_oril));
        $res = $this->db->update('occ_oril', $data);
        return $res;
    }
}

/* End of file ModelName.php */
