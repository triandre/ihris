<?php
defined('BASEPATH') or exit('No direct script access allowed');

use GuzzleHttp\Client;


class Model_pengajaran extends CI_Model
{
    private $_client;
    public function __construct()
    {
        $this->_client = new Client([
            'base_uri' => 'https://apilocal.umsu.ac.id/simakad/'
        ]);
    }
    public function getPengajaran()
    {
        $email = $this->session->userdata('email');
        $response = $this->_client->request(
            'POST',
            'jadwalDosen',
            [
                'form_params' => [
                    'email' => $email
                ]
            ]
        );
        $result = json_decode($response->getBody()->getContents(), true);
        return $result['data'];
    }

    public function getProdi()
    {

        $this->db->select('*');
        $this->db->from('mm_prodi');
        $query = $this->db->get();
        return $query->result_array();
    }


    public function getDetail($id)
    {
        $this->db->select('*');
        $this->db->from('ins_bbp');
        $this->db->where('id', $id);
        $this->db->where('aktif', 1);
        $query = $this->db->get();
        return $query->row_array();
    }
}

/* End of file ModelName.php */
