<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_vs extends CI_Model
{
    public function getVs()
    {
        $nidn = $this->session->userdata('nidn');
        $this->db->select('*');
        $this->db->from('occ_vs');
        $this->db->where('nidn', $nidn);
        $this->db->where('aktif', 1);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function storeVs($data)
    {
        $query = $this->db->insert('occ_vs', $data);
        return $query;
    }

    public function getDetail($reff)
    {
        $this->db->select('*');
        $this->db->from('occ_vs');
        $this->db->where('reff', $reff);
        $this->db->where('aktif', 1);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function updateVs($reff, $data)
    {
        $this->db->where(array('reff' => $reff));
        $res = $this->db->update('occ_vs', $data);
        return $res;
    }
}

/* End of file ModelName.php */
