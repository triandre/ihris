<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Penghargaan extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_dosen/m_penunjang/ModelPenghargaan', 'mp');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Penghargaan',
            'active_menu_penunjang' => 'menu-open',
            'active_menu_pjg' => 'active',
            'active_menu_phg' => 'active',
            'phg' => $this->mp->getView()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/penunjang/Penghargaan/v_penghargaan', $data);
        $this->load->view('layouts/footer');
    }

    public function createPenghargaan()
    {
        $data = array(
            'title' => 'Tambah Penghargaan',
            'active_menu_penunjang' => 'menu-open',
            'active_menu_pjg' => 'active',
            'active_menu_phg' => 'active',
            'phg' => $this->mp->getView()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/penunjang/Penghargaan/c_penghargaan', $data);
        $this->load->view('layouts/footer');
    }

    public function savePenghargaan()
    {
        $data = array(
            'title' => 'Tambah Penghargaan',
            'active_menu_penunjang' => 'menu-open',
            'active_menu_pjg' => 'active',
            'active_menu_phg' => 'active',
            'phg' => $this->mp->getView()
        );
        $nidn = $this->session->userdata('nidn');
        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/penghargaan/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data['occ_penghargaan']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/penghargaan/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
            $data = array(
                'nidn' => $nidn,
                'kategori_penghargaan' => htmlspecialchars($this->input->post('kategori_penghargaan', true)),
                'tingkat_penghargaan' => htmlspecialchars($this->input->post('tingkat_penghargaan', true)),
                'jenis_penghargaan' => htmlspecialchars($this->input->post('jenis_penghargaan', true)),
                'nama_penghargaan' => htmlspecialchars($this->input->post('nama_penghargaan', true)),
                'tahun' => htmlspecialchars($this->input->post('tahun', true)),
                'instansi' => htmlspecialchars($this->input->post('instansi', true)),
                'file' => $upload1,
                'aktif' => 1,
                'date_created' => date('Y-m-d H:i:s')

            );


            $result = $this->mp->storePenghargaan($data);

            if ($result >= 1) {
                $this->session->set_flashdata('sukses', 'Disimpan');
                redirect('penghargaan');
            } else {
                $this->session->set_flashdata('gagal', 'Disimpan');
                redirect('createPenghargaan');
            }
        }
    }

    public function hapusPenghargaan($id_penghargaan)
    {
        $id_penghargaan = $this->uri->segment(3);
        $aktif = 0;

        $this->db->set('aktif', $aktif);
        $this->db->where('id_penghargaan', $id_penghargaan);
        $this->db->update('occ_penghargaan');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('penghargaan');
    }

    public function detailPenghargaan()
    {
        $id_penghargaan = $this->uri->segment(3);
        $data = array(
            'title' => 'Detail Penghargaan',
            'active_menu_penunjang' => 'menu-open',
            'active_menu_pjg' => 'active',
            'active_menu_phg' => 'active',
            'd' => $this->mp->detailPenghargaan($id_penghargaan),

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/penunjang/penghargaan/d_penghargaan', $data);
        $this->load->view('layouts/footer');
    }

    public function editPenghargaan()
    {
        $id_penghargaan = $this->uri->segment(3);
        $data = array(
            'title' => 'Update Anggota Profesi',
            'active_menu_penunjang' => 'menu-open',
            'active_menu_pjg' => 'active',
            'active_menu_ap' => 'active',
            'd' => $this->mp->detailPenghargaan($id_penghargaan),

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/penunjang/penghargaan/u_penghargaan', $data);
        $this->load->view('layouts/footer');
    }

    public function editPenghargaanGo()
    {

        $data = array(
            'title' => 'Update Anggota Profesi',
            'active_menu_penunjang' => 'menu-open',
            'active_menu_pjg' => 'active',
            'active_menu_ap' => 'active'
        );
        $id_penghargaan = htmlspecialchars($this->input->post('id_penghargaan', true));

        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/penghargaan/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data['occ_penghargaan']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/penghargaan/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }
        $data = array(
            'kategori_penghargaan' => htmlspecialchars($this->input->post('kategori_penghargaan', true)),
            'tingkat_penghargaan' => htmlspecialchars($this->input->post('tingkat_penghargaan', true)),
            'jenis_penghargaan' => htmlspecialchars($this->input->post('jenis_penghargaan', true)),
            'nama_penghargaan' => htmlspecialchars($this->input->post('nama_penghargaan', true)),
            'tahun' => htmlspecialchars($this->input->post('tahun', true)),
            'instansi' => htmlspecialchars($this->input->post('instansi', true)),
            'file' => $upload1,
            'aktif' => 1
        );


        $result = $this->mp->updatePenghargaan($id_penghargaan, $data);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Diubah');
            redirect('penghargaan');
        } else {
            $this->session->set_flashdata('gagal', 'Diubah');
            redirect('penghargaan/editPenghargaan/' . $id_penghargaan);
        }
    }
}
