<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Penlain extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_dosen/m_penunjang/ModelPenlain', 'mpl');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Penunjang Lain',
            'active_menu_penunjang' => 'menu-open',
            'active_menu_pjg' => 'active',
            'active_menu_pl' => 'active',
            'pl' => $this->mpl->getView()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/penunjang/penlain/v_penlain', $data);
        $this->load->view('layouts/footer');
    }

    public function createPenlain()
    {
        $data = array(
            'title' => 'Penunjang Lain',
            'active_menu_penunjang' => 'menu-open',
            'active_menu_pjg' => 'active',
            'active_menu_pl' => 'active',
            'pl' => $this->mpl->getView()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/penunjang/penlain/c_penlain', $data);
        $this->load->view('layouts/footer');
    }

    public function savePenlain()
    {
        $data = array(
            'title' => 'Penunjang Lain',
            'active_menu_penunjang' => 'menu-open',
            'active_menu_pjg' => 'active',
            'active_menu_pl' => 'active',
            'pl' => $this->mpl->getView()
        );
        $nidn = $this->session->userdata('nidn');
        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/penlain/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data['occ_penunjang_lain']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/penlain/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
            $data = array(
                'nidn' => $nidn,
                'kategori_kgt' => htmlspecialchars($this->input->post('kategori_kgt', true)),
                'nama_kgt' => htmlspecialchars($this->input->post('nama_kgt', true)),
                'jenis_kgt' => htmlspecialchars($this->input->post('jenis_kgt', true)),
                'instansi' => htmlspecialchars($this->input->post('instansi', true)),
                'tingkat' => htmlspecialchars($this->input->post('tingkat', true)),
                'no_sk' => htmlspecialchars($this->input->post('no_sk', true)),
                'tgl_mulai' => htmlspecialchars($this->input->post('tgl_mulai', true)),
                'tgl_selesai' => htmlspecialchars($this->input->post('tgl_selesai', true)),
                'file' => $upload1,
                'aktif' => 1,
                'date_created' => date('Y-m-d H:i:s')

            );


            $result = $this->mpl->storePenlain($data);

            if ($result >= 1) {
                $this->session->set_flashdata('sukses', 'Disimpan');
                redirect('penlain');
            } else {
                $this->session->set_flashdata('gagal', 'Disimpan');
                redirect('createPenlain');
            }
        }
    }

    public function hapusPenlain($id_pl)
    {
        $id_pl = $this->uri->segment(3);
        $aktif = 0;

        $this->db->set('aktif', $aktif);
        $this->db->where('id_pl', $id_pl);
        $this->db->update('occ_penunjang_lain');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('penlain');
    }

    public function detailPenlain()
    {
        $id_pl = $this->uri->segment(3);
        $data = array(
            'title' => 'Penunjang Lain',
            'active_menu_penunjang' => 'menu-open',
            'active_menu_pjg' => 'active',
            'active_menu_pl' => 'active',
            'pl' => $this->mpl->getView(),
            'd' => $this->mpl->detailPenlain($id_pl),

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/penunjang/penlain/d_penlain', $data);
        $this->load->view('layouts/footer');
    }

    public function editPenlain()
    {
        $id_pl = $this->uri->segment(3);
        $data = array(
            'title' => 'Penunjang Lain',
            'active_menu_penunjang' => 'menu-open',
            'active_menu_pjg' => 'active',
            'active_menu_pl' => 'active',
            'pl' => $this->mpl->getView(),
            'd' => $this->mpl->detailPenlain($id_pl),

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/penunjang/penlain/u_penlain', $data);
        $this->load->view('layouts/footer');
    }

    public function editPenlainGo()
    {

        $data = array(
            'title' => 'Penunjang Lain',
            'active_menu_penunjang' => 'menu-open',
            'active_menu_pjg' => 'active',
            'active_menu_pl' => 'active',
            'pl' => $this->mpl->getView()
        );
        $id_pl = htmlspecialchars($this->input->post('id_pl', true));

        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/inpassing/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data['occ_impassing']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/impassing/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }
        $data = array(
            'kategori_kgt' => htmlspecialchars($this->input->post('kategori_kgt', true)),
            'nama_kgt' => htmlspecialchars($this->input->post('nama_kgt', true)),
            'jenis_kgt' => htmlspecialchars($this->input->post('jenis_kgt', true)),
            'instansi' => htmlspecialchars($this->input->post('instansi', true)),
            'tingkat' => htmlspecialchars($this->input->post('tingkat', true)),
            'no_sk' => htmlspecialchars($this->input->post('no_sk', true)),
            'tgl_mulai' => htmlspecialchars($this->input->post('tgl_mulai', true)),
            'tgl_selesai' => htmlspecialchars($this->input->post('tgl_selesai', true)),
            'file' => $upload1,

        );


        $result = $this->mpl->updatePenlain($id_pl, $data);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Diubah');
            redirect('penlain');
        } else {
            $this->session->set_flashdata('gagal', 'Diubah');
            redirect('penlain/editPenlain/' . $id_pl);
        }
    }
}
