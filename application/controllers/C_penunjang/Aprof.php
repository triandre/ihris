<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Aprof extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_dosen/m_penunjang/ModelAprof', 'ma');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Anggota Profesi',
            'active_menu_penunjang' => 'menu-open',
            'active_menu_pjg' => 'active',
            'active_menu_ap' => 'active',
            'aprof' => $this->ma->getView()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/penunjang/aprof/v_aprof', $data);
        $this->load->view('layouts/footer');
    }

    public function createAprof()
    {
        $data = array(
            'title' => 'Tambah Data Anggota Profesi',
            'active_menu_penunjang' => 'menu-open',
            'active_menu_pjg' => 'active',
            'active_menu_ap' => 'active',
            'aprof' => $this->ma->getView()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/penunjang/aprof/c_aprof', $data);
        $this->load->view('layouts/footer');
    }

    public function saveAprof()
    {
        $data = array(
            'title' => 'Tambah Data Anggota Profesi',
            'active_menu_penunjang' => 'menu-open',
            'active_menu_pjg' => 'active',
            'active_menu_ap' => 'active',
            'aprof' => $this->ma->getView()
        );
        $nidn = $this->session->userdata('nidn');
        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/aprof/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data['occ_aprof']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/aprof/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
            $data = array(
                'nidn' => $nidn,
                'kategori_kgt' => htmlspecialchars($this->input->post('kategori_kgt', true)),
                'nama_org' => htmlspecialchars($this->input->post('nama_org', true)),
                'peran' => htmlspecialchars($this->input->post('peran', true)),
                'mulai_anggota' => htmlspecialchars($this->input->post('mulai_anggota', true)),
                'selesai_anggota' => htmlspecialchars($this->input->post('selesai_anggota', true)),
                'instansi' => htmlspecialchars($this->input->post('instansi', true)),
                'file' => $upload1,
                'aktif' => 1,
                'date_created' => date('Y-m-d H:i:s')

            );


            $result = $this->ma->storeAprof($data);

            if ($result >= 1) {
                $this->session->set_flashdata('sukses', 'Disimpan');
                redirect('aprof');
            } else {
                $this->session->set_flashdata('gagal', 'Disimpan');
                redirect('createAprof');
            }
        }
    }

    public function hapusAprof($id_af)
    {
        $id_af = $this->uri->segment(3);
        $aktif = 0;

        $this->db->set('aktif', $aktif);
        $this->db->where('id_af', $id_af);
        $this->db->update('occ_aprof');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('aprof');
    }

    public function detailAprof()
    {
        $id_af = $this->uri->segment(3);
        $data = array(
            'title' => 'Detail Anggota Profesi',
            'active_menu_penunjang' => 'menu-open',
            'active_menu_pjg' => 'active',
            'active_menu_ap' => 'active',
            'd' => $this->ma->detailAprof($id_af),

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/penunjang/aprof/d_aprof', $data);
        $this->load->view('layouts/footer');
    }

    public function editAprof()
    {
        $id_af = $this->uri->segment(2);
        $data = array(
            'title' => 'Update Anggota Profesi',
            'active_menu_penunjang' => 'menu-open',
            'active_menu_pjg' => 'active',
            'active_menu_ap' => 'active',
            'd' => $this->ma->detailAprof($id_af),

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/penunjang/aprof/u_aprof', $data);
        $this->load->view('layouts/footer');
    }

    public function editAprofGo()
    {

        $data = array(
            'title' => 'Tambah Data Anggota Profesi',
            'active_menu_penunjang' => 'menu-open',
            'active_menu_pjg' => 'active',
            'active_menu_ap' => 'active',
            'aprof' => $this->ma->getView()
        );
        $id_af = htmlspecialchars($this->input->post('id_af', true));

        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/inpassing/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data['occ_impassing']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/impassing/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }
        $data = array(
            'kategori_kgt' => htmlspecialchars($this->input->post('kategori_kgt', true)),
            'nama_org' => htmlspecialchars($this->input->post('nama_org', true)),
            'peran' => htmlspecialchars($this->input->post('peran', true)),
            'mulai_anggota' => htmlspecialchars($this->input->post('mulai_anggota', true)),
            'selesai_anggota' => htmlspecialchars($this->input->post('selesai_anggota', true)),
            'instansi' => htmlspecialchars($this->input->post('instansi', true)),
            'file' => $upload1,
            'aktif' => 1,
            'date_created' => date('Y-m-d H:i:s')

        );


        $result = $this->ma->updateAprof($id_af, $data);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Diubah');
            redirect('aprof');
        } else {
            $this->session->set_flashdata('gagal', 'Diubah');
            redirect('aprof/editAprof/' . $id_af);
        }
    }
}
