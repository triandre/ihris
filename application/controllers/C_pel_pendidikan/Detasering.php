<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Detasering extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_dosen/m_pel_pendidikan/Model_detasering', 'dt');
        $this->load->model('m_sesi/Model_user', 'mu');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Data Detasering',
            'active_menu_pendidikan' => 'menu-open',
            'active_menu_pdd' => 'active',
            'active_menu_deta' => 'active',
            'v' => $this->dt->getDetasering(),
            'akun' => $this->mu->getUser()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/pel_pendidikan/detasering/v_detasering', $data);
        $this->load->view('layouts/footer');
    }

    public function createDetasering()
    {
        $data = array(
            'title' => 'Data Detasering',
            'active_menu_pendidikan' => 'menu-open',
            'active_menu_pdd' => 'active',
            'active_menu_deta' => 'active',
            'v' => $this->dt->getDetasering(),
            'akun' => $this->mu->getUser()
        );


        $this->form_validation->set_rules('pt', 'Perguruan Tinggi Sasaran', 'required|trim');
        $this->form_validation->set_rules('kategori_kgt', 'Kategori Kegiatan', 'required|trim');
        $this->form_validation->set_rules('tgl_mulai', 'Tanggal Mulai', 'required|trim');
        $this->form_validation->set_rules('tgl_selesai', 'Tanggal Selesai', 'required|trim');
        $this->form_validation->set_rules('bidang_tugas', 'Bidang TUgas', 'required|trim');
        $this->form_validation->set_rules('des', 'Deskripsi', 'required|trim');
        $this->form_validation->set_rules('metode', 'Metode', 'required|trim');
        $this->form_validation->set_rules('no_sk', 'No. SK', 'required|trim');
        $this->form_validation->set_rules('tgl_sk', 'Tanggal SK', 'required|trim');


        if ($this->form_validation->run() == false) {
            $this->load->view('layouts/header', $data);
            $this->load->view('dosen/pel_pendidikan/detasering/c_detasering', $data);
            $this->load->view('layouts/footer');
        } else {

            $id = $this->uuid->v4();
            $reff = str_replace('-', '', $id);
            $nidn = $this->session->userdata('nidn');
            //UPLOAD
            $upload_image = $_FILES['file']['name'];
            if ($upload_image) {
                $config['allowed_types'] = 'pdf';
                $config['max_size']      = '10000';
                $config['upload_path'] = './archive/detasering/';
                $config['encrypt_name'] = TRUE;

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('file')) {
                    $old_image = $data['occ_detasering']['file'];
                    if ($old_image != 'default.pdf') {
                        unlink(FCPATH . 'archive/detasering/' . $old_image);
                    }
                    $upload1 = $this->upload->data('file_name');
                    $this->db->set('file', $upload1);
                } else {
                    echo $this->upload->display_errors();
                }
            }

            $data = array(
                'reff' => $reff,
                'nidn' => $nidn,
                'pt' => htmlspecialchars($this->input->post('pt', true)),
                'kategori_kgt' => htmlspecialchars($this->input->post('kategori_kgt', true)),
                'tgl_mulai' => htmlspecialchars($this->input->post('tgl_mulai', true)),
                'tgl_selesai' => htmlspecialchars($this->input->post('tgl_selesai', true)),
                'metode' => htmlspecialchars($this->input->post('metode', true)),
                'des' => htmlspecialchars($this->input->post('des', true)),
                'bidang_tugas' => htmlspecialchars($this->input->post('bidang_tugas', true)),
                'no_sk' => htmlspecialchars($this->input->post('no_sk', true)),
                'tgl_sk' => htmlspecialchars($this->input->post('tgl_sk', true)),
                'file' => $upload1,
                'aktif' => 1,
                'date_created' => date('Y-m-d H:i:s')

            );
            $result = $this->dt->storeDetasering($data);

            if ($result >= 1) {
                $this->session->set_flashdata('sukses', 'Disimpan');
                redirect('detasering');
            } else {
                $this->session->set_flashdata('gagal', 'Disimpan');
                redirect('createDetasering');
            }
        }
    }

    public function detailDetasering()
    {
        $reff = $this->uri->segment(3);
        $data = array(
            'title' => 'Data Detasering',
            'active_menu_pendidikan' => 'menu-open',
            'active_menu_pdd' => 'active',
            'active_menu_deta' => 'active',
            'v' => $this->dt->getDetasering(),
            'akun' => $this->mu->getUser(),
            'd' => $this->dt->getDetail($reff)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/pel_pendidikan/detasering/d_detasering', $data);
        $this->load->view('layouts/footer');
    }

    public function perbaikanDetasering()
    {
        $reff = $this->uri->segment(3);
        $data = array(
            'title' => 'Data Detasering',
            'active_menu_pendidikan' => 'menu-open',
            'active_menu_pdd' => 'active',
            'active_menu_deta' => 'active',
            'v' => $this->dt->getDetasering(),
            'akun' => $this->mu->getUser(),
            'd' => $this->dt->getDetail($reff)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/pel_pendidikan/detasering/u_detasering', $data);
        $this->load->view('layouts/footer');
    }

    public function perbaikanDetaseringGo()
    {

        $data = array(
            'title' => 'Data Detasering',
            'active_menu_pendidikan' => 'menu-open',
            'active_menu_pdd' => 'active',
            'active_menu_deta' => 'active',
            'v' => $this->dt->getDetasering(),
            'akun' => $this->mu->getUser()
        );

        $reff = htmlspecialchars($this->input->post('reff', true));
        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/detasering/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data['occ_detasering']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/detasering/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }
        $data = array(
            'pt' => htmlspecialchars($this->input->post('pt', true)),
            'kategori_kgt' => htmlspecialchars($this->input->post('kategori_kgt', true)),
            'tgl_mulai' => htmlspecialchars($this->input->post('tgl_mulai', true)),
            'tgl_selesai' => htmlspecialchars($this->input->post('tgl_selesai', true)),
            'metode' => htmlspecialchars($this->input->post('metode', true)),
            'des' => htmlspecialchars($this->input->post('des', true)),
            'bidang_tugas' => htmlspecialchars($this->input->post('bidang_tugas', true)),
            'no_sk' => htmlspecialchars($this->input->post('no_sk', true)),
            'tgl_sk' => htmlspecialchars($this->input->post('tgl_sk', true)),
            'file' => $upload1,
            'aktif' => 1
        );


        $result = $this->dt->updateDetasering($reff, $data);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Diubah');
            redirect('detasering');
        } else {
            $this->session->set_flashdata('gagal', 'Diubah');
            redirect('detasering/perbaikanDetasering/' . $reff);
        }
    }

    public function hapusDetasering($reff)
    {
        $reff = $this->uri->segment(3);
        $aktif = 0;

        $this->db->set('aktif', $aktif);
        $this->db->where('reff', $reff);
        $this->db->update('occ_detasering');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('detasering');
    }
}
