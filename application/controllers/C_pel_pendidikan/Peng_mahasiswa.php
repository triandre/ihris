<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Peng_mahasiswa extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_dosen/m_pel_pendidikan/Model_peng_mahasiswa', 'mpm');
    }

    public function index()
    {
        $data = array(
            'title' => 'Data Pengujian Mahasiswa',
            'active_menu_pendidikan' => 'menu-open',
            'active_menu_pdd' => 'active',
            'active_menu_pm' => 'active',
            'v' => $this->mpm->getPM()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/pel_pendidikan/peng_mahasiswa/v_pm', $data);
        $this->load->view('layouts/footer');
    }
}
