<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pem_mahasiswa extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_dosen/m_pel_pendidikan/Model_pem_mahasiswa', 'mpm2');
    }

    public function index()
    {
        $data = array(
            'title' => 'Data Pembinaan Mahasiswa',
            'active_menu_pendidikan' => 'menu-open',
            'active_menu_pdd' => 'active',
            'active_menu_pm2' => 'active',
            'v' => $this->mpm2->getPM2()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/pel_pendidikan/pem_mahasiswa/v_pm2', $data);
        $this->load->view('layouts/footer');
    }
}
