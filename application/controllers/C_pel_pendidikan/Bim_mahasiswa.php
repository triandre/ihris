<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bim_mahasiswa extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_dosen/m_pel_pendidikan/Model_bim_mahasiswa', 'mbm');
    }

    public function index()
    {
        $data = array(
            'title' => 'Data Bim Mahasiswa',
            'active_menu_pendidikan' => 'menu-open',
            'active_menu_pdd' => 'active',
            'active_menu_bm' => 'active',
            'v' => $this->mbm->getMBM()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/pel_pendidikan/bim_mahasiswa/v_bm', $data);
        $this->load->view('layouts/footer');
    }
}
