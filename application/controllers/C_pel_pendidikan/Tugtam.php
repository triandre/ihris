<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tugtam extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_dosen/m_pel_pendidikan/Model_tugtam', 'mtt');
        $this->load->model('m_sesi/Model_user', 'mu');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Data Tugas Tambahan',
            'active_menu_pendidikan' => 'menu-open',
            'active_menu_pdd' => 'active',
            'active_menu_tugtam' => 'active',
            'v' => $this->mtt->getView()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/pel_pendidikan/tugtam/v_tugtam', $data);
        $this->load->view('layouts/footer');
    }

    public function createTugtam()
    {
        $data = array(
            'title' => 'Data Tugas Tambahan',
            'active_menu_pendidikan' => 'menu-open',
            'active_menu_pdd' => 'active',
            'active_menu_tugtam' => 'active',
            'v' => $this->mtt->getView()
        );

        $this->form_validation->set_rules('kategori_kgt', 'kategori kegiatan', 'required|trim');
        $this->form_validation->set_rules('jenis_tugtam', 'Jenis Tugas Tambahan', 'required|trim');
        $this->form_validation->set_rules('pt_penugasan', 'Perguruan Tinggi Penugasan', 'required|trim');
        $this->form_validation->set_rules('unit_kerja', 'Unit Kerja', 'required|trim');
        $this->form_validation->set_rules('jumlah_jam', 'Jumlah Jam', 'required|trim');
        $this->form_validation->set_rules('no_sk', 'No SK', 'required|trim');
        $this->form_validation->set_rules('tgl_mulai', 'Tanggal Mulai Terhitung', 'required|trim');
        $this->form_validation->set_rules('tgl_selesai', 'Tanggal Selesai Terhitung', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->load->view('layouts/header', $data);
            $this->load->view('dosen/pel_pendidikan/tugtam/c_tugtam', $data);
            $this->load->view('layouts/footer');
        } else {

            $id = $this->uuid->v4();
            $reff_tugtam = str_replace('-', '', $id);
            $nidn = $this->session->userdata('nidn');
            //UPLOAD
            $upload_image = $_FILES['file']['name'];
            if ($upload_image) {
                $config['allowed_types'] = 'pdf';
                $config['max_size']      = '10000';
                $config['upload_path'] = './archive/tugas_tambahan/';
                $config['encrypt_name'] = TRUE;

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('file')) {
                    $old_image = $data['occ_tugas_tambahan']['file'];
                    if ($old_image != 'default.pdf') {
                        unlink(FCPATH . 'archive/tugas_tambahan/' . $old_image);
                    }
                    $upload1 = $this->upload->data('file_name');
                    $this->db->set('file', $upload1);
                } else {
                    echo $this->upload->display_errors();
                }
            }

            $data = array(
                'reff_tugtam' => $reff_tugtam,
                'nidn' => $nidn,
                'kategori_kgt' => htmlspecialchars($this->input->post('kategori_kgt', true)),
                'jenis_tugtam' => htmlspecialchars($this->input->post('jenis_tugtam', true)),
                'pt_penugasan' => htmlspecialchars($this->input->post('pt_penugasan', true)),
                'unit_kerja' => htmlspecialchars($this->input->post('unit_kerja', true)),
                'jumlah_jam' => htmlspecialchars($this->input->post('jumlah_jam', true)),
                'no_sk' => htmlspecialchars($this->input->post('no_sk', true)),
                'tgl_mulai' => htmlspecialchars($this->input->post('tgl_mulai', true)),
                'tgl_selesai' => htmlspecialchars($this->input->post('tgl_selesai', true)),
                'file' => $upload1,
                'aktif' => 1,
                'date_created' => date('Y-m-d H:i:s')
            );
            $result = $this->mtt->storeTugtam($data);

            if ($result >= 1) {
                $this->session->set_flashdata('sukses', 'Disimpan');
                redirect('tugtam');
            } else {
                $this->session->set_flashdata('gagal', 'Disimpan');
                redirect('createTugtam');
            }
        }
    }

    public function detailTugtam()
    {
        $reff_tugtam = $this->uri->segment(3);
        $data = array(
            'title' => 'Data Tugas Tambahan',
            'active_menu_pendidikan' => 'menu-open',
            'active_menu_pdd' => 'active',
            'active_menu_tugtam' => 'active',
            'v' => $this->mtt->getView(),
            'd' => $this->mtt->getDetail($reff_tugtam)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/pel_pendidikan/tugtam/d_tugtam', $data);
        $this->load->view('layouts/footer');
    }

    public function perbaikanTugtam()
    {
        $reff_tugtam = $this->uri->segment(3);
        $data = array(
            'title' => 'Data Tugas Tambahan',
            'active_menu_pendidikan' => 'menu-open',
            'active_menu_pdd' => 'active',
            'active_menu_tugtam' => 'active',
            'v' => $this->mtt->getView(),
            'd' => $this->mtt->getDetail($reff_tugtam)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/pel_pendidikan/tugtam/u_tugtam', $data);
        $this->load->view('layouts/footer');
    }

    public function perbaikanTugtamGo()
    {

        $data = array(
            'title' => 'Data Tugas Tambahan',
            'active_menu_pendidikan' => 'menu-open',
            'active_menu_pdd' => 'active',
            'active_menu_tugtam' => 'active',
            'v' => $this->mtt->getView()
        );

        $reff_tugtam = htmlspecialchars($this->input->post('reff_tugtam', true));
        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/tugas_tambahan/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data['occ_tugas_tambahan']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/tugas_tambahan/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }

        $data = array(
            'kategori_kgt' => htmlspecialchars($this->input->post('kategori_kgt', true)),
            'jenis_tugtam' => htmlspecialchars($this->input->post('jenis_tugtam', true)),
            'pt_penugasan' => htmlspecialchars($this->input->post('pt_penugasan', true)),
            'unit_kerja' => htmlspecialchars($this->input->post('unit_kerja', true)),
            'jumlah_jam' => htmlspecialchars($this->input->post('jumlah_jam', true)),
            'no_sk' => htmlspecialchars($this->input->post('no_sk', true)),
            'tgl_mulai' => htmlspecialchars($this->input->post('tgl_mulai', true)),
            'tgl_selesai' => htmlspecialchars($this->input->post('tgl_selesai', true)),
            'file' => $upload1,
            'aktif' => 1
        );

        $result = $this->mtt->updateTugtam($reff_tugtam, $data);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Diubah');
            redirect('tugtam');
        } else {
            $this->session->set_flashdata('gagal', 'Diubah');
            redirect('tugtam/perbaikanTugtam/' . $reff_tugtam);
        }
    }

    public function hapusTugtam($reff_tugtam)
    {
        $reff_tugtam = $this->uri->segment(3);
        $aktif = 0;

        $this->db->set('aktif', $aktif);
        $this->db->where('reff_tugtam', $reff_tugtam);
        $this->db->update('occ_tugas_tambahan');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('tugtam');
    }
}
