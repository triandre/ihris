<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bahan_ajar extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_publis/Model_iba', 'mi');
        $this->load->model('m_sesi/Model_user', 'mu');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Data Buku Ajar',
            'active_menu_pendidikan' => 'menu-open',
            'active_menu_pdd' => 'active',
            'active_menu_bahan' => 'active',
            'iba' => $this->mi->getIba(),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/pel_pendidikan/bahan_ajar/v_ba', $data);
        $this->load->view('layouts/footer');
    }




    public function detailIba($id)
    {
        $id = $this->uri->segment(3);
        $data = array(
            'title' => 'Detail Buku Ajar',
            'active_menu_pendidikan' => 'menu-open',
            'active_menu_pdd' => 'active',
            'active_menu_bahan' => 'active',
            'iba' => $this->mi->getIba(),
            'd' => $this->mi->getDetail($id),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/pel_pendidikan/bahan_ajar/d_ba', $data);
        $this->load->view('layouts/footer');
    }
}
