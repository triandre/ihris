<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Phki extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_publis/Model_hki', 'mhk');
        $this->load->model('m_master/ModelPangkat', 'mp');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Riwayat HKI/PATEN',
            'active_menu_ppen' => 'menu-open',
            'active_menu_ppn' => 'active',
            'active_menu_paten' => 'active',
            'v' => $this->mhk->getHki()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/pel_penelitian/hki/v_hki', $data);
        $this->load->view('layouts/footer');
    }

    public function detailPhki()
    {
        $id = $this->uri->segment(3);
        $data = array(
            'title' => 'Form Tambah Riwayat Penelitian',
            'active_menu_ppen' => 'menu-open',
            'active_menu_ppn' => 'active',
            'active_menu_paten' => 'active',
            'd' => $this->mhk->getDetail($id)

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/pel_penelitian/hki/d_hki', $data);
        $this->load->view('layouts/footer');
    }
}
