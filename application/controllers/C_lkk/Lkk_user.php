<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Lkk_user extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }

        $this->load->library('uuid');
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('ModelDashboard', 'mds');
        $this->load->model('m_lkk/Model_lkk', 'mc');
    }

    public function index()
    {
        $data = array(
            'title' => 'Bantuan Sosia & Kesehatan',
            'active_menu_bns' => 'active',
            'dp' => $this->md->getDetailProfil(),
            'bansos' => $this->mc->getView()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('lkk/user/v_lkk', $data);
        $this->load->view('layouts/footer');
    }


    public function createBantuan()
    {
        $data = array(
            'title' => 'Dashboard',
            'active_menu_pcuti' => 'active',
            'dp' => $this->md->getDetailProfil(),
            'bansos' => $this->mc->getBansos()

        );
        $this->form_validation->set_rules('email', 'Email', 'required|trim');
        $this->form_validation->set_rules('bantuan_id', 'Jenis Bantuan', 'required|trim');
        $this->form_validation->set_rules('nama_keluarga', 'Nama Anggota/keluarga', 'required|trim');
        $this->form_validation->set_rules('sts_keluarga', 'Status Keluarga', 'required|trim');
        $this->form_validation->set_rules('tgl_h', 'Tanggal', 'required|trim');
        $this->form_validation->set_rules('jk_keluarga', 'Jenis Kelamin', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->load->view('layouts/header', $data);
            $this->load->view('lkk/user/c_lkk', $data);
            $this->load->view('layouts/footer');
        } else {

            $email = $this->input->post('email');
            $tahun = date('Y');
            $id = $this->uuid->v4();
            $reff_lkk = str_replace('-', '', $id);
            $sts_keluarga = $this->input->post('sts_keluarga');

            if ($sts_keluarga == "Anggota Sakit") {
                $cek = $this->db->query("SELECT * FROM occ_bantuan_lkk WHERE email='$email' AND aktif_lkk=1 AND tahun='$tahun' AND sts_keluarga='$sts_keluarga'")->num_rows();
                if ($cek >= '3') {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible"> <button type="button" class="close" data-dismiss="alert">&times;</button><strong>INFO!</strong>Peraturan LKK 1 Tahun 3 Kali Pengajuan</div>');
                    redirect('lkk/bansos');
                } else {
                    // cek jika ada gambar yang akan diupload
                    $upload_image = $_FILES['file_lkk']['name'];

                    if ($upload_image) {
                        $config['allowed_types'] = 'jpg|jpeg|png|pdf';
                        $config['max_size']      = '10048';
                        $config['upload_path'] = './lkk/';
                        $config['encrypt_name'] = TRUE;

                        $this->load->library('upload', $config);

                        if ($this->upload->do_upload('file_lkk')) {
                            $old_image = $data['occ_bantuan_cuti']['file_lkk'];
                            if ($old_image != 'default.jpg') {
                                unlink(FCPATH . 'lkk/' . $old_image);
                            }
                            $new_image = $this->upload->data('file_name');
                        } else {
                            echo $this->upload->display_errors();
                        }
                    }
                    $data = [
                        'email' => htmlspecialchars($this->input->post('email')),
                        'reff_lkk' => $reff_lkk,
                        'bantuan_id' => htmlspecialchars($this->input->post('bantuan_id')),
                        'nama_keluarga' => htmlspecialchars($this->input->post('nama_keluarga')),
                        'sts_keluarga' => htmlspecialchars($this->input->post('sts_keluarga')),
                        'jk_keluarga' => htmlspecialchars($this->input->post('jk_keluarga')),
                        'tgl_h' => htmlspecialchars($this->input->post('tgl_h')),
                        'file_lkk' => $new_image,
                        'status_lkk' => 1,
                        'aktif_lkk' => 1,
                        'nominal' => "1000000",
                        'tahun' => date('Y'),
                        'date_created' => date('Y-m-d H:i:s')
                    ];

                    $log = [
                        'log' => "Mengajukan Permohonan Bantuan LKK $reff_lkk",
                        'email' => $this->session->userdata('email'),
                        'date_created' => time()
                    ];
                    $this->db->insert('occ_log', $log);
                    $this->db->insert('occ_bantuan_lkk', $data);
                    $this->session->set_flashdata('sukses', 'Disimpan');
                    redirect('lkk/bansos');
                }
            } elseif ($sts_keluarga == "Istri/Suami Sakit") {
                $cek = $this->db->query("SELECT * FROM occ_bantuan_lkk WHERE email='$email' AND aktif_lkk=1 AND tahun='$tahun' AND sts_keluarga='$sts_keluarga'")->num_rows();
                if ($cek >= '3') {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible"> <button type="button" class="close" data-dismiss="alert">&times;</button><strong>INFO!</strong>Peraturan LKK 1 Tahun 3 Kali Pengajuan</div>');
                    redirect('lkk/bansos');
                } else {
                    // cek jika ada gambar yang akan diupload
                    $upload_image = $_FILES['file_lkk']['name'];

                    if ($upload_image) {
                        $config['allowed_types'] = 'jpg|jpeg|png|pdf';
                        $config['max_size']      = '10048';
                        $config['upload_path'] = './lkk/';
                        $config['encrypt_name'] = TRUE;

                        $this->load->library('upload', $config);

                        if ($this->upload->do_upload('file_lkk')) {
                            $old_image = $data['occ_bantuan_cuti']['file_lkk'];
                            if ($old_image != 'default.jpg') {
                                unlink(FCPATH . 'lkk/' . $old_image);
                            }
                            $new_image = $this->upload->data('file_name');
                        } else {
                            echo $this->upload->display_errors();
                        }
                    }
                    $data = [
                        'email' => htmlspecialchars($this->input->post('email')),
                        'reff_lkk' => $reff_lkk,
                        'bantuan_id' => htmlspecialchars($this->input->post('bantuan_id')),
                        'nama_keluarga' => htmlspecialchars($this->input->post('nama_keluarga')),
                        'sts_keluarga' => htmlspecialchars($this->input->post('sts_keluarga')),
                        'jk_keluarga' => htmlspecialchars($this->input->post('jk_keluarga')),
                        'tgl_h' => htmlspecialchars($this->input->post('tgl_h')),
                        'file_lkk' => $new_image,
                        'status_lkk' => 1,
                        'nominal' => "500000",
                        'aktif_lkk' => 1,
                        'tahun' => date('Y'),
                        'date_created' => date('Y-m-d H:i:s')
                    ];

                    $log = [
                        'log' => "Mengajukan Permohonan Bantuan LKK $reff_lkk",
                        'email' => $this->session->userdata('email'),
                        'date_created' => time()
                    ];
                    $this->db->insert('occ_log', $log);
                    $this->db->insert('occ_bantuan_lkk', $data);
                    $this->session->set_flashdata('sukses', 'Disimpan');
                    redirect('lkk/bansos');
                }
            } elseif ($sts_keluarga == "Anak Kandung Sakit") {
                $cek = $this->db->query("SELECT * FROM occ_bantuan_lkk WHERE email='$email' AND aktif_lkk=1 AND tahun='$tahun' AND sts_keluarga='$sts_keluarga'")->num_rows();
                if ($cek >= '2') {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible"> <button type="button" class="close" data-dismiss="alert">&times;</button><strong>INFO!</strong>Peraturan LKK 1 Tahun 3 Kali Pengajuan</div>');
                    redirect('lkk/bansos');
                } else {
                    // cek jika ada gambar yang akan diupload
                    $upload_image = $_FILES['file_lkk']['name'];

                    if ($upload_image) {
                        $config['allowed_types'] = 'jpg|jpeg|png|pdf';
                        $config['max_size']      = '10048';
                        $config['upload_path'] = './lkk/';
                        $config['encrypt_name'] = TRUE;

                        $this->load->library('upload', $config);

                        if ($this->upload->do_upload('file_lkk')) {
                            $old_image = $data['occ_bantuan_cuti']['file_lkk'];
                            if ($old_image != 'default.jpg') {
                                unlink(FCPATH . 'lkk/' . $old_image);
                            }
                            $new_image = $this->upload->data('file_name');
                        } else {
                            echo $this->upload->display_errors();
                        }
                    }
                    $data = [
                        'email' => htmlspecialchars($this->input->post('email')),
                        'reff_lkk' => $reff_lkk,
                        'bantuan_id' => htmlspecialchars($this->input->post('bantuan_id')),
                        'nama_keluarga' => htmlspecialchars($this->input->post('nama_keluarga')),
                        'sts_keluarga' => htmlspecialchars($this->input->post('sts_keluarga')),
                        'jk_keluarga' => htmlspecialchars($this->input->post('jk_keluarga')),
                        'tgl_h' => htmlspecialchars($this->input->post('tgl_h')),
                        'file_lkk' => $new_image,
                        'status_lkk' => 1,
                        'aktif_lkk' => 1,
                        'nominal' => "250000",
                        'tahun' => date('Y'),
                        'date_created' => date('Y-m-d H:i:s')
                    ];

                    $log = [
                        'log' => "Mengajukan Permohonan Bantuan LKK $reff_lkk",
                        'email' => $this->session->userdata('email'),
                        'date_created' => time()
                    ];
                    $this->db->insert('occ_log', $log);
                    $this->db->insert('occ_bantuan_lkk', $data);
                    $this->session->set_flashdata('sukses', 'Disimpan');
                    redirect('lkk/bansos');
                }
            } elseif ($sts_keluarga == "Santunan Duka (Anggota Meninggal)") {
                // cek jika ada gambar yang akan diupload
                $upload_image = $_FILES['file_lkk']['name'];

                if ($upload_image) {
                    $config['allowed_types'] = 'jpg|jpeg|png|pdf';
                    $config['max_size']      = '10048';
                    $config['upload_path'] = './lkk/';
                    $config['encrypt_name'] = TRUE;

                    $this->load->library('upload', $config);

                    if ($this->upload->do_upload('file_lkk')) {
                        $old_image = $data['occ_bantuan_cuti']['file_lkk'];
                        if ($old_image != 'default.jpg') {
                            unlink(FCPATH . 'lkk/' . $old_image);
                        }
                        $new_image = $this->upload->data('file_name');
                    } else {
                        echo $this->upload->display_errors();
                    }
                }
                $data = [
                    'email' => htmlspecialchars($this->input->post('email')),
                    'reff_lkk' => $reff_lkk,
                    'bantuan_id' => htmlspecialchars($this->input->post('bantuan_id')),
                    'nama_keluarga' => htmlspecialchars($this->input->post('nama_keluarga')),
                    'sts_keluarga' => htmlspecialchars($this->input->post('sts_keluarga')),
                    'jk_keluarga' => htmlspecialchars($this->input->post('jk_keluarga')),
                    'tgl_h' => htmlspecialchars($this->input->post('tgl_h')),
                    'file_lkk' => $new_image,
                    'status_lkk' => 1,
                    'aktif_lkk' => 1,
                    'nominal' => "10000000",
                    'tahun' => date('Y'),
                    'date_created' => date('Y-m-d H:i:s')
                ];

                $log = [
                    'log' => "Mengajukan Permohonan Bantuan LKK $reff_lkk",
                    'email' => $this->session->userdata('email'),
                    'date_created' => time()
                ];
                $this->db->insert('occ_log', $log);
                $this->db->insert('occ_bantuan_lkk', $data);
                $this->session->set_flashdata('sukses', 'Disimpan');
                redirect('lkk/bansos');
            } elseif ($sts_keluarga == "Santunan Duka (Suami/istri,Anak,Orangtua,Mertua)") {
                // cek jika ada gambar yang akan diupload
                $upload_image = $_FILES['file_lkk']['name'];

                if ($upload_image) {
                    $config['allowed_types'] = 'jpg|jpeg|png|pdf';
                    $config['max_size']      = '10048';
                    $config['upload_path'] = './lkk/';
                    $config['encrypt_name'] = TRUE;

                    $this->load->library('upload', $config);

                    if ($this->upload->do_upload('file_lkk')) {
                        $old_image = $data['occ_bantuan_cuti']['file_lkk'];
                        if ($old_image != 'default.jpg') {
                            unlink(FCPATH . 'lkk/' . $old_image);
                        }
                        $new_image = $this->upload->data('file_name');
                    } else {
                        echo $this->upload->display_errors();
                    }
                }
                $data = [
                    'email' => htmlspecialchars($this->input->post('email')),
                    'reff_lkk' => $reff_lkk,
                    'bantuan_id' => htmlspecialchars($this->input->post('bantuan_id')),
                    'nama_keluarga' => htmlspecialchars($this->input->post('nama_keluarga')),
                    'sts_keluarga' => htmlspecialchars($this->input->post('sts_keluarga')),
                    'jk_keluarga' => htmlspecialchars($this->input->post('jk_keluarga')),
                    'tgl_h' => htmlspecialchars($this->input->post('tgl_h')),
                    'file_lkk' => $new_image,
                    'status_lkk' => 1,
                    'aktif_lkk' => 1,
                    'nominal' => "1000000",
                    'tahun' => date('Y'),
                    'date_created' => date('Y-m-d H:i:s')
                ];

                $log = [
                    'log' => "Mengajukan Permohonan Bantuan LKK $reff_lkk",
                    'email' => $this->session->userdata('email'),
                    'date_created' => time()
                ];
                $this->db->insert('occ_log', $log);
                $this->db->insert('occ_bantuan_lkk', $data);
                $this->session->set_flashdata('sukses', 'Disimpan');
                redirect('lkk/bansos');
            } elseif ($sts_keluarga == "Santunan Duka (Anggota Pensiunan Meninggal") {
                // cek jika ada gambar yang akan diupload
                $upload_image = $_FILES['file_lkk']['name'];

                if ($upload_image) {
                    $config['allowed_types'] = 'jpg|jpeg|png|pdf';
                    $config['max_size']      = '10048';
                    $config['upload_path'] = './lkk/';
                    $config['encrypt_name'] = TRUE;

                    $this->load->library('upload', $config);

                    if ($this->upload->do_upload('file_lkk')) {
                        $old_image = $data['occ_bantuan_cuti']['file_lkk'];
                        if ($old_image != 'default.jpg') {
                            unlink(FCPATH . 'lkk/' . $old_image);
                        }
                        $new_image = $this->upload->data('file_name');
                    } else {
                        echo $this->upload->display_errors();
                    }
                }
                $data = [
                    'email' => htmlspecialchars($this->input->post('email')),
                    'reff_lkk' => $reff_lkk,
                    'bantuan_id' => htmlspecialchars($this->input->post('bantuan_id')),
                    'nama_keluarga' => htmlspecialchars($this->input->post('nama_keluarga')),
                    'sts_keluarga' => htmlspecialchars($this->input->post('sts_keluarga')),
                    'jk_keluarga' => htmlspecialchars($this->input->post('jk_keluarga')),
                    'tgl_h' => htmlspecialchars($this->input->post('tgl_h')),
                    'file_lkk' => $new_image,
                    'status_lkk' => 1,
                    'aktif_lkk' => 1,
                    'nominal' => "1500000",
                    'tahun' => date('Y'),
                    'date_created' => date('Y-m-d H:i:s')
                ];

                $log = [
                    'log' => "Mengajukan Permohonan Bantuan LKK $reff_lkk",
                    'email' => $this->session->userdata('email'),
                    'date_created' => time()
                ];
                $this->db->insert('occ_log', $log);
                $this->db->insert('occ_bantuan_lkk', $data);
                $this->session->set_flashdata('sukses', 'Disimpan');
                redirect('lkk/bansos');
            }
        }
    }

    public function hapusBantuan()
    {
        $reff_lkk = $this->uri->segment(3);
        $aktif_lkk = 0;
        $this->db->set('aktif_lkk', $aktif_lkk);
        $this->db->where('reff_lkk', $reff_lkk);
        $this->db->update('occ_bantuan_lkk');

        $log = [
            'log' => "Menghapus Permohonan $reff_lkk",
            'email' => $this->session->userdata('email'),
            'date_created' => time()
        ];


        $this->db->insert('occ_log', $log);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('lkk/bansos');
    }

    public function perbaikanBantuan()
    {
        $reff_lkk = $this->uri->segment(3);
        $data = array(
            'title' => 'Perbaikan permohonan Sosial & Kesehatan',
            'active_menu_bns' => 'active',
            'dp' => $this->md->getDetailProfil(),
            'd' => $this->mc->getDetail($reff_lkk),
            'bansos' => $this->mc->getView()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('lkk/user/u_lkk', $data);
        $this->load->view('layouts/footer');
    }

    public function perbaikanBantuanGo()
    {
        $data = array(
            'title' => 'Perbaikan permohonan Sosial & Kesehatan',
            'active_menu_bns' => 'active',
            'bansos' => $this->mc->getView()
        );
        $reff_lkk = htmlspecialchars($this->input->post('reff_lkk'));
        $bantuan_id = htmlspecialchars($this->input->post('bantuan_id'));
        $nama_keluarga = htmlspecialchars($this->input->post('nama_keluarga'));
        $sts_keluarga = htmlspecialchars($this->input->post('sts_keluarga'));
        $jk_keluarga = htmlspecialchars($this->input->post('jk_keluarga'));
        $tgl_h = htmlspecialchars($this->input->post('tgl_h'));
        $status_lkk = 1;
        $aktif_lkk = 1;

        $upload_image = $_FILES['file_lkk']['name'];

        if ($upload_image) {
            $config['allowed_types'] = 'jpg|jpeg|png|pdf';
            $config['max_size']      = '10048';
            $config['upload_path'] = './lkk/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file_lkk')) {
                $old_image = $data['occ_bantuan_cuti']['file_lkk'];
                if ($old_image != 'default.jpg') {
                    unlink(FCPATH . 'lkk/' . $old_image);
                }
                $new_image = $this->upload->data('file_name');
            } else {
                echo $this->upload->display_errors();
            }
        }


        $this->db->set('bantuan_id', $bantuan_id);
        $this->db->set('nama_keluarga', $nama_keluarga);
        $this->db->set('sts_keluarga', $sts_keluarga);
        $this->db->set('jk_keluarga', $jk_keluarga);
        $this->db->set('tgl_h', $tgl_h);
        $this->db->set('status_lkk', $status_lkk);
        $this->db->set('aktif_lkk', $aktif_lkk);
        $this->db->set('file_lkk', $new_image);
        $this->db->where('reff_lkk', $reff_lkk);
        $this->db->update('occ_bantuan_lkk');

        $log = [
            'log' => "Perbaiki Permohonan $reff_lkk",
            'email' => $this->session->userdata('email'),
            'date_created' => time()
        ];


        $this->db->insert('occ_log', $log);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('lkk/bansos');
    }

    public function cetak_lkk_dosen()
    {
        $reff_lkk = $this->uri->segment(3);
        $data = array(
            'title' => 'Cetak permohonan Sosial & Kesehatan',
            'active_menu_bns' => 'active',
            'dp' => $this->md->getDetailProfil(),
            'd' => $this->mc->getDetail($reff_lkk),
            'bansos' => $this->mc->getView()
        );
        $this->load->helper("rpkata");
        $this->load->view('lkk/user/cetak_lkk_dosen', $data);
    }
}
