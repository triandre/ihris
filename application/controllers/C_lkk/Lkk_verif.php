<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Lkk_verif extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->library('uuid');
        $this->load->model('m_presensi/ModelPresensi', 'mp');
        $this->load->model('m_lkk/Model_lkk', 'mc');
    }


    public function index()
    {
        $data = array(
            'title' => 'Dashboard',
            'active_menu_db' => 'active',

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('lkk/verif/index', $data);
        $this->load->view('layouts/footer');
    }

    public function vmasuk()
    {
        $data = array(
            'title' => 'Permohonan Bantuan Sosian & Kesehatan',
            'active_menu_validasiBansos' => 'menu-open',
            'active_menu_vb' => 'active',
            'active_menu_bansospm' => 'active',
            'bansos' => $this->mc->getView_masuk()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('lkk/verif/v_masuk', $data);
        $this->load->view('layouts/footer');
    }

    public function vproses()
    {
        $data = array(
            'title' => 'Permohonan Bantuan Sosian & Kesehatan',
            'active_menu_validasiBansos' => 'menu-open',
            'active_menu_vb' => 'active',
            'active_menu_bansospros' => 'active',
            'bansos' => $this->mc->getView_proses()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('lkk/verif/v_proses', $data);
        $this->load->view('layouts/footer');
    }

    public function vdisetujui()
    {
        $data = array(
            'title' => 'Permohonan Bantuan Sosian & Kesehatan',
            'active_menu_validasiBansos' => 'menu-open',
            'active_menu_vb' => 'active',
            'active_menu_bansospd' => 'active',
            'bansos' => $this->mc->getView_disetujui()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('lkk/verif/v_disetujui', $data);
        $this->load->view('layouts/footer');
    }

    public function vditolak()
    {
        $data = array(
            'title' => 'Permohonan Bantuan Sosian & Kesehatan',
            'active_menu_validasiBansos' => 'menu-open',
            'active_menu_vb' => 'active',
            'active_menu_bansostolak' => 'active',
            'bansos' => $this->mc->getView_ditolak()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('lkk/verif/v_ditolak', $data);
        $this->load->view('layouts/footer');
    }

    public function vditerima()
    {
        $data = array(
            'title' => 'Sudah Menerima Bantuan',
            'active_menu_validasiBansos' => 'menu-open',
            'active_menu_vb' => 'active',
            'active_menu_bansosterima' => 'active',
            'bansos' => $this->mc->getView_diterima()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('lkk/verif/v_diterima', $data);
        $this->load->view('layouts/footer');
    }


    public function prosesBantuan()
    {
        $reff_lkk =  htmlspecialchars($this->input->post('reff_lkk'));
        $catatan_verif =  htmlspecialchars($this->input->post('catatan_verif'));
        $status_lkk = 2;
        $date_verif = date('Y-m-d H:i:s');
        $this->db->set('catatan_verif', $catatan_verif);
        $this->db->set('status_lkk', $status_lkk);
        $this->db->set('date_verif', $date_verif);
        $this->db->where('reff_lkk', $reff_lkk);
        $this->db->update('occ_bantuan_lkk');

        $log = [
            'log' => "LKK Memproses Permohonan $reff_lkk",
            'email' => $this->session->userdata('email'),
            'date_created' => time()
        ];


        $this->db->insert('occ_log', $log);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('verif_lkk/vmasuk');
    }

    public function tolakBantuan()
    {
        $reff_lkk =  htmlspecialchars($this->input->post('reff_lkk'));
        $catatan_verif =  htmlspecialchars($this->input->post('catatan_verif'));
        $status_lkk = 4;
        $date_verif = date('Y-m-d H:i:s');
        $this->db->set('catatan_verif', $catatan_verif);
        $this->db->set('status_lkk', $status_lkk);
        $this->db->set('date_verif', $date_verif);
        $this->db->where('reff_lkk', $reff_lkk);
        $this->db->update('occ_bantuan_lkk');

        $log = [
            'log' => "LKK Memproses Permohonan $reff_lkk",
            'email' => $this->session->userdata('email'),
            'date_created' => time()
        ];


        $this->db->insert('occ_log', $log);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('verif_lkk/vmasuk');
    }

    public function penerimaan()
    {
        $reff_lkk = $this->uri->segment(3);
        $sts_ambil = 0;
        $date_penerimaan = date('Y-m-d H:i:s');
        $this->db->set('sts_ambil', $sts_ambil);
        $this->db->set('date_penerimaan', $date_penerimaan);
        $this->db->where('reff_lkk', $reff_lkk);
        $this->db->update('occ_bantuan_lkk');

        $log = [
            'log' => "Penerimaan Bantuan $reff_lkk",
            'email' => $this->session->userdata('email'),
            'date_created' => time()
        ];


        $this->db->insert('occ_log', $log);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('verif_lkk/vdisetujui');
    }

    public function cetakKwitansi()
    {
        $reff_lkk = $this->uri->segment(3);
        $data = array(
            'title' => 'Perbaikan permohonan Sosial & Kesehatan',
            'active_menu_bns' => 'active',
            'd' => $this->mc->getDetail($reff_lkk)
        );
        $this->load->helper("rpkata");
        $this->load->view('lkk/verif/kwitansi', $data);
    }

    public function vlaporan()
    {
        $data = array(
            'title' => 'Laporan Bantuan Sosial dan Kesehatan',
            'active_menu_laporan' => 'active',
            'bansos' => $this->mc->getView_diterima()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('lkk/verif/v_laporan', $data);
        $this->load->view('layouts/footer');
    }

    public function laporanGo()
    {
        $start = $this->input->post('start');
        $end = $this->input->post('end');

        $data = array(
            'title' => 'Laporan Bantuan Sosial dan Kesehatan',
            'active_menu_laporan' => 'active',
            'bansos' => $this->mc->getlaporan($start, $end),
            'total' => $this->mc->getJumlah($start, $end),
            'dari' => $this->input->post('start'),
            'sampai' => $this->input->post('end'),
        );

        $this->load->view('lkk/verif/p_laporan', $data);
    }
}
