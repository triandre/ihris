<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Lkk_pimpinan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->library('uuid');
        $this->load->model('m_presensi/ModelPresensi', 'mp');
        $this->load->model('m_lkk/Model_lkk', 'mc');
    }


    public function vmasuk_pimpinan()
    {
        $data = array(
            'title' => 'Permohonan Bantuan Sosian & Kesehatan',
            'active_menu_pimlkk' => 'active',
            'bansos' => $this->mc->getView_masuk_pimpinan()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('lkk/pimpinan/v_masuk', $data);
        $this->load->view('layouts/footer');
    }



    public function setujuiBantuan()
    {
        $reff_lkk =  htmlspecialchars($this->input->post('reff_lkk'));
        $catatan_pimpinan =  htmlspecialchars($this->input->post('catatan_pimpinan'));
        $status_lkk = 3;
        $date_pimpinan = date('Y-m-d H:i:s');
        $this->db->set('catatan_pimpinan', $catatan_pimpinan);
        $this->db->set('status_lkk', $status_lkk);
        $this->db->set('date_pimpinan', $date_pimpinan);
        $this->db->where('reff_lkk', $reff_lkk);
        $this->db->update('occ_bantuan_lkk');

        $log = [
            'log' => "Pimpinan Setujui Permohonan $reff_lkk",
            'email' => $this->session->userdata('email'),
            'date_created' => time()
        ];


        $this->db->insert('occ_log', $log);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('pimpinan_lkk/vmasuk_pimpinan');
    }

    public function tolakBantuan()
    {
        $reff_lkk =  htmlspecialchars($this->input->post('reff_lkk'));
        $catatan_pimpinan =  htmlspecialchars($this->input->post('catatan_pimpinan'));
        $status_lkk = 4;
        $date_pimpinan = date('Y-m-d H:i:s');
        $this->db->set('catatan_pimpinan', $catatan_pimpinan);
        $this->db->set('status_lkk', $status_lkk);
        $this->db->set('date_pimpinan', $date_pimpinan);
        $this->db->where('reff_lkk', $reff_lkk);
        $this->db->update('occ_bantuan_lkk');

        $log = [
            'log' => "Pimpinan Menolak Permohonan $reff_lkk",
            'email' => $this->session->userdata('email'),
            'date_created' => time()
        ];


        $this->db->insert('occ_log', $log);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('pimpinan_lkk/vmasuk_pimpinan');
    }
}
