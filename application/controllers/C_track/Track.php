<!-- Track -->
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Track extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_track/Model_track', 'ma');
        $this->load->model('m_sesi/Model_user', 'mu');
        $this->load->library('uuid');
    }

    public function aji()
    {
        $data = array(
            'title' => 'Tracking Artikel Di Jurnal',
            'active_menu_track' => 'menu-open',
            'active_menu_tc' => 'active',
            'active_menu_taji' => 'active',
            'aji' => $this->ma->getAji(),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('track/v_aji', $data);
        $this->load->view('layouts/footer');
    }
    public function pj()
    {
        $data = array(
            'title' => 'Tracking Pengelola Jurnal',
            'active_menu_track' => 'menu-open',
            'active_menu_tc' => 'active',
            'active_menu_tpj' => 'active',
            'v' => $this->ma->getPj(),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('track/v_pj', $data);
        $this->load->view('layouts/footer');
    }

    public function hki()
    {
        $data = array(
            'title' => 'Tracking HKI',
            'active_menu_track' => 'menu-open',
            'active_menu_tc' => 'active',
            'active_menu_tpj' => 'active',
            'hki' => $this->ma->getHki(),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('track/v_hki', $data);
        $this->load->view('layouts/footer');
    }

    public function iks()
    {
        $data = array(
            'title' => 'Tracking Keynote Speaker',
            'active_menu_track' => 'menu-open',
            'active_menu_tc' => 'active',
            'active_menu_tks' => 'active',
            'iks' => $this->ma->getIks(),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('track/v_iks', $data);
        $this->load->view('layouts/footer');
    }

    public function ifip()
    {
        $data = array(
            'title' => 'Tracking Prosidding',
            'active_menu_track' => 'menu-open',
            'active_menu_tc' => 'active',
            'active_menu_tifip' => 'active',
            'ifip' => $this->ma->getIfip(),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('track/v_ifip', $data);
        $this->load->view('layouts/footer');
    }

    public function iba()
    {
        $data = array(
            'title' => 'Tracking Buku Ajar',
            'active_menu_track' => 'menu-open',
            'active_menu_tc' => 'active',
            'active_menu_tiba' => 'active',
            'iba' => $this->ma->getIba(),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('track/v_iba', $data);
        $this->load->view('layouts/footer');
    }

    public function uipjs()
    {
        $data = array(
            'title' => 'Tracking UI. Publis Jurnal',
            'active_menu_track' => 'menu-open',
            'active_menu_tc' => 'active',
            'active_menu_tuipjs' => 'active',
            'uipjs' => $this->ma->getUipjs(),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('track/v_uipjs', $data);
        $this->load->view('layouts/footer');
    }

    public function bbp()
    {
        $data = array(
            'title' => 'Tracking Bantuan Publikasi',
            'active_menu_track' => 'menu-open',
            'active_menu_tc' => 'active',
            'active_menu_tbbp' => 'active',
            'bbp' => $this->ma->getBbp(),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('track/v_bbp', $data);
        $this->load->view('layouts/footer');
    }

    public function bfio()
    {
        $data = array(
            'title' => 'Tracking Bantuan Publikasi',
            'active_menu_track' => 'menu-open',
            'active_menu_tc' => 'active',
            'active_menu_tbfio' => 'active',
            'bfio' => $this->ma->getBfio(),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('track/v_bfio', $data);
        $this->load->view('layouts/footer');
    }
    public function ubpjs()
    {
        $data = array(
            'title' => 'Tracking Bantuan Publikasi Skripsi',
            'active_menu_track' => 'menu-open',
            'active_menu_tc' => 'active',
            'active_menu_tubpjs' => 'active',
            'ubpjs' => $this->ma->getUbpjs(),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('track/v_ubpjs', $data);
        $this->load->view('layouts/footer');
    }
}
