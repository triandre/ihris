
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kpfi extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->load->model('User_model');
    }

    public function index()
    {
        $data['title'] = 'Dashboard | LAPORAN KEGIATAN PERJALAN FORUM ILMIAH';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['kpfi'] = $this->db->get_where('ins_kpfi', ['email' => $this->session->userdata('email'), 'aktif' => 1])->result_array();
        $data['userDetail'] = $this->User_model->kepegawaian();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('kpfi/index', $data);
        $this->load->view('templates/footer');
    }

    public function new()
    {

        $data['title'] = 'Dashboard |Tambah Laporan Perjalanan Forum Ilmiah';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['userDetail'] = $this->User_model->kepegawaian();

        $this->form_validation->set_rules('kategori', 'Kategori', 'required|trim');
        $this->form_validation->set_rules('nama_fi', 'Nama Forum Ilmiah', 'required|trim|is_unique[ins_kpfi.nama_fi]', [
            'is_unique' => 'Judul Sudah Pernah Di Ajukan'
        ]);
        $this->form_validation->set_rules('penyelenggara', 'Penyelenggara', 'required|trim');
        $this->form_validation->set_rules('lokasi', 'lokasi', 'required|trim');
        $this->form_validation->set_rules('tgl_awal', 'tgl_awal', 'required|trim');
        $this->form_validation->set_rules('tgl_akhir', 'tgl_akhir', 'required|trim');
        $this->form_validation->set_rules('ringkasan', 'Ringkasan', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('kpfi/new', $data);
            $this->load->view('templates/footer');
        } else {
            $email = $this->session->userdata('email');

            $upload_image = $_FILES['upload_bukti']['name'];
            if ($upload_image) {
                $config['allowed_types'] = 'pdf';
                $config['max_size']      = '10000';
                $config['upload_path'] = './file/';
                $config['encrypt_name'] = TRUE;

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('upload_bukti')) {
                    $old_image = $data['ins_kpfi']['upload_bukti'];
                    if ($old_image != 'default.jpg') {
                        unlink(FCPATH . 'file/' . $old_image);
                    }
                    $upload1 = $this->upload->data('file_name');
                    $this->db->set('upload_bukti', $upload1);
                } else {
                    echo $this->upload->display_errors();
                }
            }


            $data = [
                'kategori' => htmlspecialchars($this->input->post('kategori', true)),
                'nama_fi' => htmlspecialchars($this->input->post('nama_fi', true)),
                'penyelenggara' => htmlspecialchars($this->input->post('penyelenggara', true)),
                'lokasi' => htmlspecialchars($this->input->post('lokasi', true)),
                'tgl_awal' => htmlspecialchars($this->input->post('tgl_awal', true)),
                'tgl_akhir' => htmlspecialchars($this->input->post('tgl_akhir', true)),
                'ringkasan' => htmlspecialchars($this->input->post('ringkasan', true)),
                'email' => $email,
                'sts' => 1,
                'aktif' => 1,
                'upload_bukti' => $upload1,
                'date_created' => date('Y-m-d H:i:s')
            ];

            $log = [
                'log' => "Membuat Usulan Kegiatan Perjalanan Forum Ilmiah",
                'email' => $email,
                'date_created' => time()
            ];

            $this->db->insert('ins_kpfi', $data);
            $this->db->insert('log', $log);

            $this->session->set_flashdata('flash_a', 'success');
            redirect('kpfi');
        }
    }

    public function detail($id)
    {

        $data['title'] = 'Dashboard | Detail Laporan Perjalanan Forum Ilmiah';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['det'] = $this->db->get_where('ins_kpfi', ['id' => $id])->row_array();
        $data['userDetail'] = $this->User_model->kepegawaian();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('kpfi/detail', $data);
        $this->load->view('templates/footer');
    }

    public function nonaktif($id)
    {

        $aktif = 0;

        $this->db->set('aktif', $aktif);
        $this->db->where('id', $id);
        $this->db->update('ins_kpfi');

        $log = [
            'log' => "Hapus Usulan Kegiatan Perjalanan Forum Ilmiah $id",
            'email' => $this->session->userdata('email'),
            'date_created' => time()
        ];

        $this->db->insert('log', $log);

        $this->session->set_flashdata('flash_a', 'Success');
        redirect('kpfi');
    }

    public function edit($id)
    {

        $data['title'] = 'Dashboard | Edit Laporan Perjalanan Forum Ilmiah';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['kpfi'] = $this->db->get_where('ins_kpfi', ['id' => $id])->row_array();
        $data['userDetail'] = $this->User_model->kepegawaian();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('kpfi/edit', $data);
        $this->load->view('templates/footer');
    }

    public function editGo()
    {

        $data['title'] = 'Dashboard |Edit Laporan Perjalanan Forum Ilmiah';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['userDetail'] = $this->User_model->kepegawaian();
        $id = htmlspecialchars($this->input->post('id', true));
        $kategori = htmlspecialchars($this->input->post('kategori', true));
        $nama_fi = htmlspecialchars($this->input->post('nama_fi', true));
        $penyelenggara =  htmlspecialchars($this->input->post('penyelenggara', true));
        $lokasi =  htmlspecialchars($this->input->post('lokasi', true));
        $tgl_awal =  htmlspecialchars($this->input->post('tgl_awal', true));
        $tgl_akhir =  htmlspecialchars($this->input->post('tgl_akhir', true));
        $ringkasan =  htmlspecialchars($this->input->post('ringkasan', true));

        $upload_image = $_FILES['upload_bukti']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './file/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('upload_bukti')) {
                $old_image = $data['ins_kpfi']['upload_bukti'];
                if ($old_image != 'default.jpg') {
                    unlink(FCPATH . 'file/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('upload_bukti', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }
        $this->db->set('kategori', $kategori);
        $this->db->set('nama_fi', $nama_fi);
        $this->db->set('penyelenggara', $penyelenggara);
        $this->db->set('lokasi', $lokasi);
        $this->db->set('tgl_awal', $tgl_awal);
        $this->db->set('tgl_akhir', $tgl_akhir);
        $this->db->set('ringkasan', $ringkasan);
        $this->db->where('id', $id);
        $this->db->update('ins_kpfi');

        $log = [
            'log' => "Memngubah Data Kegiatan Perjalan Forum Ilmiah ID $id",
            'email' => $this->session->userdata('email'),
            'date_created' => time()
        ];

        $this->db->insert('log', $log);
        $this->session->set_flashdata('flash_a', 'success');
        redirect('kpfi');
    }

    public function cetak($id)
    {
        $data['title'] = 'Cetak Tiket ';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['iden'] = $this->db->get_where('identitas', ['email' => $this->session->userdata('email')])->row_array();
        $data['det'] = $this->db->get_where('ins_kpfi', ['id' => $id])->row_array();
        $data['userDetail'] = $this->User_model->kepegawaian();
        $this->load->view('cetak/c_kpfi', $data);
    }

    public function perbaiki($id)
    {
        $data['title'] = 'Dashboard | Perbaiki Laporan Perjalanan Forum Ilmiah';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['det'] = $this->db->get_where('ins_kpfi', ['id' => $id])->row_array();
        $data['userDetail'] = $this->User_model->kepegawaian();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('kpfi/perbaiki', $data);
        $this->load->view('templates/footer');
    }

    public function perbaikiGo()
    {

        $data['title'] = 'Dashboard |Edit Laporan Perjalanan Forum Ilmiah';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $id = htmlspecialchars($this->input->post('id', true));
        $kategori = htmlspecialchars($this->input->post('kategori', true));
        $nama_fi = htmlspecialchars($this->input->post('nama_fi', true));
        $penyelenggara =  htmlspecialchars($this->input->post('penyelenggara', true));
        $lokasi =  htmlspecialchars($this->input->post('lokasi', true));
        $tgl_awal =  htmlspecialchars($this->input->post('tgl_awal', true));
        $tgl_akhir =  htmlspecialchars($this->input->post('tgl_akhir', true));
        $ringkasan =  htmlspecialchars($this->input->post('ringkasan', true));
        $sts = 1;

        $upload_image = $_FILES['upload_bukti']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './file/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('upload_bukti')) {
                $old_image = $data['ins_kpfi']['upload_bukti'];
                if ($old_image != 'default.jpg') {
                    unlink(FCPATH . 'file/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('upload_bukti', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }
        $this->db->set('kategori', $kategori);
        $this->db->set('nama_fi', $nama_fi);
        $this->db->set('penyelenggara', $penyelenggara);
        $this->db->set('sts', $sts);
        $this->db->set('lokasi', $lokasi);
        $this->db->set('tgl_awal', $tgl_awal);
        $this->db->set('tgl_akhir', $tgl_akhir);
        $this->db->set('ringkasan', $ringkasan);
        $this->db->set('upload_bukti', $upload1);
        $this->db->where('id', $id);
        $this->db->update('ins_kpfi');

        $log = [
            'log' => "Memngubah Data Kegiatan Perjalan Forum Ilmiah ID $id",
            'email' => $this->session->userdata('email'),
            'date_created' => time()
        ];

        $this->db->insert('log', $log);
        $this->session->set_flashdata('flash_a', 'success');
        redirect('kpfi');
    }
}
