<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bbp extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_publis/Model_bbp', 'mb');
        $this->load->model('m_publis/Model_aji', 'ma');
        $this->load->model('m_sesi/Model_user', 'mu');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Data Usulan Bantuan Publikasi',
            'active_menu_insentif' => 'menu-open',
            'active_menu_ins' => 'active',
            'active_menu_bbp' => 'active',
            'bbp' => $this->mb->getBbp(),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/publis/bbp/v_bbp', $data);
        $this->load->view('layouts/footer');
    }

    public function createBbp()
    {
        $data = array(
            'title' => 'Form Usulan Bantuan Publikasi',
            'active_menu_insentif' => 'menu-open',
            'active_menu_ins' => 'active',
            'active_menu_bbp' => 'active',
            'bbp' => $this->mb->getBbp(),
            'akun' => $this->mu->getUser(),
            'kontribusi' => $this->ma->getKontribusi(),
            'lembaga' => $this->ma->getLembaga(),
            'kategori' => $this->ma->getKategori(),
        );

        $this->form_validation->set_rules('judul_artikel', 'judul artikel', 'required|trim|is_unique[ins_bbp.judul_artikel]', [
            'is_unique' => 'Judul Artikel Sudah Pernah Di Ajukan'
        ]);
        $this->form_validation->set_rules('nama_jurnal', 'Nama Jurnal', 'required|trim');
        $this->form_validation->set_rules('url_artikel', 'URL Artikel', 'required|trim');
        $this->form_validation->set_rules('issn', 'ISSN', 'required|trim');
        $this->form_validation->set_rules('vnt', 'vnt', 'required|trim');
        $this->form_validation->set_rules('lembaga_pengindeks', 'Lembaga Pengindeks', 'required|trim');
        $this->form_validation->set_rules('biaya_publikasi', 'Biaya Publikasi', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->load->view('layouts/header', $data);
            $this->load->view('dosen/publis/bbp/c_bbp', $data);
            $this->load->view('layouts/footer');
        } else {
            $lembaga_pengindeks = htmlspecialchars($this->input->post('lembaga_pengindeks', true));
            $biaya_publikasi = htmlspecialchars($this->input->post('biaya_publikasi', true));
            if ($lembaga_pengindeks == "Sinta 1") {
                $pagu = "3000000";
            } elseif ($lembaga_pengindeks == "Sinta 2") {
                $pagu = "2000000";
            } elseif ($lembaga_pengindeks == "Sinta 3") {
                $pagu = "1500000";
            } elseif ($lembaga_pengindeks == "Sinta 4") {
                $pagu = "1000000";
            } elseif ($lembaga_pengindeks == "Sinta 5") {
                $pagu = "750000";
            } elseif ($lembaga_pengindeks == "Sinta 6") {
                $pagu = "500000";
            } elseif ($lembaga_pengindeks == "Copernicus") {
                $pagu = "500000";
            } elseif ($lembaga_pengindeks == "DOAJ Internasional") {
                $pagu = "500000";
            } elseif ($lembaga_pengindeks == "EBSCO") {
                $pagu = "500000";
            } elseif ($lembaga_pengindeks == "Web Of Science (WoS)") {
                $pagu = "10000000";
            } elseif ($lembaga_pengindeks == "Q1") {
                $pagu = "25000000";
            } elseif ($lembaga_pengindeks == "Q2") {
                $pagu = "17500000";
            } elseif ($lembaga_pengindeks == "Q3") {
                $pagu = "12500000";
            } elseif ($lembaga_pengindeks == "Q4") {
                $pagu = "7500000";
            } elseif ($lembaga_pengindeks == "Proquest") {
                $pagu = "500000";
            } elseif ($lembaga_pengindeks == "ACI") {
                $pagu = "500000";
            } elseif ($lembaga_pengindeks == "DOAJ Nasional") {
                $pagu = "500000";
            } elseif ($lembaga_pengindeks == "Google Scholar Internasional") {
                $pagu = "500000";
            } elseif ($lembaga_pengindeks == "Google Scholar Nasional") {
                $pagu = "250000";
            } else {
                $pagu = "0";
            }

            if ($biaya_publikasi > $pagu) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>INFO!</strong> Mohon Maaf Biaya Publikasi Anda Melebihi Biaya Publikasi yang sudah di tentukan, Silahkan Baca Panduan Terbaru!!</div>');
                redirect('bbp');
            }

            $email = $this->session->userdata('email');
            $judul = htmlspecialchars($this->input->post('judul_artikel', true));
            $kategori_publikasi = htmlspecialchars($this->input->post('kategori_publikasi'));

            $internasional = $this->db->query("SELECT * FROM ins_bbp WHERE aktif=1 AND email='$email' AND batch=6")->num_rows();
            if ($internasional >= '1') {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>INFO!</strong> Mohon Maaf Skim Anda sudah Melebihi Batas</div>');
                redirect('bbp');
            } else {

                $upload_image = $_FILES['upload_permohonan']['name'];
                if ($upload_image) {
                    $config['allowed_types'] = 'pdf';
                    $config['max_size']      = '10000';
                    $config['upload_path'] = './file/';
                    $config['encrypt_name'] = TRUE;

                    $this->load->library('upload', $config);

                    if ($this->upload->do_upload('upload_permohonan')) {
                        $old_image = $data['ins_bbp']['upload_permohonan'];
                        if ($old_image != 'default.jpg') {
                            unlink(FCPATH . 'file/' . $old_image);
                        }
                        $upload1 = $this->upload->data('file_name');
                        $this->db->set('upload_permohonan', $upload1);
                    } else {
                        echo $this->upload->display_errors();
                    }
                }

                $data = [
                    'judul_artikel' => htmlspecialchars($this->input->post('judul_artikel', true)),
                    'nama_jurnal' => htmlspecialchars($this->input->post('nama_jurnal', true)),
                    'url_artikel' => htmlspecialchars($this->input->post('url_artikel', true)),
                    'issn' => htmlspecialchars($this->input->post('issn', true)),
                    'vnt' => htmlspecialchars($this->input->post('vnt', true)),
                    'kategori_publikasi' => htmlspecialchars($this->input->post('kategori_publikasi', true)),
                    'lembaga_pengindeks' => $lembaga_pengindeks,
                    'biaya_publikasi' => htmlspecialchars($this->input->post('biaya_publikasi', true)),
                    'email' => $email,
                    'pagu' => $pagu,
                    'sts' => 1,
                    'batch' => 6,
                    'aktif' => 1,
                    'upload_permohonan' => $upload1,
                    'date_created' => date('Y-m-d H:i:s')
                ];

                $log = [
                    'log' => "Membuat Usulan Bantuan Biaya Publikasih $judul",
                    'email' => $email,
                    'date_created' => time()
                ];

                $this->db->insert('ins_bbp', $data);
                $this->db->insert('occ_log', $log);

                $this->session->set_flashdata('sukses', 'Disimpan');
                redirect('bbp');
            }
        }
    }



    public function detailBbp($id)
    {
        $id = $this->uri->segment(3);
        $data = array(
            'title' => 'Detail Usulan Bantuan Publikasi',
            'active_menu_insentif' => 'menu-open',
            'active_menu_ins' => 'active',
            'active_menu_bbp' => 'active',
            'bbp' => $this->mb->getBbp(),
            'd' => $this->mb->getDetail($id),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/publis/bbp/d_bbp', $data);
        $this->load->view('layouts/footer');
    }

    public function perbaikan()
    {
        $id = $this->uri->segment(3);
        $data = array(
            'title' => 'Form Usulan Bantuan Publikasi',
            'active_menu_insentif' => 'menu-open',
            'active_menu_ins' => 'active',
            'active_menu_bbp' => 'active',
            'bbp' => $this->mb->getBbp(),
            'akun' => $this->mu->getUser(),
            'kontribusi' => $this->ma->getKontribusi(),
            'lembaga' => $this->ma->getLembaga(),
            'kategori' => $this->ma->getKategori(),
            'd' => $this->mb->getDetail($id)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/publis/bbp/u_bbp', $data);
        $this->load->view('layouts/footer');
    }

    public function perbaikanGo()
    {

        $data = array(
            'title' => 'Form Usulan Bantuan Publikasi',
            'active_menu_insentif' => 'menu-open',
            'active_menu_ins' => 'active',
            'active_menu_bbp' => 'active',
            'aji' => $this->ma->getAji(),
            'akun' => $this->mu->getUser(),
            'kontribusi' => $this->ma->getKontribusi(),
            'lembaga' => $this->ma->getLembaga(),
            'kategori' => $this->ma->getKategori(),
        );

        $id = htmlspecialchars($this->input->post('id', true));
        $judul_artikel = htmlspecialchars($this->input->post('judul_artikel', true));
        $nama_jurnal = htmlspecialchars($this->input->post('nama_jurnal', true));
        $url_artikel = htmlspecialchars($this->input->post('url_artikel', true));
        $issn = htmlspecialchars($this->input->post('issn', true));
        $vnt = htmlspecialchars($this->input->post('vnt', true));
        $email = $this->session->userdata('email');
        $sts = 1;
        $batch = 6;
        $date_created = date('Y-m-d h:i:s');
        $kategori_publikasi = htmlspecialchars($this->input->post('kategori_publikasi'));
        $lembaga_pengindeks = htmlspecialchars($this->input->post('lembaga_pengindeks', true));
        $biaya_publikasi = htmlspecialchars($this->input->post('biaya_publikasi', true));
        if ($lembaga_pengindeks == "Sinta 1") {
            $pagu = "3000000";
        } elseif ($lembaga_pengindeks == "Sinta 2") {
            $pagu = "2000000";
        } elseif ($lembaga_pengindeks == "Sinta 3") {
            $pagu = "1500000";
        } elseif ($lembaga_pengindeks == "Sinta 4") {
            $pagu = "1000000";
        } elseif ($lembaga_pengindeks == "Sinta 5") {
            $pagu = "750000";
        } elseif ($lembaga_pengindeks == "Sinta 6") {
            $pagu = "500000";
        } elseif ($lembaga_pengindeks == "Copernicus") {
            $pagu = "500000";
        } elseif ($lembaga_pengindeks == "DOAJ Internasional") {
            $pagu = "500000";
        } elseif ($lembaga_pengindeks == "EBSCO") {
            $pagu = "500000";
        } elseif ($lembaga_pengindeks == "Web Of Science (WoS)") {
            $pagu = "10000000";
        } elseif ($lembaga_pengindeks == "Q1") {
            $pagu = "25000000";
        } elseif ($lembaga_pengindeks == "Q2") {
            $pagu = "17500000";
        } elseif ($lembaga_pengindeks == "Q3") {
            $pagu = "12500000";
        } elseif ($lembaga_pengindeks == "Q4") {
            $pagu = "7500000";
        } elseif ($lembaga_pengindeks == "Proquest") {
            $pagu = "500000";
        } elseif ($lembaga_pengindeks == "ACI") {
            $pagu = "500000";
        } elseif ($lembaga_pengindeks == "DOAJ Nasional") {
            $pagu = "500000";
        } elseif ($lembaga_pengindeks == "Google Scholar Internasional") {
            $pagu = "500000";
        } elseif ($lembaga_pengindeks == "Google Scholar Nasional") {
            $pagu = "250000";
        } else {
            $pagu = "0";
        }

        if ($biaya_publikasi > $pagu) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>INFO!</strong> Mohon Maaf Biaya Publikasi Anda Melebihi Biaya Publikasi yang sudah di tentukan, Silahkan Baca Panduan Terbaru!!</div>');
            redirect('bbp');
        }


        $upload_image = $_FILES['upload_permohonan']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './file/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('upload_permohonan')) {
                $old_image = $data['ins_bbp']['upload_permohonan'];
                if ($old_image != 'default.jpg') {
                    unlink(FCPATH . 'file/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('upload_permohonan', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }

        $this->db->set('judul_artikel', $judul_artikel);
        $this->db->set('judul_artikel', $judul_artikel);
        $this->db->set('nama_jurnal', $nama_jurnal);
        $this->db->set('url_artikel', $url_artikel);
        $this->db->set('issn', $issn);
        $this->db->set('vnt', $vnt);
        $this->db->set('pagu', $pagu);
        $this->db->set('batch', $batch);
        $this->db->set('sts', $sts);
        $this->db->set('lembaga_pengindeks', $lembaga_pengindeks);
        $this->db->set('biaya_publikasi', $biaya_publikasi);
        $this->db->set('date_created', $date_created);
        $this->db->where('id', $id);
        $this->db->update('ins_bbp');

        $log = [
            'log' => "Memngubah Data Insentif Forum Ilmiah Proseeding ID $id",
            'email' => $email,
            'date_created' => time()
        ];

        $this->db->insert('occ_log', $log);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('bbp');
    }

    public function cetak()
    {
        $id = $this->uri->segment(3);
        $data = array(
            'title' => 'Detail Usulan Bantuan Publikasi',
            'bbp' => $this->mb->getBbp(),
            'd' => $this->mb->getDetail($id),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('dosen/publis/cetak/c_bbp', $data);
    }

    public function history($email)
    {

        $data['title'] = 'Dashboard | History Usulan Bantuan Publikasi';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['history'] = $this->db->get_where('ins_aji', ['email' => $email])->result_array();
        $data['userDetail'] = $this->User_model->kepegawaian();
        $data['det'] = $this->db->get_where('user', ['email' => $email])->row_array();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('iaj/history', $data);
        $this->load->view('templates/footer');
    }
}
