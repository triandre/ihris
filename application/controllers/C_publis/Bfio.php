<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bfio extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_publis/Model_bfio', 'mbf');
        $this->load->model('m_publis/Model_aji', 'ma');
        $this->load->model('m_sesi/Model_user', 'mu');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Data Usulan Bantuan Seminar Nasional/Internasional',
            'active_menu_insentif' => 'menu-open',
            'active_menu_ins' => 'active',
            'active_menu_bfio' => 'active',
            'bfio' => $this->mbf->getBfio(),
            'akun' => $this->mu->getUser()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/publis/bfio/v_bfio', $data);
        $this->load->view('layouts/footer');
    }

    public function createBfio()
    {
        $data = array(
            'title' => 'Form Usulan Bantuan Seminar Nasional/Internasional',
            'active_menu_insentif' => 'menu-open',
            'active_menu_ins' => 'active',
            'active_menu_bfio' => 'active',
            'bfio' => $this->mbf->getbfio(),
            'akun' => $this->mu->getUser(),
            'kontribusi' => $this->ma->getKontribusi(),
            'lembaga' => $this->ma->getLembaga(),
            'kategori' => $this->ma->getKategori(),
        );
        $this->form_validation->set_rules('nama_fi', 'Nama Forum ilmiah', 'required|trim');
        $this->form_validation->set_rules('web_fi', 'Web Forum ilmiah', 'required|trim');
        $this->form_validation->set_rules('institusi_penyelenggara', 'institusi penyelenggara', 'required|trim');
        $this->form_validation->set_rules('tgl_penyelenggara', 'Tanggal penyelenggara', 'required|trim');
        $this->form_validation->set_rules('lokasi', 'Lokasi', 'required|trim');
        $this->form_validation->set_rules('jenis_fi', 'jenis_fi', 'required|trim');
        $this->form_validation->set_rules('kgt', 'kgt', 'required|trim');
        $this->form_validation->set_rules('biaya_pendaftaran', 'biaya_pendaftaran', 'required|trim');
        $this->form_validation->set_rules('judul_artikel', 'Judul Artikel', 'required|trim|is_unique[ins_bfio.judul_artikel]', [
            'is_unique' => 'Judul Artikel Sudah Pernah Di Ajukan'
        ]);
        $this->form_validation->set_rules('pertanyaan1', 'Jawab', 'required|trim');
        $this->form_validation->set_rules('pertanyaan2', 'Jawab', 'required|trim');
        $this->form_validation->set_rules('pertanyaan3', 'Jawab', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->load->view('layouts/header', $data);
            $this->load->view('dosen/publis/bfio/c_bfio', $data);
            $this->load->view('layouts/footer');
        } else {
            $email = $this->session->userdata('email');
            $judul = htmlspecialchars($this->input->post('judul_artikel', true));
            $jenis_fi = htmlspecialchars($this->input->post('jenis_fi'));
            $kgt = htmlspecialchars($this->input->post('kgt'));

            if ($kgt == "Daring") {
                $insentif_tambahan = "100000";
            } else {
                $insentif_tambahan = "0";
            }

            $internasional = $this->db->query("SELECT * FROM ins_bfio WHERE aktif=1 AND email='$email' AND batch=6")->num_rows();
            if ($internasional >= '1') {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>INFO!</strong> Mohon Maaf Skim Anda sudah Melebihi Batas</div>');
                redirect('bfio');
            } else {

                $upload_image = $_FILES['upload_laporan']['name'];
                if ($upload_image) {
                    $config['allowed_types'] = 'pdf';
                    $config['max_size']      = '10000';
                    $config['upload_path'] = './file/';
                    $config['encrypt_name'] = TRUE;

                    $this->load->library('upload', $config);

                    if ($this->upload->do_upload('upload_laporan')) {
                        $old_image = $data['ins_bfio']['upload_laporan'];
                        if ($old_image != 'default.jpg') {
                            unlink(FCPATH . 'file/' . $old_image);
                        }
                        $upload1 = $this->upload->data('file_name');
                        $this->db->set('upload_laporan', $upload1);
                    } else {
                        echo $this->upload->display_errors();
                    }
                }

                $data = [
                    'nama_fi' => htmlspecialchars($this->input->post('nama_fi', true)),
                    'web_fi' => htmlspecialchars($this->input->post('web_fi', true)),
                    'institusi_penyelenggara' => htmlspecialchars($this->input->post('institusi_penyelenggara', true)),
                    'tgl_penyelenggara' => htmlspecialchars($this->input->post('tgl_penyelenggara', true)),
                    'lokasi' => htmlspecialchars($this->input->post('lokasi', true)),
                    'jenis_fi' => htmlspecialchars($this->input->post('jenis_fi', true)),
                    'kgt' => htmlspecialchars($this->input->post('kgt', true)),
                    'biaya_pendaftaran' => preg_replace('/[^0-9]/', '', htmlspecialchars($this->input->post('biaya_pendaftaran', true))),
                    'insentif' => preg_replace('/[^0-9]/', '', htmlspecialchars($this->input->post('biaya_pendaftaran', true))),
                    'insentif_disetujui' => preg_replace('/[^0-9]/', '', htmlspecialchars($this->input->post('biaya_pendaftaran', true))),
                    'judul_artikel' => htmlspecialchars($this->input->post('judul_artikel', true)),
                    'pertanyaan1' => htmlspecialchars($this->input->post('pertanyaan1', true)),
                    'pertanyaan2' => htmlspecialchars($this->input->post('pertanyaan2', true)),
                    'pertanyaan3' => htmlspecialchars($this->input->post('pertanyaan3', true)),
                    'pertanyaan4' => htmlspecialchars($this->input->post('pertanyaan4', true)),
                    'insentif_tambahan' => $insentif_tambahan,
                    'email' => $email,
                    'upload_laporan' => $upload1,
                    'sts' => 1,
                    'batch' => 6,
                    'aktif' => 1,
                    'date_created' => date('Y-m-d H:i:s')
                ];


                $log = [
                    'log' => "Membuat Usulan Bantuan Mengikuti Seminar Internasional/Nasional $judul",
                    'email' => $email,
                    'date_created' => time()
                ];

                $this->db->insert('ins_bfio', $data);
                $this->db->insert('occ_log', $log);

                $this->session->set_flashdata('sukses', 'Disimpan');
                redirect('bfio');
            }
        }
    }



    public function detailbfio($id)
    {
        $id = $this->uri->segment(3);
        $data = array(
            'title' => 'Detail Usulan  Bantuan Seminar Nasional/Internasional',
            'active_menu_insentif' => 'menu-open',
            'active_menu_ins' => 'active',
            'active_menu_bfio' => 'active',
            'bfio' => $this->mbf->getbfio(),
            'd' => $this->mbf->getDetail($id),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/publis/bfio/d_bfio', $data);
        $this->load->view('layouts/footer');
    }

    public function perbaikan()
    {
        $id = $this->uri->segment(3);
        $data = array(
            'title' => 'Form Usulan  Bantuan Seminar Nasional/Internasional',
            'active_menu_insentif' => 'menu-open',
            'active_menu_ins' => 'active',
            'active_menu_bfio' => 'active',
            'bfio' => $this->mbf->getbfio(),
            'akun' => $this->mu->getUser(),
            'kontribusi' => $this->ma->getKontribusi(),
            'lembaga' => $this->ma->getLembaga(),
            'kategori' => $this->ma->getKategori(),
            'd' => $this->mbf->getDetail($id)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/publis/bfio/u_bfio', $data);
        $this->load->view('layouts/footer');
    }

    public function perbaikanGo()
    {

        $data = array(
            'title' => 'Form Usulan  Bantuan Seminar Nasional/Internasional',
            'active_menu_insentif' => 'menu-open',
            'active_menu_ins' => 'active',
            'active_menu_bfio' => 'active',
            'aji' => $this->ma->getAji(),
            'akun' => $this->mu->getUser(),
            'kontribusi' => $this->ma->getKontribusi(),
            'lembaga' => $this->ma->getLembaga(),
            'kategori' => $this->ma->getKategori(),
        );

        $id = htmlspecialchars($this->input->post('id', true));
        $nama_fi = htmlspecialchars($this->input->post('nama_fi', true));
        $web_fi = htmlspecialchars($this->input->post('web_fi', true));
        $kgt = htmlspecialchars($this->input->post('kgt', true));
        $institusi_penyelenggara = htmlspecialchars($this->input->post('institusi_penyelenggara', true));
        $tgl_penyelenggara = htmlspecialchars($this->input->post('tgl_penyelenggara', true));
        $lokasi = htmlspecialchars($this->input->post('lokasi', true));
        $jenis_fi = htmlspecialchars($this->input->post('jenis_fi', true));
        $biaya_pendaftaran = preg_replace('/[^0-9]/', '', htmlspecialchars($this->input->post('biaya_pendaftaran', true)));
        $insentif = preg_replace('/[^0-9]/', '', htmlspecialchars($this->input->post('biaya_pendaftaran', true)));
        $insentif_disetujui = preg_replace('/[^0-9]/', '', htmlspecialchars($this->input->post('biaya_pendaftaran', true)));
        $judul_artikel = htmlspecialchars($this->input->post('judul_artikel', true));
        $pertanyaan1 = htmlspecialchars($this->input->post('pertanyaan1', true));
        $pertanyaan2 = htmlspecialchars($this->input->post('pertanyaan2', true));
        $pertanyaan3 = htmlspecialchars($this->input->post('pertanyaan3', true));
        $pertanyaan4 = htmlspecialchars($this->input->post('pertanyaan4', true));
        $email = $this->session->userdata('email');
        $sts = 1;
        $batch = 6;
        $date_created = date('Y-m-d h:i:s');


        if ($kgt == "Daring") {
            $insentif_tambahan = "100000";
        } else {
            $insentif_tambahan = "0";
        }



        $upload_image = $_FILES['upload_laporan']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './file/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('upload_laporan')) {
                $old_image = $data['ins_bfio']['upload_laporan'];
                if ($old_image != 'default.jpg') {
                    unlink(FCPATH . 'file/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('upload_laporan', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }

        $this->db->set('nama_fi', $nama_fi);
        $this->db->set('web_fi', $web_fi);
        $this->db->set('institusi_penyelenggara', $institusi_penyelenggara);
        $this->db->set('tgl_penyelenggara', $tgl_penyelenggara);
        $this->db->set('lokasi', $lokasi);
        $this->db->set('jenis_fi', $jenis_fi);
        $this->db->set('date_created', $date_created);
        $this->db->set('insentif_tambahan', $insentif_tambahan);
        $this->db->set('biaya_pendaftaran', $biaya_pendaftaran);
        $this->db->set('insentif', $biaya_pendaftaran);
        $this->db->set('insentif_disetujui', $biaya_pendaftaran);
        $this->db->set('judul_artikel', $judul_artikel);
        $this->db->set('pertanyaan1', $pertanyaan1);
        $this->db->set('pertanyaan2', $pertanyaan2);
        $this->db->set('pertanyaan3', $pertanyaan3);
        $this->db->set('pertanyaan4', $pertanyaan4);
        $this->db->set('sts', $sts);
        $this->db->set('batch', $batch);
        $this->db->where('id', $id);
        $this->db->update('ins_bfio');

        $log = [
            'log' => "Memngubah Data Usulan Bantuan Mengikuti Seminar Internasional/Nasional ID $id",
            'email' => $email,
            'date_created' => time()
        ];

        $this->db->insert('occ_log', $log);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('bfio');
    }

    public function cetak()
    {
        $id = $this->uri->segment(3);
        $data = array(
            'title' => 'Detail Usulan Bantuan Publikasi',
            'bfio' => $this->mbf->getbfio(),
            'd' => $this->mbf->getDetail($id),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('dosen/publis/cetak/c_bfio', $data);
    }


    public function history($email)
    {

        $data['title'] = 'Dashboard | History Usulan  Bantuan Seminar Nasional/Internasional';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['history'] = $this->db->get_where('ins_aji', ['email' => $email])->result_array();
        $data['userDetail'] = $this->User_model->kepegawaian();
        $data['det'] = $this->db->get_where('user', ['email' => $email])->row_array();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('iaj/history', $data);
        $this->load->view('templates/footer');
    }
}
