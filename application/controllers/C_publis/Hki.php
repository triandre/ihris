<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Hki extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_publis/Model_hki', 'mh');
        $this->load->model('m_sesi/Model_user', 'mu');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Data Usulan Insentif Hki/Paten',
            'active_menu_insentif' => 'menu-open',
            'active_menu_ins' => 'active',
            'active_menu_hki' => 'active',
            'hki' => $this->mh->getHki(),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/publis/hki/v_hki', $data);
        $this->load->view('layouts/footer');
    }

    public function createHki()
    {
        $data = array(
            'title' => 'Data Usulan Insentif Hki/Paten',
            'active_menu_insentif' => 'menu-open',
            'active_menu_ins' => 'active',
            'active_menu_hki' => 'active',
            'hki' => $this->mh->getHki(),
            'akun' => $this->mu->getUser()
        );

        $this->form_validation->set_rules('judul_hki', 'Judul HKI', 'required|trim|is_unique[ins_hki.judul_hki]', [
            'is_unique' => 'Judul HKI Sudah Pernah Di Ajukan'
        ]);
        $this->form_validation->set_rules('no_sertifikat', 'Nomor Sertifikat', 'required|trim|is_unique[ins_hki.no_sertifikat]', [
            'is_unique' => 'Nomor Sertifikat HKI Sudah Pernah Di Ajukan'
        ]);
        $this->form_validation->set_rules('tgl_pengajuan_hki', 'Tanggal Pengajuan HKI', 'required|trim');
        $this->form_validation->set_rules('tgl_persetujuan_hki', 'Tanggal Persetujuan HKI', 'required|trim');
        $this->form_validation->set_rules('jenis_hki', 'Jenis HKI', 'required|trim');
        $this->form_validation->set_rules('pertanyaan1', 'pertanyan1', 'required|trim');
        $this->form_validation->set_rules('pertanyaan2', 'pertanyan2', 'required|trim');
        $this->form_validation->set_rules('kategori_hki', 'Kategori HKI', 'required|trim');
        $this->form_validation->set_rules('url_ipr', 'URL IPR', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->load->view('layouts/header', $data);
            $this->load->view('dosen/publis/hki/c_hki', $data);
            $this->load->view('layouts/footer');
        } else {
            $email = $this->session->userdata('email');
            $kategori_hki = htmlspecialchars($this->input->post('kategori_hki'));

            $internasional = $this->db->query("SELECT * FROM ins_hki WHERE aktif=1  AND email='$email' AND batch=6")->num_rows();
            if ($internasional >= '1') {
                $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible"> <button type="button" class="close" data-dismiss="alert">&times;</button><strong>INFO!</strong> Mohon Maaf Skim Anda sudah Melebihi Batas</div>');
                redirect('hki');
            } else {

                $upload_image = $_FILES['upload_sertifikat']['name'];
                if ($upload_image) {
                    $config['allowed_types'] = 'pdf';
                    $config['max_size']      = '10000';
                    $config['upload_path'] = './file/';
                    $config['encrypt_name'] = TRUE;

                    $this->load->library('upload', $config);

                    if ($this->upload->do_upload('upload_sertifikat')) {
                        $old_image = $data['ins_hki']['upload_sertifikat'];
                        if ($old_image != 'default.jpg') {
                            unlink(FCPATH . 'file/' . $old_image);
                        }
                        $sertifikat = $this->upload->data('file_name');
                        $this->db->set('upload_sertifikat', $sertifikat);
                    } else {
                        echo $this->upload->display_errors();
                    }
                }

                $data = [
                    'judul_hki' =>  htmlspecialchars($this->input->post('judul_hki', true)),
                    'no_sertifikat' =>  htmlspecialchars($this->input->post('no_sertifikat', true)),
                    'tgl_pengajuan_hki' => $this->input->post('tgl_pengajuan_hki', true),
                    'tgl_persetujuan_hki' =>  $this->input->post('tgl_persetujuan_hki', true),
                    'jenis_hki' =>  htmlspecialchars($this->input->post('jenis_hki', true)),
                    'pertanyaan1' =>  htmlspecialchars($this->input->post('pertanyaan1', true)),
                    'pertanyaan2' =>  htmlspecialchars($this->input->post('pertanyaan2', true)),
                    'url_ipr' =>  htmlspecialchars($this->input->post('url_ipr', true)),
                    'kategori_hki' =>  htmlspecialchars($this->input->post('kategori_hki', true)),
                    'email' => $email,
                    'sts' => 1,
                    'aktif' => 1,
                    'batch' => 6,
                    'insentif' => 0,
                    'upload_sertifikat' => $sertifikat,
                    'date_created' => date('Y-m-d H:i:s')
                ];

                $log = [
                    'log' => "Membuat Insentif HKI",
                    'email' => $email,
                    'date_created' => time()
                ];

                $this->db->insert('ins_hki', $data);
                $this->db->insert('occ_log', $log);

                $this->session->set_flashdata('sukses', 'Disimpan');
                redirect('hki');
            }
        }
    }



    public function detailHki($id)
    {
        $id = $this->uri->segment(3);
        $data = array(
            'title' => 'Detail Usulan Insentif Hki/Paten',
            'active_menu_insentif' => 'menu-open',
            'active_menu_ins' => 'active',
            'active_menu_hki' => 'active',
            'hki' => $this->mh->getHki(),
            'd' => $this->mh->getDetail($id),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/publis/hki/d_hki', $data);
        $this->load->view('layouts/footer');
    }


    public function nonaktif($id)
    {

        $aktif = 0;

        $this->db->set('aktif', $aktif);
        $this->db->where('id', $id);
        $this->db->update('ins_aji');


        $log = [
            'log' => "Menghapus ID $id",
            'email' => $this->session->userdata('email'),
            'date_created' => time()
        ];

        $this->db->insert('log', $log);

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('aji');
    }



    public function perbaikan()
    {
        $id = $this->uri->segment(3);
        $data = array(
            'title' => 'Form Usulan Insentif Artikel Dijurnal',
            'active_menu_insentif' => 'menu-open',
            'active_menu_ins' => 'active',
            'active_menu_aji' => 'active',
            'd' => $this->mh->getDetail($id),
            'hki' => $this->mh->getHki(),
            'akun' => $this->mu->getUser()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/publis/hki/u_hki', $data);
        $this->load->view('layouts/footer');
    }

    public function perbaikanGo()
    {

        $data = array(
            'title' => 'Form Usulan Insentif Artikel Dijurnal',
            'active_menu_insentif' => 'menu-open',
            'active_menu_ins' => 'active',
            'active_menu_aji' => 'active',
            'hki' => $this->mh->getHki(),
            'akun' => $this->mu->getUser()
        );


        $id = htmlspecialchars($this->input->post('id', true));
        $email = $this->session->userdata('email');
        $judul_hki =  htmlspecialchars($this->input->post('judul_hki', true));
        $no_sertifikat =  htmlspecialchars($this->input->post('no_sertifikat', true));
        $tgl_pengajuan_hki = $this->input->post('tgl_pengajuan_hki', true);
        $tgl_persetujuan_hki =  $this->input->post('tgl_persetujuan_hki', true);
        $jenis_hki =  htmlspecialchars($this->input->post('jenis_hki', true));
        $kategori_hki =  htmlspecialchars($this->input->post('kategori_hki', true));
        $pertanyaan1 =  htmlspecialchars($this->input->post('pertanyaan1', true));
        $pertanyaan2 =  htmlspecialchars($this->input->post('pertanyaan2', true));
        $url_ipr =  htmlspecialchars($this->input->post('url_ipr', true));
        $email = $email;
        $sts = 1;
        $aktif = 1;
        $batch = 6;
        $date_created = date('Y-m-d h:i:s');

        $upload_image = $_FILES['upload_sertifikat']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10240';
            $config['upload_path'] = './file/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('upload_sertifikat')) {
                $old_image = $data['ins_hki']['upload_sertifikat'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'file/' . $old_image);
                }
                $sertifikat1 = $this->upload->data('file_name');
                $this->db->set('upload_sertifikat', $sertifikat1);
            } else {
                echo $this->upload->display_errors();
            }
        }


        $this->db->set('judul_hki', $judul_hki);
        $this->db->set('no_sertifikat', $no_sertifikat);
        $this->db->set('tgl_pengajuan_hki', $tgl_pengajuan_hki);
        $this->db->set('tgl_persetujuan_hki', $tgl_persetujuan_hki);
        $this->db->set('jenis_hki', $jenis_hki);
        $this->db->set('kategori_hki', $kategori_hki);
        $this->db->set('url_ipr', $url_ipr);
        $this->db->set('date_created', $date_created);
        $this->db->set('pertanyaan2', $pertanyaan2);
        $this->db->set('pertanyaan1', $pertanyaan1);
        $this->db->set('sts', $sts);
        $this->db->set('aktif', $aktif);
        $this->db->set('batch', $batch);
        $this->db->where('id', $id);
        $this->db->update('ins_hki');

        $log = [
            'log' => "Memngubah Data Insentif HKI ID $id",
            'email' => $email,
            'date_created' => time()
        ];

        $this->db->insert('occ_log', $log);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('hki');
    }

    public function cetak($id)
    {
        $data['title'] = 'Cetak Kwitansi';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['iden'] = $this->db->get_where('identitas', ['email' => $this->session->userdata('email')])->row_array();
        $data['userDetail'] = $this->User_model->kepegawaian();
        $data['det'] = $this->db->get_where('ins_aji', ['id' => $id])->row_array();
        $this->load->view('cetak/c_iaj', $data);
    }

    public function history($email)
    {

        $data['title'] = 'Dashboard | History Usulan Insentif Artikel Dijurnal';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['history'] = $this->db->get_where('ins_aji', ['email' => $email])->result_array();
        $data['userDetail'] = $this->User_model->kepegawaian();
        $data['det'] = $this->db->get_where('user', ['email' => $email])->row_array();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('iaj/history', $data);
        $this->load->view('templates/footer');
    }
}
