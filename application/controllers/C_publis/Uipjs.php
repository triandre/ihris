<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Uipjs extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_publis/Model_uipjs', 'mps');
        $this->load->model('m_sesi/Model_user', 'mu');
        $this->load->model('m_publis/Model_aji', 'ma');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Data Usulan Insentif Publikasi Jurnal dari Skripsi/Thesis/Disertasi',
            'active_menu_insentif' => 'menu-open',
            'active_menu_ins' => 'active',
            'active_menu_uipjs' => 'active',
            'uipjs' => $this->mps->getUipjs(),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/publis/uipjs/v_uipjs', $data);
        $this->load->view('layouts/footer');
    }

    public function createUipjs()
    {
        $data = array(
            'title' => 'Data Usulan Insentif Publikasi Jurnal dari Skripsi/Thesis/Disertasi',
            'active_menu_insentif' => 'menu-open',
            'active_menu_ins' => 'active',
            'active_menu_uipjs' => 'active',
            'uipjs' => $this->mps->getUipjs(),
            'kategori' => $this->ma->getKategori(),
            'lembaga' => $this->ma->getLembaga2(),
            'akun' => $this->mu->getUser()
        );


        $this->form_validation->set_rules('judul', 'Judul', 'required|trim|is_unique[ins_uipjs.judul]', [
            'is_unique' => 'Judul Sudah Pernah Di Ajukan'
        ]);
        $this->form_validation->set_rules('nama_penulis', 'Nama Penulis', 'required|trim');
        $this->form_validation->set_rules('vnt', 'Volume Nomor Tahun', 'required|trim');
        $this->form_validation->set_rules('halaman', 'Halaman', 'required|trim');
        $this->form_validation->set_rules('nama_jurnal', 'Nama Jurnal', 'required|trim');
        $this->form_validation->set_rules('eissn', 'EISSN', 'required|trim');
        $this->form_validation->set_rules('kontribusi', 'Kontribusi', 'required|trim');
        $this->form_validation->set_rules('kategori_jurnal', 'Kategori Jurnal', 'required|trim');
        $this->form_validation->set_rules('lembaga_pengindeks', 'Lembaga Pengindeks', 'required|trim');
        $this->form_validation->set_rules('url_jurnal', 'URL JURNAL', 'required|trim');
        $this->form_validation->set_rules('nama_program', 'Nama Program', 'required|trim');
        $this->form_validation->set_rules('nomor_kontrak', 'NOMOR KONTRAK', 'required|trim');
        $this->form_validation->set_rules('pertanyaan1', 'pertanyaan1', 'required|trim');
        $this->form_validation->set_rules('peringkat_jurnal', 'Peringkat Jurnal', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->load->view('layouts/header', $data);
            $this->load->view('dosen/publis/uipjs/c_uipjs', $data);
            $this->load->view('layouts/footer');
        } else {
            //PERIKSA
            $email = $this->session->userdata('email');
            $peringkat_jurnal = htmlspecialchars($this->input->post('peringkat_jurnal'));
            $lembaga_pengindeks = htmlspecialchars($this->input->post('lembaga_pengindeks', true));

            if ($lembaga_pengindeks == "Sinta 1") {
                $pagu = "5000000";
            } elseif ($lembaga_pengindeks == "Sinta 2") {
                $pagu = "2000000";
            } elseif ($lembaga_pengindeks == "Sinta 3") {
                $pagu = "1500000";
            } elseif ($lembaga_pengindeks == "Sinta 4") {
                $pagu = "1250000";
            } elseif ($lembaga_pengindeks == "Sinta 5") {
                $pagu = "1000000";
            } elseif ($lembaga_pengindeks == "Sinta 6") {
                $pagu = "500000";
            } elseif ($lembaga_pengindeks == "Web Of Science (WoS)") {
                $pagu = "6000000";
            } elseif ($lembaga_pengindeks == "Q1") {
                $pagu = "10000000";
            } elseif ($lembaga_pengindeks == "Q2") {
                $pagu = "8000000";
            } elseif ($lembaga_pengindeks == "Q3") {
                $pagu = "6000000";
            } elseif ($lembaga_pengindeks == "Q4") {
                $pagu = "4000000";
            } else {
                $pagu = "0";
            }

            $internasional = $this->db->query("SELECT * FROM ins_uipjs WHERE aktif=1 AND email='$email' AND batch=6")->num_rows();
            if ($internasional >= '1') {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>INFO!</strong> Mohon Maaf Skim Anda sudah Melebihi Batas</div>');
                redirect('uipjs');
            } else {

                // UPLOAD ARTIKEL ILMIAH 
                $upload_image = $_FILES['upload_artikel']['name'];

                if ($upload_image) {
                    $config['allowed_types'] = 'pdf';
                    $config['max_size']      = '10000';
                    $config['upload_path'] = './file/';
                    $config['encrypt_name'] = TRUE;

                    $this->load->library('upload', $config);

                    if ($this->upload->do_upload('upload_artikel')) {
                        $old_image = $data['ins_uipjs']['upload_artikel'];
                        if ($old_image != 'default.jpg') {
                            unlink(FCPATH . 'file/' . $old_image);
                        }
                        $artikel = $this->upload->data('file_name');
                        $this->db->set('upload_artikel', $artikel);
                    } else {
                        echo $this->upload->display_errors();
                    }
                }
                // UPLOAD review
                $upload_image = $_FILES['upload_bukti_review']['name'];

                if ($upload_image) {
                    $config['allowed_types'] = 'pdf';
                    $config['max_size']      = '10000';
                    $config['upload_path'] = './file/';
                    $config['encrypt_name'] = TRUE;

                    $this->load->library('upload', $config);

                    if ($this->upload->do_upload('upload_bukti_review')) {
                        $old_image = $data['ins_uipjs']['upload_bukti_review'];
                        if ($old_image != 'default.jpg') {
                            unlink(FCPATH . 'file/' . $old_image);
                        }
                        $review = $this->upload->data('file_name');
                        $this->db->set('upload_bukti_review', $review);
                    } else {
                        echo $this->upload->display_errors();
                    }
                }

                $data = [
                    'kontribusi' =>  htmlspecialchars($this->input->post('kontribusi', true)),
                    'judul' =>  htmlspecialchars($this->input->post('judul', true)),
                    'vnt' =>  htmlspecialchars($this->input->post('vnt', true)),
                    'nama_jurnal' =>  htmlspecialchars($this->input->post('nama_jurnal', true)),
                    'halaman' =>  htmlspecialchars($this->input->post('halaman', true)),
                    'kategori_jurnal' =>  htmlspecialchars($this->input->post('kategori_jurnal', true)),
                    'lembaga_pengindeks' =>  $lembaga_pengindeks,
                    'pagu' =>  $pagu,
                    'nama_program' =>  htmlspecialchars($this->input->post('nama_program', true)),
                    'pertanyaan1' =>  htmlspecialchars($this->input->post('pertanyaan1', true)),
                    'eissn' =>  htmlspecialchars($this->input->post('eissn', true)),
                    'nama_penulis' =>  htmlspecialchars($this->input->post('nama_penulis', true)),
                    'nomor_kontrak' =>  htmlspecialchars($this->input->post('nomor_kontrak', true)),
                    'url_jurnal' =>  htmlspecialchars($this->input->post('url_jurnal', true)),
                    'nilai_sjr' =>  htmlspecialchars($this->input->post('nilai_sjr', true)),
                    'url_sjr' =>  htmlspecialchars($this->input->post('url_sjr', true)),
                    'peringkat_jurnal' =>  htmlspecialchars($this->input->post('peringkat_jurnal', true)),
                    'email' => $this->session->userdata('email'),
                    'sts' => 1,
                    'batch' => 6,
                    'aktif' => 1,
                    'upload_bukti_review' => $review,
                    'upload_artikel' => $artikel,
                    'date_created' => date('Y-m-d H:i:s')
                ];


                $log = [
                    'log' => "Membuat Insentif Artikel Dijurnal",
                    'email' => $email,
                    'date_created' => time()
                ];

                $this->db->insert('ins_uipjs', $data);
                $this->db->insert('occ_log', $log);

                $this->session->set_flashdata('sukses', 'Disimpan');
                redirect('uipjs');
            }
        }
    }



    public function detailUipjs($id)
    {
        $id = $this->uri->segment(3);
        $data = array(
            'title' => 'Data Usulan Insentif Publikasi Jurnal dari Skripsi/Thesis/Disertasi',
            'active_menu_insentif' => 'menu-open',
            'active_menu_ins' => 'active',
            'active_menu_uipjs' => 'active',
            'uipjs' => $this->mps->getUipjs(),
            'akun' => $this->mu->getUser(),
            'd' => $this->mps->getDetail($id)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/publis/uipjs/d_uipjs', $data);
        $this->load->view('layouts/footer');
    }


    public function nonaktif($id)
    {

        $aktif = 0;

        $this->db->set('aktif', $aktif);
        $this->db->where('id', $id);
        $this->db->update('ins_aji');


        $log = [
            'log' => "Menghapus ID $id",
            'email' => $this->session->userdata('email'),
            'date_created' => time()
        ];

        $this->db->insert('log', $log);

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('aji');
    }



    public function perbaikan()
    {
        $id = $this->uri->segment(3);
        $data = array(
            'title' => 'Usulan Insentif Publikasi Jurnal dari Skripsi/Thesis/Disertasi',
            'active_menu_insentif' => 'menu-open',
            'active_menu_ins' => 'active',
            'active_menu_uipjs' => 'active',
            'uipjs' => $this->mps->getUipjs(),
            'akun' => $this->mu->getUser(),
            'lembaga' => $this->ma->getLembaga2(),
            'd' => $this->mps->getDetail($id)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/publis/uipjs/u_uipjs', $data);
        $this->load->view('layouts/footer');
    }

    public function perbaikanGo()
    {
        $data = array(
            'title' => 'Usulan Insentif Publikasi Jurnal dari Skripsi/Thesis/Disertasi',
            'active_menu_insentif' => 'menu-open',
            'active_menu_ins' => 'active',
            'active_menu_uipjs' => 'active',
            'uipjs' => $this->mps->getUipjs(),
            'akun' => $this->mu->getUser()
        );
        $this->form_validation->set_rules('judul', 'Judul', 'required|trim');
        $this->form_validation->set_rules('nama_penulis', 'Nama Penulis', 'required|trim');
        $this->form_validation->set_rules('vnt', 'Volume Nomor Tahun', 'required|trim');
        $this->form_validation->set_rules('halaman', 'Halaman', 'required|trim');
        $this->form_validation->set_rules('nama_jurnal', 'Nama Jurnal', 'required|trim');
        $this->form_validation->set_rules('eissn', 'EISSN', 'required|trim');
        $this->form_validation->set_rules('kontribusi', 'Kontribusi', 'required|trim');
        $this->form_validation->set_rules('kategori_jurnal', 'Kategori Jurnal', 'required|trim');
        $this->form_validation->set_rules('lembaga_pengindeks', 'Lembaga Pengindeks', 'required|trim');
        $this->form_validation->set_rules('url_jurnal', 'URL JURNAL', 'required|trim');
        $this->form_validation->set_rules('nama_program', 'Nama Program', 'required|trim');
        $this->form_validation->set_rules('nomor_kontrak', 'NOMOR KONTRAK', 'required|trim');
        $this->form_validation->set_rules('pertanyaan1', 'pertanyaan1', 'required|trim');

        $lembaga_pengindeks = htmlspecialchars($this->input->post('lembaga_pengindeks', true));

        if ($lembaga_pengindeks == "Sinta 1") {
            $pagu = "5000000";
        } elseif ($lembaga_pengindeks == "Sinta 2") {
            $pagu = "2000000";
        } elseif ($lembaga_pengindeks == "Sinta 3") {
            $pagu = "1500000";
        } elseif ($lembaga_pengindeks == "Sinta 4") {
            $pagu = "1250000";
        } elseif ($lembaga_pengindeks == "Sinta 5") {
            $pagu = "1000000";
        } elseif ($lembaga_pengindeks == "Sinta 6") {
            $pagu = "500000";
        } elseif ($lembaga_pengindeks == "Web Of Science (WoS)") {
            $pagu = "6000000";
        } elseif ($lembaga_pengindeks == "Q1") {
            $pagu = "10000000";
        } elseif ($lembaga_pengindeks == "Q2") {
            $pagu = "8000000";
        } elseif ($lembaga_pengindeks == "Q3") {
            $pagu = "6000000";
        } elseif ($lembaga_pengindeks == "Q4") {
            $pagu = "4000000";
        } else {
            $pagu = "0";
        }

        $email = $this->session->userdata('email');
        $kontribusi =  htmlspecialchars($this->input->post('kontribusi', true));
        $judul =  htmlspecialchars($this->input->post('judul', true));
        $vnt =  htmlspecialchars($this->input->post('vnt', true));
        $nama_jurnal =  htmlspecialchars($this->input->post('nama_jurnal', true));
        $halaman =  htmlspecialchars($this->input->post('halaman', true));
        $kategori_jurnal =  htmlspecialchars($this->input->post('kategori_jurnal', true));
        $nama_program =  htmlspecialchars($this->input->post('nama_program', true));
        $pertanyaan1 =  htmlspecialchars($this->input->post('pertanyaan1', true));
        $eissn =  htmlspecialchars($this->input->post('eissn', true));
        $nama_penulis =  htmlspecialchars($this->input->post('nama_penulis', true));
        $peringkat_jurnal =  htmlspecialchars($this->input->post('peringkat_jurnal', true));
        $nomor_kontrak =  htmlspecialchars($this->input->post('nomor_kontrak', true));
        $url_jurnal =  htmlspecialchars($this->input->post('url_jurnal', true));
        $nilai_sjr =  htmlspecialchars($this->input->post('nilai_sjr', true));
        $url_sjr =  htmlspecialchars($this->input->post('url_sjr', true));
        $id =  htmlspecialchars($this->input->post('id', true));
        $email = $this->session->userdata('email');
        $sts = 1;
        $aktif = 1;
        $batch = 6;
        $date_created = date('Y-m-d h:i:s');
        $upload_image = $_FILES['upload_artikel']['name'];

        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './file/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('upload_artikel')) {
                $old_image = $data['ins_uipjs']['upload_artikel'];
                if ($old_image != 'default.jpg') {
                    unlink(FCPATH . 'file/' . $old_image);
                }
                $artikel = $this->upload->data('file_name');
                $this->db->set('upload_artikel', $artikel);
            } else {
                echo $this->upload->display_errors();
            }
        }
        // UPLOAD review
        $upload_image = $_FILES['upload_bukti_review']['name'];

        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './file/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('upload_bukti_review')) {
                $old_image = $data['ins_uipjs']['upload_bukti_review'];
                if ($old_image != 'default.jpg') {
                    unlink(FCPATH . 'file/' . $old_image);
                }
                $review = $this->upload->data('file_name');
                $this->db->set('upload_bukti_review', $review);
            } else {
                echo $this->upload->display_errors();
            }
        }


        $this->db->set('kontribusi', $kontribusi);
        $this->db->set('judul', $judul);
        $this->db->set('vnt', $vnt);
        $this->db->set('nama_jurnal', $nama_jurnal);
        $this->db->set('halaman', $halaman);
        $this->db->set('kategori_jurnal', $kategori_jurnal);
        $this->db->set('lembaga_pengindeks', $lembaga_pengindeks);
        $this->db->set('pagu', $pagu);
        $this->db->set('nama_program', $nama_program);
        $this->db->set('pertanyaan1', $pertanyaan1);
        $this->db->set('eissn', $eissn);
        $this->db->set('nama_penulis', $nama_penulis);
        $this->db->set('peringkat_jurnal', $peringkat_jurnal);
        $this->db->set('nomor_kontrak', $nomor_kontrak);
        $this->db->set('url_jurnal', $url_jurnal);
        $this->db->set('nilai_sjr', $nilai_sjr);
        $this->db->set('url_sjr', $url_sjr);
        $this->db->set('date_created', $date_created);
        $this->db->set('email', $email);
        $this->db->set('sts', $sts);
        $this->db->set('batch', $batch);
        $this->db->set('aktif', $aktif);
        $this->db->where('id', $id);
        $this->db->update('ins_uipjs');

        $log = [
            'log' => "Perbaikan Data ID $id",
            'email' => $email,
            'date_created' => time()
        ];

        $this->db->insert('occ_log', $log);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('uipjs');
    }

    public function cetak($id)
    {
        $data['title'] = 'Cetak Kwitansi';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['iden'] = $this->db->get_where('identitas', ['email' => $this->session->userdata('email')])->row_array();
        $data['userDetail'] = $this->User_model->kepegawaian();
        $data['det'] = $this->db->get_where('ins_aji', ['id' => $id])->row_array();
        $this->load->view('cetak/c_iaj', $data);
    }

    public function history($email)
    {

        $data['title'] = 'Dashboard | History Usulan Insentif Artikel Dijurnal';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['history'] = $this->db->get_where('ins_aji', ['email' => $email])->result_array();
        $data['userDetail'] = $this->User_model->kepegawaian();
        $data['det'] = $this->db->get_where('user', ['email' => $email])->row_array();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('iaj/history', $data);
        $this->load->view('templates/footer');
    }
}
