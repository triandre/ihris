<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bidilmu extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_dosen/m_data_pribadi/Model_bidilmu', 'bid');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Dashboard',
            'active_menu_profil' => 'menu-open',
            'active_menu_prf' => 'active',
            'active_menu_data_pribadi' => 'active',
            'klg' => $this->bid->getView(),
            'd' => $this->bid->getDetail()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/data_pribadi/bidilmu/c_bidilmu', $data);
        $this->load->view('layouts/footer');
    }

    public function updateBidilmu()
    {
        $data = array(
            'title' => 'Dashboard',
            'active_menu_profil' => 'menu-open',
            'active_menu_prf' => 'active',
            'active_menu_data_pribadi' => 'active',
            'klg' => $this->bid->getView(),
            'd' => $this->bid->getDetail(),

        );
        $nidn = $this->session->userdata('nidn');
        $data = array(

            'nidn' => $nidn,
            'bidang_ilmu' => htmlspecialchars($this->input->post('bidang_ilmu', true)),
            'date_created' => date('Y-m-d H:i:s')
        );
        $result = $this->bid->storeBidilmu($data);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('dpribadi');
        } else {
            $this->session->set_flashdata('gagal', 'Disimpan');
            redirect('dpribadi');
        }
    }
}
