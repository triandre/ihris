<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Alamat extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_dosen/m_data_pribadi/Model_alamat', 'alm');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Dashboard',
            'active_menu_profil' => 'menu-open',
            'active_menu_prf' => 'active',
            'active_menu_data_pribadi' => 'active',
            'klg' => $this->alm->getView(),
            'd' => $this->alm->getDetail(),
            'draft' => $this->alm->viewDraft(),
            'diajukan' => $this->alm->viewDiajukan(),
            'disetujui' => $this->alm->viewDisetujui(),
            'ditolak' => $this->alm->viewTolak(),
            'ditangguhkan' => $this->alm->viewDitangguhkan(),
            'nDraft' => $this->alm->numsDraft(),
            'nDiajukan' => $this->alm->numsDiajukan(),
            'nDisetujui' => $this->alm->numsDisetujui(),
            'nDitangguhkan' => $this->alm->numsDitangguhkan(),
            'nDitolak' => $this->alm->numsTolak()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/data_pribadi/alamat/c_alamat', $data);
        $this->load->view('layouts/footer');
    }

    public function updateAlamat()
    {
        $data = array(
            'title' => 'Dashboard',
            'active_menu_profil' => 'menu-open',
            'active_menu_prf' => 'active',
            'active_menu_data_pribadi' => 'active',
            'klg' => $this->alm->getView(),
            'd' => $this->alm->getDetail(),
            'draft' => $this->alm->viewDraft(),
            'diajukan' => $this->alm->viewDiajukan(),
            'disetujui' => $this->alm->viewDisetujui(),
            'ditolak' => $this->alm->viewTolak(),
            'ditangguhkan' => $this->alm->viewDitangguhkan(),
            'nDraft' => $this->alm->numsDraft(),
            'nDiajukan' => $this->alm->numsDiajukan(),
            'nDisetujui' => $this->alm->numsDisetujui(),
            'nDitangguhkan' => $this->alm->numsDitangguhkan(),
            'nDitolak' => $this->alm->numsTolak()
        );


        //upload

        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/alamat/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data['occ_alamat']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/alamat/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }

        $id = $this->uuid->v4();
        $reff_alamat = str_replace('-', '', $id);
        $nidn = $this->session->userdata('nidn');
        $sts = 1;
        $kode_ajuan = "ADALAMAT";
        $jenis_ajuan = "Ajuan Data Alamat dan Kontak";

        $data = array(
            'reff_alamat' => $reff_alamat,
            'nidn' => $nidn,
            'alamat' => htmlspecialchars($this->input->post('alamat', true)),
            'rt' => htmlspecialchars($this->input->post('rt', true)),
            'rw' => htmlspecialchars($this->input->post('rw', true)),
            'dusun' => htmlspecialchars($this->input->post('dusun', true)),
            'kelurahan' => htmlspecialchars($this->input->post('kelurahan', true)),
            'kecamatan' => htmlspecialchars($this->input->post('kecamatan', true)),
            'kota' => htmlspecialchars($this->input->post('kota', true)),
            'provinsi' => htmlspecialchars($this->input->post('provinsi', true)),
            'kode_pos' => htmlspecialchars($this->input->post('kode_pos', true)),
            'no_telpon' => htmlspecialchars($this->input->post('no_telpon', true)),
            'no_hp' => htmlspecialchars($this->input->post('no_hp', true)),
            'sts' => $sts,
            'file' => $upload1,
            'aktif' => 1,
            'date_created' => date('Y-m-d H:i:s'),
            'tgl_ajuan' => date('Y-m-d H:i:s')

        );

        $pdd = array(
            'reff' => $reff_alamat,
            'kode_ajuan' => $kode_ajuan,
            'jenis_ajuan' => $jenis_ajuan,
            'tgl_ajuan' => date('Y-m-d H:i:s'),
            'nidn' => $nidn,
            'sts' => $sts

        );

        $result = $this->alm->storeAlamat($data);
        $result2 = $this->md->storePdd($pdd);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('ualamat');
        } else {
            $this->session->set_flashdata('gagal', 'Disimpan');
            redirect('ualamat');
        }
    }



    public function detailRiwayatAlamat()
    {
        $reff = $this->uri->segment(3);
        $data = array(
            'title' => 'Dashboard',
            'active_menu_profil' => 'menu-open',
            'active_menu_prf' => 'active',
            'active_menu_data_pribadi' => 'active',
            'kpd' => $this->alm->getView(),
            'd' => $this->alm->getDetail(),
            'draft' => $this->alm->viewDraft(),
            'diajukan' => $this->alm->viewDiajukan(),
            'disetujui' => $this->alm->viewDisetujui(),
            'ditolak' => $this->alm->viewTolak(),
            'ditangguhkan' => $this->alm->viewDitangguhkan(),
            'nDraft' => $this->alm->numsDraft(),
            'nDiajukan' => $this->alm->numsDiajukan(),
            'nDisetujui' => $this->alm->numsDisetujui(),
            'nDitangguhkan' => $this->alm->numsDitangguhkan(),
            'nDitolak' => $this->alm->numsTolak(),
            'dr' => $this->alm->detailRiwayatAlamat($reff),

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/data_pribadi/alamat/d_alamat', $data);
        $this->load->view('layouts/footer');
    }
}
