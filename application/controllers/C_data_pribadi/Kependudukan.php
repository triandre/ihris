<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kependudukan extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_dosen/m_data_pribadi/Model_kependudukan', 'kpp');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Dashboard',
            'active_menu_profil' => 'menu-open',
            'active_menu_prf' => 'active',
            'active_menu_data_pribadi' => 'active',
            'kpd' => $this->kpp->getView(),
            'd' => $this->kpp->getDetail(),
            'draft' => $this->kpp->viewDraft(),
            'diajukan' => $this->kpp->viewDiajukan(),
            'disetujui' => $this->kpp->viewDisetujui(),
            'ditolak' => $this->kpp->viewTolak(),
            'ditangguhkan' => $this->kpp->viewDitangguhkan(),
            'nDraft' => $this->kpp->numsDraft(),
            'nDiajukan' => $this->kpp->numsDiajukan(),
            'nDisetujui' => $this->kpp->numsDisetujui(),
            'nDitangguhkan' => $this->kpp->numsDitangguhkan(),
            'nDitolak' => $this->kpp->numsTolak()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/data_pribadi/kependudukan/c_kependudukan', $data);
        $this->load->view('layouts/footer');
    }

    public function updateKependudukan()
    {
        $data = array(
            'title' => 'Dashboard',
            'active_menu_profil' => 'menu-open',
            'active_menu_prf' => 'active',
            'active_menu_data_pribadi' => 'active',
            'kpd' => $this->kpp->getView(),
            'd' => $this->kpp->getDetail(),
            'draft' => $this->kpp->viewDraft(),
            'diajukan' => $this->kpp->viewDiajukan(),
            'disetujui' => $this->kpp->viewDisetujui(),
            'ditolak' => $this->kpp->viewTolak(),
            'ditangguhkan' => $this->kpp->viewDitangguhkan(),
            'nDraft' => $this->kpp->numsDraft(),
            'nDiajukan' => $this->kpp->numsDiajukan(),
            'nDisetujui' => $this->kpp->numsDisetujui(),
            'nDitangguhkan' => $this->kpp->numsDitangguhkan(),
            'nDitolak' => $this->kpp->numsTolak()
        );


        //upload

        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/kependudukan/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data['occ_kependudukan']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/kependudukan/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }

        $id = $this->uuid->v4();
        $reff_kependudukan = str_replace('-', '', $id);
        $nidn = $this->session->userdata('nidn');
        $sts = 1;
        $kode_ajuan = "ADKDD";
        $jenis_ajuan = "Ajuan Data Kependudukan";

        $data = array(
            'reff_kependudukan' => $reff_kependudukan,
            'nidn' => $nidn,
            'nik' => htmlspecialchars($this->input->post('nik', true)),
            'agama' => htmlspecialchars($this->input->post('agama', true)),
            'kewarganegaraan' => htmlspecialchars($this->input->post('kewarganegaraan', true)),
            'sts' => $sts,
            'file' => $upload1,
            'aktif' => 1,
            'date_created' => date('Y-m-d H:i:s'),
            'tgl_ajuan' => date('Y-m-d H:i:s')

        );

        $pdd = array(
            'reff' => $reff_kependudukan,
            'kode_ajuan' => $kode_ajuan,
            'jenis_ajuan' => $jenis_ajuan,
            'tgl_ajuan' => date('Y-m-d H:i:s'),
            'nidn' => $nidn,
            'sts' => $sts

        );

        $result = $this->kpp->storeKependudukan($data);
        $result2 = $this->md->storePdd($pdd);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('ukependudukan');
        } else {
            $this->session->set_flashdata('gagal', 'Disimpan');
            redirect('ukependudukan');
        }
    }



    public function detailRiwayatKependudukan()
    {
        $reff = $this->uri->segment(3);
        $data = array(
            'title' => 'Dashboard',
            'active_menu_profil' => 'menu-open',
            'active_menu_prf' => 'active',
            'active_menu_data_pribadi' => 'active',
            'kpd' => $this->kpp->getView(),
            'd' => $this->kpp->getDetail(),
            'draft' => $this->kpp->viewDraft(),
            'diajukan' => $this->kpp->viewDiajukan(),
            'disetujui' => $this->kpp->viewDisetujui(),
            'ditolak' => $this->kpp->viewTolak(),
            'ditangguhkan' => $this->kpp->viewDitangguhkan(),
            'nDraft' => $this->kpp->numsDraft(),
            'nDiajukan' => $this->kpp->numsDiajukan(),
            'nDisetujui' => $this->kpp->numsDisetujui(),
            'nDitangguhkan' => $this->kpp->numsDitangguhkan(),
            'nDitolak' => $this->kpp->numsTolak(),
            'dr' => $this->kpp->detailRiwayatKependudukan($reff),

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/data_pribadi/kependudukan/d_kependudukan', $data);
        $this->load->view('layouts/footer');
    }
}
