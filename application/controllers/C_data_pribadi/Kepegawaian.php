<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kepegawaian extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_dosen/m_data_pribadi/Model_kepegawaian', 'kpp');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Dashboard',
            'active_menu_profil' => 'menu-open',
            'active_menu_prf' => 'active',
            'active_menu_data_pribadi' => 'active',
            'kpd' => $this->kpp->getView(),
            'prodi' => $this->kpp->getProdi(),
            'd' => $this->kpp->getDetail(),
            'draft' => $this->kpp->viewDraft(),
            'diajukan' => $this->kpp->viewDiajukan(),
            'disetujui' => $this->kpp->viewDisetujui(),
            'ditolak' => $this->kpp->viewTolak(),
            'ditangguhkan' => $this->kpp->viewDitangguhkan(),
            'nDraft' => $this->kpp->numsDraft(),
            'nDiajukan' => $this->kpp->numsDiajukan(),
            'nDisetujui' => $this->kpp->numsDisetujui(),
            'nDitangguhkan' => $this->kpp->numsDitangguhkan(),
            'nDitolak' => $this->kpp->numsTolak()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/data_pribadi/kepegawaian/c_kepegawaian', $data);
        $this->load->view('layouts/footer');
    }

    public function updateKepegawaian()
    {
        $data = array(
            'title' => 'Dashboard',
            'active_menu_profil' => 'menu-open',
            'active_menu_prf' => 'active',
            'active_menu_data_pribadi' => 'active',
            'kpd' => $this->kpp->getView(),
            'd' => $this->kpp->getDetail(),
            'draft' => $this->kpp->viewDraft(),
            'diajukan' => $this->kpp->viewDiajukan(),
            'disetujui' => $this->kpp->viewDisetujui(),
            'ditolak' => $this->kpp->viewTolak(),
            'ditangguhkan' => $this->kpp->viewDitangguhkan(),
            'nDraft' => $this->kpp->numsDraft(),
            'nDiajukan' => $this->kpp->numsDiajukan(),
            'nDisetujui' => $this->kpp->numsDisetujui(),
            'nDitangguhkan' => $this->kpp->numsDitangguhkan(),
            'nDitolak' => $this->kpp->numsTolak()
        );


        //upload

        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/kepegawaian/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data['occ_kepegawaian']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/kepegawaian/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }

        $id = $this->uuid->v4();
        $reff_kepegawaian = str_replace('-', '', $id);
        $nidn = $this->session->userdata('nidn');
        $sts = 1;
        $kode_ajuan = "ADKPG";
        $jenis_ajuan = "Ajuan Data kepegawaian";

        $data = array(
            'reff_kepegawaian' => $reff_kepegawaian,
            'nidn' => $nidn,
            'kode_prodi' => htmlspecialchars($this->input->post('kode_prodi', true)),
            'status_pegawai' => htmlspecialchars($this->input->post('status_pegawai', true)),
            'status_keaktifan' => htmlspecialchars($this->input->post('status_keaktifan', true)),
            'no_sk_pns' => htmlspecialchars($this->input->post('no_sk_pns', true)),
            'tgl_sk_pns' => htmlspecialchars($this->input->post('tgl_sk_pns', true)),
            'no_sk_tmmd' => htmlspecialchars($this->input->post('no_sk_tmmd', true)),
            'tgl_sk_tmmd' => htmlspecialchars($this->input->post('tgl_sk_tmmd', true)),
            'pangkat' => htmlspecialchars($this->input->post('pangkat', true)),
            'golongan' => htmlspecialchars($this->input->post('golongan', true)),
            'sumber_gaji' => htmlspecialchars($this->input->post('sumber_gaji', true)),
            'sts' => $sts,
            'file' => $upload1,
            'aktif' => 1,
            'date_created' => date('Y-m-d H:i:s'),
            'tgl_ajuan' => date('Y-m-d H:i:s')

        );

        $pdd = array(
            'reff' => $reff_kepegawaian,
            'kode_ajuan' => $kode_ajuan,
            'jenis_ajuan' => $jenis_ajuan,
            'tgl_ajuan' => date('Y-m-d H:i:s'),
            'nidn' => $nidn,
            'sts' => $sts

        );

        $result = $this->kpp->storekepegawaian($data);
        $result2 = $this->md->storePdd($pdd);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('ukepegawaian');
        } else {
            $this->session->set_flashdata('gagal', 'Disimpan');
            redirect('ukepegawaian');
        }
    }



    public function detailRiwayatKepegawaian()
    {
        $reff = $this->uri->segment(3);
        $data = array(
            'title' => 'Dashboard',
            'active_menu_profil' => 'menu-open',
            'active_menu_prf' => 'active',
            'active_menu_data_pribadi' => 'active',
            'kpd' => $this->kpp->getView(),
            'd' => $this->kpp->getDetail(),
            'draft' => $this->kpp->viewDraft(),
            'diajukan' => $this->kpp->viewDiajukan(),
            'disetujui' => $this->kpp->viewDisetujui(),
            'ditolak' => $this->kpp->viewTolak(),
            'ditangguhkan' => $this->kpp->viewDitangguhkan(),
            'nDraft' => $this->kpp->numsDraft(),
            'nDiajukan' => $this->kpp->numsDiajukan(),
            'nDisetujui' => $this->kpp->numsDisetujui(),
            'nDitangguhkan' => $this->kpp->numsDitangguhkan(),
            'nDitolak' => $this->kpp->numsTolak(),
            'dr' => $this->kpp->detailRiwayatKepegawaian($reff),

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/data_pribadi/kepegawaian/d_kepegawaian', $data);
        $this->load->view('layouts/footer');
    }
}
