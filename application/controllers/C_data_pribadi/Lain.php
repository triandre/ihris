<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Lain extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_dosen/m_data_pribadi/Model_lain', 'ml');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Dashboard',
            'active_menu_profil' => 'menu-open',
            'active_menu_prf' => 'active',
            'active_menu_data_pribadi' => 'active',
            'kpd' => $this->ml->getView(),
            'd' => $this->ml->getDetail(),
            'draft' => $this->ml->viewDraft(),
            'diajukan' => $this->ml->viewDiajukan(),
            'disetujui' => $this->ml->viewDisetujui(),
            'ditolak' => $this->ml->viewTolak(),
            'ditangguhkan' => $this->ml->viewDitangguhkan(),
            'nDraft' => $this->ml->numsDraft(),
            'nDiajukan' => $this->ml->numsDiajukan(),
            'nDisetujui' => $this->ml->numsDisetujui(),
            'nDitangguhkan' => $this->ml->numsDitangguhkan(),
            'nDitolak' => $this->ml->numsTolak()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/data_pribadi/lain/c_lain', $data);
        $this->load->view('layouts/footer');
    }

    public function updateLain()
    {
        $data = array(
            'title' => 'Dashboard',
            'active_menu_profil' => 'menu-open',
            'active_menu_prf' => 'active',
            'active_menu_data_pribadi' => 'active',
            'kpd' => $this->ml->getView(),
            'd' => $this->ml->getDetail(),
            'draft' => $this->ml->viewDraft(),
            'diajukan' => $this->ml->viewDiajukan(),
            'disetujui' => $this->ml->viewDisetujui(),
            'ditolak' => $this->ml->viewTolak(),
            'ditangguhkan' => $this->ml->viewDitangguhkan(),
            'nDraft' => $this->ml->numsDraft(),
            'nDiajukan' => $this->ml->numsDiajukan(),
            'nDisetujui' => $this->ml->numsDisetujui(),
            'nDitangguhkan' => $this->ml->numsDitangguhkan(),
            'nDitolak' => $this->ml->numsTolak()
        );


        //upload

        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/lain/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data['occ_lain']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/lain/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }

        $id = $this->uuid->v4();
        $reff_lain = str_replace('-', '', $id);
        $nidn = $this->session->userdata('nidn');
        $sts = 1;
        $kode_ajuan = "ADLLN";
        $jenis_ajuan = "Ajuan Data Lain-Lain";

        $data = array(
            'reff_lain' => $reff_lain,
            'nidn' => $nidn,
            'npwp' => htmlspecialchars($this->input->post('npwp', true)),
            'nama_wajib_pajak' => htmlspecialchars($this->input->post('nama_wajib_pajak', true)),
            'sinta_id' => htmlspecialchars($this->input->post('sinta_id', true)),
            'scopus_id' => htmlspecialchars($this->input->post('scopus_id', true)),
            'gs' => htmlspecialchars($this->input->post('gs', true)),
            'sts' => $sts,
            'file' => $upload1,
            'aktif' => 1,
            'date_created' => date('Y-m-d H:i:s'),
            'tgl_ajuan' => date('Y-m-d H:i:s')

        );

        $pdd = array(
            'reff' => $reff_lain,
            'kode_ajuan' => $kode_ajuan,
            'jenis_ajuan' => $jenis_ajuan,
            'tgl_ajuan' => date('Y-m-d H:i:s'),
            'nidn' => $nidn,
            'sts' => $sts

        );

        $result = $this->ml->storeLain($data);
        $result2 = $this->md->storePdd($pdd);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('ulain');
        } else {
            $this->session->set_flashdata('gagal', 'Disimpan');
            redirect('ulain');
        }
    }



    public function detailRiwayatLain()
    {
        $reff = $this->uri->segment(3);
        $data = array(
            'title' => 'Dashboard',
            'active_menu_profil' => 'menu-open',
            'active_menu_prf' => 'active',
            'active_menu_data_pribadi' => 'active',
            'kpd' => $this->ml->getView(),
            'd' => $this->ml->getDetail(),
            'draft' => $this->ml->viewDraft(),
            'diajukan' => $this->ml->viewDiajukan(),
            'disetujui' => $this->ml->viewDisetujui(),
            'ditolak' => $this->ml->viewTolak(),
            'ditangguhkan' => $this->ml->viewDitangguhkan(),
            'nDraft' => $this->ml->numsDraft(),
            'nDiajukan' => $this->ml->numsDiajukan(),
            'nDisetujui' => $this->ml->numsDisetujui(),
            'nDitangguhkan' => $this->ml->numsDitangguhkan(),
            'nDitolak' => $this->ml->numsTolak(),
            'dr' => $this->ml->detailRiwayatLain($reff),

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/data_pribadi/lain/d_lain', $data);
        $this->load->view('layouts/footer');
    }
}
