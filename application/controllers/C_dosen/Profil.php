<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Profil extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');

        //load library
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Profil',
            'active_menu_profil' => 'active',
            'active_menu_data_pribadi' => 'active',
            'bio' => $this->md->getProfil(),
            'diajukan' => $this->md->viewDiajukan(),
            'disetujui' => $this->md->viewDisetujui(),
            'ditolak' => $this->md->viewTolak(),
            'nDiajukan' => $this->md->numsDiajukan(),
            'nDisetujui' => $this->md->numsDisetujui(),
            'nDitolak' => $this->md->numsTolak()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/profil/index', $data);
        $this->load->view('layouts/footer');
    }

    public function simpanProfil()
    {
        $data = array(
            'title' => 'Profil',
            'active_menu_profil' => 'active',
            'active_menu_data_pribadi' => 'active',
            'bio' => $this->md->getProfil()
        );
        $id = $this->uuid->v4();
        $reff_bio = str_replace('-', '', $id);
        $nidn = $this->session->userdata('nidn');
        $sts = 2;
        $kode_ajuan = "APDP";
        $jenis_ajuan = "Ajuan Perubahan Data Profil";

        //upload

        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/biodata/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data['occ_biodata']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/biodata/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }

        $data = array(
            'reff_bio' => $reff_bio,
            'nidn' => $nidn,
            'nama' => htmlspecialchars($this->input->post('nama', true)),
            'jk' => htmlspecialchars($this->input->post('jk', true)),
            'tempat_lahir' => htmlspecialchars($this->input->post('tempat_lahir', true)),
            'tgl_lahir' => htmlspecialchars($this->input->post('tgl_lahir', true)),
            'nama_ibu' => htmlspecialchars($this->input->post('nama_ibu', true)),
            'sts' => $sts,
            'file' => $upload1,
            'created' => date('Y-m-d H:i:s'),
            'tgl_ajuan' => date('Y-m-d H:i:s')

        );

        $pdd = array(
            'reff' => $reff_bio,
            'kode_ajuan' => $kode_ajuan,
            'jenis_ajuan' => $jenis_ajuan,
            'tgl_ajuan' => date('Y-m-d H:i:s'),
            'nidn' => $nidn,
            'sts' => $sts

        );

        $result = $this->md->storeProfil($data);
        $result2 = $this->md->storePdd($pdd);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('dosen');
        } else {
            $this->session->set_flashdata('gagal', 'Disimpan');
            redirect('dosen/profil');
        }
    }

    public function detailAjuan($reff)
    {
        $reff = $this->uri->segment(3);
        $data = array(
            'title' => 'Detail Pengajuan Perubahan Profil',
            'active_menu_profil' => 'active',
            'active_menu_data_pribadi' => 'active',
            'p' => $this->md->getProfil(),
            'bio' => $this->md->getProfilBaru($reff),
            'draft' => $this->md->viewDraft(),
            'diajukan' => $this->md->viewDiajukan(),
            'disetujui' => $this->md->viewDisetujui(),
            'ditolak' => $this->md->viewTolak(),
            'ditangguhkan' => $this->md->viewDitangguhkan(),
            'nDraft' => $this->md->numsDraft(),
            'nDiajukan' => $this->md->numsDiajukan(),
            'nDisetujui' => $this->md->numsDisetujui(),
            'nDitangguhkan' => $this->md->numsDitangguhkan(),
            'nDitolak' => $this->md->numsTolak()
        );

        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/d_detail', $data);
        $this->load->view('layouts/footer');
    }
}
