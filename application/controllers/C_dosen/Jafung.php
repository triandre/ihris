<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jafung extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_dosen/ModelJafung', 'mj');
        $this->load->model('m_master/ModelMasterJafung', 'mmj');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Jabatan Fungsional',
            'active_menu_profil' => 'menu-open',
            'active_menu_prf' => 'active',
            'active_menu_data_jafung' => 'active',
            'jafung' => $this->mj->getJafung(),

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/jafung/v_jafung', $data);
        $this->load->view('layouts/footer');
    }

    public function createJafung()
    {
        $data = array(
            'title' => 'Jabatan Fungsional',
            'active_menu_profil' => 'menu-open',
            'active_menu_prf' => 'active',
            'active_menu_data_impassing' => 'active',
            'jafung' => $this->mj->getJafung(),
            'mjf' => $this->mmj->getMasterJafung()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/jafung/c_jafung', $data);
        $this->load->view('layouts/footer');
    }

    public function saveJafung()
    {
        $data = array(
            'title' => 'Jabatan Fungsional',
            'active_menu_profil' => 'active',
            'active_menu_data_pribadi' => 'active',
            'jafung' => $this->mj->getJafung()
        );

        $id = $this->uuid->v4();
        $reff_jafung = str_replace('-', '', $id);
        $nidn = $this->session->userdata('nidn');
        $sts = 1;
        $kode_ajuan = "ADJF";
        $jenis_ajuan = "Ajuan Data Baru Jabatan Fungsional";

        //upload

        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/jafung/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data['occ_jafung']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/jafung/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }

        $data = array(
            'reff_jafung' => $reff_jafung,
            'nidn' => $nidn,
            'id_jfungsional' => htmlspecialchars($this->input->post('id_jfungsional', true)),
            'no_sk_jafung' => htmlspecialchars($this->input->post('no_sk_jafung', true)),
            'sk_terhitung' => htmlspecialchars($this->input->post('sk_terhitung', true)),
            'kelebihan_pengajaran' => htmlspecialchars($this->input->post('kelebihan_pengajaran', true)),
            'kelebihan_penelitian' => htmlspecialchars($this->input->post('kelebihan_penelitian', true)),
            'kelebihan_pangmas' => htmlspecialchars($this->input->post('kelebihan_pangmas', true)),
            'kelebihan_penunjang' => htmlspecialchars($this->input->post('kelebihan_penunjang', true)),
            'sts' => $sts,
            'file' => $upload1,
            'active' => 1,
            'created' => date('Y-m-d H:i:s'),
            'tgl_ajuan' => date('Y-m-d H:i:s')

        );

        $pdd = array(
            'reff' => $reff_jafung,
            'kode_ajuan' => $kode_ajuan,
            'jenis_ajuan' => $jenis_ajuan,
            'tgl_ajuan' => date('Y-m-d H:i:s'),
            'nidn' => $nidn,
            'sts' => $sts

        );

        $result = $this->mj->storeJafung($data);
        $result2 = $this->md->storePdd($pdd);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('jafung');
        } else {
            $this->session->set_flashdata('gagal', 'Disimpan');
            redirect('createJafung');
        }
    }

    public function hapusJafung($id_jafung)
    {
        $id_jafung = $this->uri->segment(2);
        $active = 0;

        $this->db->set('active', $active);
        $this->db->where('id_jafung', $id_jafung);
        $this->db->update('occ_jafung');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('jafung');
    }

    public function detailJafung()
    {
        $id_jafung = $this->uri->segment(2);
        $data = array(
            'title' => 'Detail Impassing',
            'active_menu_profil' => 'menu-open',
            'active_menu_prf' => 'active',
            'active_menu_data_jafung' => 'active',
            'd' => $this->mj->detailjafung($id_jafung),

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/jafung/d_jafung', $data);
        $this->load->view('layouts/footer');
    }



    public function riwayatJafung()
    {
        $data = array(
            'title' => 'Jabatan Fungsional',
            'active_menu_profil' => 'menu-open',
            'active_menu_prf' => 'active',
            'active_menu_data_jafung' => 'active',
            'jafung' => $this->mj->getJafung(),
            'draft' => $this->mj->viewDraft(),
            'diajukan' => $this->mj->viewDiajukan(),
            'disetujui' => $this->mj->viewDisetujui(),
            'ditolak' => $this->mj->viewTolak(),
            'ditangguhkan' => $this->mj->viewDitangguhkan(),
            'nDraft' => $this->mj->numsDraft(),
            'nDiajukan' => $this->mj->numsDiajukan(),
            'nDisetujui' => $this->mj->numsDisetujui(),
            'nDitangguhkan' => $this->mj->numsDitangguhkan(),
            'nDitolak' => $this->mj->numsTolak()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/jafung/riw_jafung', $data);
        $this->load->view('layouts/footer');
    }

    public function detailJafungRiwayat()
    {
        $reff = $this->uri->segment(2);
        $data = array(
            'title' => 'Detail Impassing',
            'active_menu_profil' => 'menu-open',
            'active_menu_prf' => 'active',
            'active_menu_data_jafung' => 'active',
            'd' => $this->mj->detailJafungRiwayat($reff),

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/jafung/d_jafung', $data);
        $this->load->view('layouts/footer');
    }
}
