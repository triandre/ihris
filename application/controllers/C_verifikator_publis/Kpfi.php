<!-- INSENTIF ARTIKEL DIJURNAL -->
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kpfi extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->load->model('User_model');
    }

    public function index()
    {
        $data['title'] = 'Dashboard | Kegiatan Perjalanan Forum Ilmiah';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['kpfi'] = $this->db->get_where('ins_kpfi', ['sts' => 1, 'aktif' => 1])->result_array();
        $data['userDetail'] = $this->User_model->kepegawaian();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('kpfi/index', $data);
        $this->load->view('templates/footer');
    }

    public function setuju($id)
    {

        $data['title'] = 'Dashboard |  Kegiatan Perjalanan Forum Ilmiah';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['det'] = $this->db->get_where('ins_kpfi', ['id' => $id])->row_array();
        $data['userDetail'] = $this->User_model->kepegawaian();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('kpfi/setuju', $data);
        $this->load->view('templates/footer');
    }

    public function fee($id)
    {

        $data['title'] = 'Dashboard |  Kegiatan Perjalanan Forum Ilmiah';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['det'] = $this->db->get_where('ins_kpfi', ['id' => $id])->row_array();
        $data['userDetail'] = $this->User_model->kepegawaian();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('kpfi/setuju', $data);
        $this->load->view('templates/footer');
    }


    public function feeGo()
    {

        $id = $this->input->post('id', true);
        $ket = $this->input->post('ket', true);
        $insentif = $this->input->post('insentif', true);
        $sts = 2;
        $date_validator = date('Y-m-d H:i:s');

        $this->db->set('date_validator', $date_validator);
        $this->db->set('sts', $sts);
        $this->db->set('ket', $ket);
        $this->db->set('insentif', $insentif);
        $this->db->set('validator', $this->session->userdata('email'));
        $this->db->where('id', $id);
        $this->db->update('ins_kpfi');

        $log = [
            'log' => "Mengusul Kegiatan Perjalanan Forum Ilmiah ID $id",
            'email' => $this->session->userdata('email'),
            'date_created' => time()
        ];

        $this->db->insert('log', $log);
        $this->session->set_flashdata('flash_a', 'success');
        redirect('insentif/kpfi');
    }


    public function tolak($id)
    {

        $data['title'] = 'Dashboard | Kegiatan Perjalanan Forum Ilmiah';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['det'] = $this->db->get_where('ins_kpfi', ['id' => $id])->row_array();
        $data['userDetail'] = $this->User_model->kepegawaian();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('kpfi/tolak', $data);
        $this->load->view('templates/footer');
    }

    public function tolakGo()
    {

        $id = $this->input->post('id', true);
        $ket = $this->input->post('ket', true);
        $sts = 6;
        $date_validator = date('Y-m-d H:i:s');

        $this->db->set('date_validator', $date_validator);
        $this->db->set('sts', $sts);
        $this->db->set('ket', $ket);
        $this->db->set('validator', $this->session->userdata('email'));
        $this->db->where('id', $id);
        $this->db->update('ins_kpfi');

        $log = [
            'log' => "Menolak Insentif Buku Jurnal Data ID $id",
            'email' => $this->session->userdata('email'),
            'date_created' => time()
        ];

        $this->db->insert('log', $log);
        $this->session->set_flashdata('flash_a', 'success');
        redirect('insentif/kpfi');
    }
}
