<!-- INSENTIF ARTIKEL DIJURNAL -->
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bfio extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_verifikator_publis/Model_bfio', 'ma');
        $this->load->model('m_sesi/Model_user', 'mu');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Data Usulan Bantuan Mengikuti Seminar Internasional/Nasional',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vbfio' => 'active',
            'v' => $this->ma->getBfio(),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('verifikator_publis/bfio/v_bfio', $data);
        $this->load->view('layouts/footer');
    }

    public function history()
    {
        $email = $this->uri->segment(3);
        $data = array(
            'title' => 'Data Usulan Bantuan Mengikuti Seminar Internasional/Nasional',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vbfio' => 'active',
            'v' => $this->ma->getBfio(),
            'h' => $this->ma->getHistory($email),
            'akun' => $this->mu->getUser(),
        );

        $this->load->view('layouts/header', $data);
        $this->load->view('verifikator_publis/bfio/h_bfio', $data);
        $this->load->view('layouts/footer');
    }

    public function tolak()
    {

        $id = $this->uri->segment(3);
        $data = array(
            'title' => 'Data Usulan Bantuan Mengikuti Seminar Internasional/Nasional',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vbfio' => 'active',
            'v' => $this->ma->getBfio(),
            'd' => $this->ma->getDetail($id),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('verifikator_publis/bfio/t_bfio', $data);
        $this->load->view('layouts/footer');
    }


    public function tolakGo()
    {

        $id = $this->input->post('id', true);
        $ket = $this->input->post('ket', true);
        $sts = 6;
        $date_validator = date('Y-m-d H:i:s');
        $this->db->set('date_validator', $date_validator);
        $this->db->set('sts', $sts);
        $this->db->set('ket', $ket);
        $this->db->set('validator', $this->session->userdata('email'));
        $this->db->where('id', $id);
        $this->db->update('ins_bfio');

        $log = [
            'log' => "Menolak Form Usulan Bantuan Mengikuti Seminar Internasional/Nasional  ID $id",
            'email' => $this->session->userdata('email'),
            'date_created' => time()
        ];
        $this->db->insert('occ_log', $log);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('vbfio');
    }

    public function setujui($id)
    {

        $id = $this->uri->segment(3);
        $data = array(
            'title' => 'Data Usulan Bantuan Mengikuti Seminar Internasional/Nasional',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vbfio' => 'active',
            'v' => $this->ma->getBfio(),
            'd' => $this->ma->getDetail($id),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('verifikator_publis/bfio/s_bfio', $data);
        $this->load->view('layouts/footer');
    }


    public function setujuiGo()
    {

        $id = $this->input->post('id', true);
        $insentif_tambahan = preg_replace('/[^0-9]/', '', htmlspecialchars($this->input->post('insentif_tambahan', true)));
        $ket = $this->input->post('ket', true);
        $sts = 3;
        $date_validator = date('Y-m-d H:i:s');

        $this->db->set('date_validator', $date_validator);
        $this->db->set('insentif_tambahan', $insentif_tambahan);
        $this->db->set('sts', $sts);
        $this->db->set('ket', $ket);
        $this->db->set('validator', $this->session->userdata('email'));
        $this->db->where('id', $id);
        $this->db->update('ins_bfio');

        $log = [
            'log' => "Mengusul Insentif Form Usulan Bantuan Mengikuti Seminar Internasional/Nasional ID $id",
            'email' => $this->session->userdata('email'),
            'date_created' => time()
        ];


        $this->db->insert('occ_log', $log);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('vbfio');
    }

    public function detailBfio()
    {
        $id = $this->uri->segment(3);
        $data = array(
            'title' => 'Data Usulan Bantuan Mengikuti Seminar Internasional/Nasional',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vbfio' => 'active',
            'v' => $this->ma->getBfio(),
            'd' => $this->ma->getDetail($id),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('verifikator_publis/bfio/d_bfio', $data);
        $this->load->view('layouts/footer');
    }
}
