<!-- INSENTIF ARTIKEL DIJURNAL -->
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bbp extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_verifikator_publis/Model_bbp', 'ma');
        $this->load->model('m_sesi/Model_user', 'mu');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Data Bantuan Biaya Publikasi',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vbbp' => 'active',
            'v' => $this->ma->getBbp(),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('verifikator_publis/bbp/v_bbp', $data);
        $this->load->view('layouts/footer');
    }

    public function history()
    {
        $email = $this->uri->segment(3);
        $data = array(
            'title' => 'Histori Bantuan Biaya Publikasi',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vbbp' => 'active',
            'v' => $this->ma->getBbp(),
            'h' => $this->ma->getHistory($email),
            'akun' => $this->mu->getUser(),
        );

        $this->load->view('layouts/header', $data);
        $this->load->view('verifikator_publis/bbp/h_bbp', $data);
        $this->load->view('layouts/footer');
    }

    public function detailBbp()
    {
        $id = $this->uri->segment(3);
        $data = array(
            'title' => 'Histori Bantuan Biaya Publikasi',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vbbp' => 'active',
            'v' => $this->ma->getBbp(),
            'd' => $this->ma->getDetail($id),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('verifikator_publis/bbp/d_bbp', $data);
        $this->load->view('layouts/footer');
    }


    public function tolak()
    {

        $id = $this->uri->segment(3);
        $data = array(
            'title' => 'Penolakan Bantuan Biaya Publikasi',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vbbp' => 'active',
            'v' => $this->ma->getBbp(),
            'd' => $this->ma->getDetail($id),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('verifikator_publis/bbp/t_bbp', $data);
        $this->load->view('layouts/footer');
    }

    public function tolakGo()
    {

        $id = $this->input->post('id', true);
        $ket = $this->input->post('ket', true);
        $sts = 6;
        $date_validator = date('Y-m-d H:i:s');

        $this->db->set('date_validator', $date_validator);
        $this->db->set('sts', $sts);
        $this->db->set('ket', $ket);
        $this->db->set('validator', $this->session->userdata('email'));
        $this->db->where('id', $id);
        $this->db->update('ins_bbp');

        $log = [
            'log' => "Menolak Usulan Bantuan Biaya Publikasi Data ID $id",
            'email' => $this->session->userdata('email'),
            'date_created' => time()
        ];
        $this->db->insert('occ_log', $log);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('vbbp');
    }

    public function setujui()
    {
        $id = $this->uri->segment(3);
        $data = array(
            'title' => 'Histori Bantuan Biaya Publikasi',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vbbp' => 'active',
            'v' => $this->ma->getBbp(),
            'd' => $this->ma->getDetail($id),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('verifikator_publis/bbp/s_bbp', $data);
        $this->load->view('layouts/footer');
    }


    public function setujuiGo()
    {

        $id = $this->input->post('id', true);
        $ket = $this->input->post('ket', true);
        $insentif = preg_replace('/[^0-9]/', '', htmlspecialchars($this->input->post('insentif', true)));
        $pagu = preg_replace('/[^0-9]/', '', htmlspecialchars($this->input->post('pagu', true)));
        $sts = 3;
        $date_validator = date('Y-m-d H:i:s');

        if ($insentif > $pagu) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>INFO!</strong> Mohon Maaf Insentif Anda Melebihi Biaya Publikasi yang sudah di tentukan, Silahkan Baca Panduan Terbaru!!</div>');
            redirect('vbbp');
        } else {

            $this->db->set('date_validator', $date_validator);
            $this->db->set('insentif', $insentif);
            $this->db->set('sts', $sts);
            $this->db->set('ket', $ket);
            $this->db->set('validator', $this->session->userdata('email'));
            $this->db->where('id', $id);
            $this->db->update('ins_bbp');

            $log = [
                'log' => "Mengusul Usulan Bantuan Biaya Publikasi Data ID $id",
                'email' => $this->session->userdata('email'),
                'date_created' => time()
            ];


            $this->db->insert('occ_log', $log);
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('vbbp');
        }
    }
}
