<!-- INSENTIF ARTIKEL DIJURNAL -->
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ubpjs extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_verifikator_publis/Model_ubpjs', 'ma');
        $this->load->model('m_sesi/Model_user', 'mu');
        $this->load->library('uuid');
    }
    public function index()
    {
        $data = array(
            'title' => 'Data Usulan Bantuan Publikasi dari Skripsi/Thesis/Disertasi',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vubpjs' => 'active',
            'v' => $this->ma->getUbpjs(),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('verifikator_publis/ubpjs/v_ubpjs', $data);
        $this->load->view('layouts/footer');
    }

    public function detailUbpjs($id)
    {
        $id = $this->uri->segment(3);
        $data = array(
            'title' => 'Detail Usulan Bantuan Publikasi dari Skripsi/Thesis/Disertasi',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vubpjs' => 'active',
            'v' => $this->ma->getUbpjs(),
            'd' => $this->ma->getDetail($id),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('verifikator_publis/ubpjs/d_ubpjs', $data);
        $this->load->view('layouts/footer');
    }

    public function history()
    {
        $email = $this->uri->segment(3);
        $data = array(
            'title' => 'Detail Usulan Bantuan Publikasi dari Skripsi/Thesis/Disertasi',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vubpjs' => 'active',
            'v' => $this->ma->getUbpjs(),
            'h' => $this->ma->getHistory($email),
            'akun' => $this->mu->getUser(),
        );

        $this->load->view('layouts/header', $data);
        $this->load->view('verifikator_publis/ubpjs/h_ubpjs', $data);
        $this->load->view('layouts/footer');
    }


    public function setujui($id)
    {

        $id = $this->uri->segment(3);
        $data = array(
            'title' => 'Setujui Usulan Bantuan Publikasi dari Skripsi/Thesis/Disertasi',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vubpjs' => 'active',
            'v' => $this->ma->getUbpjs(),
            'd' => $this->ma->getDetail($id),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('verifikator_publis/ubpjs/s_ubpjs', $data);
        $this->load->view('layouts/footer');
    }

    public function setujuiGo()
    {

        $id = $this->input->post('id', true);
        $ket = $this->input->post('ket', true);
        $insentif = preg_replace('/[^0-9]/', '', htmlspecialchars($this->input->post('insentif', true)));
        $pagu = preg_replace('/[^0-9]/', '', htmlspecialchars($this->input->post('pagu', true)));
        $sts = 3;
        $date_validator = date('Y-m-d H:i:s');

        if ($insentif > $pagu) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>INFO!</strong> Mohon Maaf Insentif Anda Melebihi Biaya Publikasi yang sudah di tentukan, Silahkan Baca Panduan Terbaru!!</div>');
            redirect('vubpjs');
        } else {

            $this->db->set('date_validator', $date_validator);
            $this->db->set('insentif', $insentif);
            $this->db->set('sts', $sts);
            $this->db->set('ket', $ket);
            $this->db->set('validator', $this->session->userdata('email'));
            $this->db->where('id', $id);
            $this->db->update('ins_ubpjs');

            $log = [
                'log' => "Mengusul Usulan Bantuan Publikasi Jurnal dari Skripsi/Thesis/Disertasi  Data ID $id",
                'email' => $this->session->userdata('email'),
                'date_created' => time()
            ];


            $this->db->insert('occ_log', $log);
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('vubpjs');
        }
    }


    public function tolak()
    {
        $id = $this->uri->segment(3);
        $data = array(
            'title' => 'Penolakan Usulan Publikasi dari Skripsi/Thesis/Disertasi',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vubpjs' => 'active',
            'v' => $this->ma->getUbpjs(),
            'd' => $this->ma->getDetail($id),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('verifikator_publis/ubpjs/t_ubpjs', $data);
        $this->load->view('layouts/footer');
    }






    public function tolakGo()
    {

        $id = $this->input->post('id', true);
        $ket = $this->input->post('ket', true);
        $sts = 6;
        $date_validator = date('Y-m-d H:i:s');

        $this->db->set('date_validator', $date_validator);
        $this->db->set('sts', $sts);
        $this->db->set('ket', $ket);
        $this->db->set('validator', $this->session->userdata('email'));
        $this->db->where('id', $id);
        $this->db->update('ins_ubpjs');

        $log = [
            'log' => "Menolak Usulan Bantuan Publikasi Jurnal dari Skripsi/Thesis/Disertasi  Data ID $id",
            'email' => $this->session->userdata('email'),
            'date_created' => time()
        ];
        $this->db->insert('occ_log', $log);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('vubpjs');
    }
}
