<?php
defined('BASEPATH') or exit('No direct script access allowed');

class VdataJafung extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_admin/validasiDosen/Model_valDataJafung', 'mvdj');
    }

    public function index()
    {
        $data = array(
            'title' => 'Daftar Ajuan Data Jabatan Fungsional',
            'active_menu_vdd' => 'menu-open',
            'active_menu_vd' => 'active',
            'active_menu_v_jafung' => 'active',
            'diajukan' => $this->mvdj->viewDiajukan(),
            'ditolak' => $this->mvdj->viewDitolak(),
            'disetujui' => $this->mvdj->viewDisetujui()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('superpower/validasi_dosen/dataJafung/v_jafung', $data);
        $this->load->view('layouts/footer');
    }

    // AJUAN DATA PROFIL
    public function detailJafung()
    {
        $reff = $this->uri->segment(3);
        $nidn = $this->uri->segment(4);
        $data = array(
            'title' => 'Detail Ajuan Data JAfung',
            'active_menu_vdd' => 'menu-open',
            'active_menu_vd' => 'active',
            'active_menu_v_jafung' => 'active',
            'd' => $this->mvdj->detailJafung($reff),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('superpower/validasi_dosen/dataJafung/d_jafung', $data);
        $this->load->view('layouts/footer');
    }

    public function tolakjafung()
    {
        $verifikasi = $this->session->userdata('email');
        $reff_jafung = $this->input->post('reff_jafung', true);
        $komentar = $this->input->post('komentar', true);
        $sts = 3;
        $notif = 1;
        $tgl_ditolak = date('Y-m-d H:i:s');

        $this->db->set('tgl_ditolak', $tgl_ditolak);
        $this->db->set('sts', $sts);
        $this->db->set('komentar', $komentar);
        $this->db->where('reff_jafung', $reff_jafung);
        $this->db->update('occ_jafung');

        $this->db->set('tgl_verifikasi', $tgl_ditolak);
        $this->db->set('sts', $sts);
        $this->db->set('notif', $notif);
        $this->db->set('verifikator_id', $verifikasi);
        $this->db->set('komentar', $komentar);
        $this->db->where('reff', $reff_jafung);
        $this->db->update('occ_pdd');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('vjafung');
    }


    public function setujuijafung()
    {
        $reff_jafung = $this->uri->segment(3);
        $sts = 2;
        $verifikasi = $this->session->userdata('email');
        $notif = 2;
        $tgl_ditolak = date('Y-m-d H:i:s');

        $this->db->set('tgl_setujui', $tgl_ditolak);
        $this->db->set('sts', $sts);
        $this->db->where('reff_jafung', $reff_jafung);
        $this->db->update('occ_jafung');

        $this->db->set('tgl_verifikasi', $tgl_ditolak);
        $this->db->set('sts', $sts);
        $this->db->set('notif', $notif);
        $this->db->set('verifikator_id', $verifikasi);
        $this->db->where('reff', $reff_jafung);
        $this->db->update('occ_pdd');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('vjafung');
    }



    public function detailKeluarga()
    {
        $reff = $this->uri->segment(3);
        $nidn = $this->uri->segment(4);
        $data = array(
            'title' => 'Detail Ajuan Data Keluarga',
            'active_menu_vdd' => 'menu-open',
            'active_menu_vd' => 'active',
            'active_menu_vpenvor' => 'active',
            'dr' => $this->mvdj->detailKeluarga($reff),
            'd' => $this->mvdj->detailKeluargaLama($nidn)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('superpower/validasi_dosen/dataPribadi/d_dataKeluarga', $data);
        $this->load->view('layouts/footer');
    }
    public function detailKependudukan()
    {
        $reff = $this->uri->segment(3);
        $nidn = $this->uri->segment(4);
        $data = array(
            'title' => 'Detail Ajuan Data Kependudukan',
            'active_menu_vdd' => 'menu-open',
            'active_menu_vd' => 'active',
            'active_menu_vpenvor' => 'active',
            'dr' => $this->mvdj->detailKependudukan($reff),
            'd' => $this->mvdj->detailKependudukanLama($nidn)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('superpower/validasi_dosen/dataPribadi/d_dataKependudukan', $data);
        $this->load->view('layouts/footer');
    }
    public function detailKeanggotaan()
    {
        $reff = $this->uri->segment(3);
        $nidn = $this->uri->segment(4);
        $data = array(
            'title' => 'Detail Ajuan Data Keanggotaan Muhammadiyah',
            'active_menu_vdd' => 'menu-open',
            'active_menu_vd' => 'active',
            'active_menu_vpenvor' => 'active',
            'dr' => $this->mvdj->detailKeanggotaan($reff),
            'd' => $this->mvdj->detailKeanggotaanLama($nidn)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('superpower/validasi_dosen/dataPribadi/d_dataKeanggotaan', $data);
        $this->load->view('layouts/footer');
    }
    public function detailKepegawaian()
    {
        $reff = $this->uri->segment(3);
        $nidn = $this->uri->segment(4);
        $data = array(
            'title' => 'Detail Ajuan Data Kepegawaian',
            'active_menu_vdd' => 'menu-open',
            'active_menu_vd' => 'active',
            'active_menu_vpenvor' => 'active',
            'dr' => $this->mvdj->detailKepegawaian($reff),
            'd' => $this->mvdj->detailKepegawaianLama($nidn)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('superpower/validasi_dosen/dataPribadi/d_dataKepegawaian', $data);
        $this->load->view('layouts/footer');
    }
    public function detailAlamat()
    {
        $reff = $this->uri->segment(3);
        $nidn = $this->uri->segment(4);
        $data = array(
            'title' => 'Detail Ajuan Data Alamat Dan Kontak',
            'active_menu_vdd' => 'menu-open',
            'active_menu_vd' => 'active',
            'active_menu_vpenvor' => 'active',
            'dr' => $this->mvdj->detailAlamat($reff),
            'd' => $this->mvdj->detailAlamatLama($nidn)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('superpower/validasi_dosen/dataPribadi/d_dataAlamat', $data);
        $this->load->view('layouts/footer');
    }
    public function detailLainlain()
    {
        $reff = $this->uri->segment(3);
        $nidn = $this->uri->segment(4);
        $data = array(
            'title' => 'Detail Ajuan Data Lain-lain',
            'active_menu_vdd' => 'menu-open',
            'active_menu_vd' => 'active',
            'active_menu_vpenvor' => 'active',
            'dr' => $this->mvdj->detailLainlain($reff),
            'd' => $this->mvdj->detailLainlainLama($nidn)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('superpower/validasi_dosen/dataPribadi/d_dataLainlain', $data);
        $this->load->view('layouts/footer');
    }
}
