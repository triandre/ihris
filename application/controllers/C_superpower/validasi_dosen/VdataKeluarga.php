<?php
defined('BASEPATH') or exit('No direct script access allowed');

class VdataKeluarga extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_admin/validasiDosen/Model_valDataPribadi', 'kpp');
    }


    public function tolakDataKeluarga()
    {
        $verifikasi = $this->session->userdata('email');
        $reff_keluarga = $this->input->post('reff_keluarga', true);
        $komentar = $this->input->post('komentar', true);
        $sts = 3;
        $notif = 1;
        $tgl_ditolak = date('Y-m-d H:i:s');

        $this->db->set('tgl_ditolak', $tgl_ditolak);
        $this->db->set('sts', $sts);
        $this->db->set('komentar', $komentar);
        $this->db->where('reff_keluarga', $reff_keluarga);
        $this->db->update('occ_keluarga');

        $this->db->set('tgl_verifikasi', $tgl_ditolak);
        $this->db->set('sts', $sts);
        $this->db->set('notif', $notif);
        $this->db->set('verifikator_id', $verifikasi);
        $this->db->set('komentar', $komentar);
        $this->db->where('reff', $reff_keluarga);
        $this->db->update('occ_pdd');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('vdataPribadi');
    }

    public function setujuiDataKeluarga()
    {
        $verifikasi = $this->session->userdata('email');
        $reff_keluarga = $this->uri->segment(3);
        $sts = 2;
        $notif = 1;
        $tgl_setujui = date('Y-m-d H:i:s');

        $this->db->set('tgl_setujui', $tgl_setujui);
        $this->db->set('sts', $sts);
        $this->db->where('reff_keluarga', $reff_keluarga);
        $this->db->update('occ_keluarga');

        $this->db->set('tgl_verifikasi', $tgl_setujui);
        $this->db->set('sts', $sts);
        $this->db->set('notif', $notif);
        $this->db->set('verifikator_id', $verifikasi);
        $this->db->where('reff', $reff_keluarga);
        $this->db->update('occ_pdd');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('vdataPribadi');
    }
}
