<?php
defined('BASEPATH') or exit('No direct script access allowed');

class VdataPribadi extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_admin/validasiDosen/Model_valDataPribadi', 'kpp');
    }

    public function index()
    {
        $data = array(
            'title' => 'Daftar Ajuan Perubahan Data Pribadi',
            'active_menu_vdd' => 'menu-open',
            'active_menu_vd' => 'active',
            'active_menu_vdp' => 'active',
            'diajukan' => $this->kpp->viewDiajukan(),
            'ditolak' => $this->kpp->viewDitolak(),
            'disetujui' => $this->kpp->viewDisetujui()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('superpower/validasi_dosen/dataPribadi/v_dataPribadi', $data);
        $this->load->view('layouts/footer');
    }

    // AJUAN DATA PROFIL
    public function detailProfil()
    {
        $reff = $this->uri->segment(3);
        $nidn = $this->uri->segment(4);
        $data = array(
            'title' => 'Detail Ajuan Perubahan Data Pribadi',
            'active_menu_vdd' => 'menu-open',
            'active_menu_vd' => 'active',
            'active_menu_vdp' => 'active',
            'dr' => $this->kpp->detailProfil($reff),
            'd' => $this->kpp->detailProfilLama($nidn)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('superpower/validasi_dosen/dataPribadi/d_dataPribadi', $data);
        $this->load->view('layouts/footer');
    }

    public function tolakProfil()
    {
        $verifikasi = $this->session->userdata('email');
        $reff_profil = $this->input->post('reff_profil', true);
        $komentar = $this->input->post('komentar', true);
        $sts = 3;
        $notif = 1;
        $tgl_ditolak = date('Y-m-d H:i:s');

        $this->db->set('tgl_ditolak', $tgl_ditolak);
        $this->db->set('sts', $sts);
        $this->db->set('komentar', $komentar);
        $this->db->where('reff_profil', $reff_profil);
        $this->db->update('occ_profil');

        $this->db->set('tgl_verifikasi', $tgl_ditolak);
        $this->db->set('sts', $sts);
        $this->db->set('notif', $notif);
        $this->db->set('verifikator_id', $verifikasi);
        $this->db->set('komentar', $komentar);
        $this->db->where('reff', $reff_profil);
        $this->db->update('occ_pdd');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('vdataPribadi');
    }


    public function setujuiDataProfil()
    {
        $reff_profil = $this->uri->segment(3);
        $sts = 2;
        $verifikasi = $this->session->userdata('email');
        $notif = 1;
        $tgl_ditolak = date('Y-m-d H:i:s');

        $this->db->set('tgl_ditolak', $tgl_ditolak);
        $this->db->set('sts', $sts);
        $this->db->where('reff_profil', $reff_profil);
        $this->db->update('occ_profil');

        $this->db->set('tgl_verifikasi', $tgl_ditolak);
        $this->db->set('sts', $sts);
        $this->db->set('notif', $notif);
        $this->db->set('verifikator_id', $verifikasi);
        $this->db->where('reff', $reff_profil);
        $this->db->update('occ_pdd');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('vdataPribadi');
    }



    public function detailKeluarga()
    {
        $reff = $this->uri->segment(3);
        $nidn = $this->uri->segment(4);
        $data = array(
            'title' => 'Detail Ajuan Data Keluarga',
            'active_menu_vdd' => 'menu-open',
            'active_menu_vd' => 'active',
            'active_menu_vdp' => 'active',
            'dr' => $this->kpp->detailKeluarga($reff),
            'd' => $this->kpp->detailKeluargaLama($nidn)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('superpower/validasi_dosen/dataPribadi/d_dataKeluarga', $data);
        $this->load->view('layouts/footer');
    }
    public function detailKependudukan()
    {
        $reff = $this->uri->segment(3);
        $nidn = $this->uri->segment(4);
        $data = array(
            'title' => 'Detail Ajuan Data Kependudukan',
            'active_menu_vdd' => 'menu-open',
            'active_menu_vd' => 'active',
            'active_menu_vdp' => 'active',
            'dr' => $this->kpp->detailKependudukan($reff),
            'd' => $this->kpp->detailKependudukanLama($nidn)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('superpower/validasi_dosen/dataPribadi/d_dataKependudukan', $data);
        $this->load->view('layouts/footer');
    }
    public function detailKeanggotaan()
    {
        $reff = $this->uri->segment(3);
        $nidn = $this->uri->segment(4);
        $data = array(
            'title' => 'Detail Ajuan Data Keanggotaan Muhammadiyah',
            'active_menu_vdd' => 'menu-open',
            'active_menu_vd' => 'active',
            'active_menu_vdp' => 'active',
            'dr' => $this->kpp->detailKeanggotaan($reff),
            'd' => $this->kpp->detailKeanggotaanLama($nidn)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('superpower/validasi_dosen/dataPribadi/d_dataKeanggotaan', $data);
        $this->load->view('layouts/footer');
    }
    public function detailKepegawaian()
    {
        $reff = $this->uri->segment(3);
        $nidn = $this->uri->segment(4);
        $data = array(
            'title' => 'Detail Ajuan Data Kepegawaian',
            'active_menu_vdd' => 'menu-open',
            'active_menu_vd' => 'active',
            'active_menu_vdp' => 'active',
            'dr' => $this->kpp->detailKepegawaian($reff),
            'd' => $this->kpp->detailKepegawaianLama($nidn)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('superpower/validasi_dosen/dataPribadi/d_dataKepegawaian', $data);
        $this->load->view('layouts/footer');
    }
    public function detailAlamat()
    {
        $reff = $this->uri->segment(3);
        $nidn = $this->uri->segment(4);
        $data = array(
            'title' => 'Detail Ajuan Data Alamat Dan Kontak',
            'active_menu_vdd' => 'menu-open',
            'active_menu_vd' => 'active',
            'active_menu_vdp' => 'active',
            'dr' => $this->kpp->detailAlamat($reff),
            'd' => $this->kpp->detailAlamatLama($nidn)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('superpower/validasi_dosen/dataPribadi/d_dataAlamat', $data);
        $this->load->view('layouts/footer');
    }
    public function detailLainlain()
    {
        $reff = $this->uri->segment(3);
        $nidn = $this->uri->segment(4);
        $data = array(
            'title' => 'Detail Ajuan Data Lain-lain',
            'active_menu_vdd' => 'menu-open',
            'active_menu_vd' => 'active',
            'active_menu_vdp' => 'active',
            'dr' => $this->kpp->detailLainlain($reff),
            'd' => $this->kpp->detailLainlainLama($nidn)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('superpower/validasi_dosen/dataPribadi/d_dataLainlain', $data);
        $this->load->view('layouts/footer');
    }
}
