<?php
defined('BASEPATH') or exit('No direct script access allowed');

class VdataPenfor extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_admin/validasiDosen/Model_valDataPenfor', 'mvdp');
    }

    public function index()
    {
        $data = array(
            'title' => 'Daftar Ajuan Perubahan Data Pendidikan Formal',
            'active_menu_vdd' => 'menu-open',
            'active_menu_vd' => 'active',
            'active_menu_vpenvor' => 'active',
            'diajukan' => $this->mvdp->viewDiajukan(),
            'ditolak' => $this->mvdp->viewDitolak(),
            'disetujui' => $this->mvdp->viewDisetujui()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('superpower/validasi_dosen/dataPenfor/v_penfor', $data);
        $this->load->view('layouts/footer');
    }

    // AJUAN DATA PROFIL
    public function detailPenfor()
    {
        $reff = $this->uri->segment(3);
        $nidn = $this->uri->segment(4);
        $data = array(
            'title' => 'Detail Ajuan Perubahan Data Pendidikan Formal',
            'active_menu_vdd' => 'menu-open',
            'active_menu_vd' => 'active',
            'active_menu_vpenvor' => 'active',
            'd' => $this->mvdp->detailPenfor($reff),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('superpower/validasi_dosen/dataPenfor/d_penfor', $data);
        $this->load->view('layouts/footer');
    }

    public function tolakPenfor()
    {
        $verifikasi = $this->session->userdata('email');
        $reff_penfor = $this->input->post('reff_penfor', true);
        $komentar = $this->input->post('komentar', true);
        $sts = 3;
        $notif = 1;
        $tgl_ditolak = date('Y-m-d H:i:s');

        $this->db->set('tgl_ditolak', $tgl_ditolak);
        $this->db->set('sts', $sts);
        $this->db->set('komentar', $komentar);
        $this->db->where('reff_penfor', $reff_penfor);
        $this->db->update('occ_pendidikan_formal');

        $this->db->set('tgl_verifikasi', $tgl_ditolak);
        $this->db->set('sts', $sts);
        $this->db->set('notif', $notif);
        $this->db->set('verifikator_id', $verifikasi);
        $this->db->set('komentar', $komentar);
        $this->db->where('reff', $reff_penfor);
        $this->db->update('occ_pdd');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('vpenfor');
    }


    public function setujuiPenfor()
    {
        $reff_penfor = $this->uri->segment(3);
        $sts = 2;
        $verifikasi = $this->session->userdata('email');
        $notif = 2;
        $tgl_ditolak = date('Y-m-d H:i:s');

        $this->db->set('tgl_validasi', $tgl_ditolak);
        $this->db->set('sts', $sts);
        $this->db->where('reff_penfor', $reff_penfor);
        $this->db->update('occ_pendidikan_formal');

        $this->db->set('tgl_verifikasi', $tgl_ditolak);
        $this->db->set('sts', $sts);
        $this->db->set('notif', $notif);
        $this->db->set('verifikator_id', $verifikasi);
        $this->db->where('reff', $reff_penfor);
        $this->db->update('occ_pdd');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('vpenfor');
    }



    public function detailKeluarga()
    {
        $reff = $this->uri->segment(3);
        $nidn = $this->uri->segment(4);
        $data = array(
            'title' => 'Detail Ajuan Data Keluarga',
            'active_menu_vdd' => 'menu-open',
            'active_menu_vd' => 'active',
            'active_menu_vpenvor' => 'active',
            'dr' => $this->mvdp->detailKeluarga($reff),
            'd' => $this->mvdp->detailKeluargaLama($nidn)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('superpower/validasi_dosen/dataPribadi/d_dataKeluarga', $data);
        $this->load->view('layouts/footer');
    }
    public function detailKependudukan()
    {
        $reff = $this->uri->segment(3);
        $nidn = $this->uri->segment(4);
        $data = array(
            'title' => 'Detail Ajuan Data Kependudukan',
            'active_menu_vdd' => 'menu-open',
            'active_menu_vd' => 'active',
            'active_menu_vpenvor' => 'active',
            'dr' => $this->mvdp->detailKependudukan($reff),
            'd' => $this->mvdp->detailKependudukanLama($nidn)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('superpower/validasi_dosen/dataPribadi/d_dataKependudukan', $data);
        $this->load->view('layouts/footer');
    }
    public function detailKeanggotaan()
    {
        $reff = $this->uri->segment(3);
        $nidn = $this->uri->segment(4);
        $data = array(
            'title' => 'Detail Ajuan Data Keanggotaan Muhammadiyah',
            'active_menu_vdd' => 'menu-open',
            'active_menu_vd' => 'active',
            'active_menu_vpenvor' => 'active',
            'dr' => $this->mvdp->detailKeanggotaan($reff),
            'd' => $this->mvdp->detailKeanggotaanLama($nidn)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('superpower/validasi_dosen/dataPribadi/d_dataKeanggotaan', $data);
        $this->load->view('layouts/footer');
    }
    public function detailKepegawaian()
    {
        $reff = $this->uri->segment(3);
        $nidn = $this->uri->segment(4);
        $data = array(
            'title' => 'Detail Ajuan Data Kepegawaian',
            'active_menu_vdd' => 'menu-open',
            'active_menu_vd' => 'active',
            'active_menu_vpenvor' => 'active',
            'dr' => $this->mvdp->detailKepegawaian($reff),
            'd' => $this->mvdp->detailKepegawaianLama($nidn)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('superpower/validasi_dosen/dataPribadi/d_dataKepegawaian', $data);
        $this->load->view('layouts/footer');
    }
    public function detailAlamat()
    {
        $reff = $this->uri->segment(3);
        $nidn = $this->uri->segment(4);
        $data = array(
            'title' => 'Detail Ajuan Data Alamat Dan Kontak',
            'active_menu_vdd' => 'menu-open',
            'active_menu_vd' => 'active',
            'active_menu_vpenvor' => 'active',
            'dr' => $this->mvdp->detailAlamat($reff),
            'd' => $this->mvdp->detailAlamatLama($nidn)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('superpower/validasi_dosen/dataPribadi/d_dataAlamat', $data);
        $this->load->view('layouts/footer');
    }
    public function detailLainlain()
    {
        $reff = $this->uri->segment(3);
        $nidn = $this->uri->segment(4);
        $data = array(
            'title' => 'Detail Ajuan Data Lain-lain',
            'active_menu_vdd' => 'menu-open',
            'active_menu_vd' => 'active',
            'active_menu_vpenvor' => 'active',
            'dr' => $this->mvdp->detailLainlain($reff),
            'd' => $this->mvdp->detailLainlainLama($nidn)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('superpower/validasi_dosen/dataPribadi/d_dataLainlain', $data);
        $this->load->view('layouts/footer');
    }
}
