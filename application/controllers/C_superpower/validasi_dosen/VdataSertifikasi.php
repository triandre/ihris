<?php
defined('BASEPATH') or exit('No direct script access allowed');

class VdataSertifikasi extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_admin/validasiDosen/Model_valDataSertifikasi', 'mvds');
    }

    public function index()
    {
        $data = array(
            'title' => 'Daftar Ajuan Data Sertifikasi',
            'active_menu_vdd' => 'menu-open',
            'active_menu_vd' => 'active',
            'active_menu_vsertifikasi' => 'active',
            'diajukan' => $this->mvds->viewDiajukan(),
            'ditolak' => $this->mvds->viewDitolak(),
            'disetujui' => $this->mvds->viewDisetujui()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('superpower/validasi_dosen/dataSertifikasi/v_sertifikasi', $data);
        $this->load->view('layouts/footer');
    }

    // AJUAN DATA PROFIL
    public function detailsertifikasi()
    {
        $reff = $this->uri->segment(3);
        $nidn = $this->uri->segment(4);
        $data = array(
            'title' => 'Detail Ajuan Perubahan Data Pendidikan Formal',
            'active_menu_vdd' => 'menu-open',
            'active_menu_vd' => 'active',
            'active_menu_vsertifikasi' => 'active',
            'd' => $this->mvds->detailSertifikasi($reff),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('superpower/validasi_dosen/dataSertifikasi/d_sertifikasi', $data);
        $this->load->view('layouts/footer');
    }

    public function tolakSertifikasi()
    {
        $verifikasi = $this->session->userdata('email');
        $reff_sertifikasi = $this->input->post('reff_sertifikasi', true);
        $komentar = $this->input->post('komentar', true);
        $sts = 3;
        $notif = 1;
        $tgl_ditolak = date('Y-m-d H:i:s');

        $this->db->set('tgl_ditolak', $tgl_ditolak);
        $this->db->set('sts', $sts);
        $this->db->set('komentar', $komentar);
        $this->db->where('reff_sertifikasi', $reff_sertifikasi);
        $this->db->update('occ_sertifikasi');

        $this->db->set('tgl_verifikasi', $tgl_ditolak);
        $this->db->set('sts', $sts);
        $this->db->set('notif', $notif);
        $this->db->set('verifikator_id', $verifikasi);
        $this->db->set('komentar', $komentar);
        $this->db->where('reff', $reff_sertifikasi);
        $this->db->update('occ_pdd');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('vSertifikasi');
    }


    public function setujuiSertifikasi()
    {
        $reff_sertifikasi = $this->uri->segment(3);
        $sts = 2;
        $verifikasi = $this->session->userdata('email');
        $notif = 2;
        $tgl_ditolak = date('Y-m-d H:i:s');

        $this->db->set('tgl_setujui', $tgl_ditolak);
        $this->db->set('sts', $sts);
        $this->db->where('reff_sertifikasi', $reff_sertifikasi);
        $this->db->update('occ_sertifikasi');

        $this->db->set('tgl_verifikasi', $tgl_ditolak);
        $this->db->set('sts', $sts);
        $this->db->set('notif', $notif);
        $this->db->set('verifikator_id', $verifikasi);
        $this->db->where('reff', $reff_sertifikasi);
        $this->db->update('occ_pdd');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('vSertifikasi');
    }
}
