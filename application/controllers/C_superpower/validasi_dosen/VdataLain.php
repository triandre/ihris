<?php
defined('BASEPATH') or exit('No direct script access allowed');

class VdataLain extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_admin/validasiDosen/Model_valDataPribadi', 'kpp');
    }


    public function tolakDataLain()
    {
        $verifikasi = $this->session->userdata('email');
        $reff_lain = $this->input->post('reff_lain', true);
        $komentar = $this->input->post('komentar', true);
        $sts = 3;
        $notif = 1;
        $tgl_ditolak = date('Y-m-d H:i:s');

        $this->db->set('tgl_ditolak', $tgl_ditolak);
        $this->db->set('sts', $sts);
        $this->db->set('komentar', $komentar);
        $this->db->where('reff_lain', $reff_lain);
        $this->db->update('occ_lain');

        $this->db->set('tgl_verifikasi', $tgl_ditolak);
        $this->db->set('sts', $sts);
        $this->db->set('notif', $notif);
        $this->db->set('verifikator_id', $verifikasi);
        $this->db->set('komentar', $komentar);
        $this->db->where('reff', $reff_lain);
        $this->db->update('occ_pdd');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('vdataPribadi');
    }

    public function setujuiDataLain()
    {
        $verifikasi = $this->session->userdata('email');
        $reff_lain = $this->uri->segment(3);
        $sts = 2;
        $notif = 1;
        $tgl_disetujui = date('Y-m-d H:i:s');

        $this->db->set('tgl_disetujui', $tgl_disetujui);
        $this->db->set('sts', $sts);
        $this->db->where('reff_lain', $reff_lain);
        $this->db->update('occ_lain');

        $this->db->set('tgl_verifikasi', $tgl_disetujui);
        $this->db->set('sts', $sts);
        $this->db->set('notif', $notif);
        $this->db->set('verifikator_id', $verifikasi);
        $this->db->where('reff', $reff_lain);
        $this->db->update('occ_pdd');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('vdataPribadi');
    }
}
