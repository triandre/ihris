<?php
defined('BASEPATH') or exit('No direct script access allowed');

class VdataKepangkatan extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_admin/validasiDosen/Model_valDataKepangkatan', 'mvdk');
    }

    public function index()
    {
        $data = array(
            'title' => 'Daftar Ajuan Data Kepangkatan ',
            'active_menu_vdd' => 'menu-open',
            'active_menu_vd' => 'active',
            'active_menu_vkepangkatan' => 'active',
            'diajukan' => $this->mvdk->viewDiajukan(),
            'ditolak' => $this->mvdk->viewDitolak(),
            'disetujui' => $this->mvdk->viewDisetujui()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('superpower/validasi_dosen/dataKepangkatan/v_kepangkatan', $data);
        $this->load->view('layouts/footer');
    }

    // AJUAN DATA PROFIL
    public function detailKepangkatan()
    {
        $reff = $this->uri->segment(3);
        $nidn = $this->uri->segment(4);
        $data = array(
            'title' => 'Detail Ajuan Perubahan Data Pendidikan Formal',
            'active_menu_vdd' => 'menu-open',
            'active_menu_vd' => 'active',
            'active_menu_vkepangkatan' => 'active',
            'd' => $this->mvdk->detailKepangkatan($reff),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('superpower/validasi_dosen/dataKepangkatan/d_kepangkatan', $data);
        $this->load->view('layouts/footer');
    }

    public function tolakKepangkatan()
    {
        $verifikasi = $this->session->userdata('email');
        $reff_impassing = $this->input->post('reff_impassing', true);
        $komentar = $this->input->post('komentar', true);
        $sts = 3;
        $notif = 1;
        $tgl_ditolak = date('Y-m-d H:i:s');

        $this->db->set('tgl_ditolak', $tgl_ditolak);
        $this->db->set('sts', $sts);
        $this->db->set('komentar', $komentar);
        $this->db->where('reff_impassing', $reff_impassing);
        $this->db->update('occ_impassing');

        $this->db->set('tgl_verifikasi', $tgl_ditolak);
        $this->db->set('sts', $sts);
        $this->db->set('notif', $notif);
        $this->db->set('verifikator_id', $verifikasi);
        $this->db->set('komentar', $komentar);
        $this->db->where('reff', $reff_impassing);
        $this->db->update('occ_pdd');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('vKepangkatan');
    }


    public function setujuiKepangkatan()
    {
        $reff_impassing = $this->uri->segment(3);
        $sts = 2;
        $verifikasi = $this->session->userdata('email');
        $notif = 2;
        $tgl_ditolak = date('Y-m-d H:i:s');

        $this->db->set('tgl_setujui', $tgl_ditolak);
        $this->db->set('sts', $sts);
        $this->db->where('reff_impassing', $reff_impassing);
        $this->db->update('occ_impassing');

        $this->db->set('tgl_verifikasi', $tgl_ditolak);
        $this->db->set('sts', $sts);
        $this->db->set('notif', $notif);
        $this->db->set('verifikator_id', $verifikasi);
        $this->db->where('reff', $reff_impassing);
        $this->db->update('occ_pdd');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('vKepangkatan');
    }



    public function detailKeluarga()
    {
        $reff = $this->uri->segment(3);
        $nidn = $this->uri->segment(4);
        $data = array(
            'title' => 'Detail Ajuan Data Keluarga',
            'active_menu_vdd' => 'menu-open',
            'active_menu_vd' => 'active',
            'active_menu_vkepangkatan' => 'active',
            'dr' => $this->mvdk->detailKeluarga($reff),
            'd' => $this->mvdk->detailKeluargaLama($nidn)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('superpower/validasi_dosen/dataPribadi/d_dataKeluarga', $data);
        $this->load->view('layouts/footer');
    }
    public function detailKependudukan()
    {
        $reff = $this->uri->segment(3);
        $nidn = $this->uri->segment(4);
        $data = array(
            'title' => 'Detail Ajuan Data Kependudukan',
            'active_menu_vdd' => 'menu-open',
            'active_menu_vd' => 'active',
            'active_menu_vkepangkatan' => 'active',
            'dr' => $this->mvdk->detailKependudukan($reff),
            'd' => $this->mvdk->detailKependudukanLama($nidn)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('superpower/validasi_dosen/dataPribadi/d_dataKependudukan', $data);
        $this->load->view('layouts/footer');
    }
    public function detailKeanggotaan()
    {
        $reff = $this->uri->segment(3);
        $nidn = $this->uri->segment(4);
        $data = array(
            'title' => 'Detail Ajuan Data Keanggotaan Muhammadiyah',
            'active_menu_vdd' => 'menu-open',
            'active_menu_vd' => 'active',
            'active_menu_vkepangkatan' => 'active',
            'dr' => $this->mvdk->detailKeanggotaan($reff),
            'd' => $this->mvdk->detailKeanggotaanLama($nidn)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('superpower/validasi_dosen/dataPribadi/d_dataKeanggotaan', $data);
        $this->load->view('layouts/footer');
    }
    public function detailKepegawaian()
    {
        $reff = $this->uri->segment(3);
        $nidn = $this->uri->segment(4);
        $data = array(
            'title' => 'Detail Ajuan Data Kepegawaian',
            'active_menu_vdd' => 'menu-open',
            'active_menu_vd' => 'active',
            'active_menu_vkepangkatan' => 'active',
            'dr' => $this->mvdk->detailKepegawaian($reff),
            'd' => $this->mvdk->detailKepegawaianLama($nidn)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('superpower/validasi_dosen/dataPribadi/d_dataKepegawaian', $data);
        $this->load->view('layouts/footer');
    }
    public function detailAlamat()
    {
        $reff = $this->uri->segment(3);
        $nidn = $this->uri->segment(4);
        $data = array(
            'title' => 'Detail Ajuan Data Alamat Dan Kontak',
            'active_menu_vdd' => 'menu-open',
            'active_menu_vd' => 'active',
            'active_menu_vkepangkatan' => 'active',
            'dr' => $this->mvdk->detailAlamat($reff),
            'd' => $this->mvdk->detailAlamatLama($nidn)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('superpower/validasi_dosen/dataPribadi/d_dataAlamat', $data);
        $this->load->view('layouts/footer');
    }
    public function detailLainlain()
    {
        $reff = $this->uri->segment(3);
        $nidn = $this->uri->segment(4);
        $data = array(
            'title' => 'Detail Ajuan Data Lain-lain',
            'active_menu_vdd' => 'menu-open',
            'active_menu_vd' => 'active',
            'active_menu_vkepangkatan' => 'active',
            'dr' => $this->mvdk->detailLainlain($reff),
            'd' => $this->mvdk->detailLainlainLama($nidn)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('superpower/validasi_dosen/dataPribadi/d_dataLainlain', $data);
        $this->load->view('layouts/footer');
    }
}
