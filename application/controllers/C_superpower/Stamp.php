<!-- INSENTIF ARTIKEL DIJURNAL -->
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Stamp extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_stamp/Model_stamp', 'ma');
        $this->load->model('m_sesi/Model_user', 'mu');
        $this->load->library('uuid');
    }

    public function v_pj()
    {
        $data = array(
            'title' => 'Data Insentif Pengelola Jurnal',
            'active_menu_stamp' => 'menu-open',
            'active_menu_stp' => 'active',
            'active_menu_tpj' => 'active',
            'v' => $this->ma->getpj(),
            'akun' => $this->mu->getUser()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('superpower/stamp/v_pj', $data);
        $this->load->view('layouts/footer');
    }

    public function v_bbp()
    {
        $data = array(
            'title' => 'Data Bantuan Biaya Publikasi',
            'active_menu_stamp' => 'menu-open',
            'active_menu_stp' => 'active',
            'active_menu_tbbp' => 'active',
            'v' => $this->ma->getbbp(),
            'akun' => $this->mu->getUser()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('superpower/stamp/v_bbp', $data);
        $this->load->view('layouts/footer');
    }

    public function v_bhki()
    {
        $data = array(
            'title' => 'Data Bantuan HKI',
            'active_menu_stamp' => 'menu-open',
            'active_menu_stp' => 'active',
            'active_menu_tbhki' => 'active',
            'v' => $this->ma->getbhki(),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('superpower/stamp/v_bhki', $data);
        $this->load->view('layouts/footer');
    }

    public function v_bfio()
    {
        $data = array(
            'title' => 'Data Bantuan Seminar Nasional/Internasional',
            'active_menu_stamp' => 'menu-open',
            'active_menu_stp' => 'active',
            'active_menu_tbfio' => 'active',
            'v' => $this->ma->getbfio(),
            'akun' => $this->mu->getUser()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('superpower/stamp/v_bfio', $data);
        $this->load->view('layouts/footer');
    }

    public function v_ubpjs()
    {
        $data = array(
            'title' => 'Data Bantuan Publikasi Jurnal dari Skripsi/Thesis/Disertas',
            'active_menu_stamp' => 'menu-open',
            'active_menu_stp' => 'active',
            'active_menu_tubpjs' => 'active',
            'v' => $this->ma->getubpjs(),
            'akun' => $this->mu->getUser()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('superpower/stamp/v_ubpjs', $data);
        $this->load->view('layouts/footer');
    }


    public function stamp_pj()
    {
        $id = $this->uri->segment(3);
        $stempel = 2;
        $date_tempel = date('Y-m-d H:i:s');

        $this->db->set('date_stempel', $date_tempel);
        $this->db->set('stempel', $stempel);
        $this->db->where('id', $id);
        $this->db->update('ins_pj');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('stamp/v_pj');
    }

    public function stamp_bfio()
    {
        $id = $this->uri->segment(3);
        $stempel = 2;
        $date_tempel = date('Y-m-d H:i:s');

        $this->db->set('date_stempel', $date_tempel);
        $this->db->set('stempel', $stempel);
        $this->db->where('id', $id);
        $this->db->update('ins_bfio');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('stamp/v_bfio');
    }


    public function stamp_bbp()
    {
        $id = $this->uri->segment(3);
        $stempel = 2;
        $date_tempel = date('Y-m-d H:i:s');

        $this->db->set('date_stempel', $date_tempel);
        $this->db->set('stempel', $stempel);
        $this->db->where('id', $id);
        $this->db->update('ins_bbp');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('stamp/v_bbp');
    }

    public function stamp_ubpjs()
    {
        $id = $this->uri->segment(3);
        $stempel = 2;
        $date_tempel = date('Y-m-d H:i:s');

        $this->db->set('date_stempel', $date_tempel);
        $this->db->set('stempel', $stempel);
        $this->db->where('id', $id);
        $this->db->update('ins_ubpjs');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('stamp/v_ubpjs');
    }


    public function stamp_bhki()
    {
        $id = $this->uri->segment(3);
        $stempel = 2;
        $date_tempel = date('Y-m-d H:i:s');

        $this->db->set('date_stempel', $date_tempel);
        $this->db->set('stempel', $stempel);
        $this->db->where('id', $id);
        $this->db->update('ins_bhki');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('stamp/v_bhki');
    }
}
