<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Data_pegawai extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_superadmin/Model_dataPegawai', 'mm');
    }

    public function index()
    {
        $data = array(
            'title' => 'Data Dosen',
            'active_menu_ddt' => 'menu-open',
            'active_menu_dt' => 'active',
            'active_menu_data_sd' => 'active',
            'v' => $this->mm->getDosen()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('superpower/data_pegawai/v_data_pegawai', $data);
        $this->load->view('layouts/footer');
    }


    public function tendik()
    {
        $data = array(
            'title' => 'Data Tendik',
            'active_menu_ddt' => 'menu-open',
            'active_menu_dt' => 'active',
            'active_menu_data_st' => 'active',
            'v' => $this->mm->getTendik()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('superpower/data_pegawai/v_data_pegawai', $data);
        $this->load->view('layouts/footer');
    }

    public function detail_pegawai()
    {
        $nidn = $this->uri->segment(3);
        $data = array(
            'title' => 'Detail Pegawai',
            'active_menu_ddt' => 'menu-open',
            'active_menu_dt' => 'active',
            'active_menu_data_st' => 'active',
            'gb' => $this->mm->getBiodata($nidn),
            'gk' => $this->mm->getKependudukan($nidn),
            'gs' => $this->mm->getUser($nidn),
            'ga' => $this->mm->getAlamat($nidn),
            'gpek' => $this->mm->getKepegawaian($nidn),
            'gpn' => $this->mm->getPenempatan($nidn),
            'gkm' => $this->mm->getKemuhammadiyahan($nidn),
            'gll' => $this->mm->getLainlain($nidn),

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('detail_pegawai/index', $data);
        $this->load->view('layouts/footer');
    }
}
