<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Penempatan extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_dosen/ModelPenempatan', 'mp');
        $this->load->model('m_sesi/Model_user', 'mu');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Penempatan',
            'active_menu_profil' => 'menu-open',
            'active_menu_prf' => 'active',
            'active_menu_data_penempatan' => 'active',
            'penempatan' => $this->mp->getPenempatanAdmin(),
            'akun' => $this->mu->getUser()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/penempatan/v_penempatan', $data);
        $this->load->view('layouts/footer');
    }



    public function createPenempatan()
    {
        $data = array(
            'title' => 'Penempatan',
            'active_menu_profil' => 'menu-open',
            'active_menu_prf' => 'active',
            'active_menu_data_penempatan' => 'active',
            'penempatan' => $this->mp->getPenempatanAdmin(),
            'akun' => $this->mu->getUser(),
            'pegawai' => $this->mp->getPegawai(),
            'unit' => $this->mp->getUnit(),

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/penempatan/c_penempatan', $data);
        $this->load->view('layouts/footer');
    }

    public function savePenempatan()
    {
        $data = array(
            'title' => 'Penempatan',
            'active_menu_profil' => 'menu-open',
            'active_menu_prf' => 'active',
            'active_menu_data_penempatan' => 'active',
            'penempatan' => $this->mp->getPenempatanAdmin(),
            'akun' => $this->mu->getUser(),
            'pegawai' => $this->mp->getPegawai(),
            'unit' => $this->mp->getUnit()
        );
        $id = $this->uuid->v4();
        $reff_penempatan = str_replace('-', '', $id);

        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf|jpg|jpeg';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/penempatan/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data['occ_penempatan']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/penempatan/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }
        $data = array(
            'reff_penempatan' => $reff_penempatan,
            'nidn' =>  htmlspecialchars($this->input->post('nidn', true)),
            'nickname_unit' =>  htmlspecialchars($this->input->post('nickname_unit', true)),
            'tgl_mulai' =>  $this->input->post('tgl_mulai'),
            'jabatan' =>  htmlspecialchars($this->input->post('jabatan', true)),
            'no_sk' =>  htmlspecialchars($this->input->post('no_sk', true)),
            'file' => $upload1,
            'date_created' => date('Y-m-d H:i:s')
        );

        $result = $this->mp->save_penempatan($data);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('penempatanAdmin');
        } else {
            $this->session->set_flashdata('gagal', 'Disimpan');
            redirect('createPenempatan');
        }
    }

    public function detailPenempatan()
    {
        $reff_penempatan = $this->uri->segment(2);
        $data = array(
            'title' => 'Penempatan',
            'active_menu_profil' => 'menu-open',
            'active_menu_prf' => 'active',
            'active_menu_data_penempatan' => 'active',
            'penempatan' => $this->mp->getPenempatanAdmin(),
            'akun' => $this->mu->getUser(),
            'pegawai' => $this->mp->getPegawai(),
            'unit' => $this->mp->getUnit(),
            'd' => $this->mp->getPenempatanDetail($reff_penempatan)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/penempatan/d_penempatan', $data);
        $this->load->view('layouts/footer');
    }

    public function updatePenempatan()
    {
        $reff_penempatan = $this->uri->segment(2);
        $data = array(
            'title' => 'Penempatan',
            'active_menu_profil' => 'menu-open',
            'active_menu_prf' => 'active',
            'active_menu_data_penempatan' => 'active',
            'penempatan' => $this->mp->getPenempatanAdmin(),
            'akun' => $this->mu->getUser(),
            'pegawai' => $this->mp->getPegawai(),
            'unit' => $this->mp->getUnit(),
            'd' => $this->mp->getPenempatanDetail($reff_penempatan)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/penempatan/u_penempatan', $data);
        $this->load->view('layouts/footer');
    }

    public function updatePenempatanGo()
    {
        $data = array(
            'title' => 'Penempatan',
            'active_menu_profil' => 'menu-open',
            'active_menu_prf' => 'active',
            'active_menu_data_penempatan' => 'active',
            'penempatan' => $this->mp->getPenempatanAdmin(),
            'akun' => $this->mu->getUser(),
            'pegawai' => $this->mp->getPegawai(),
            'unit' => $this->mp->getUnit()
        );
        $reff_penempatan = htmlspecialchars($this->input->post('reff_penempatan', true));
        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf|jpg|jpeg';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/penempatan/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data['occ_penempatan']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/penempatan/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }
        $data = array(
            'nickname_unit' =>  htmlspecialchars($this->input->post('nickname_unit', true)),
            'tgl_mulai' =>  $this->input->post('tgl_mulai'),
            'jabatan' =>  htmlspecialchars($this->input->post('jabatan', true)),
            'no_sk' =>  htmlspecialchars($this->input->post('no_sk', true)),
            'file' => $upload1,
            'date_created' => date('Y-m-d H:i:s')
        );

        $result = $this->mp->updatePenempatan($data, $reff_penempatan);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('penempatanAdmin');
        } else {
            $this->session->set_flashdata('gagal', 'Disimpan');
            redirect('updatePenempatan' . $reff_penempatan);
        }
    }
}
