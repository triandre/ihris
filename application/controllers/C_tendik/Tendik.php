<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tendik extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('ModelDashboard', 'mds');
        $this->load->model('ModelTrack', 'mt');
    }

    public function index()
    {
        $data = array(
            'title' => 'Dashboard',
            'active_menu_db' => 'active',
            'dp' => $this->md->getDetailProfil(),
            'ss' => $this->md->getUser(),
            'pt' => $this->mds->getPendidikanTerakhir(),
            'ksj' => $this->mds->getKesejahteraan(),
            'penelitian' => $this->mds->getPenelitian(),
            'viSci' => $this->mds->getViSci(),
            'tunjangan' => $this->mds->getTunjangan(),
            'pembicara' => $this->mds->getPembicara(),
            'bukuAjar' => $this->mds->getBukuAjar(),
            'pdd' => $this->mt->getData()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('tendik/dashboard', $data);
        $this->load->view('layouts/footer');
    }

    public function data_pribadi()
    {
        $data = array(
            'title' => 'Dashboard',
            'active_menu_profil' => 'menu-open',
            'active_menu_prf' => 'active',
            'active_menu_data_pribadi' => 'active',
            'dp' => $this->md->getDetailProfil(),
            'kdd' => $this->md->getDetailKdd(),
            'klg' => $this->md->getDetailKlg(),
            'ktam' => $this->md->getDetailKtam(),
            'bid' => $this->md->getDetailBid(),
            'alm' => $this->md->getDetailAlm(),
            'kpg' => $this->md->getDetaiKpg(),
            'lln' => $this->md->getDetailLln(),
            'ss' => $this->md->getUser()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('tendik/profil', $data);
        $this->load->view('layouts/footer');
    }
}
