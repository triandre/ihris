<!-- INSENTIF ARTIKEL DIJURNAL -->
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Iba extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_pimpinan2/Model_iba', 'ma');
        $this->load->model('m_sesi/Model_user', 'mu');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Data Usulan Insentif Buku Ajar',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_viba' => 'active',
            'v' => $this->ma->getIba(),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('verifikator_publis/iba/v_iba', $data);
        $this->load->view('layouts/footer');
    }


    public function fee()
    {
        $id = $this->uri->segment(4);
        $data = array(
            'title' => 'Persetujuan Usulan Insentif Buku Ajar',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_viba' => 'active',
            'iba' => $this->ma->getIba(),
            'd' => $this->ma->getDetail($id),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('pimpinan2/iba/s_iba', $data);
        $this->load->view('layouts/footer');
    }

    public function feeGo()
    {

        $id = $this->input->post('id', true);
        $catatan2 = $this->input->post('catatan2', true);
        $insentif_disetujui = preg_replace('/[^0-9]/', '', htmlspecialchars($this->input->post('insentif_disetujui', true)));
        $sts = 4;
        $date_wr2 = date('Y-m-d H:i:s');

        $this->db->set('insentif_disetujui', $insentif_disetujui);
        $this->db->set('date_wr2', $date_wr2);
        $this->db->set('sts', $sts);
        $this->db->set('catatan2', $catatan2);
        $this->db->where('id', $id);
        $this->db->update('ins_iba');

        $log = [
            'log' => "Setujui Insentif Buku Ajar ID $id",
            'email' => $this->session->userdata('email'),
            'date_created' => time()
        ];

        $this->db->insert('occ_log', $log);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('pimpinan2/viba');
    }


    public function tolak()
    {

        $id = $this->uri->segment(4);
        $data = array(
            'title' => 'Penolakan Usulan Insentif Buku Ajar',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_viba' => 'active',
            'v' => $this->ma->getIba(),
            'd' => $this->ma->getDetail($id),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('pimpinan2/iba/t_iba', $data);
        $this->load->view('layouts/footer');
    }

    public function tolakGo()
    {

        $id = $this->input->post('id', true);
        $tolak2 = $this->input->post('tolak2', true);
        $sts = 6;
        $date_wr2 = date('Y-m-d H:i:s');

        $this->db->set('date_wr2', $date_wr2);
        $this->db->set('sts', $sts);
        $this->db->set('tolak2', $tolak2);
        $this->db->where('id', $id);
        $this->db->update('ins_iba');

        $log = [
            'log' => "Menolak Insentif Buku Data ID $id",
            'email' => $this->session->userdata('email'),
            'date_created' => time()
        ];

        $this->db->insert('occ_log', $log);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('pimpinan2/viba');
    }
}
