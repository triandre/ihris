<!-- INSENTIF ARTIKEL DIJURNAL -->
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ubpjs extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_pimpinan2/Model_ubpjs', 'ma');
        $this->load->model('m_sesi/Model_user', 'mu');
        $this->load->library('uuid');
    }
    public function index()
    {
        $data = array(
            'title' => 'Data Usulan Bantuan Publikasi dari Skripsi/Thesis/Disertasi',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vubpjs' => 'active',
            'v' => $this->ma->getUbpjs(),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('verifikator_publis/ubpjs/v_ubpjs', $data);
        $this->load->view('layouts/footer');
    }



    public function fee($id)
    {

        $id = $this->uri->segment(4);
        $data = array(
            'title' => 'Setujui Usulan Bantuan Publikasi dari Skripsi/Thesis/Disertasi',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vubpjs' => 'active',
            'v' => $this->ma->getUbpjs(),
            'd' => $this->ma->getDetail($id),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('pimpinan2/ubpjs/s_ubpjs', $data);
        $this->load->view('layouts/footer');
    }

    public function feeGo()
    {

        $id = $this->input->post('id', true);
        $catatan2 = $this->input->post('catatan2', true);
        $biaya_disetujui = preg_replace('/[^0-9]/', '', htmlspecialchars($this->input->post('biaya_disetujui', true)));
        $pagu = preg_replace('/[^0-9]/', '', htmlspecialchars($this->input->post('pagu', true)));
        $sts = 4;
        $date_wr2 = date('Y-m-d H:i:s');

        if ($biaya_disetujui > $pagu) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>INFO!</strong> Mohon Maaf Insentif Anda Melebihi Biaya Publikasi yang sudah di tentukan, Silahkan Baca Panduan Terbaru!!</div>');
            redirect('vubpjs');
        } else {

             $this->db->set('date_wr2', $date_wr2);
             $this->db->set('catatan2', $catatan2);
             $this->db->set('biaya_disetujui', $biaya_disetujui);
            $this->db->set('sts', $sts);
            $this->db->where('id', $id);
            $this->db->update('ins_ubpjs');

            $log = [
                'log' => "Mengusul Usulan Bantuan Publikasi Jurnal dari Skripsi/Thesis/Disertasi  Data ID $id",
                'email' => $this->session->userdata('email'),
                'date_created' => time()
            ];


            $this->db->insert('occ_log', $log);
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('pimpinan2/vubpjs');
        }
    }


    public function tolak()
    {
        $id = $this->uri->segment(4);
        $data = array(
            'title' => 'Penolakan Usulan Publikasi dari Skripsi/Thesis/Disertasi',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vubpjs' => 'active',
            'v' => $this->ma->getUbpjs(),
            'd' => $this->ma->getDetail($id),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('pimpinan2/ubpjs/t_ubpjs', $data);
        $this->load->view('layouts/footer');
    }



    public function tolakGo()
    {

        $id = $this->input->post('id', true);
        $tolak2 = $this->input->post('tolak2', true);
        $sts = 6;
        $date_validator = date('Y-m-d H:i:s');

        $this->db->set('date_wr2', $date_wr2);
        $this->db->set('sts', $sts);
        $this->db->set('tolak2', $tolak2);
        $this->db->where('id', $id);
        $this->db->update('ins_ubpjs');

        $log = [
            'log' => "Menolak Usulan Bantuan Publikasi Jurnal dari Skripsi/Thesis/Disertasi  Data ID $id",
            'email' => $this->session->userdata('email'),
            'date_created' => time()
        ];
        $this->db->insert('occ_log', $log);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('pimpinan2/vubpjs');
    }
}
