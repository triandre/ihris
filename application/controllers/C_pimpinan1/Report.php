<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Report extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->load->model('Report_model');
    }
    public function index()
    {
        $data['title'] = 'Dashboard | Report';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['jenis_insentif'] = $this->db->get('jenis_insentif')->result();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('report/index', $data);
        $this->load->view('templates/footer');
    }

    public function cetak()
    {
        $data['title'] = 'Dashboard | Report';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $jenis_insentif = $this->input->post('jenis_insentif', true);
        $dari_tanggal = $this->input->post('dari_tanggal', true);
        $sampai_tanggal = $this->input->post('sampai_tanggal', true);

        if ($jenis_insentif == 8) {
            $dari_tanggal = $this->input->post('dari_tanggal', true);
            $sampai_tanggal = $this->input->post('sampai_tanggal', true);
            $data['gas'] = $this->Report_model->inspj($dari_tanggal, $sampai_tanggal);
            $data['zat'] = $this->Report_model->total_inspj($dari_tanggal, $sampai_tanggal);
            $this->load->view('report/cetak_pj', $data);
        } elseif ($jenis_insentif == 7) {
            $dari_tanggal = $this->input->post('dari_tanggal', true);
            $sampai_tanggal = $this->input->post('sampai_tanggal', true);
            $data['gas'] = $this->Report_model->insiks($dari_tanggal, $sampai_tanggal);
            $data['zat'] = $this->Report_model->total_insiks($dari_tanggal, $sampai_tanggal);
            $this->load->view('report/cetak_ks', $data);
        } elseif ($jenis_insentif == 6) {
            $dari_tanggal = $this->input->post('dari_tanggal', true);
            $sampai_tanggal = $this->input->post('sampai_tanggal', true);
            $data['gas'] = $this->Report_model->insbfio($dari_tanggal, $sampai_tanggal);
            $data['zat'] = $this->Report_model->total_insbfio($dari_tanggal, $sampai_tanggal);
            $this->load->view('report/cetak_bfio', $data);
        } elseif ($jenis_insentif == 5) {
            $dari_tanggal = $this->input->post('dari_tanggal', true);
            $sampai_tanggal = $this->input->post('sampai_tanggal', true);
            $data['gas'] = $this->Report_model->insiba($dari_tanggal, $sampai_tanggal);
            $data['zat'] = $this->Report_model->total_insiba($dari_tanggal, $sampai_tanggal);
            $this->load->view('report/cetak_iba', $data);
        } elseif ($jenis_insentif == 4) {
            $dari_tanggal = $this->input->post('dari_tanggal', true);
            $sampai_tanggal = $this->input->post('sampai_tanggal', true);
            $data['gas'] = $this->Report_model->insbbp($dari_tanggal, $sampai_tanggal);
            $data['zat'] = $this->Report_model->total_insbpp($dari_tanggal, $sampai_tanggal);
            $this->load->view('report/cetak_bbp', $data);
        } elseif ($jenis_insentif == 3) {
            $dari_tanggal = $this->input->post('dari_tanggal', true);
            $sampai_tanggal = $this->input->post('sampai_tanggal', true);
            $data['gas'] = $this->Report_model->insifip($dari_tanggal, $sampai_tanggal);
            $data['zat'] = $this->Report_model->total_insifip($dari_tanggal, $sampai_tanggal);
            $this->load->view('report/cetak_ifip', $data);
        } elseif ($jenis_insentif == 2) {
            $dari_tanggal = $this->input->post('dari_tanggal', true);
            $sampai_tanggal = $this->input->post('sampai_tanggal', true);
            $data['gas'] = $this->Report_model->inshki($dari_tanggal, $sampai_tanggal);
            $data['zat'] = $this->Report_model->total_inshki($dari_tanggal, $sampai_tanggal);
            $this->load->view('report/cetak_hki', $data);
        } elseif ($jenis_insentif == 1) {
            $dari_tanggal = $this->input->post('dari_tanggal', true);
            $sampai_tanggal = $this->input->post('sampai_tanggal', true);
            $data['gas'] = $this->Report_model->insaji($dari_tanggal, $sampai_tanggal);
            $data['zat'] = $this->Report_model->total_insaji($dari_tanggal, $sampai_tanggal);
            $this->load->view('report/cetak_iaj', $data);
        } elseif ($jenis_insentif == 10) {
            $dari_tanggal = $this->input->post('dari_tanggal', true);
            $sampai_tanggal = $this->input->post('sampai_tanggal', true);
            $data['gas'] = $this->Report_model->insubpjs($dari_tanggal, $sampai_tanggal);
            $data['zat'] = $this->Report_model->total_insubpjs($dari_tanggal, $sampai_tanggal);
            $this->load->view('report/cetak_ubpjs', $data);
        } elseif ($jenis_insentif == 9) {
            $dari_tanggal = $this->input->post('dari_tanggal', true);
            $sampai_tanggal = $this->input->post('sampai_tanggal', true);
            $data['gas'] = $this->Report_model->insuipjs($dari_tanggal, $sampai_tanggal);
            $data['zat'] = $this->Report_model->total_insuipjs($dari_tanggal, $sampai_tanggal);
            $this->load->view('report/cetak_uipjs', $data);
        }
    }
}
