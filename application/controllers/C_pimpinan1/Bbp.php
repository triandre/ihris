<!-- INSENTIF ARTIKEL DIJURNAL -->
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bbp extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_pimpinan1/Model_bbp', 'ma');
        $this->load->model('m_sesi/Model_user', 'mu');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Data Bantuan Biaya Publikasi',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vbbp' => 'active',
            'v' => $this->ma->getBbp(),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('verifikator_publis/bbp/v_bbp', $data);
        $this->load->view('layouts/footer');
    }


    public function tolak()
    {

        $id = $this->uri->segment(4);
        $data = array(
            'title' => 'Penolakan Bantuan Biaya Publikasi',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vbbp' => 'active',
            'v' => $this->ma->getBbp(),
            'd' => $this->ma->getDetail($id),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('pimpinan1/bbp/t_bbp', $data);
        $this->load->view('layouts/footer');
    }

    public function tolakGo()
    {

        $id = $this->input->post('id', true);
        $tolak1 = $this->input->post('tolak1', true);
        $sts = 6;
        $date_wr1 = date('Y-m-d H:i:s');

        $this->db->set('date_wr1', $date_wr1);
        $this->db->set('sts', $sts);
        $this->db->set('tolak1', $tolak1);
        $this->db->where('id', $id);
        $this->db->update('ins_bbp');

        $log = [
            'log' => "Pak WR1 Menolak Usulan Bantuan Biaya Publikasi Data ID $id",
            'email' => $this->session->userdata('email'),
            'date_created' => time()
        ];
        $this->db->insert('occ_log', $log);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('pimpinan1/vbbp');
    }

    public function fee()
    {
        $id = $this->uri->segment(4);
        $data = array(
            'title' => 'Histori Bantuan Biaya Publikasi',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vbbp' => 'active',
            'v' => $this->ma->getBbp(),
            'd' => $this->ma->getDetail($id),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('pimpinan1/bbp/s_bbp', $data);
        $this->load->view('layouts/footer');
    }


    public function feeGo()
    {

        $id = $this->input->post('id', true);
        $catatan1 = $this->input->post('catatan1', true);
        $sts = 3;
        $date_wr1 = date('Y-m-d H:i:s');

       
            $this->db->set('date_wr1', $date_wr1);
            $this->db->set('sts', $sts);
            $this->db->set('catatan1', $catatan1);
            $this->db->where('id', $id);
            $this->db->update('ins_bbp');

            $log = [
                'log' => "Pak WR 1 Menyetuji Usulan Bantuan Biaya Publikasi Data ID $id",
                'email' => $this->session->userdata('email'),
                'date_created' => time()
            ];


            $this->db->insert('occ_log', $log);
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('pimpinan1/vbbp');
        
    }
}
