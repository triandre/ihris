<!-- INSENTIF ARTIKEL DIJURNAL -->
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Uipjs extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_pimpinan1/Model_uipjs', 'ma');
        $this->load->model('m_sesi/Model_user', 'mu');
        $this->load->library('uuid');
    }


    public function index()
    {
        $data = array(
            'title' => 'Data Usulan Insentif Publikasi dari Skripsi/Thesis/Disertasi',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vuipjs' => 'active',
            'v' => $this->ma->getUipjs(),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('verifikator_publis/uipjs/v_uipjs', $data);
        $this->load->view('layouts/footer');
    }



    public function fee($id)
    {

        $id = $this->uri->segment(4);
        $data = array(
            'title' => 'Setujui Usulan Insentif Publikasi dari Skripsi/Thesis/Disertasi',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vuipjs' => 'active',
            'v' => $this->ma->getUipjs(),
            'd' => $this->ma->getDetail($id),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('pimpinan1/uipjs/s_uipjs', $data);
        $this->load->view('layouts/footer');
    }


    public function feeGo()
    {

        $id = $this->input->post('id', true);
        $catatan1 = $this->input->post('catatan1', true);
        $sts = 3;
        $date_wr1 = date('Y-m-d H:i:s');

        $this->db->set('date_wr1', $date_wr1);
        $this->db->set('sts', $sts);
        $this->db->set('catatan1', $catatan1);
        $this->db->where('id', $id);
        $this->db->update('ins_uipjs');

        $log = [
            'log' => "Pak WR1 Menyetujui Insentif Publikasi Jurnal dari Skripsi/Thesis/Disertasi Data ID $id",
            'email' => $this->session->userdata('email'),
            'date_created' => time()
        ];
        $this->db->insert('occ_log', $log);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('pimpinan1/vuipjs');
    }


    public function tolak()
    {
        $id = $this->uri->segment(4);
        $data = array(
            'title' => 'Penolakan Usulan Publikasi dari Skripsi/Thesis/Disertasi',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vuipjs' => 'active',
            'v' => $this->ma->getUipjs(),
            'd' => $this->ma->getDetail($id),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('pimpinan1/uipjs/t_uipjs', $data);
        $this->load->view('layouts/footer');
    }

    public function tolakGo()
    {

        $id = $this->input->post('id', true);
        $tolak1 = $this->input->post('tolak1', true);
        $sts = 6;
        $date_wr1 = date('Y-m-d H:i:s');


        $this->db->set('date_wr1', $date_wr1);
        $this->db->set('sts', $sts);
        $this->db->set('tolak1', $tolak1);
        $this->db->where('id', $id);
        $this->db->update('ins_uipjs');

        $log = [
            'log' => "Pak WR 1 Menilak Insentif Publikasi Jurnal dari Skripsi/Thesis/Disertasi Data ID $id",
            'email' => $this->session->userdata('email'),
            'date_created' => time()
        ];

        $this->db->insert('occ_log', $log);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('pimpinan1/vuipjs');
    }
}
