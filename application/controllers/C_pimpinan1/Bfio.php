<!-- INSENTIF ARTIKEL DIJURNAL -->
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bfio extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_pimpinan1/Model_bfio', 'ma');
        $this->load->model('m_sesi/Model_user', 'mu');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Data Usulan Bantuan Mengikuti Seminar Internasional/Nasional',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vbfio' => 'active',
            'v' => $this->ma->getBfio(),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('verifikator_publis/bfio/v_bfio', $data);
        $this->load->view('layouts/footer');
    }


    public function tolak()
    {

        $id = $this->uri->segment(4);
        $data = array(
            'title' => 'Data Usulan Bantuan Mengikuti Seminar Internasional/Nasional',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vbfio' => 'active',
            'v' => $this->ma->getBfio(),
            'd' => $this->ma->getDetail($id),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('pimpinan1/bfio/t_bfio', $data);
        $this->load->view('layouts/footer');
    }


    public function tolakGo()
    {

        $id = $this->input->post('id', true);
        $tolak1 = $this->input->post('tolak1', true);
        $sts = 6;
        $date_wr1 = date('Y-m-d H:i:s');

        $this->db->set('date_wr1', $date_wr1);
        $this->db->set('sts', $sts);
        $this->db->set('tolak1', $tolak1);
        $this->db->where('id', $id);
        $this->db->update('ins_bfio');

        $log = [
            'log' => "Pak Wr 1 Menolak Form Usulan Bantuan Mengikuti Seminar Internasional/Nasional  ID $id",
            'email' => $this->session->userdata('email'),
            'date_created' => time()
        ];
        $this->db->insert('occ_log', $log);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('pimpinan1/vbfio');
    }

    public function fee($id)
    {

        $id = $this->uri->segment(4);
        $data = array(
            'title' => 'Data Usulan Bantuan Mengikuti Seminar Internasional/Nasional',
            'active_menu_validasiInsentif' => 'menu-open',
            'active_menu_vi' => 'active',
            'active_menu_vbfio' => 'active',
            'v' => $this->ma->getBfio(),
            'd' => $this->ma->getDetail($id),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('pimpinan1/bfio/s_bfio', $data);
        $this->load->view('layouts/footer');
    }


    public function feeGo()
    {

        $id = $this->input->post('id', true);
        $catatan1 = $this->input->post('catatan1', true);
        $sts = 3;
        $date_wr1 = date('Y-m-d H:i:s');

        $this->db->set('date_wr1', $date_wr1);
        $this->db->set('sts', $sts);
        $this->db->set('catatan1', $catatan1);
        $this->db->where('id', $id);
        $this->db->update('ins_bfio');

        $log = [
            'log' => "Pak Wr 1 Menyetujui Insentif Form Usulan Bantuan Mengikuti Seminar Internasional/Nasional ID $id",
            'email' => $this->session->userdata('email'),
            'date_created' => time()
        ];


        $this->db->insert('occ_log', $log);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('pimpinan1/vbfio');
    }
}
