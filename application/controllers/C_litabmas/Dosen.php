<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dosen extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_publis/Model_aji', 'ma');
        $this->load->model('m_sesi/Model_user', 'mu');
        $this->load->model('m_litabmas/Model_litabmas', 'mlt');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Data Usulan Insentif Artikel Dijurnal',
            'active_menu_sipemas_penelitian' => 'menu-open',
            'active_menu_sp' => 'active',
            'active_menu_penelitian_ub' => 'active',
            'akun' => $this->mu->getUser(),
            'penempatan' => $this->mlt->getPenempatan(),
            'jafung' => $this->mlt->getJafung(),
            'penfor' => $this->mlt->getPenfor(),
            'lain' => $this->mlt->getLain(),
            'cek_tahap' => $this->mlt->cekLtb(),
        );

        $this->load->view('layouts/header', $data);
        $this->load->view('litabmas/dosen/index', $data);
        $this->load->view('layouts/footer');
    }

    public function cek()
    {
        $data = array(
            'title' => 'Data Usulan Insentif Artikel Dijurnal',
            'active_menu_sipemas_penelitian' => 'menu-open',
            'active_menu_sp' => 'active',
            'active_menu_penelitian_ub' => 'active',
            'akun' => $this->mu->getUser(),
            'skema' => $this->mlt->getSkema(),
            'periode' => $this->mlt->getPeriode(),
            'akun' => $this->mu->getUser(),
            'penempatan' => $this->mlt->getPenempatan(),
            'jafung' => $this->mlt->getJafung(),
            'penfor' => $this->mlt->getPenfor(),
            'lain' => $this->mlt->getLain(),
            'cek_tahap' => $this->mlt->cekLtb(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('litabmas/dosen/cek_ltb', $data);
        $this->load->view('layouts/footer');
    }


    public function tahap1()
    {
        $data = array(
            'title' => 'Data Usulan Insentif Artikel Dijurnal',
            'active_menu_sipemas_penelitian' => 'menu-open',
            'active_menu_sp' => 'active',
            'active_menu_penelitian_ub' => 'active',
            'akun' => $this->mu->getUser(),
            'skema' => $this->mlt->getSkema(),
            'periode' => $this->mlt->getPeriode(),
            'akun' => $this->mu->getUser(),
            'penempatan' => $this->mlt->getPenempatan(),
            'jafung' => $this->mlt->getJafung(),
            'penfor' => $this->mlt->getPenfor(),
            'lain' => $this->mlt->getLain(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('litabmas/dosen/tahap1', $data);
        $this->load->view('layouts/footer');
    }

    public function tahap2()
    {
        $data = array(
            'title' => 'Data Usulan Insentif Artikel Dijurnal',
            'active_menu_sipemas_penelitian' => 'menu-open',
            'active_menu_sp' => 'active',
            'active_menu_penelitian_ub' => 'active',
            'akun' => $this->mu->getUser(),
            'skema' => $this->mlt->getSkema(),
            'periode' => $this->mlt->getPeriode(),
            'kolaborasi' => $this->mlt->getKolaborasi(),
            'rumpun' => $this->mlt->getRumpun(),
            'bidang' => $this->mlt->getBidang(),
            'tema' => $this->mlt->getTema(),
            'topik' => $this->mlt->getTopik(),
            'd' => $this->mlt->getlitabmas(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('litabmas/dosen/tahap2', $data);
        $this->load->view('layouts/footer');
    }

    public function save_tahap2()
    {
        $id = $this->uuid->v4();
        $reff_ltb = str_replace('-', '', $id);
        $email = $this->session->userdata('email');
        $data = array(
            'reff_ltb' => $reff_ltb,
            'email' => $email,
            'id_skema' => htmlspecialchars($this->input->post('id_skema', true)),
            'id_periode' => htmlspecialchars($this->input->post('id_periode', true)),
            'tahun_usulan' => htmlspecialchars($this->input->post('tahun_usulan', true)),
            'tahun_pelaksanaan' => htmlspecialchars($this->input->post('tahun_pelaksanaan', true)),
            'sts_tahap' => 1,
            'date_registrasi' => date('Y-m-d H:i:s'),
            'date_created' => date('Y-m-d H:i:s')
        );

        $log_system = array(
            'reff_ltb' => $reff_ltb,
            'email_log' => $email,
            'logsystem' => "Mendaftar LITABMAS",
            'date_created' => date('Y-m-d H:i:s')
        );

        $this->db->insert('ltb_litabmas', $data);
        $this->db->insert('ltb_log', $log_system);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('litabmas/tahap3');
    }


    public function updateTahap2()
    {
        $email = $this->session->userdata('email');
        $reff_ltb = htmlspecialchars($this->input->post('reff_ltb', true));
        $judul = htmlspecialchars($this->input->post('judul', true));
        $id_skema = htmlspecialchars($this->input->post('id_skema', true));
        $jenis_kolaborasi_id = htmlspecialchars($this->input->post('jenis_kolaborasi_id', true));
        $rumpun_ilmu_id = htmlspecialchars($this->input->post('rumpun_ilmu_id', true));
        $bfp_id = htmlspecialchars($this->input->post('bfp_id', true));
        $tema_ltb_id = htmlspecialchars($this->input->post('tema_ltb_id', true));
        $topik_ltb_id = htmlspecialchars($this->input->post('topik_ltb_id', true));
        $lama_kegiatan = htmlspecialchars($this->input->post('lama_kegiatan', true));

        $log_system = array(
            'reff_ltb' => $reff_ltb,
            'email_log' => $email,
            'logsystem' => "Menyimpan/Updated Tahap 2 LITABMAS",
            'date_created' => date('Y-m-d H:i:s')
        );

        $this->db->set('judul', $judul);
        $this->db->set('id_skema', $id_skema);
        $this->db->set('jenis_kolaborasi_id', $jenis_kolaborasi_id);
        $this->db->set('rumpun_ilmu_id', $rumpun_ilmu_id);
        $this->db->set('bfp_id', $bfp_id);
        $this->db->set('tema_ltb_id', $tema_ltb_id);
        $this->db->set('topik_ltb_id', $topik_ltb_id);
        $this->db->set('lama_kegiatan', $lama_kegiatan);
        $this->db->where('reff_ltb', $reff_ltb);
        $this->db->update('ltb_litabmas');


        $this->db->insert('ltb_log', $log_system);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('litabmas/tahap2');
    }


    public function dospem()
    {
        $id = $this->uuid->v4();
        $reff_dospem = str_replace('-', '', $id);
        $email = $this->session->userdata('email');
        $data = array(
            'reff_dospem' => $reff_dospem,
            'email_created' => $email,
            'reff_ltb' => htmlspecialchars($this->input->post('reff_ltb', true)),
            'email_pembimbing' => htmlspecialchars($this->input->post('email_pembimbing', true)),
            'peran_pembimbing' => htmlspecialchars($this->input->post('peran', true)),
            'tugas_pembimbing' => htmlspecialchars($this->input->post('tugas', true)),
            'sts_pembimbing' => 1,
            'date_created' => date('Y-m-d H:i:s')
        );

        $log_system = array(
            'reff_ltb' => htmlspecialchars($this->input->post('reff_ltb', true)),
            'email_log' => $email,
            'logsystem' => "Menambahkan Pembimbing ",
            'date_created' => date('Y-m-d H:i:s')
        );

        $this->db->insert('ltb_pembimbing', $data);
        $this->db->insert('ltb_log', $log_system);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('litabmas/anggota_penelitian');
    }


    public function anggotaPen()
    {
        $id = $this->uuid->v4();
        $reff_anggota = str_replace('-', '', $id);
        $email = $this->session->userdata('email');
        $data = array(
            'reff_anggota' => $reff_anggota,
            'email_created' => $email,
            'reff_ltb' => htmlspecialchars($this->input->post('reff_ltb', true)),
            'email_anggota' => htmlspecialchars($this->input->post('email_anggota', true)),
            'peran_anggota' => htmlspecialchars($this->input->post('peran', true)),
            'tugas_anggota' => htmlspecialchars($this->input->post('tugas', true)),
            'sts_anggota' => 1,
            'date_created' => date('Y-m-d H:i:s')
        );

        $log_system = array(
            'reff_ltb' => htmlspecialchars($this->input->post('reff_ltb', true)),
            'email_log' => $email,
            'logsystem' => "Menambahkan anggota ",
            'date_created' => date('Y-m-d H:i:s')
        );

        $this->db->insert('ltb_anggota', $data);
        $this->db->insert('ltb_log', $log_system);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('litabmas/anggota_penelitian');
    }

    public function anggotaMahasiswa()
    {
        $id = $this->uuid->v4();
        $reff_mahasiswa = str_replace('-', '', $id);
        $email = $this->session->userdata('email');
        $data = array(
            'reff_mahasiswa' => $reff_mahasiswa,
            'email_created' => $email,
            'reff_ltb' => htmlspecialchars($this->input->post('reff_ltb', true)),
            'email_mahasiswa' => htmlspecialchars($this->input->post('email_mahasiswa', true)),
            'npm' => htmlspecialchars($this->input->post('npm', true)),
            'nama_mahasiswa' => htmlspecialchars($this->input->post('nama_mahasiswa', true)),
            'prodi_mahasiswa' => htmlspecialchars($this->input->post('prodi_mahasiswa', true)),
            'peran_mahasiswa' => htmlspecialchars($this->input->post('peran', true)),
            'tugas_mahasiswa' => htmlspecialchars($this->input->post('tugas', true)),
            'sts_mahasiswa' => 2,
            'date_created' => date('Y-m-d H:i:s')
        );

        $log_system = array(
            'reff_ltb' => htmlspecialchars($this->input->post('reff_ltb', true)),
            'email_log' => $email,
            'logsystem' => "Menambahkan mahasiswa ",
            'date_created' => date('Y-m-d H:i:s')
        );

        $this->db->insert('ltb_mahasiswa', $data);
        $this->db->insert('ltb_log', $log_system);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('litabmas/anggota_penelitian');
    }



    public function tahap3()
    {
        $data = array(
            'title' => 'Data Usulan Insentif Artikel Dijurnal',
            'active_menu_sipemas_penelitian' => 'menu-open',
            'active_menu_sp' => 'active',
            'active_menu_penelitian_ub' => 'active',
            'aji' => $this->ma->getAji(),
            'akun' => $this->mu->getUser(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('litabmas/dosen/tahap3', $data);
        $this->load->view('layouts/footer');
    }

    public function anggota_penelitian()
    {
        $data = array(
            'title' => 'Data Usulan Insentif Artikel Dijurnal',
            'active_menu_sipemas_penelitian' => 'menu-open',
            'active_menu_sp' => 'active',
            'active_menu_penelitian_ub' => 'active',
            'aji' => $this->ma->getAji(),
            'akun' => $this->mu->getUser(),
            'pembimbing' => $this->mlt->getPembimbing(),
            'd' => $this->mlt->getlitabmas(),
            'pbb' => $this->mlt->getPbb(),
            'agt' => $this->mlt->getAnggota(),
            'mhs' => $this->mlt->getMahasiswa(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('litabmas/dosen/anggota_penelitian', $data);
        $this->load->view('layouts/footer');
    }

    public function sub_usul()
    {
        $data = array(
            'title' => 'Data Usulan Insentif Artikel Dijurnal',
            'active_menu_sipemas_penelitian' => 'menu-open',
            'active_menu_sp' => 'active',
            'active_menu_penelitian_ub' => 'active',
            'akun' => $this->mu->getUser(),
            'd' => $this->mlt->getlitabmas(),
            'lur' => $this->mlt->getlur(),
            'lurTam' => $this->mlt->getlurTambah(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('litabmas/dosen/sub_usul', $data);
        $this->load->view('layouts/footer');
    }

    public function luaranTC()
    {
        $id = $this->uuid->v4();
        $reff_luaran = str_replace('-', '', $id);
        $email = $this->session->userdata('email');
        $data = array(
            'reff_luaran' => $reff_luaran,
            'email_created' => $email,
            'reff_ltb' => htmlspecialchars($this->input->post('reff_ltb', true)),
            'luaran' => htmlspecialchars($this->input->post('luaran', true)),
            'date_created' => date('Y-m-d H:i:s')
        );

        $log_system = array(
            'reff_ltb' => htmlspecialchars($this->input->post('reff_ltb', true)),
            'email_log' => $email,
            'logsystem' => "Menambahkan Luaran ",
            'date_created' => date('Y-m-d H:i:s')
        );


        $this->db->insert('ltb_luaran', $data);
        $this->db->insert('ltb_log', $log_system);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('litabmas/sub_usul');
    }


    public function hapus_luaranTC()
    {
        $id_luaran = $this->uri->segment(3);

        $this->db->where('id_luaran', $id_luaran);
        $this->db->delete('ltb_luaran');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('litabmas/sub_usul');
    }

    public function hapus_luaranTambahan()
    {
        $id_luaran_tambahan = $this->uri->segment(3);

        $this->db->where('id_luaran_tambahan', $id_luaran_tambahan);
        $this->db->delete('ltb_luaran_tambahan');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('litabmas/sub_usul');
    }

    public function luaranTambahan()
    {
        $id = $this->uuid->v4();
        $reff_tambahan = str_replace('-', '', $id);
        $email = $this->session->userdata('email');
        $data = array(
            'reff_tambahan' => $reff_tambahan,
            'email_created' => $email,
            'reff_ltb' => htmlspecialchars($this->input->post('reff_ltb', true)),
            'keterangan' => htmlspecialchars($this->input->post('keterangan', true)),
            'tahun' => htmlspecialchars($this->input->post('tahun', true)),
            'date_created' => date('Y-m-d H:i:s')
        );

        $log_system = array(
            'reff_ltb' => htmlspecialchars($this->input->post('reff_ltb', true)),
            'email_log' => $email,
            'logsystem' => "Menambahkan Luaran Tambahan ",
            'date_created' => date('Y-m-d H:i:s')
        );


        $this->db->insert('ltb_luaran_tambahan', $data);
        $this->db->insert('ltb_log', $log_system);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('litabmas/sub_usul');
    }


    public function uploadDokUsulan()
    {

        $data = array(
            'title' => 'Data Usulan Insentif Artikel Dijurnal',
            'active_menu_sipemas_penelitian' => 'menu-open',
            'active_menu_sp' => 'active',
            'active_menu_penelitian_ub' => 'active',
            'akun' => $this->mu->getUser(),
            'd' => $this->mlt->getlitabmas(),
            'lur' => $this->mlt->getlur(),
            'lurTam' => $this->mlt->getlurTambah(),
        );

        $reff_ltb = htmlspecialchars($this->input->post('reff_ltb', true));
        $email = $this->session->userdata('email');
        $upload_image = $_FILES['file_substansi']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10048';
            $config['upload_path'] = './arc_litabmas/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file_substansi')) {
                $old_image = $data['ltb_litabmas']['file_substansi'];
                if ($old_image != 'default.jpg') {
                    unlink(FCPATH . 'arc_litabmas/' . $old_image);
                }
                $new_image = $this->upload->data('file_name');
            } else {
                echo $this->upload->display_errors();
            }
        }
        $log_system = array(
            'reff_ltb' => htmlspecialchars($this->input->post('reff_ltb', true)),
            'email_log' => $email,
            'logsystem' => "Menambahkan File Substansi ",
            'date_created' => date('Y-m-d H:i:s')
        );


        $this->db->set('file_substansi', $new_image);
        $this->db->where('reff_ltb', $reff_ltb);
        $this->db->update('ltb_litabmas');


        $this->db->insert('ltb_log', $log_system);
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('litabmas/sub_usul');
    }

    public function rab()
    {
        $data = array(
            'title' => 'RAB',
            'active_menu_sipemas_penelitian' => 'menu-open',
            'active_menu_sp' => 'active',
            'active_menu_penelitian_ub' => 'active',
            'akun' => $this->mu->getUser(),
            'd' => $this->mlt->getlitabmas(),
            'viw' => $this->mlt->getRab(),
            'lurTam' => $this->mlt->getlurTambah(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('litabmas/dosen/rab', $data);
        $this->load->view('layouts/footer');
    }
}
