<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mjaf extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_manajemen/Model_mjaf', 'mj');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Manajemen Jafung',
            'active_menu_manajemen' => 'menu-open',
            'active_menu_mm' => 'active',
            'active_menu_mjaf' => 'active',
            'v' => $this->mj->getView()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('manajemen/mjaf/v_mjaf', $data);
        $this->load->view('layouts/footer');
    }

    public function new_jaf()
    {
        $data = array(
            'title' => 'Tambah Data Jafung',
            'active_menu_manajemen' => 'menu-open',
            'active_menu_mm' => 'active',
            'active_menu_mjaf' => 'active',
            'v' => $this->mj->getView()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('manajemen/mjaf/c_mjaf', $data);
        $this->load->view('layouts/footer');
    }

    public function save_jaf()
    {
        $data = array(
            'title' => 'Tambah Data Jafung',
            'active_menu_manajemen' => 'menu-open',
            'active_menu_mm' => 'active',
            'active_menu_mjaf' => 'active',
            'v' => $this->mj->getView()
        );


        $data = array(
            'skor' =>  htmlspecialchars($this->input->post('skor', true)),
            'jafung' =>  htmlspecialchars($this->input->post('jafung', true)),
            'aktif' => 1,
            'at_created' => date('Y-m-d H:i:s')
        );

        $result = $this->mj->save_jaf($data);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('mjaf');
        } else {
            $this->session->set_flashdata('gagal', 'Disimpan');
            redirect('mjaf/new');
        }
    }


    public function hapus_jaf()
    {
        $id_jfungsional = $this->uri->segment(3);
        $aktif = 0;

        $this->db->set('aktif', $aktif);
        $this->db->where('id_jfungsional', $id_jfungsional);
        $this->db->update('mm_jafung');
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('mjaf');
    }

    public function edit_jaf()
    {
        $id = $this->uri->segment(3);
        $data = array(
            'title' => 'Edit Data Jafung',
            'active_menu_manajemen' => 'menu-open',
            'active_menu_mm' => 'active',
            'active_menu_mjaf' => 'active',
            'd' => $this->mj->detail_jaf($id)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('manajemen/mjaf/e_mjaf', $data);
        $this->load->view('layouts/footer');
    }

    public function editGo_jaf()
    {
        $data = array(
            'title' => 'Edit Data Fakultas',
            'active_menu_manajemen' => 'menu-open',
            'active_menu_mm' => 'active',
            'active_menu_mjaf' => 'active',
            'v' => $this->mj->getView()
        );
        $id_jfungsional =  htmlspecialchars($this->input->post('id_jfungsional', true));
        $skor = htmlspecialchars($this->input->post('skor', true));
        $jafung = htmlspecialchars($this->input->post('jafung', true));

        $this->db->set('skor', $skor);
        $this->db->set('jafung', $jafung);
        $this->db->where('id_jfungsional', $id_jfungsional);
        $this->db->update('mm_jafung');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('mjaf');
    }
}
