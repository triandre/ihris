<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Maik extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_manajemen/Model_maik', 'mm');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Manajemen AIK',
            'active_menu_manajemen' => 'menu-open',
            'active_menu_mm' => 'active',
            'active_menu_maik' => 'active',
            'v' => $this->mm->getView()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('manajemen/maik/v_maik', $data);
        $this->load->view('layouts/footer');
    }

    public function new_aik()
    {
        $data = array(
            'title' => 'Tambah Data AIK',
            'active_menu_manajemen' => 'menu-open',
            'active_menu_mm' => 'active',
            'active_menu_maik' => 'active',
            'v' => $this->mm->getView()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('manajemen/maik/c_maik', $data);
        $this->load->view('layouts/footer');
    }

    public function save_aik()
    {
        $data = array(
            'title' => 'Tambah Data AIK',
            'active_menu_manajemen' => 'menu-open',
            'active_menu_mm' => 'active',
            'active_menu_maik' => 'active',
            'v' => $this->mm->getView()

        );

        $data = array(
            'aik' =>  htmlspecialchars($this->input->post('aik', true)),
            'aktif' => 1,
            'date_created' => date('Y-m-d H:i:s')
        );

        $result = $this->mm->save_aik($data);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('maik');
        } else {
            $this->session->set_flashdata('gagal', 'Disimpan');
            redirect('maik/new');
        }
    }


    public function hapus_aik()
    {
        $id_kriteria = $this->uri->segment(3);
        $aktif = 0;

        $this->db->set('aktif', $aktif);
        $this->db->where('id_kriteria', $id_kriteria);
        $this->db->update('mm_aik');
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('maik');
    }

    public function edit_aik()
    {
        $id_kriteria = $this->uri->segment(3);
        $data = array(
            'title' => 'Detail Data Pengguna',
            'active_menu_manajemen' => 'menu-open',
            'active_menu_mm' => 'active',
            'active_menu_maik' => 'active',
            'd' => $this->mm->detail_aik($id_kriteria)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('manajemen/maik/e_maik', $data);
        $this->load->view('layouts/footer');
    }

    public function editGo_aik()
    {
        $id_kriteria =  htmlspecialchars($this->input->post('id_kriteria', true));
        $aik = htmlspecialchars($this->input->post('aik', true));

        $this->db->set('aik', $aik);
        $this->db->where('id_kriteria', $id_kriteria);
        $this->db->update('mm_aik');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('maik');
    }
}
