<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mfak extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_manajemen/Model_mfak', 'mm');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Manajemen Fakultas',
            'active_menu_manajemen' => 'menu-open',
            'active_menu_mm' => 'active',
            'active_menu_mfak' => 'active',
            'v' => $this->mm->getView()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('manajemen/mfak/v_mfak', $data);
        $this->load->view('layouts/footer');
    }

    public function new_fak()
    {
        $data = array(
            'title' => 'Tambah Data fakultas',
            'active_menu_manajemen' => 'menu-open',
            'active_menu_mm' => 'active',
            'active_menu_mfak' => 'active',
            'v' => $this->mm->getView()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('manajemen/mfak/c_mfak', $data);
        $this->load->view('layouts/footer');
    }

    public function save_fak()
    {
        $data = array(
            'title' => 'Tambah Data fakultas',
            'active_menu_manajemen' => 'menu-open',
            'active_menu_mm' => 'active',
            'active_menu_mfak' => 'active',
            'v' => $this->mm->getView()
        );
        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf|jpg|jpeg';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/arsip/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data['mm_fakultas']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/arsip/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }
        $data = array(
            'kode_fakultas' =>  htmlspecialchars($this->input->post('kode_fakultas', true)),
            'fakultas' =>  htmlspecialchars($this->input->post('fakultas', true)),
            'aktif' => 1,
            'file' => $upload1,
            'date_created' => date('Y-m-d H:i:s')
        );

        $result = $this->mm->save_fak($data);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('mfak');
        } else {
            $this->session->set_flashdata('gagal', 'Disimpan');
            redirect('mfak/new');
        }
    }


    public function hapus_fak()
    {
        $id = $this->uri->segment(3);
        $aktif = 0;

        $this->db->set('aktif', $aktif);
        $this->db->where('id', $id);
        $this->db->update('mm_fakultas');
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('mfak');
    }

    public function edit_fak()
    {
        $id = $this->uri->segment(3);
        $data = array(
            'title' => 'Edit Data Fakultas',
            'active_menu_manajemen' => 'menu-open',
            'active_menu_mm' => 'active',
            'active_menu_mfak' => 'active',
            'd' => $this->mm->detail_fak($id)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('manajemen/mfak/e_mfak', $data);
        $this->load->view('layouts/footer');
    }

    public function editGo_fak()
    {
        $data = array(
            'title' => 'Edit Data Fakultas',
            'active_menu_manajemen' => 'menu-open',
            'active_menu_mm' => 'active',
            'active_menu_mfak' => 'active',
            'v' => $this->mm->getView()
        );
        $id =  htmlspecialchars($this->input->post('id', true));
        $kode_fakultas = htmlspecialchars($this->input->post('kode_fakultas', true));
        $fakultas = htmlspecialchars($this->input->post('fakultas', true));

        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf|jpg|jpeg';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/arsip/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data['mm_fakultas']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/arsip/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }

        $this->db->set('kode_fakultas', $kode_fakultas);
        $this->db->set('fakultas', $fakultas);
        $this->db->where('id', $id);
        $this->db->update('mm_fakultas');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('mfak');
    }
}
