<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Muser extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_manajemen/Model_muser', 'mm');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Manajemen Pengguna',
            'active_menu_manajemen' => 'menu-open',
            'active_menu_mm' => 'active',
            'active_menu_muser' => 'active',
            'v' => $this->mm->getView()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('manajemen/muser/v_muser', $data);
        $this->load->view('layouts/footer');
    }

    public function new_user()
    {
        $data = array(
            'title' => 'Tambah Data Pengguna',
            'active_menu_manajemen' => 'menu-open',
            'active_menu_mm' => 'active',
            'active_menu_muser' => 'active',
            'v' => $this->mm->getView()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('manajemen/muser/c_muser', $data);
        $this->load->view('layouts/footer');
    }

    public function save_user()
    {
        $data = array(
            'title' => 'Tambah Data Pengguna',
            'active_menu_manajemen' => 'menu-open',
            'active_menu_mm' => 'active',
            'active_menu_muser' => 'active',
            'v' => $this->mm->getView()

        );
        $katasandi = "umsu123abc";
        $password = password_hash($katasandi, PASSWORD_DEFAULT);
        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf|jpg|jpeg';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/arsip/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data['occ_user']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/arsip/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }
        $data = array(
            'nidn' =>  htmlspecialchars($this->input->post('nidn', true)),
            'npp' =>  htmlspecialchars($this->input->post('npp', true)),
            'email' =>  htmlspecialchars($this->input->post('email', true)),
            'name' => htmlspecialchars($this->input->post('name', true)),
            'role_id' => htmlspecialchars($this->input->post('role_id', true)),
            'password' => $password,
            'image' => "default.jpg",
            'is_active' => 1,
            'file' => $upload1,
            'date_created' => date('Y-m-d H:i:s')
        );

        $result = $this->mm->save_user($data);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('muser');
        } else {
            $this->session->set_flashdata('gagal', 'Disimpan');
            redirect('muser/new');
        }
    }


    public function on_user()
    {
        $id = $this->uri->segment(3);
        $is_active = 1;

        $this->db->set('is_active', $is_active);
        $this->db->where('id', $id);
        $this->db->update('occ_user');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('muser');
    }

    public function off_user()
    {
        $id = $this->uri->segment(3);
        $is_active = 0;

        $this->db->set('is_active', $is_active);
        $this->db->where('id', $id);
        $this->db->update('occ_user');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('muser');
    }

    public function reset_password()
    {
        $id = $this->uri->segment(3);
        $katasandi = "umsuunggul";
        $password = password_hash($katasandi, PASSWORD_DEFAULT);

        $this->db->set('password', $password);
        $this->db->where('id', $id);
        $this->db->update('occ_user');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('muser');
    }

    public function detail_user()
    {
        $id = $this->uri->segment(3);
        $data = array(
            'title' => 'Detail Data Pengguna',
            'active_menu_manajemen' => 'menu-open',
            'active_menu_mm' => 'active',
            'active_menu_muser' => 'active',
            'd' => $this->mm->detail_user($id)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('manajemen/muser/d_muser', $data);
        $this->load->view('layouts/footer');
    }

    public function edit_user()
    {
        $id = $this->uri->segment(3);
        $data = array(
            'title' => 'Detail Data Pengguna',
            'active_menu_manajemen' => 'menu-open',
            'active_menu_mm' => 'active',
            'active_menu_muser' => 'active',
            'd' => $this->mm->detail_user($id)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('manajemen/muser/e_muser', $data);
        $this->load->view('layouts/footer');
    }

    public function edit_userGo()
    {

        $data = array(
            'title' => 'Tambah Data Pengguna',
            'active_menu_manajemen' => 'menu-open',
            'active_menu_mm' => 'active',
            'active_menu_muser' => 'active',
            'v' => $this->mm->getView()

        );
        $id = htmlspecialchars($this->input->post('id', true));
        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf|jpg|jpeg';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/arsip/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data['occ_user']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/arsip/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }
        $data = array(
            'nidn' =>  htmlspecialchars($this->input->post('nidn', true)),
            'npp' =>  htmlspecialchars($this->input->post('npp', true)),
            'email' =>  htmlspecialchars($this->input->post('email', true)),
            'name' => htmlspecialchars($this->input->post('name', true)),
            'role_id' => htmlspecialchars($this->input->post('role_id', true)),
            'file' => $upload1
        );

        $result = $this->mm->edit_user($data, $id);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('muser');
        } else {
            $this->session->set_flashdata('gagal', 'Disimpan');
            redirect('muser/edit/' . $id);
        }
    }
}
