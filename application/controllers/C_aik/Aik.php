<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Aik extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_dosen/m_aik/Model_aik', 'ma');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Al Islam Kemuhammadiyahan',
            'active_menu_aik' => 'active',
            'aik' => $this->ma->getView()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/aik/v_aik', $data);
        $this->load->view('layouts/footer');
    }

    public function createAik()
    {
        $data = array(
            'title' => 'Al Islam Kemuhammadiyahan',
            'active_menu_aik' => 'active',
            'aik' => $this->ma->getView(),
            'kriteria' => $this->ma->getKriteria()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/aik/c_aik', $data);
        $this->load->view('layouts/footer');
    }

    public function saveAik()
    {
        $data = array(
            'title' => 'Al Islam Kemuhammadiyahan',
            'active_menu_aik' => 'active',
            'aik' => $this->ma->getView(),
            'kriteria' => $this->ma->getKriteria()
        );
        $id = $this->uuid->v4();
        $reff = str_replace('-', '', $id);
        $nidn = $this->session->userdata('nidn');
        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/aik/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data['occ_aik']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/aik/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }

        $data = array(
            'nidn' => $nidn,
            'reff' => $reff,
            'id_kriteria' => htmlspecialchars($this->input->post('id_kriteria', true)),
            'nama_kegiatan' => htmlspecialchars($this->input->post('nama_kegiatan', true)),
            'tgl_mulai' => htmlspecialchars($this->input->post('tgl_mulai', true)),
            'tgl_selesai' => htmlspecialchars($this->input->post('tgl_selesai', true)),
            'url' => htmlspecialchars($this->input->post('url', true)),
            'aktif' => 1,
            'file' => $upload1,
            'date_created' => date('Y-m-d H:i:s')
        );


        $result = $this->ma->storeAik($data);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('aik');
        } else {
            $this->session->set_flashdata('gagal', 'Disimpan');
            redirect('createAika');
        }
    }


    public function hapusAik($reff)
    {
        $reff = $this->uri->segment(3);
        $aktif = 0;

        $this->db->set('aktif', $aktif);
        $this->db->where('reff', $reff);
        $this->db->update('occ_aik');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('aik');
    }



    public function editAik()
    {
        $reff = $this->uri->segment(3);
        $data = array(
            'title' => 'Al Islam Kemuhammadiyahan',
            'active_menu_aik' => 'active',
            'aik' => $this->ma->getView(),
            'kriteria' => $this->ma->getKriteria(),
            'd' => $this->ma->editAik($reff)

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/aik/u_aik', $data);
        $this->load->view('layouts/footer');
    }

    public function editAikGo()
    {

        $data = array(
            'title' => 'Al Islam Kemuhammadiyahan',
            'active_menu_aik' => 'active',
            'aik' => $this->ma->getView(),
            'kriteria' => $this->ma->getKriteria(),
        );

        $reff = htmlspecialchars($this->input->post('reff', true));

        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/aik/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data['occ_aik']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/aik/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }
        $data = array(
            'id_kriteria' => htmlspecialchars($this->input->post('id_kriteria', true)),
            'nama_kegiatan' => htmlspecialchars($this->input->post('nama_kegiatan', true)),
            'tgl_mulai' => htmlspecialchars($this->input->post('tgl_mulai', true)),
            'tgl_selesai' => htmlspecialchars($this->input->post('tgl_selesai', true)),
            'url' => htmlspecialchars($this->input->post('url', true)),
            'aktif' => 1,
            'file' => $upload1
        );


        $result = $this->ma->updateAik($reff, $data);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Diubah');
            redirect('aik');
        } else {
            $this->session->set_flashdata('gagal', 'Diubah');
            redirect('aik/editAik/' . $reff);
        }
    }
}
