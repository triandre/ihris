<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cadmin extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('ModelDashboard', 'mds');
        $this->load->model('m_cuti/Model_cuti_admin', 'mc');
    }

    public function index()
    {
        $data = array(
            'title' => 'Permohonan Cuti',
            'active_menu_cuti' => 'active',
            'dp' => $this->md->getDetailProfil(),
            'cutiku' => $this->mc->getView(),
            'hak' => $this->mc->cutiku()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('cuti/admin/v_cuti', $data);
        $this->load->view('layouts/footer');
    }

    public function listCutiSetujui()
    {
        $data = array(
            'title' => 'Dashboard',
            'active_menu_cuti' => 'active',
            'dp' => $this->md->getDetailProfil(),
            'cutiku' => $this->mc->getViewSetujui(),
            'hak' => $this->mc->cutiku()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('cuti/admin/v_cuti', $data);
        $this->load->view('layouts/footer');
    }

    public function listCutiProses()
    {
        $data = array(
            'title' => 'Dashboard',
            'active_menu_cuti' => 'active',
            'dp' => $this->md->getDetailProfil(),
            'cutiku' => $this->mc->getViewProses(),
            'hak' => $this->mc->cutiku()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('cuti/admin/v_cuti', $data);
        $this->load->view('layouts/footer');
    }

    public function listCutiTolak()
    {
        $data = array(
            'title' => 'Dashboard',
            'active_menu_cuti' => 'active',
            'dp' => $this->md->getDetailProfil(),
            'cutiku' => $this->mc->getViewTolak(),
            'hak' => $this->mc->cutiku()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('cuti/admin/v_cuti', $data);
        $this->load->view('layouts/footer');
    }


    public function cutiProses()
    {
        $id_cuti = $this->uri->segment(3);
        $sts = 2;
        $tglProses = date('Y-m-d H:i:s');

        $this->db->set('sts', $sts);
        $this->db->set('tglProses', $tglProses);
        $this->db->where('id_cuti', $id_cuti);
        $this->db->update('occ_permohonan_cuti');


        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('admin/cuti');
    }

    public function cutiTolak()
    {
        $id_cuti = $this->uri->segment(3);
        $email = $this->uri->segment(4);
        $data = array(
            'title' => 'Permohonan Cuti',
            'active_menu_cuti' => 'active',
            'd' => $this->mc->detailCuti($id_cuti),
            'hak' => $this->mc->cutiMu($email)

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('cuti/admin/t_cuti', $data);
        $this->load->view('layouts/footer');
    }

    public function cutiTolakGo()
    {
        $id_cuti = htmlspecialchars($this->input->post('id_cuti', true));
        $stokCuti = htmlspecialchars($this->input->post('stokCuti', true));
        $cutiDiambil = htmlspecialchars($this->input->post('cutiDiambil', true));
        $jenisCuti = htmlspecialchars($this->input->post('jenisCuti', true));
        $pesan = htmlspecialchars($this->input->post('pesan', true));
        $email = htmlspecialchars($this->input->post('email', true));
        $sts = 4;
        $tglTolak = date('Y-m-d H:i:s');
        $kembalikan =  $stokCuti +  $cutiDiambil;

        if ($jenisCuti == "Cuti Umum") {
            $this->db->set('cutiUmum', $kembalikan);
            $this->db->where('email', $email);
            $this->db->update('mm_hak_cuti');
            $this->db->set('pesan', $pesan);
            $this->db->set('sts', $sts);
            $this->db->set('tglTolak', $tglTolak);
            $this->db->where('id_cuti', $id_cuti);
            $this->db->update('occ_permohonan_cuti');
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('admin/cuti');
        } elseif ($jenisCuti == "Cuti Umroh") {
            $this->db->set('cutiUmroh', $kembalikan);
            $this->db->where('email', $email);
            $this->db->update('mm_hak_cuti');
            $this->db->set('pesan', $pesan);
            $this->db->set('sts', $sts);
            $this->db->set('tglTolak', $tglTolak);
            $this->db->where('id_cuti', $id_cuti);
            $this->db->update('occ_permohonan_cuti');
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('admin/cuti');
        } else {
            $this->db->set('cutiHamil', $kembalikan);
            $this->db->where('email', $email);
            $this->db->update('mm_hak_cuti');
            $this->db->set('pesan', $pesan);
            $this->db->set('sts', $sts);
            $this->db->set('tglTolak', $tglTolak);
            $this->db->where('id_cuti', $id_cuti);
            $this->db->update('occ_permohonan_cuti');
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('admin/cuti');
        }
    }

    public function vizinsakit()
    {
        $data = array(
            'title' => 'Permohonan Cuti',
            'active_menu_izinsakit' => 'active',
            'dp' => $this->md->getDetailProfil(),
            'izinsakit' => $this->mc->getView_izinsakit()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('cuti/admin/v_izinsakit', $data);
        $this->load->view('layouts/footer');
    }
    public function vizinsakit_setujui()
    {
        $data = array(
            'title' => 'Permohonan Cuti',
            'active_menu_izinsakit' => 'active',
            'dp' => $this->md->getDetailProfil(),
            'izinsakit' => $this->mc->getView_izinsakit_setujui()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('cuti/admin/v_izinsakit', $data);
        $this->load->view('layouts/footer');
    }
    public function vizinsakit_tolak()
    {
        $data = array(
            'title' => 'Permohonan Cuti',
            'active_menu_izinsakit' => 'active',
            'dp' => $this->md->getDetailProfil(),
            'izinsakit' => $this->mc->getView_izinsakit_tolak()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('cuti/admin/v_izinsakit', $data);
        $this->load->view('layouts/footer');
    }

    public function fizinsakit_tolak()
    {
        $id_id = $this->uri->segment(4);
        $data = array(
            'title' => 'Permohonan Cuti',
            'active_menu_izinsakit' => 'active',
            'd' => $this->mc->detailizinsakit($id_id)

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('cuti/admin/t_izinsakit', $data);
        $this->load->view('layouts/footer');
    }

    public function fizinsakit_tolak_go()
    {
        $id_is = htmlspecialchars($this->input->post('id_is', true));
        $pesan = htmlspecialchars($this->input->post('pesan', true));
        $sts = 3;
        $tglTolak = date('Y-m-d H:i:s');


        $this->db->set('pesan', $pesan);
        $this->db->set('sts', $sts);
        $this->db->set('tglTolak', $tglTolak);
        $this->db->where('id_is', $id_is);
        $this->db->update('occ_izin_sakit');
        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('admin/izinsakit');
    }

    public function izinsakit_setujuiGo()
    {
        $id_is = $this->uri->segment(4);
        $sts = 2;
        $tglSetujui = date('Y-m-d H:i:s');

        $this->db->set('sts', $sts);
        $this->db->set('tglSetujui', $tglSetujui);
        $this->db->where('id_is', $id_is);
        $this->db->update('occ_izin_sakit');


        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('admin/izinsakit');
    }
}
