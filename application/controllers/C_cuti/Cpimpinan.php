<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cpimpinan extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('ModelDashboard', 'mds');
        $this->load->model('m_cuti/Model_cuti_pimpinan', 'mc');
    }

    public function index()
    {
        $data = array(
            'title' => 'Permohonan Cuti',
            'active_menu_cuti' => 'active',
            'dp' => $this->md->getDetailProfil(),
            'cutiku' => $this->mc->getView(),
            'hak' => $this->mc->cutiku()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('cuti/pimpinan/v_cuti', $data);
        $this->load->view('layouts/footer');
    }

    public function listCutiSetujui()
    {
        $data = array(
            'title' => 'Dashboard',
            'active_menu_cuti' => 'active',
            'dp' => $this->md->getDetailProfil(),
            'cutiku' => $this->mc->getViewSetujui(),
            'hak' => $this->mc->cutiku()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('cuti/pimpinan/v_cuti', $data);
        $this->load->view('layouts/footer');
    }

    public function listCutiProses()
    {
        $data = array(
            'title' => 'Dashboard',
            'active_menu_cuti' => 'active',
            'dp' => $this->md->getDetailProfil(),
            'cutiku' => $this->mc->getViewProses(),
            'hak' => $this->mc->cutiku()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('cuti/pimpinan/v_cuti', $data);
        $this->load->view('layouts/footer');
    }

    public function listCutiTolak()
    {
        $data = array(
            'title' => 'Dashboard',
            'active_menu_cuti' => 'active',
            'dp' => $this->md->getDetailProfil(),
            'cutiku' => $this->mc->getViewTolak(),
            'hak' => $this->mc->cutiku()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('cuti/pimpinan/v_cuti', $data);
        $this->load->view('layouts/footer');
    }


    public function cutiSetujui()
    {
        $id_cuti = $this->uri->segment(3);
        $sts = 3;
        $tglsetujui = date('Y-m-d H:i:s');

        $this->db->set('sts', $sts);
        $this->db->set('tglsetujui', $tglsetujui);
        $this->db->where('id_cuti', $id_cuti);
        $this->db->update('occ_permohonan_cuti');


        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('pimpinan/cuti');
    }

    public function cutiTolak()
    {
        $id_cuti = $this->uri->segment(3);
        $email = $this->uri->segment(4);
        $data = array(
            'title' => 'Permohonan Cuti',
            'active_menu_cuti' => 'active',
            'd' => $this->mc->detailCuti($id_cuti),
            'hak' => $this->mc->cutiMu($email)

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('cuti/pimpinan/t_cuti', $data);
        $this->load->view('layouts/footer');
    }

    public function cutiTolakGo()
    {
        $id_cuti = htmlspecialchars($this->input->post('id_cuti', true));
        $stokCuti = htmlspecialchars($this->input->post('stokCuti', true));
        $cutiDiambil = htmlspecialchars($this->input->post('cutiDiambil', true));
        $jenisCuti = htmlspecialchars($this->input->post('jenisCuti', true));
        $pesan = htmlspecialchars($this->input->post('pesan', true));
        $email = htmlspecialchars($this->input->post('email', true));
        $sts = 4;
        $tglTolak = date('Y-m-d H:i:s');
        $kembalikan =  $stokCuti +  $cutiDiambil;

        if ($jenisCuti == "Cuti Umum") {
            $this->db->set('cutiUmum', $kembalikan);
            $this->db->where('email', $email);
            $this->db->update('mm_hak_cuti');
            $this->db->set('pesan', $pesan);
            $this->db->set('sts', $sts);
            $this->db->set('tglTolak', $tglTolak);
            $this->db->where('id_cuti', $id_cuti);
            $this->db->update('occ_permohonan_cuti');
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('pimpinan/cuti');
        } elseif ($jenisCuti == "Cuti Umroh") {
            $this->db->set('cutiUmroh', $kembalikan);
            $this->db->where('email', $email);
            $this->db->update('mm_hak_cuti');
            $this->db->set('pesan', $pesan);
            $this->db->set('sts', $sts);
            $this->db->set('tglTolak', $tglTolak);
            $this->db->where('id_cuti', $id_cuti);
            $this->db->update('occ_permohonan_cuti');
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('pimpinan/cuti');
        } else {
            $this->db->set('cutiHamil', $kembalikan);
            $this->db->where('email', $email);
            $this->db->update('mm_hak_cuti');
            $this->db->set('pesan', $pesan);
            $this->db->set('sts', $sts);
            $this->db->set('tglTolak', $tglTolak);
            $this->db->where('id_cuti', $id_cuti);
            $this->db->update('occ_permohonan_cuti');
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('pimpinan/cuti');
        }
    }
}
