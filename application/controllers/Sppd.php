<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Sppd extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->library('uuid');
        $this->load->model('m_presensi/ModelPresensi', 'mp');
        $this->load->model('m_lkk/Model_lkk', 'mc');
        $this->dbSppd = $this->load->database('db2', TRUE);
    }


    public function view_sppd()
    {
        $email=$this->session->userdata('email');
        $data = array(
            'title' => 'Permohonan SPPD',
            'active_menu_sppd' => 'active',
            'bansos' => $this->mc->getView_masuk_pimpinan(),
             'sppd'=> $this->dbSppd->query("SELECT * FROM app_sppd LEFT JOIN app_pimpinan ON app_pimpinan.email = app_sppd.email_pimpinan where app_sppd.status='1' AND app_sppd.email_pimpinan='$email'")->result_array(),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('sppd/index', $data);
        $this->load->view('layouts/footer');
    }

    public static function format($date)
    {
        $str = explode('-', $date);
        $bulan = array(
            '01' => 'Januari',
            '02' => 'Februari',
            '03' => 'Maret',
            '04' => 'April',
            '05' => 'Mei',
            '06' => 'Juni',
            '07' => 'Juli',
            '08' => 'Agustus',
            '09' => 'September',
            '10' => 'Oktober',
            '11' => 'November',
            '12' => 'Desember'
        );
        return $str['2'] . " " . $bulan[$str[1]] . " " . $str[0];
    }

    public function detail($id)
    {
        $id = $this->uri->segment(3);
       $data = array(
            'title' => 'Permohonan SPPD',
            'active_menu_sppd' => 'active',
            'bansos' => $this->mc->getView_masuk_pimpinan(),
             'sppd'=> $this->dbSppd->query("SELECT * FROM app_sppd LEFT JOIN app_pimpinan ON app_pimpinan.email = app_sppd.email_pimpinan where app_sppd.id='$id'")->row_array(),
        );
        $this->load->view('sppd/detail', $data);
    }

     public function setuju($id)
    {
        $id = $this->uri->segment(3);
        $status = 2;

        $this->dbSppd->set('status', $status);
        $this->dbSppd->where('id', $id);
        $this->dbSppd->update('app_sppd');

       $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('pimpinan_sppd/view_sppd');
    }

    public function tolak()
    {
         $id = $this->input->post('id');
        $status = 3;
        $catatan_pimpinan = $this->input->post('catatan_pimpinan');

        $this->dbSppd->set('status', $status);
        $this->dbSppd->set('catatan_pimpinan', $catatan_pimpinan);
        $this->dbSppd->where('id', $id);
        $this->dbSppd->update('app_sppd');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('pimpinan_sppd/view_sppd');
    }

}
