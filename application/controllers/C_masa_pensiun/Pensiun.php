<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pensiun extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_pensiun/ModelPensiun', 'mpn');
    }

    public function index()
    {
        $data = array(
            'title' => 'Masa Pensiun',
            'active_menu_ddt' => 'menu-open',
            'active_menu_dt' => 'active',
            'active_menu_masa_pensiun' => 'active',

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('pensiun/index', $data);
        $this->load->view('layouts/footer');
    }

    public function search_pegawai()
    {
        $status_pegawai = $this->input->post('status_pegawai', true);
        if ($status_pegawai == '1') {
            $data = array(
                'title' => 'Masa Pensiun Tendik',
                'active_menu_ddt' => 'menu-open',
                'active_menu_dt' => 'active',
                'active_menu_masa_pensiun' => 'active',
                'view' => $this->mpn->viewData($status_pegawai)

            );
            $this->load->view('layouts/header', $data);
            $this->load->view('pensiun/v_ptendik', $data);
            $this->load->view('layouts/footer');
        } else {
            $data = array(
                'title' => 'Masa Pensiun Dosen',
                'active_menu_ddt' => 'menu-open',
                'active_menu_dt' => 'active',
                'active_menu_masa_pensiun' => 'active',
                'view' => $this->mpn->viewData($status_pegawai)

            );
            $this->load->view('layouts/header', $data);
            $this->load->view('pensiun/v_pdosen', $data);
            $this->load->view('layouts/footer');
        }
    }
}
