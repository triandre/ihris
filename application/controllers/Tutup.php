<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tutup extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }

        //load model user
        $this->load->model('ModelUser', 'mu');
        $this->load->library('upload');
        $this->load->model('m_dosen/ModelDosenDetail', 'mddd');
        $this->load->model('ModelDashboard', 'mds');
        $this->load->model('m_sesi/Model_user', 'mu');
        $this->load->model('ModelTrack', 'mt');
    }

    //menampilkan data user
    public function index()
    {


        $this->load->view('tutup');
    }
}
