<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pengelola_jurnal extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_dosen/m_pengabdian/Model_pengelola_jurnal', 'mpj');
        $this->load->model('m_sesi/Model_user', 'mu');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Pengelola Jurnal',
            'active_menu_pengabdian' => 'menu-open',
            'active_menu_pgb' => 'active',
            'active_menu_pengjur' => 'active',
            'v' => $this->mpj->getView()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/pel_pengabdian/pj/v_pj', $data);
        $this->load->view('layouts/footer');
    }

    public function createPengelola_jurnal()
    {
        $data = array(
            'title' => 'Tambah Data Pengelola Jurnal',
            'active_menu_pengabdian' => 'menu-open',
            'active_menu_pgb' => 'active',
            'active_menu_pengjur' => 'active',
            'v' => $this->mpj->getView()
        );
        $this->form_validation->set_rules('kategori_kgt', 'Kategori Kegiatan', 'required|trim');
        $this->form_validation->set_rules('media_publikasi', 'Media Publikasi', 'required|trim');
        $this->form_validation->set_rules('peran', 'peran', 'required|trim');
        $this->form_validation->set_rules('no_sk', 'No Sk PEnugasan', 'required|trim');
        $this->form_validation->set_rules('tgl_mulai', 'Tanggal Mulai', 'required|trim');
        $this->form_validation->set_rules('tgl_selesai', 'Tanggal Selesai', 'required|trim');
        $this->form_validation->set_rules('status', 'status', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->load->view('layouts/header', $data);
            $this->load->view('dosen/pel_pengabdian/pj/c_pj', $data);
            $this->load->view('layouts/footer');
        } else {

            $id = $this->uuid->v4();
            $reff_pengjur = str_replace('-', '', $id);
            $nidn = $this->session->userdata('nidn');
            //UPLOAD
            $upload_image = $_FILES['file']['name'];
            if ($upload_image) {
                $config['allowed_types'] = 'pdf';
                $config['max_size']      = '10000';
                $config['upload_path'] = './archive/pengelola_jurnal/';
                $config['encrypt_name'] = TRUE;

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('file')) {
                    $old_image = $data['occ_pengelola_jurnal']['file'];
                    if ($old_image != 'default.pdf') {
                        unlink(FCPATH . 'archive/pengelola_jurnal/' . $old_image);
                    }
                    $upload1 = $this->upload->data('file_name');
                    $this->db->set('file', $upload1);
                } else {
                    echo $this->upload->display_errors();
                }
            }

            $data = array(
                'reff_pengjur' => $reff_pengjur,
                'nidn' => $nidn,
                'kategori_kgt' => htmlspecialchars($this->input->post('kategori_kgt', true)),
                'media_publikasi' => htmlspecialchars($this->input->post('media_publikasi', true)),
                'peran' => htmlspecialchars($this->input->post('peran', true)),
                'no_sk' => htmlspecialchars($this->input->post('no_sk', true)),
                'tgl_mulai' => htmlspecialchars($this->input->post('tgl_mulai', true)),
                'tgl_selesai' => htmlspecialchars($this->input->post('tgl_selesai', true)),
                'status' => htmlspecialchars($this->input->post('status', true)),
                'file' => $upload1,
                'aktif' => 1,
                'date_created' => date('Y-m-d H:i:s')

            );
            $result = $this->mpj->storePengelola_jurnal($data);

            if ($result >= 1) {
                $this->session->set_flashdata('sukses', 'Disimpan');
                redirect('pengelola_jurnal');
            } else {
                $this->session->set_flashdata('gagal', 'Disimpan');
                redirect('createPengelola_jurnal');
            }
        }
    }

    public function detailPengelola_jurnal()
    {
        $reff_pengjur = $this->uri->segment(2);
        $data = array(
            'title' => 'Detail data',
            'active_menu_pengabdian' => 'menu-open',
            'active_menu_pgb' => 'active',
            'active_menu_pengjur' => 'active',
            'v' => $this->mpj->getView(),
            'd' => $this->mpj->getDetail($reff_pengjur)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/pel_pengabdian/pj/d_pj', $data);
        $this->load->view('layouts/footer');
    }

    public function editPengelola_jurnal()
    {
        $reff_pengjur = $this->uri->segment(2);
        $data = array(
            'title' => 'Edit data',
            'active_menu_pengabdian' => 'menu-open',
            'active_menu_pgb' => 'active',
            'active_menu_pengjur' => 'active',
            'v' => $this->mpj->getView(),
            'd' => $this->mpj->getDetail($reff_pengjur)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/pel_pengabdian/pj/u_pj', $data);
        $this->load->view('layouts/footer');
    }

    public function updatePengelola_jurnal()
    {
        $data = array(
            'title' => 'Edit data',
            'active_menu_pengabdian' => 'menu-open',
            'active_menu_pgb' => 'active',
            'active_menu_pengjur' => 'active',
            'v' => $this->mpj->getView()
        );

        $reff_pengjur = htmlspecialchars($this->input->post('reff_pengjur', true));
        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/pengelola_jurnal/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data['occ_pengelola_jurnal']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/pengelola_jurnal/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }
        $data = array(
            'kategori_kgt' => htmlspecialchars($this->input->post('kategori_kgt', true)),
            'media_publikasi' => htmlspecialchars($this->input->post('media_publikasi', true)),
            'peran' => htmlspecialchars($this->input->post('peran', true)),
            'no_sk' => htmlspecialchars($this->input->post('no_sk', true)),
            'tgl_mulai' => htmlspecialchars($this->input->post('tgl_mulai', true)),
            'tgl_selesai' => htmlspecialchars($this->input->post('tgl_selesai', true)),
            'status' => htmlspecialchars($this->input->post('status', true)),
            'file' => $upload1,
            'aktif' => 1
        );


        $result = $this->mpj->updatePengelola_jurnal($reff_pengjur, $data);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Diubah');
            redirect('pengelola_jurnal');
        } else {
            $this->session->set_flashdata('gagal', 'Diubah');
            redirect('editPengelola_jurnal/' . $reff_pengjur);
        }
    }

    public function hapusPengelola_jurnal($reff_pengjur)
    {
        $reff_pengjur = $this->uri->segment(2);
        $aktif = 0;

        $this->db->set('aktif', $aktif);
        $this->db->where('reff_pengjur', $reff_pengjur);
        $this->db->update('occ_pengelola_jurnal');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('pengelola_jurnal');
    }
}
