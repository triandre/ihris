<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jabatan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_dosen/m_pengabdian/Model_jabatan_struktural', 'mjs');
        $this->load->model('m_sesi/Model_user', 'mu');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Riwayat Jabatan Struktural',
            'active_menu_pengabdian' => 'menu-open',
            'active_menu_pgb' => 'active',
            'active_menu_jabatan' => 'active',
            'v' => $this->mjs->getView()
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/pel_pengabdian/jabatan/v_jabatan', $data);
        $this->load->view('layouts/footer');
    }

    public function createJabatan()
    {
        $data = array(
            'title' => 'Tambah Riwayat Jabatan Struktural',
            'active_menu_pengabdian' => 'menu-open',
            'active_menu_pgb' => 'active',
            'active_menu_jabatan' => 'active',
            'v' => $this->mjs->getView()
        );
        $this->form_validation->set_rules('kategori_kgt', 'Kategori Kegiatan', 'required|trim');
        $this->form_validation->set_rules('jabatan_tugas', 'Jabatan Tugas', 'required|trim');
        $this->form_validation->set_rules('lokasi', 'Lokasi', 'required|trim');
        $this->form_validation->set_rules('no_sk', 'No Sk PEnugasan', 'required|trim');
        $this->form_validation->set_rules('tgl_mulai', 'Tanggal Mulai', 'required|trim');
        $this->form_validation->set_rules('tgl_selesai', 'Tanggal Selesai', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->load->view('layouts/header', $data);
            $this->load->view('dosen/pel_pengabdian/jabatan/c_jabatan', $data);
            $this->load->view('layouts/footer');
        } else {

            $id = $this->uuid->v4();
            $reff_jastru = str_replace('-', '', $id);
            $nidn = $this->session->userdata('nidn');
            //UPLOAD
            $upload_image = $_FILES['file']['name'];
            if ($upload_image) {
                $config['allowed_types'] = 'pdf';
                $config['max_size']      = '10000';
                $config['upload_path'] = './archive/jabatan/';
                $config['encrypt_name'] = TRUE;

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('file')) {
                    $old_image = $data['occ_jabatan_struktural']['file'];
                    if ($old_image != 'default.pdf') {
                        unlink(FCPATH . 'archive/jabatan/' . $old_image);
                    }
                    $upload1 = $this->upload->data('file_name');
                    $this->db->set('file', $upload1);
                } else {
                    echo $this->upload->display_errors();
                }
            }

            $data = array(
                'reff_jastru' => $reff_jastru,
                'nidn' => $nidn,
                'kategori_kgt' => htmlspecialchars($this->input->post('kategori_kgt', true)),
                'jabatan_tugas' => htmlspecialchars($this->input->post('jabatan_tugas', true)),
                'lokasi' => htmlspecialchars($this->input->post('lokasi', true)),
                'no_sk' => htmlspecialchars($this->input->post('no_sk', true)),
                'tgl_mulai' => htmlspecialchars($this->input->post('tgl_mulai', true)),
                'tgl_selesai' => htmlspecialchars($this->input->post('tgl_selesai', true)), 'file' => $upload1,
                'aktif' => 1,
                'date_created' => date('Y-m-d H:i:s')

            );
            $result = $this->mjs->storeJabatan($data);

            if ($result >= 1) {
                $this->session->set_flashdata('sukses', 'Disimpan');
                redirect('jabatan');
            } else {
                $this->session->set_flashdata('gagal', 'Disimpan');
                redirect('createJabatan');
            }
        }
    }

    public function detailJabatan()
    {
        $reff_jastru = $this->uri->segment(2);
        $data = array(
            'title' => 'Tambah Riwayat Jabatan Struktural',
            'active_menu_pengabdian' => 'menu-open',
            'active_menu_pgb' => 'active',
            'active_menu_jabatan' => 'active',
            'v' => $this->mjs->getView(),
            'd' => $this->mjs->getDetail($reff_jastru)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/pel_pengabdian/jabatan/d_jabatan', $data);
        $this->load->view('layouts/footer');
    }

    public function editJabatan()
    {
        $reff_jastru = $this->uri->segment(2);
        $data = array(
            'title' => 'Tambah Riwayat Jabatan Struktural',
            'active_menu_pengabdian' => 'menu-open',
            'active_menu_pgb' => 'active',
            'active_menu_jabatan' => 'active',
            'v' => $this->mjs->getView(),
            'd' => $this->mjs->getDetail($reff_jastru)
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/pel_pengabdian/jabatan/u_jabatan', $data);
        $this->load->view('layouts/footer');
    }

    public function updateJabatan()
    {
        $data = array(
            'title' => 'Tambah Riwayat Jabatan Struktural',
            'active_menu_pengabdian' => 'menu-open',
            'active_menu_pgb' => 'active',
            'active_menu_jabatan' => 'active',
            'v' => $this->mjs->getView()
        );

        $reff_jastru = htmlspecialchars($this->input->post('reff_jastru', true));
        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/jabatan/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data['occ_jabatan_struktural']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/jabatan/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }
        $data = array(
            'kategori_kgt' => htmlspecialchars($this->input->post('kategori_kgt', true)),
            'jabatan_tugas' => htmlspecialchars($this->input->post('jabatan_tugas', true)),
            'lokasi' => htmlspecialchars($this->input->post('lokasi', true)),
            'no_sk' => htmlspecialchars($this->input->post('no_sk', true)),
            'tgl_mulai' => htmlspecialchars($this->input->post('tgl_mulai', true)),
            'tgl_selesai' => htmlspecialchars($this->input->post('tgl_selesai', true)), 'file' => $upload1,
            'aktif' => 1,
        );


        $result = $this->mjs->updateJabatan($reff_jastru, $data);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Diubah');
            redirect('jabatan');
        } else {
            $this->session->set_flashdata('gagal', 'Diubah');
            redirect('editJabatan/' . $reff_jastru);
        }
    }

    public function hapusJabatan($reff_jastru)
    {
        $reff_jastru = $this->uri->segment(2);
        $aktif = 0;

        $this->db->set('aktif', $aktif);
        $this->db->where('reff_jastru', $reff_jastru);
        $this->db->update('occ_jabatan_struktural');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('jabatan');
    }
}
