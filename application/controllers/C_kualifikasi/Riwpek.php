<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Riwpek extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_dosen/ModelRiwpek', 'mr');
        $this->load->model('m_master/ModelPangkat', 'mp');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Riwayat Pekerjaan',
            'active_menu_kualifikasi' => 'menu-open',
            'active_menu_kl' => 'active',
            'active_menu_riwpek' => 'active',
            'riwpek' => $this->mr->getRiwpek(),

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/kualifikasi/riwpek/v_riwpek', $data);
        $this->load->view('layouts/footer');
    }

    public function createRiwpek()
    {
        $data = array(
            'title' => 'Tambah Data Riwayat Pekerjaan',
            'active_menu_kualifikasi' => 'menu-open',
            'active_menu_kl' => 'active',
            'active_menu_riwpek' => 'active',
            'penfor' => $this->mr->getRiwpek()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/kualifikasi/riwpek/c_riwpek', $data);
        $this->load->view('layouts/footer');
    }

    public function saveRiwpek()
    {
        $data = array(
            'title' => 'Tambah Data Riwayat Pekerjaan',
            'active_menu_kualifikasi' => 'menu-open',
            'active_menu_kl' => 'active',
            'active_menu_riwpek' => 'active',
            'penfor' => $this->mr->getRiwpek()

        );

        $nidn = $this->session->userdata('nidn');
        //upload
        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/riwpek/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data['occ_riwpek']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/riwpek/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }

        $data = array(
            'nidn' => $nidn,
            'bidang_usaha' => htmlspecialchars($this->input->post('bidang_usaha', true)),
            'jenis_pekerjaan' => htmlspecialchars($this->input->post('jenis_pekerjaan', true)),
            'jabatan' => htmlspecialchars($this->input->post('jabatan', true)),
            'instansi' => htmlspecialchars($this->input->post('instansi', true)),
            'devisi' => htmlspecialchars($this->input->post('devisi', true)),
            'deskripsi' => htmlspecialchars($this->input->post('deskripsi', true)),
            'mulai_bekerja' => htmlspecialchars($this->input->post('mulai_bekerja', true)),
            'selesai_bekerja' => htmlspecialchars($this->input->post('selesai_bekerja', true)),
            'area_pekerjaan' => htmlspecialchars($this->input->post('area_pekerjaan', true)),
            'file' => $upload1,
            'active' => 1,
            'created' => date('Y-m-d H:i:s')

        );


        $result = $this->mr->storeRiwpek($data);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('riwpek');
        } else {
            $this->session->set_flashdata('gagal', 'Disimpan');
            redirect('createRiwpek');
        }
    }

    public function hapusRiwpek($id_riwpek)
    {
        $id_riwpek = $this->uri->segment(2);
        $active = 0;

        $this->db->set('active', $active);
        $this->db->where('id_riwpek', $id_riwpek);
        $this->db->update('occ_riwpek');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('riwpek');
    }

    public function detailRiwpek()
    {
        $id_riwpek = $this->uri->segment(2);
        $data = array(
            'title' => 'Detail Riwayat Pekerjaan',
            'active_menu_kualifikasi' => 'menu-open',
            'active_menu_kl' => 'active',
            'active_menu_riwpek' => 'active',
            'd' => $this->mr->detailRiwpek($id_riwpek),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/kualifikasi/riwpek/d_riwpek', $data);
        $this->load->view('layouts/footer');
    }

    public function editRiwpek()
    {
        $id_riwpek = $this->uri->segment(2);
        $data = array(
            'title' => 'Detail Riwayat Pekerjaan',
            'active_menu_kualifikasi' => 'menu-open',
            'active_menu_kl' => 'active',
            'active_menu_riwpek' => 'active',
            'd' => $this->mr->detailRiwpek($id_riwpek),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/kualifikasi/riwpek/e_riwpek', $data);
        $this->load->view('layouts/footer');
    }

    public function updateRiwpek()
    {
        $data = array(
            'title' => 'Tambah Data Riwayat Pekerjaan',
            'active_menu_kualifikasi' => 'menu-open',
            'active_menu_kl' => 'active',
            'active_menu_riwpek' => 'active',
            'penfor' => $this->mr->getRiwpek()

        );

        $id_riwpek = $this->input->post('id_riwpek');
        //upload
        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/riwpek/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data['occ_riwpek']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/riwpek/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }

        $data = array(
            'bidang_usaha' => htmlspecialchars($this->input->post('bidang_usaha', true)),
            'jenis_pekerjaan' => htmlspecialchars($this->input->post('jenis_pekerjaan', true)),
            'jabatan' => htmlspecialchars($this->input->post('jabatan', true)),
            'instansi' => htmlspecialchars($this->input->post('instansi', true)),
            'devisi' => htmlspecialchars($this->input->post('devisi', true)),
            'deskripsi' => htmlspecialchars($this->input->post('deskripsi', true)),
            'mulai_bekerja' => htmlspecialchars($this->input->post('mulai_bekerja', true)),
            'selesai_bekerja' => htmlspecialchars($this->input->post('selesai_bekerja', true)),
            'area_pekerjaan' => htmlspecialchars($this->input->post('area_pekerjaan', true)),
            'file' => $upload1

        );


        $result = $this->mr->updateRiwpek($id_riwpek, $data);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('riwpek');
        } else {
            $this->session->set_flashdata('gagal', 'Disimpan');
            redirect('createRiwpek');
        }
    }
}
