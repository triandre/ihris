<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Penfor extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_dosen/ModelPenfor', 'mpf');
        $this->load->model('m_master/ModelPangkat', 'mp');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Pendidikan Formal',
            'active_menu_kualifikasi' => 'menu-open',
            'active_menu_kl' => 'active',
            'active_menu_penfor' => 'active',
            'penfor' => $this->mpf->getPenfor(),

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/kualifikasi/penfor/v_penfor', $data);
        $this->load->view('layouts/footer');
    }

    public function createPenfor()
    {
        $data = array(
            'title' => 'Ajuan Data Pendidikan Formal',
            'active_menu_kualifikasi' => 'menu-open',
            'active_menu_kl' => 'active',
            'active_menu_penfor' => 'active',
            'penfor' => $this->mpf->getPenfor(),

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/kualifikasi/penfor/c_penfor', $data);
        $this->load->view('layouts/footer');
    }

    public function savePenfor()
    {
        $data = array(
            'title' => 'Ajuan Data Pendidikan Formal',
            'active_menu_kualifikasi' => 'menu-open',
            'active_menu_kl' => 'active',
            'active_menu_penfor' => 'active',
            'penfor' => $this->mpf->getPenfor(),

        );
        $id = $this->uuid->v4();
        $reff_penfor = str_replace('-', '', $id);
        $nidn = $this->session->userdata('nidn');
        $sts = 2;
        $kode_ajuan = "ADPF";
        $jenis_ajuan = "Ajuan Data Pendidikan Formal";

        //upload

        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/penfor/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data['occ_pendidikan_formal']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/penfor/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }

        $data = array(
            'reff_penfor' => $reff_penfor,
            'nidn' => $nidn,
            'program_studi' => htmlspecialchars($this->input->post('program_studi', true)),
            'jenjang' => htmlspecialchars($this->input->post('jenjang', true)),
            'perguruan_tinggi' => htmlspecialchars($this->input->post('perguruan_tinggi', true)),
            'gelar_akademik' => htmlspecialchars($this->input->post('gelar_akademik', true)),
            'bidang_studi' => htmlspecialchars($this->input->post('bidang_studi', true)),
            'tahun_masuk' => htmlspecialchars($this->input->post('tahun_masuk', true)),
            'tahun_lulus' => htmlspecialchars($this->input->post('tahun_lulus', true)),
            'tgl_kelulusan' => htmlspecialchars($this->input->post('tgl_kelulusan', true)),
            'nomor_induk' => htmlspecialchars($this->input->post('nomor_induk', true)),
            'jumlah_semester_tempuh' => htmlspecialchars($this->input->post('jumlah_semester_tempuh', true)),
            'jumlah_kelulusan' => htmlspecialchars($this->input->post('jumlah_kelulusan', true)),
            'ipk_kelulusan' => htmlspecialchars($this->input->post('ipk_kelulusan', true)),
            'no_sk_penyetaraan' => htmlspecialchars($this->input->post('no_sk_penyetaraan', true)),
            'tgl_sk_penyetaraan' => htmlspecialchars($this->input->post('tgl_sk_penyetaraan', true)),
            'nomor_ijazah' => htmlspecialchars($this->input->post('nomor_ijazah', true)),
            'judul' => htmlspecialchars($this->input->post('judul', true)),
            'sts' => $sts,
            'file' => $upload1,
            'active' => 1,
            'tgl_ajuan' => date('Y-m-d H:i:s')

        );

        $pdd = array(
            'reff' => $reff_penfor,
            'kode_ajuan' => $kode_ajuan,
            'jenis_ajuan' => $jenis_ajuan,
            'tgl_ajuan' => date('Y-m-d H:i:s'),
            'nidn' => $nidn,
            'sts' => $sts

        );

        $result = $this->mpf->storePenfor($data);
        $result2 = $this->md->storePdd($pdd);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('pendidikan_formal');
        } else {
            $this->session->set_flashdata('gagal', 'Disimpan');
            redirect('createPenfor');
        }
    }

    public function hapusPenfor($id_penfor)
    {
        $id_penfor = $this->uri->segment(2);
        $active = 0;

        $this->db->set('active', $active);
        $this->db->where('id_penfor', $id_penfor);
        $this->db->update('occ_pendidikan_formal');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('pendidikan_formal');
    }

    public function detailPenfor()
    {
        $id_penfor = $this->uri->segment(2);
        $data = array(
            'title' => 'Detail Pendidikan Formal',
            'active_menu_kualifikasi' => 'menu-open',
            'active_menu_kl' => 'active',
            'active_menu_penfor' => 'active',
            'd' => $this->mpf->detailPenfor($id_penfor),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/kualifikasi/penfor/d_penfor', $data);
        $this->load->view('layouts/footer');
    }


    public function riwayatPenfor()
    {
        $data = array(
            'title' => 'Pendidikan Formal',
            'active_menu_kualifikasi' => 'menu-open',
            'active_menu_kl' => 'active',
            'active_menu_penfor' => 'active',
            'penfor' => $this->mpf->getpenfor(),
            'draft' => $this->mpf->viewDraft(),
            'diajukan' => $this->mpf->viewDiajukan(),
            'disetujui' => $this->mpf->viewDisetujui(),
            'ditolak' => $this->mpf->viewTolak(),
            'ditangguhkan' => $this->mpf->viewDitangguhkan(),
            'nDraft' => $this->mpf->numsDraft(),
            'nDiajukan' => $this->mpf->numsDiajukan(),
            'nDisetujui' => $this->mpf->numsDisetujui(),
            'nDitangguhkan' => $this->mpf->numsDitangguhkan(),
            'nDitolak' => $this->mpf->numsTolak()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/kualifikasi/penfor/r_penfor', $data);
        $this->load->view('layouts/footer');
    }

    public function detailPenforRiwayat()
    {
        $reff = $this->uri->segment(2);
        $data = array(
            'title' => 'Detail Pendidikan Formal',
            'active_menu_kualifikasi' => 'menu-open',
            'active_menu_kl' => 'active',
            'active_menu_penfor' => 'active',
            'd' => $this->mpf->detailPenforRiwayat($reff),
        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/kualifikasi/penfor/d_penfor', $data);
        $this->load->view('layouts/footer');
    }
}
