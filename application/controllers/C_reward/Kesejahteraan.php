<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kesejahteraan extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('m_dosen/m_reward/ModelKesejahteraan', 'mk');
        $this->load->library('uuid');
    }

    public function index()
    {
        $data = array(
            'title' => 'Kesejahteraan',
            'active_menu_reward' => 'menu-open',
            'active_menu_rw' => 'active',
            'active_menu_kesejahteraan' => 'active',
            'ksj' => $this->mk->getView()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/reward/kesejahteraan/v_kesejahteraan', $data);
        $this->load->view('layouts/footer');
    }

    public function createKesejahteraan()
    {
        $data = array(
            'title' => 'Kesejahteraan',
            'active_menu_reward' => 'menu-open',
            'active_menu_rw' => 'active',
            'active_menu_kesejahteraan' => 'active',
            'ksj' => $this->mk->getView()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/reward/kesejahteraan/c_kesejahteraan', $data);
        $this->load->view('layouts/footer');
    }

    public function saveKesejahteraan()
    {
        $data = array(
            'title' => 'Kesejahteraan',
            'active_menu_reward' => 'menu-open',
            'active_menu_rw' => 'active',
            'active_menu_kesejahteraan' => 'active',
            'ksj' => $this->mk->getView()

        );
        $nidn = $this->session->userdata('nidn');

        $data = array(
            'nidn' => $nidn,
            'jenis_kesejahteraan' => htmlspecialchars($this->input->post('jenis_kesejahteraan', true)),
            'layanan' => htmlspecialchars($this->input->post('layanan', true)),
            'tahun_mulai' => htmlspecialchars($this->input->post('tahun_mulai', true)),
            'tahun_selesai' => htmlspecialchars($this->input->post('tahun_selesai', true)),
            'penyelenggara' => htmlspecialchars($this->input->post('penyelenggara', true)),
            'aktif' => 1,
            'date_created' => date('Y-m-d H:i:s')
        );


        $result = $this->mk->storeKesejahteraan($data);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('kesejahteraan');
        } else {
            $this->session->set_flashdata('gagal', 'Disimpan');
            redirect('createKesejahteraan');
        }
    }


    public function hapusKesejahteraan($id_kesejahteraan)
    {
        $id_kesejahteraan = $this->uri->segment(3);
        $aktif = 0;

        $this->db->set('aktif', $aktif);
        $this->db->where('id_kesejahteraan', $id_kesejahteraan);
        $this->db->update('occ_kesejahteraan');

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('kesejahteraan');
    }

    public function detailKesejahteraan()
    {
        $id_kesejahteraan = $this->uri->segment(3);
        $data = array(
            'title' => ' Detail Kesejahteraan',
            'active_menu_reward' => 'menu-open',
            'active_menu_rw' => 'active',
            'active_menu_kesejahteraan' => 'active',
            'd' => $this->mk->detailKesejahteraan($id_kesejahteraan),

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/reward/kesejahteraan/d_kesejahteraan', $data);
        $this->load->view('layouts/footer');
    }

    public function editKesejahteraan()
    {
        $id_kesejahteraan = $this->uri->segment(3);
        $data = array(
            'title' => ' Update Kesejahteraan',
            'active_menu_reward' => 'menu-open',
            'active_menu_rw' => 'active',
            'active_menu_kesejahteraan' => 'active',
            'd' => $this->mk->detailKesejahteraan($id_kesejahteraan),

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('dosen/reward/kesejahteraan/u_kesejahteraan', $data);
        $this->load->view('layouts/footer');
    }

    public function editKesejahteraanGo()
    {

        $data = array(
            'title' => ' Update Kesejahteraan',
            'active_menu_reward' => 'menu-open',
            'active_menu_rw' => 'active',
            'active_menu_kesejahteraan' => 'active'
        );
        $id_kesejahteraan = htmlspecialchars($this->input->post('id_kesejahteraan', true));

        $data = array(
            'jenis_kesejahteraan' => htmlspecialchars($this->input->post('jenis_kesejahteraan', true)),
            'layanan' => htmlspecialchars($this->input->post('layanan', true)),
            'tahun_mulai' => htmlspecialchars($this->input->post('tahun_mulai', true)),
            'tahun_selesai' => htmlspecialchars($this->input->post('tahun_selesai', true)),
            'penyelenggara' => htmlspecialchars($this->input->post('penyelenggara', true)),
            'aktif' => 1
        );


        $result = $this->mk->updateKesejahteraan($id_kesejahteraan, $data);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Diubah');
            redirect('kesejahteraan');
        } else {
            $this->session->set_flashdata('gagal', 'Diubah');
            redirect('kesejahteraan/editKesejahteraan/' . $id_kesejahteraan);
        }
    }
}
