<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Laporan extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('login'));
        }
        $this->load->model('m_dosen/ModelDosen', 'md');
        $this->load->model('ModelDashboard', 'mds');
        $this->load->model('m_cuti/Model_cuti', 'mc');
    }

    public function cuti()
    {
        $data = array(
            'title' => 'Laporan Cuti',
            'active_menu_laporan' => 'menu-open',
            'active_menu_lpr' => 'active',
            'active_menu_lap_cuti' => 'active',
            'dp' => $this->md->getDetailProfil(),
            'cutiku' => $this->mc->getView(),
            'hak' => $this->mc->cutiku()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('laporan/v_cuti', $data);
        $this->load->view('layouts/footer');
    }

    public function cutiGo()
    {
        $date_start = htmlspecialchars($this->input->post('date_start', true));
        $date_end = htmlspecialchars($this->input->post('date_end', true));

        $data = array(
            'title' => 'Laporan Cuti',
            'active_menu_laporan' => 'menu-open',
            'active_menu_lpr' => 'active',
            'active_menu_lap_cuti' => 'active',
            'dp' => $this->md->getDetailProfil(),
            'lap_cuti' => $this->mc->getLaporanCuti($date_start, $date_end),
            'tgl_mulai' => $date_start,
            'tgl_selesai' => $date_end,

        );
        $this->load->view('laporan/p_cuti', $data);
    }


    public function sakit()
    {
        $data = array(
            'title' => 'Laporan Izin Sakit',
            'active_menu_laporan' => 'menu-open',
            'active_menu_lpr' => 'active',
            'active_menu_lap_sakit' => 'active',
            'dp' => $this->md->getDetailProfil(),
            'cutiku' => $this->mc->getView(),
            'hak' => $this->mc->cutiku()

        );
        $this->load->view('layouts/header', $data);
        $this->load->view('laporan/v_sakit', $data);
        $this->load->view('layouts/footer');
    }

    public function sakitGo()
    {
        $date_start = htmlspecialchars($this->input->post('date_start', true));
        $date_end = htmlspecialchars($this->input->post('date_end', true));

        $data = array(
            'title' => 'Laporan Cuti',
            'active_menu_laporan' => 'menu-open',
            'active_menu_lpr' => 'active',
            'active_menu_lap_cuti' => 'active',
            'dp' => $this->md->getDetailProfil(),
            'lap_cuti' => $this->mc->getLaporanSakit($date_start, $date_end),
            'tgl_mulai' => $date_start,
            'tgl_selesai' => $date_end,

        );
        $this->load->view('laporan/p_sakit', $data);
    }
}
