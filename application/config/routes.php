<?php
defined('BASEPATH') or exit('No direct script access allowed');
$route['default_controller'] = 'Login/index';
$route['translate_uri_dashes'] = FALSE;

//===================================================

//------------------AUTH----------------------------

//===================================================

$route['login'] = 'Login/index';

$route['forgotPassword'] = 'Login/forgotPassword';

$route['login/resetpassword'] = 'Login/resetPassword';

$route['passwordBaru'] = 'Login/changePassword';

$route['login/registration'] = 'Login/registration';

$route['logout'] = 'Login/logout';

//===================================================

//------------------TENDIK----------------------------

//===================================================

$route['tendik'] = 'C_tendik/Tendik/index';

//===================================================

//------------------PENGATURAN-----------------------

$route['ubahPassword'] = 'Pengaturan/ubahPassword';
//===================================================
//------------------DOSEN----------------------------
//===================================================
$route['dosen'] = 'C_dosen/Dosen/index';
//===================================================
//------------------OPERATOR----------------------------
//===================================================
$route['operator'] = 'C_operator/Operator/index';
//===================================================
//------------------ADMINKEU----------------------------
//===================================================
$route['adminkeu'] = 'C_adminkeu/Adminkeu/index';
//===================================================
//------------------LKK------------------------------
//===================================================
$route['verif_lkk'] = 'C_lkk/Lkk_verif/index';
//===================================================

// Data Pribadi

$route['dpribadi'] = 'C_data_pribadi/DataPribadi/index';

// Profil

$route['uprofil'] = 'C_data_pribadi/Profil/index';

$route['profil/update'] = 'C_data_pribadi/Profil/updateProfil';

$route['profil/uploadFoto'] = 'C_data_pribadi/Profil/uploadFoto';

$route['profil/detail/(:any)'] = 'C_data_pribadi/Profil/detailRiwayatProfil/(:any)';

// Kependudukan

$route['ukependudukan'] = 'C_data_pribadi/Kependudukan/index';

$route['kependudukan/update'] = 'C_data_pribadi/Kependudukan/updateKependudukan';

$route['kependudukan/detail/(:any)'] = 'C_data_pribadi/Kependudukan/detailRiwayatKependudukan/(:any)';

// Keluarga

$route['ukeluarga'] = 'C_data_pribadi/Keluarga/index';

$route['keluarga/update'] = 'C_data_pribadi/Keluarga/updateKeluarga';

$route['keluarga/detail/(:any)'] = 'C_data_pribadi/Keluarga/detailRiwayatKeluarga/(:any)';

// Muhammadiyah

$route['umuhammadiyah'] = 'C_data_pribadi/Muhammadiyah/index';

$route['muhammadiyah/update'] = 'C_data_pribadi/Muhammadiyah/updateMuhammadiyah';

$route['muhammadiyah/detail/(:any)'] = 'C_data_pribadi/Muhammadiyah/detailRiwayatMuhammadiyah/(:any)';

// Bidang Ilmu

$route['ubidilmu'] = 'C_data_pribadi/Bidilmu/index';

$route['bidilmu/update'] = 'C_data_pribadi/Bidilmu/updateBidilmu';

// Alamat dan Kontak

$route['ualamat'] = 'C_data_pribadi/Alamat/index';

$route['alamat/update'] = 'C_data_pribadi/Alamat/updateAlamat';

$route['alamat/detail/(:any)'] = 'C_data_pribadi/Alamat/detailRiwayatAlamat/(:any)';

// Kepegawaian

$route['ukepegawaian'] = 'C_data_pribadi/Kepegawaian/index';

$route['kepegawaian/update'] = 'C_data_pribadi/Kepegawaian/updateKepegawaian';

$route['kepegawaian/detail/(:any)'] = 'C_data_pribadi/Kepegawaian/detailRiwayatKepegawaian/(:any)';

//Lain Lain

$route['ulain'] = 'C_data_pribadi/Lain/index';

$route['lain/update'] = 'C_data_pribadi/Lain/updateLain';

$route['lain/detail/(:any)'] = 'C_data_pribadi/Lain/detailRiwayatLain/(:any)';

//PROFIL

$route['dosen/profil'] = 'C_dosen/Profil/index';

// IMPASSING

$route['impassing'] = 'C_dosen/Impassing/index';

$route['createImpassing'] = 'C_dosen/Impassing/createImpassing';

$route['saveImpassing'] = 'C_dosen/Impassing/saveImpassing';

$route['updateImpassing'] = 'C_dosen/Impassing/updateImpassing';

$route['detailImpassing/(:any)'] = 'C_dosen/Impassing/detailImpassing/(:any)';

$route['editImpassing/(:any)'] = 'C_dosen/Impassing/editImpassing/(:any)';

$route['hapusImpassing/(:any)'] = 'C_dosen/Impassing/hapusImpassing/(:any)';

// JABATAN FUNGSIONAL

$route['jafung'] = 'C_dosen/Jafung/index';

$route['createJafung'] = 'C_dosen/Jafung/createJafung';

$route['saveJafung'] = 'C_dosen/Jafung/saveJafung';

$route['detailJafung/(:any)'] = 'C_dosen/Jafung/detailJafung/(:any)';

$route['hapusJafung/(:any)'] = 'C_dosen/Jafung/hapusJafung/(:any)';

$route['riwayatJafung'] = 'C_dosen/Jafung/riwayatJafung';

$route['detailJafungRiwayat/(:any)'] = 'C_dosen/Jafung/detailJafungRiwayat/(:any)';

// PENEMPATAN



$route['penempatan'] = 'C_dosen/Penempatan/index';

$route['detailPenempatan/(:any)'] = 'C_dosen/Penempatan/detailPenempatan/(:any)';

// KUALIFIKASI

// PENDIDIKAN FORMAL

$route['pendidikan_formal'] = 'C_kualifikasi/Penfor/index';

$route['createPenfor'] = 'C_kualifikasi/Penfor/createPenfor';

$route['savePenfor'] = 'C_kualifikasi/Penfor/savePenfor';

$route['detailPenfor/(:any)'] = 'C_kualifikasi/Penfor/detailPenfor/(:any)';

$route['hapusPenfor/(:any)'] = 'C_kualifikasi/Penfor/hapusPenfor/(:any)';

$route['riwayatPenfor'] = 'C_kualifikasi/Penfor/riwayatPenfor';

$route['detailPenforRiwayat/(:any)'] = 'C_kualifikasi/Penfor/detailPenforRiwayat/(:any)';

// DIKTLAT

$route['diklat'] = 'C_kualifikasi/Diklat/index';

$route['createDiklat'] = 'C_kualifikasi/Diklat/createDiklat';

$route['createDiklatNon'] = 'C_kualifikasi/Diklat/createDiklatNon';

$route['saveDiklat'] = 'C_kualifikasi/Diklat/saveDiklat';

$route['saveDiklatNon'] = 'C_kualifikasi/Diklat/saveDiklatNon';

$route['detailDiklat/(:any)'] = 'C_kualifikasi/Diklat/detailDiklat/(:any)';

$route['hapusDiklat/(:any)'] = 'C_kualifikasi/Diklat/hapusDiklat/(:any)';

$route['riwayatDiklat'] = 'C_kualifikasi/Diklat/riwayatDiklat';

$route['detailDiklatRiwayat/(:any)'] = 'C_kualifikasi/Diklat/detailDiklatRiwayat/(:any)';

//RIWAYAT PEKERJAAN

$route['riwpek'] = 'C_kualifikasi/Riwpek/index';

$route['createRiwpek'] = 'C_kualifikasi/Riwpek/createRiwpek';

$route['saveRiwpek'] = 'C_kualifikasi/Riwpek/saveRiwpek';

$route['detailRiwpek/(:any)'] = 'C_kualifikasi/Riwpek/detailRiwpek/(:any)';

$route['editRiwpek/(:any)'] = 'C_kualifikasi/Riwpek/editRiwpek/(:any)';

$route['hapusRiwpek/(:any)'] = 'C_kualifikasi/Riwpek/hapusRiwpek/(:any)';

$route['updateRiwpek'] = 'C_kualifikasi/Riwpek/updateRiwpek';

//  KOMPETENSI

// SERTIFIKASI

$route['sertifikasi'] = 'C_kompetensi/Sertifikasi/index';

$route['createSertifikasi'] = 'C_kompetensi/Sertifikasi/createSertifikasi';

$route['saveSertifikasi'] = 'C_kompetensi/Sertifikasi/saveSertifikasi';

$route['detailSertifikasi/(:any)'] = 'C_kompetensi/Sertifikasi/detailSertifikasi/(:any)';

$route['hapusSertifikasi/(:any)'] = 'C_kompetensi/Sertifikasi/hapusSertifikasi/(:any)';

$route['createSertifikasiProfesi'] = 'C_kompetensi/Sertifikasi/createSertifikasiProfesi';

$route['saveSertifikasiProfesi'] = 'C_kompetensi/Sertifikasi/saveSertifikasiProfesi';

$route['riwayatSertifikasi'] = 'C_kompetensi/Sertifikasi/riwayatSertifikasi';

$route['detailRiwSertifikasi/(:any)'] = 'C_kompetensi/Sertifikasi/detailRiwSertifikasi/(:any)';

//TES

$route['tes'] = 'C_kompetensi/Tes/index';

$route['createTes'] = 'C_kompetensi/Tes/createTes';

$route['saveTes'] = 'C_kompetensi/Tes/saveTes';

$route['detailTes/(:any)'] = 'C_kompetensi/Tes/detailTes/(:any)';

$route['hapusTes/(:any)'] = 'C_kompetensi/Tes/hapusTes/(:any)';

$route['riwayatTes'] = 'C_kompetensi/Tes/riwayatTes';

$route['detailRiwTes/(:any)'] = 'C_kompetensi/Tes/detailRiwTes/(:any)';

// DOSEN - KUALIFIKASI

// DOSEN - PERMOHONAN INSENTIF

//(DOSEN)INSENTIF ARTIKEL DIJURNAL

$route['aji'] = 'C_publis/Aji/index';

$route['createAji'] = 'C_publis/Aji/createAji';

// $route['createAji'] = 'Tutup/index';

$route['aji/detail/(:any)'] = 'C_publis/Aji/detailAji/(:any)';

$route['aji/history/(:any)'] = 'C_publis/Aji/history/(:any)';

$route['aji/perbaikan/(:any)'] = 'C_publis/Aji/perbaikan/(:any)';

// $route['aji/perbaikan/(:any)'] = 'Tutup/index';

$route['aji/perbaikanGo'] = 'C_publis/Aji/perbaikanGo';

//(DOSEN)INSENTIF HKI

$route['hki'] = 'C_publis/Hki/index';

$route['createHki'] = 'C_publis/Hki/createHki';

// $route['createHki'] = 'Tutup/index';

$route['hki/detail/(:any)'] = 'C_publis/Hki/detailHki/(:any)';

$route['hki/history/(:any)'] = 'C_publis/Hki/history/(:any)';

$route['hki/perbaikan/(:any)'] = 'C_publis/Hki/perbaikan/(:any)';

// $route['hki/perbaikan/(:any)'] = 'Tutup/index';

$route['hki/perbaikanGo'] = 'C_publis/Hki/perbaikanGo';

//(DOSEN)INSENTIF PENGELOLA JURNAL

$route['pj'] = 'C_publis/Pj/index';

$route['createPj'] = 'C_publis/Pj/createPj';

$route['pj/detail/(:any)'] = 'C_publis/Pj/detailPj/(:any)';

$route['pj/history/(:any)'] = 'C_publis/Pj/history/(:any)';

$route['pj/perbaikan/(:any)'] = 'C_publis/Pj/perbaikan/(:any)';

$route['pj/perbaikanGo'] = 'C_publis/Pj/perbaikanGo';

$route['pj/cetak/(:any)'] = 'C_publis/Pj/cetak/(:any)';

//(DOSEN)INSENTIF BUKU AJAR DLL

$route['iba'] = 'C_publis/Iba/index';

$route['createIba'] = 'C_publis/Iba/createIba';

// $route['createIba'] = 'Tutup/index';

$route['iba/detail/(:any)'] = 'C_publis/Iba/detailIba/(:any)';

$route['iba/history/(:any)'] = 'C_publis/Iba/history/(:any)';

$route['iba/perbaikan/(:any)'] = 'C_publis/Iba/perbaikan/(:any)';

// $route['iba/perbaikan/(:any)'] =  'Tutup/index';

$route['iba/perbaikanGo'] = 'C_publis/Iba/perbaikanGo';

//(DOSEN)INSENTIF KEYNOTE SPEAKER

$route['iks'] = 'C_publis/Iks/index';

$route['createIks'] = 'C_publis/Iks/createIks';

// $route['createIks'] =  'Tutup/index';

$route['iks/detail/(:any)'] = 'C_publis/Iks/detailIks/(:any)';

$route['iks/history/(:any)'] = 'C_publis/Iks/history/(:any)';

$route['iks/perbaikan/(:any)'] = 'C_publis/Iks/perbaikan/(:any)';

// $route['iks/perbaikan/(:any)'] = 'Tutup/index';

$route['iks/perbaikanGo'] = 'C_publis/Iks/perbaikanGo';

//(DOSEN)INSENTIF HKI PROSIDDING

$route['ifip'] = 'C_publis/Ifip/index';

$route['createIfip'] = 'C_publis/Ifip/createIfip';

// $route['createIfip'] = 'Tutup/index';

$route['ifip/detail/(:any)'] = 'C_publis/Ifip/detailIfip/(:any)';

$route['ifip/history/(:any)'] = 'C_publis/Ifip/history/(:any)';

$route['ifip/perbaikan/(:any)'] = 'C_publis/Ifip/perbaikan/(:any)';

// $route['ifip/perbaikan/(:any)'] = 'Tutup/index';

$route['ifip/perbaikanGo'] = 'C_publis/Ifip/perbaikanGo';

//(DOSEN)INSENTIF JURNAL DARI SKRIPSI

$route['uipjs'] = 'C_publis/Uipjs/index';

$route['createUipjs'] = 'C_publis/Uipjs/createUipjs';

// $route['createUipjs'] = 'Tutup/index';

$route['uipjs/detail/(:any)'] = 'C_publis/Uipjs/detailUipjs/(:any)';

$route['uipjs/history/(:any)'] = 'C_publis/Uipjs/history/(:any)';

$route['uipjs/perbaikan/(:any)'] = 'C_publis/Uipjs/perbaikan/(:any)';

// $route['uipjs/perbaikan/(:any)'] = 'Tutup/index';

$route['uipjs/perbaikanGo'] = 'C_publis/Uipjs/perbaikanGo';

//(DOSEN)BANTUAN BIAYA PUBLIKASI

$route['bbp'] = 'C_publis/Bbp/index';

$route['createBbp'] = 'C_publis/Bbp/createBbp';

$route['bbp/detail/(:any)'] = 'C_publis/Bbp/detailBbp/(:any)';

$route['bbp/history/(:any)'] = 'C_publis/Bbp/history/(:any)';

$route['bbp/perbaikan/(:any)'] = 'C_publis/Bbp/perbaikan/(:any)';

$route['bbp/cetak/(:any)'] = 'C_publis/Bbp/cetak/(:any)';

$route['bbp/perbaikanGo'] = 'C_publis/Bbp/perbaikanGo';

//(DOSEN)BANTUAN BIAYA PROSIDDING

$route['bfio'] = 'C_publis/Bfio/index';

$route['createBfio'] = 'C_publis/Bfio/createBfio';

$route['bfio/detail/(:any)'] = 'C_publis/Bfio/detailBfio/(:any)';

$route['bfio/history/(:any)'] = 'C_publis/Bfio/history/(:any)';

$route['bfio/perbaikan/(:any)'] = 'C_publis/Bfio/perbaikan/(:any)';

$route['bfio/perbaikanGo'] = 'C_publis/Bfio/perbaikanGo';

$route['bfio/cetak/(:any)'] = 'C_publis/Bfio/cetak/(:any)';

//(DOSEN)BANTUAN BIAYA PUBLIKASI JURNAL DARI SKRIPSI

$route['ubpjs'] = 'C_publis/Ubpjs/index';

$route['createUbpjs'] = 'C_publis/Ubpjs/createUbpjs';

$route['ubpjs/detail/(:any)'] = 'C_publis/Ubpjs/detailUbpjs/(:any)';

$route['ubpjs/history/(:any)'] = 'C_publis/Ubpjs/history/(:any)';

$route['ubpjs/perbaikan/(:any)'] = 'C_publis/Ubpjs/perbaikan/(:any)';

$route['ubpjs/perbaikanGo'] = 'C_publis/Ubpjs/perbaikanGo';

$route['ubpjs/cetak/(:any)'] = 'C_publis/Ubpjs/cetak/(:any)';

//(DOSEN)BANTUAN HLKI

$route['bhki'] = 'C_publis/Bhki/index';

$route['createbhki'] = 'C_publis/Bhki/createbhki';

$route['bhki/detail/(:any)'] = 'C_publis/Bhki/detailbhki/(:any)';

$route['bhki/history/(:any)'] = 'C_publis/Bhki/history/(:any)';

$route['bhki/perbaikan/(:any)'] = 'C_publis/Bhki/perbaikan/(:any)';

$route['bhki/perbaikanGo'] = 'C_publis/Bhki/perbaikanGo';

$route['bhki/cetak/(:any)'] = 'C_publis/Bhki/cetak/(:any)';

// Pelaksanaan Pendidikan 

// Pengajaran

$route['dosenPengajaran'] = 'C_pel_pendidikan/Pengajaran/index';

$route['createPengajaran'] = 'C_pel_pendidikan/Pengajaran/createPengajaran';

// Bimbingan Mahasiswa

$route['dosenBm'] = 'C_pel_pendidikan/Bim_mahasiswa/index';

// Pengujian Mahasiswa

$route['dosenPm'] = 'C_pel_pendidikan/Peng_mahasiswa/index';

// Pembinaan Mahasiswa

$route['dosenPm2'] = 'C_pel_pendidikan/Pem_mahasiswa/index';

// Bahan Ajar 

$route['bahanAjar'] = 'C_pel_pendidikan/Bahan_ajar/index';

$route['ba/detail/(:any)'] = 'C_pel_pendidikan/Bahan_ajar/detailIba/(:any)';

// Visiting Scient

$route['dosenVs'] = 'C_pel_pendidikan/Vs/index';

$route['createVs'] = 'C_pel_pendidikan/Vs/createVs';

$route['vs/detailVs/(:any)'] = 'C_pel_pendidikan/Vs/detailVs/(:any)';

$route['vs/hapusVs/(:any)'] = 'C_pel_pendidikan/Vs/hapusVs/(:any)';

$route['vs/perbaikanVs/(:any)'] = 'C_pel_pendidikan/Vs/perbaikanVs/(:any)';

$route['vs/perbaikanVsGo'] = 'C_pel_pendidikan/Vs/perbaikanVsGo';

//Pembimbingan Dosen

$route['pemdos'] = 'C_pel_pendidikan/Pemdos/index';

$route['createPemdos'] = 'C_pel_pendidikan/Pemdos/createPemdos';

$route['pemdos/detailPemdos/(:any)'] = 'C_pel_pendidikan/Pemdos/detailPemdos/(:any)';

$route['pemdos/hapusPemdos/(:any)'] = 'C_pel_pendidikan/Pemdos/hapusPemdos/(:any)';

$route['pemdos/perbaikanPemdos/(:any)'] = 'C_pel_pendidikan/Pemdos/perbaikanPemdos/(:any)';

$route['pemdos/perbaikanPemdosGo'] = 'C_pel_pendidikan/Pemdos/perbaikanPemdosGo';

//Tugas Tambahan

$route['tugtam'] = 'C_pel_pendidikan/Tugtam/index';

$route['createTugtam'] = 'C_pel_pendidikan/Tugtam/createTugtam';

$route['tugtam/detailTugtam/(:any)'] = 'C_pel_pendidikan/Tugtam/detailTugtam/(:any)';

$route['tugtam/hapusTugtam/(:any)'] = 'C_pel_pendidikan/Tugtam/hapusTugtam/(:any)';

$route['tugtam/perbaikanTugtam/(:any)'] = 'C_pel_pendidikan/Tugtam/perbaikanTugtam/(:any)';

$route['tugtam/perbaikanTugtamGo'] = 'C_pel_pendidikan/Tugtam/perbaikanTugtamGo';

// Detasering

$route['detasering'] = 'C_pel_pendidikan/Detasering/index';

$route['createDetasering'] = 'C_pel_pendidikan/Detasering/createDetasering';

$route['detasering/detailDetasering/(:any)'] = 'C_pel_pendidikan/Detasering/detailDetasering/(:any)';

$route['detasering/perbaikanDetasering/(:any)'] = 'C_pel_pendidikan/Detasering/perbaikanDetasering/(:any)';

$route['detasering/perbaikanDetaseringGo'] = 'C_pel_pendidikan/Detasering/perbaikanDetaseringGo';

$route['detasering/hapusDetasering/(:any)'] = 'C_pel_pendidikan/Detasering/hapusDetasering/(:any)';

// Riwayat Orasi Ilmiah

$route['oril'] = 'C_pel_pendidikan/Oril/index';

$route['createOril'] = 'C_pel_pendidikan/Oril/createOril';

$route['oril/detailOril/(:any)'] = 'C_pel_pendidikan/Oril/detailOril/(:any)';

$route['oril/perbaikanOril/(:any)'] = 'C_pel_pendidikan/Oril/perbaikanOril/(:any)';

$route['oril/perbaikanOrilGo'] = 'C_pel_pendidikan/Oril/perbaikanOrilGo';

$route['oril/hapusOril/(:any)'] = 'C_pel_pendidikan/Oril/hapusOril/(:any)';

//PENUNJANG

//ANGGOTA PROFESI

$route['aprof'] = 'C_penunjang/Aprof/index';

$route['createAprof'] = 'C_penunjang/Aprof/createAprof';

$route['saveAprof'] = 'C_penunjang/Aprof/saveAprof';

$route['aprof/detailAprof/(:any)'] = 'C_penunjang/Aprof/detailAprof/(:any)';

$route['aprof/hapusAprof/(:any)'] = 'C_penunjang/Aprof/hapusAprof/(:any)';

$route['aprof/editAprof/(:any)'] = 'C_penunjang/Aprof/editAprof/(:any)';

$route['aprof/editAprofGo'] = 'C_penunjang/Aprof/editAprofGo';

//PENGHARGAAN

$route['penghargaan'] = 'C_penunjang/Penghargaan/index';

$route['createPenghargaan'] = 'C_penunjang/Penghargaan/createPenghargaan';

$route['savePenghargaan'] = 'C_penunjang/Penghargaan/savePenghargaan';

$route['penghargaan/detailPenghargaan/(:any)'] = 'C_penunjang/Penghargaan/detailPenghargaan/(:any)';

$route['penghargaan/hapusPenghargaan/(:any)'] = 'C_penunjang/Penghargaan/hapusPenghargaan/(:any)';

$route['penghargaan/editPenghargaan/(:any)'] = 'C_penunjang/Penghargaan/editPenghargaan/(:any)';

$route['penghargaan/editPenghargaanGo'] = 'C_penunjang/Penghargaan/editPenghargaanGo';

//PENUNJANG LAIN

$route['penlain'] = 'C_penunjang/Penlain/index';

$route['createPenlain'] = 'C_penunjang/Penlain/createPenlain';

$route['savePenlain'] = 'C_penunjang/Penlain/savePenlain';

$route['penlain/detailPenlain/(:any)'] = 'C_penunjang/Penlain/detailPenlain/(:any)';

$route['penlain/hapusPenlain/(:any)'] = 'C_penunjang/Penlain/hapusPenlain/(:any)';

$route['penlain/editPenlain/(:any)'] = 'C_penunjang/Penlain/editPenlain/(:any)';

$route['penlain/editPenlainGo'] = 'C_penunjang/Penlain/editPenlainGo';

//Reward

//Beasiswa

$route['beasiswa'] = 'C_reward/Beasiswa/index';

$route['createBeasiswa'] = 'C_reward/Beasiswa/createBeasiswa';

$route['saveBeasiswa'] = 'C_reward/Beasiswa/saveBeasiswa';

$route['beasiswa/detailBeasiswa/(:any)'] = 'C_reward/Beasiswa/detailBeasiswa/(:any)';

$route['beasiswa/hapusBeasiswa/(:any)'] = 'C_reward/Beasiswa/hapusBeasiswa/(:any)';

$route['beasiswa/editBeasiswa/(:any)'] = 'C_reward/Beasiswa/editBeasiswa/(:any)';

$route['beasiswa/editBeasiswaGo'] = 'C_reward/Beasiswa/editBeasiswaGo';

//Kesejahteraan

$route['kesejahteraan'] = 'C_reward/Kesejahteraan/index';

$route['createKesejahteraan'] = 'C_reward/Kesejahteraan/createKesejahteraan';

$route['saveKesejahteraan'] = 'C_reward/Kesejahteraan/saveKesejahteraan';

$route['kesejahteraan/detailKesejahteraan/(:any)'] = 'C_reward/Kesejahteraan/detailKesejahteraan/(:any)';

$route['kesejahteraan/hapusKesejahteraan/(:any)'] = 'C_reward/Kesejahteraan/hapusKesejahteraan/(:any)';

$route['kesejahteraan/editKesejahteraan/(:any)'] = 'C_reward/Kesejahteraan/editKesejahteraan/(:any)';

$route['kesejahteraan/editKesejahteraanGo'] = 'C_reward/Kesejahteraan/editKesejahteraanGo';

//Tunjangan

$route['tunjangan'] = 'C_reward/Tunjangan/index';

$route['createTunjangan'] = 'C_reward/Tunjangan/createTunjangan';

$route['saveTunjangan'] = 'C_reward/Tunjangan/saveTunjangan';

$route['tunjangan/detailTunjangan/(:any)'] = 'C_reward/Tunjangan/detailTunjangan/(:any)';

$route['tunjangan/hapusTunjangan/(:any)'] = 'C_reward/Tunjangan/hapusTunjangan/(:any)';

$route['tunjangan/editTunjangan/(:any)'] = 'C_reward/Tunjangan/editTunjangan/(:any)';

$route['tunjangan/editTunjanganGo'] = 'C_reward/Tunjangan/editTunjanganGo';

//AIK

$route['aik'] = 'C_aik/Aik/index';

$route['createAik'] = 'C_aik/Aik/createAik';

$route['saveAik'] = 'C_aik/Aik/saveAik';

$route['aik/detailAik/(:any)'] = 'C_aik/Aik/detailAik/(:any)';

$route['aik/hapusAik/(:any)'] = 'C_aik/Aik/hapusAik/(:any)';

$route['aik/editAik/(:any)'] = 'C_aik/Aik/editAik/(:any)';

$route['aik/editAikGo'] = 'C_aik/Aik/editAikGo';

// PELAKSANAAN PENELITIAN

// PENELITIAN

$route['penelitian'] = 'C_pel_penelitian/Penelitian/index';

$route['createPenelitian'] = 'C_pel_penelitian/Penelitian/createPenelitian';

$route['savePenelitian'] = 'C_pel_penelitian/Penelitian/savePenelitian';

$route['penelitian/detailPenelitian/(:any)'] = 'C_pel_penelitian/Penelitian/detailPenelitian/(:any)';

$route['penelitian/hapusPenelitian/(:any)'] = 'C_pel_penelitian/Penelitian/hapusPenelitian/(:any)';

$route['penelitian/perbaikanPenelitian/(:any)'] = 'C_pel_penelitian/Penelitian/perbaikanPenelitian/(:any)';

$route['penelitian/perbaikanPenelitianGo'] = 'C_pel_penelitian/Penelitian/perbaikanPenelitianGo';

// PUBLIKASI KARYA

$route['pubkar'] = 'C_pel_penelitian/Pubkar/index';

$route['pubkar/detailPubkar/(:any)'] = 'C_pelaks_Pubkar/Pubkar/detailPubkar/(:any)';

// PATEN/HKI

$route['phki'] = 'C_pel_penelitian/Phki/index';

$route['phki/detailPhki/(:any)'] = 'C_pel_penelitian/Phki/detailPhki/(:any)';

//PENGABDIAN

$route['pengabdian'] = 'C_pengabdian/Pengabdian/index';

$route['createPengabdian'] = 'C_pengabdian/Pengabdian/createPengabdian';

$route['detailPengabdian/(:any)'] = 'C_pengabdian/Pengabdian/detailPengabdian/(:any)';

$route['editPengabdian/(:any)'] = 'C_pengabdian/Pengabdian/editPengabdian/(:any)';

$route['updatePengabdian'] = 'C_pengabdian/Pengabdian/updatePengabdian';

$route['hapusPengabdian/(:any)'] = 'C_pengabdian/Pengabdian/hapusPengabdian/(:any)';

//PENGELOLA JURNAL

$route['pengelola_jurnal'] = 'C_pengabdian/Pengelola_jurnal/index';

$route['createPengelola_jurnal'] = 'C_pengabdian/Pengelola_jurnal/createPengelola_jurnal';

$route['detailPengelola_jurnal/(:any)'] = 'C_pengabdian/Pengelola_jurnal/detailPengelola_jurnal/(:any)';

$route['editPengelola_jurnal/(:any)'] = 'C_pengabdian/Pengelola_jurnal/editPengelola_jurnal/(:any)';

$route['hapusPengelola_jurnal/(:any)'] = 'C_pengabdian/Pengelola_jurnal/hapusPengelola_jurnal/(:any)';

$route['updatePengelola_jurnal'] = 'C_pengabdian/Pengelola_jurnal/updatePengelola_jurnal';

//JABATAN STRUKTURAL

$route['jabatan'] = 'C_pengabdian/Jabatan/index';

$route['createJabatan'] = 'C_pengabdian/Jabatan/createJabatan';

$route['detailJabatan/(:any)'] = 'C_pengabdian/Jabatan/detailJabatan/(:any)';

$route['editJabatan/(:any)'] = 'C_pengabdian/Jabatan/editJabatan/(:any)';

$route['updateJabatan'] = 'C_pengabdian/Jabatan/updateJabatan';

$route['hapusJabatan/(:any)'] = 'C_pengabdian/Jabatan/hapusJabatan/(:any)';

//PEMBICARA

$route['pembicara'] = 'C_pengabdian/Pembicara/index';

$route['detailPembicara/(:any)'] = 'C_pengabdian/Pembicara/detailPembicara/(:any)';

//Dashboard

$route['track'] = 'Track_pdd/index';



// PERBAIKAN DATA



//===================================================

//------------VALIDATOR PIMPINAN 1-------------------

//===================================================



$route['pimpinan1'] = 'C_pimpinan1/Dashboard/index';

//------------SUB AJI-------------------

$route['pimpinan1/vaji'] = 'C_pimpinan1/Aji/index';

$route['pimpinan1/vaji/fee/(:any)'] = 'C_pimpinan1/Aji/fee/(:any)';

$route['pimpinan1/vaji/feeGo'] = 'C_pimpinan1/Aji/feeGo';

$route['pimpinan1/vaji/tolak/(:any)'] = 'C_pimpinan1/Aji/tolak/(:any)';

$route['pimpinan1/vaji/tolakGo'] = 'C_pimpinan1/Aji/tolakGo';

//------------SUB BBP-------------------

$route['pimpinan1/vbbp'] = 'C_pimpinan1/Bbp/index';

$route['pimpinan1/vbbp/fee/(:any)'] = 'C_pimpinan1/Bbp/fee/(:any)';

$route['pimpinan1/vbbp/feeGo'] = 'C_pimpinan1/Bbp/feeGo';

$route['pimpinan1/vbbp/tolak/(:any)'] = 'C_pimpinan1/Bbp/tolak/(:any)';

$route['pimpinan1/vbbp/tolakGo'] = 'C_pimpinan1/Bbp/tolakGo';

//------------SUB BFIO-------------------

$route['pimpinan1/vbfio'] = 'C_pimpinan1/Bfio/index';

$route['pimpinan1/vbfio/fee/(:any)'] = 'C_pimpinan1/Bfio/fee/(:any)';

$route['pimpinan1/vbfio/feeGo'] = 'C_pimpinan1/Bfio/feeGo';

$route['pimpinan1/vbfio/tolak/(:any)'] = 'C_pimpinan1/Bfio/tolak/(:any)';

$route['pimpinan1/vbfio/tolakGo'] = 'C_pimpinan1/Bfio/tolakGo';

//------------SUB HKI-------------------

$route['pimpinan1/vhki'] = 'C_pimpinan1/Hki/index';

$route['pimpinan1/vhki/fee/(:any)'] = 'C_pimpinan1/Hki/fee/(:any)';

$route['pimpinan1/vhki/feeGo'] = 'C_pimpinan1/Hki/feeGo';

$route['pimpinan1/vhki/tolak/(:any)'] = 'C_pimpinan1/Hki/tolak/(:any)';

$route['pimpinan1/vhki/tolakGo'] = 'C_pimpinan1/Hki/tolakGo';

//------------SUB IBA-------------------

$route['pimpinan1/viba'] = 'C_pimpinan1/Iba/index';

$route['pimpinan1/viba/fee/(:any)'] = 'C_pimpinan1/Iba/fee/(:any)';

$route['pimpinan1/viba/feeGo'] = 'C_pimpinan1/Iba/feeGo';

$route['pimpinan1/viba/tolak/(:any)'] = 'C_pimpinan1/Iba/tolak/(:any)';

$route['pimpinan1/viba/tolakGo'] = 'C_pimpinan1/Iba/tolakGo';

//------------SUB IFIP-------------------

$route['pimpinan1/vifip'] = 'C_pimpinan1/Ifip/index';

$route['pimpinan1/vifip/fee/(:any)'] = 'C_pimpinan1/Ifip/fee/(:any)';

$route['pimpinan1/vifip/feeGo'] = 'C_pimpinan1/Ifip/feeGo';

$route['pimpinan1/vifip/tolak/(:any)'] = 'C_pimpinan1/Ifip/tolak/(:any)';

$route['pimpinan1/vifip/tolakGo'] = 'C_pimpinan1/Ifip/tolakGo';

//------------SUB IKS-------------------

$route['pimpinan1/viks'] = 'C_pimpinan1/Iks/index';

$route['pimpinan1/viks/fee/(:any)'] = 'C_pimpinan1/Iks/fee/(:any)';

$route['pimpinan1/viks/feeGo'] = 'C_pimpinan1/Iks/feeGo';

$route['pimpinan1/viks/tolak/(:any)'] = 'C_pimpinan1/Iks/tolak/(:any)';

$route['pimpinan1/viks/tolakGo'] = 'C_pimpinan1/Iks/tolakGo';

//------------SUB PJ-------------------

$route['pimpinan1/vpj'] = 'C_pimpinan1/Pj/index';

$route['pimpinan1/vpj/fee/(:any)'] = 'C_pimpinan1/Pj/fee/(:any)';

$route['pimpinan1/vpj/feeGo'] = 'C_pimpinan1/Pj/feeGo';

$route['pimpinan1/vpj/tolak/(:any)'] = 'C_pimpinan1/Pj/tolak/(:any)';

$route['pimpinan1/vpj/tolakGo'] = 'C_pimpinan1/Pj/tolakGo';

//------------SUB UBPJS-------------------

$route['pimpinan1/vubpjs'] = 'C_pimpinan1/Ubpjs/index';

$route['pimpinan1/vubpjs/fee/(:any)'] = 'C_pimpinan1/Ubpjs/fee/(:any)';

$route['pimpinan1/vubpjs/feeGo'] = 'C_pimpinan1/Ubpjs/feeGo';

$route['pimpinan1/vubpjs/tolak/(:any)'] = 'C_pimpinan1/Ubpjs/tolak/(:any)';

$route['pimpinan1/vubpjs/tolakGo'] = 'C_pimpinan1/Ubpjs/tolakGo';

//------------SUB UIPJS-------------------

$route['pimpinan1/vuipjs'] = 'C_pimpinan1/Uipjs/index';

$route['pimpinan1/vuipjs/fee/(:any)'] = 'C_pimpinan1/Uipjs/fee/(:any)';

$route['pimpinan1/vuipjs/feeGo'] = 'C_pimpinan1/Uipjs/feeGo';

$route['pimpinan1/vuipjs/tolak/(:any)'] = 'C_pimpinan1/Uipjs/tolak/(:any)';

$route['pimpinan1/vuipjs/tolakGo'] = 'C_pimpinan1/Uipjs/tolakGo';



//===================================================

//------------VALIDATOR PIMPINAN 2-------------------

//===================================================



$route['pimpinan2'] = 'C_pimpinan2/Dashboard/index';

//------------SUB AJI-------------------

$route['pimpinan2/vaji'] = 'C_pimpinan2/Aji/index';

$route['pimpinan2/vaji/fee/(:any)'] = 'C_pimpinan2/Aji/fee/(:any)';

$route['pimpinan2/vaji/feeGo'] = 'C_pimpinan2/Aji/feeGo';

$route['pimpinan2/vaji/tolak/(:any)'] = 'C_pimpinan2/Aji/tolak/(:any)';

$route['pimpinan2/vaji/tolakGo'] = 'C_pimpinan2/Aji/tolakGo';

//------------SUB BBP-------------------

$route['pimpinan2/vbbp'] = 'C_pimpinan2/Bbp/index';

$route['pimpinan2/vbbp/fee/(:any)'] = 'C_pimpinan2/Bbp/fee/(:any)';

$route['pimpinan2/vbbp/feeGo'] = 'C_pimpinan2/Bbp/feeGo';

$route['pimpinan2/vbbp/tolak/(:any)'] = 'C_pimpinan2/Bbp/tolak/(:any)';

$route['pimpinan2/vbbp/tolakGo'] = 'C_pimpinan2/Bbp/tolakGo';

//------------SUB BFIO-------------------

$route['pimpinan2/vbfio'] = 'C_pimpinan2/Bfio/index';

$route['pimpinan2/vbfio/fee/(:any)'] = 'C_pimpinan2/Bfio/fee/(:any)';

$route['pimpinan2/vbfio/feeGo'] = 'C_pimpinan2/Bfio/feeGo';

$route['pimpinan2/vbfio/tolak/(:any)'] = 'C_pimpinan2/Bfio/tolak/(:any)';

$route['pimpinan2/vbfio/tolakGo'] = 'C_pimpinan2/Bfio/tolakGo';

//------------SUB HKI-------------------

$route['pimpinan2/vhki'] = 'C_pimpinan2/Hki/index';

$route['pimpinan2/vhki/fee/(:any)'] = 'C_pimpinan2/Hki/fee/(:any)';

$route['pimpinan2/vhki/feeGo'] = 'C_pimpinan2/Hki/feeGo';

$route['pimpinan2/vhki/tolak/(:any)'] = 'C_pimpinan2/Hki/tolak/(:any)';

$route['pimpinan2/vhki/tolakGo'] = 'C_pimpinan2/Hki/tolakGo';

//------------SUB IBA-------------------

$route['pimpinan2/viba'] = 'C_pimpinan2/Iba/index';

$route['pimpinan2/viba/fee/(:any)'] = 'C_pimpinan2/Iba/fee/(:any)';

$route['pimpinan2/viba/feeGo'] = 'C_pimpinan2/Iba/feeGo';

$route['pimpinan2/viba/tolak/(:any)'] = 'C_pimpinan2/Iba/tolak/(:any)';

$route['pimpinan2/viba/tolakGo'] = 'C_pimpinan2/Iba/tolakGo';

//------------SUB IFIP-------------------

$route['pimpinan2/vifip'] = 'C_pimpinan2/Ifip/index';

$route['pimpinan2/vifip/fee/(:any)'] = 'C_pimpinan2/Ifip/fee/(:any)';

$route['pimpinan2/vifip/feeGo'] = 'C_pimpinan2/Ifip/feeGo';

$route['pimpinan2/vifip/tolak/(:any)'] = 'C_pimpinan2/Ifip/tolak/(:any)';

$route['pimpinan2/vifip/tolakGo'] = 'C_pimpinan2/Ifip/tolakGo';

//------------SUB IKS-------------------

$route['pimpinan2/viks'] = 'C_pimpinan2/Iks/index';

$route['pimpinan2/viks/fee/(:any)'] = 'C_pimpinan2/Iks/fee/(:any)';

$route['pimpinan2/viks/feeGo'] = 'C_pimpinan2/Iks/feeGo';

$route['pimpinan2/viks/tolak/(:any)'] = 'C_pimpinan2/Iks/tolak/(:any)';

$route['pimpinan2/viks/tolakGo'] = 'C_pimpinan2/Iks/tolakGo';

//------------SUB PJ-------------------

$route['pimpinan2/vpj'] = 'C_pimpinan2/Pj/index';

$route['pimpinan2/vpj/fee/(:any)'] = 'C_pimpinan2/Pj/fee/(:any)';

$route['pimpinan2/vpj/feeGo'] = 'C_pimpinan2/Pj/feeGo';

$route['pimpinan2/vpj/tolak/(:any)'] = 'C_pimpinan2/Pj/tolak/(:any)';

$route['pimpinan2/vpj/tolakGo'] = 'C_pimpinan2/Pj/tolakGo';

//------------SUB UBPJS-------------------

$route['pimpinan2/vubpjs'] = 'C_pimpinan2/Ubpjs/index';

$route['pimpinan2/vubpjs/fee/(:any)'] = 'C_pimpinan2/Ubpjs/fee/(:any)';

$route['pimpinan2/vubpjs/feeGo'] = 'C_pimpinan2/Ubpjs/feeGo';

$route['pimpinan2/vubpjs/tolak/(:any)'] = 'C_pimpinan2/Ubpjs/tolak/(:any)';

$route['pimpinan2/vubpjs/tolakGo'] = 'C_pimpinan2/Ubpjs/tolakGo';

//------------SUB UIPJS-------------------

$route['pimpinan2/vuipjs'] = 'C_pimpinan2/Uipjs/index';

$route['pimpinan2/vuipjs/fee/(:any)'] = 'C_pimpinan2/Uipjs/fee/(:any)';

$route['pimpinan2/vuipjs/feeGo'] = 'C_pimpinan2/Uipjs/feeGo';

$route['pimpinan2/vuipjs/tolak/(:any)'] = 'C_pimpinan2/Uipjs/tolak/(:any)';

$route['pimpinan2/vuipjs/tolakGo'] = 'C_pimpinan2/Uipjs/tolakGo';



//------------SUB BHKI-------------------

$route['pimpinan2/vbhki'] = 'C_pimpinan2/Bhki/index';

$route['pimpinan2/vbhki/history/(:any)'] = 'C_pimpinan2/Bhki/history/(:any)';

$route['pimpinan2/vbhki/fee/(:any)'] = 'C_pimpinan2/Bhki/fee/(:any)';

$route['pimpinan2/vbhki/feeGo'] = 'C_pimpinan2/Bhki/feeGo';

$route['pimpinan2/vbhki/tolak/(:any)'] = 'C_pimpinan2/Bhki/tolak/(:any)';

$route['pimpinan2/vbhki/tolakGo'] = 'C_pimpinan2/Bhki/tolakGo';



//===================================================

//------------VALIDATOR ADMIN BAUM-------------------

//===================================================

$route['superpower'] = 'C_superpower/Home/index';

$route['lppm'] = 'C_lppm/Dashboard/index';

$route['wr2'] = 'C_superpower/Home/index';

$route['vdataPribadi'] = 'C_superpower/validasi_dosen/VdataPribadi/index';

$route['vpenfor'] = 'C_superpower/validasi_dosen/VdataPenfor/index';

$route['seluruhDosen'] = 'C_superpower/data_pegawai/Data_pegawai/index';

$route['seluruhTendik'] = 'C_superpower/data_pegawai/Data_pegawai/tendik';

$route['detail/pegawai/(:any)'] = 'C_superpower/data_pegawai/Data_pegawai/detail_pegawai/(:any)';

//VALIDASI DATA PRIBADI

//DETAIL DATA PRIBADI

// PERSETUJUAN AJUAN DATA

$route['ajuan_pdd/setujuiDataProfil/(:any)'] = 'C_superpower/validasi_dosen/VdataPribadi/setujuiDataProfil/(:any)';

$route['ajuan_pdd/setujuiDataKeluarga/(:any)'] = 'C_superpower/validasi_dosen/VdataKeluarga/setujuiDataKeluarga/(:any)';

$route['ajuan_pdd/setujuiDataLain/(:any)'] = 'C_superpower/validasi_dosen/VdataLain/setujuiDataLain/(:any)';

$route['ajuan_pdd/setujuiDataKepegawaian/(:any)'] = 'C_superpower/validasi_dosen/VdataKepegawaian/setujuiDataKepegawaian/(:any)';

$route['ajuan_pdd/setujuiDataKependudukan/(:any)'] = 'C_superpower/validasi_dosen/VdataKependudukan/setujuiDataKependudukan/(:any)';

$route['ajuan_pdd/setujuiDataAlamat/(:any)'] = 'C_superpower/validasi_dosen/VdataAlamat/setujuiDataAlamat/(:any)';

$route['ajuan_pdd/setujuiDataKeanggotaan/(:any)'] = 'C_superpower/validasi_dosen/VdataKeanggotaan/setujuiDataKeanggotaan/(:any)';

// PENOLAKAN AJUAN DATA

$route['ajuan_pdd/detailProfil/(:any)/(:any)'] = 'C_superpower/validasi_dosen/VdataPribadi/detailProfil/(:any)/(:any)';

$route['ajuan_pdd/tolakProfil'] = 'C_superpower/validasi_dosen/VdataPribadi/tolakProfil';

$route['ajuan_pdd/tolakDataKeluarga'] = 'C_superpower/validasi_dosen/VdataKeluarga/tolakDataKeluarga';

$route['ajuan_pdd/tolakDataLain'] = 'C_superpower/validasi_dosen/VdataLain/tolakDataLain';

$route['ajuan_pdd/tolakDataKepegawaian'] = 'C_superpower/validasi_dosen/VdataKepegawaian/tolakDataKepegawaian';

$route['ajuan_pdd/tolakDataKependudukan'] = 'C_superpower/validasi_dosen/VdataKependudukan/tolakDataKependudukan';

$route['ajuan_pdd/tolakDataAlamat'] = 'C_superpower/validasi_dosen/VdataAlamat/tolakDataAlamat';

$route['ajuan_pdd/tolakDataKeanggotaan'] = 'C_superpower/validasi_dosen/VdataKeanggotaan/tolakDataKeanggotaan';

// DETAIL AJUAN DATA

$route['ajuan_pdd/detailKeluarga/(:any)/(:any)'] = 'C_superpower/validasi_dosen/VdataPribadi/detailKeluarga/(:any)/(:any)';

$route['ajuan_pdd/detailKependudukan/(:any)/(:any)'] = 'C_superpower/validasi_dosen/VdataPribadi/detailKependudukan/(:any)/(:any)';

$route['ajuan_pdd/detailKeanggotaan/(:any)/(:any)'] = 'C_superpower/validasi_dosen/VdataPribadi/detailKeanggotaan/(:any)/(:any)';

$route['ajuan_pdd/detailKepegawaian/(:any)/(:any)'] = 'C_superpower/validasi_dosen/VdataPribadi/detailKepegawaian/(:any)/(:any)';

$route['ajuan_pdd/detailAlamat/(:any)/(:any)'] = 'C_superpower/validasi_dosen/VdataPribadi/detailAlamat/(:any)/(:any)';

$route['ajuan_pdd/detailLainlain/(:any)/(:any)'] = 'C_superpower/validasi_dosen/VdataPribadi/detailLainlain/(:any)/(:any)';



// AJUAN DATA PENFOR

$route['ajuan_pdd/detailPenfor/(:any)/(:any)'] = 'C_superpower/validasi_dosen/VdataPenfor/detailPenfor/(:any)/(:any)';

$route['ajuan_pdd/tolakPenfor'] = 'C_superpower/validasi_dosen/VdataPenfor/tolakPenfor';

$route['ajuan_pdd/setujuiPenfor/(:any)'] = 'C_superpower/validasi_dosen/VdataPenfor/setujuiPenfor/(:any)';



// AJUAN DATA JAFUNG

$route['vjafung'] = 'C_superpower/validasi_dosen/VdataJafung/index';

$route['ajuan_pdd/detailJafung/(:any)/(:any)'] = 'C_superpower/validasi_dosen/VdataJafung/detailJafung/(:any)/(:any)';

$route['ajuan_pdd/tolakJafung'] = 'C_superpower/validasi_dosen/VdataJafung/tolakJafung';

$route['ajuan_pdd/setujuiJafung/(:any)'] = 'C_superpower/validasi_dosen/VdataJafung/setujuiJafung/(:any)';



// AJUAN DATA KEPANGKATAN

$route['vKepangkatan'] = 'C_superpower/validasi_dosen/VdataKepangkatan/index';

$route['ajuan_pdd/detailKepangkatan/(:any)/(:any)'] = 'C_superpower/validasi_dosen/VdataKepangkatan/detailKepangkatan/(:any)/(:any)';

$route['ajuan_pdd/tolakKepangkatan'] = 'C_superpower/validasi_dosen/VdataKepangkatan/tolakKepangkatan';

$route['ajuan_pdd/setujuiKepangkatan/(:any)'] = 'C_superpower/validasi_dosen/VdataKepangkatan/setujuiKepangkatan/(:any)';



// AJUAN DATA SERTIFIKASI

$route['vSertifikasi'] = 'C_superpower/validasi_dosen/VdataSertifikasi/index';

$route['ajuan_pdd/detailSertifikasi/(:any)/(:any)'] = 'C_superpower/validasi_dosen/VdataSertifikasi/detailSertifikasi/(:any)/(:any)';

$route['ajuan_pdd/tolakSertifikasi'] = 'C_superpower/validasi_dosen/VdataSertifikasi/tolakSertifikasi';

$route['ajuan_pdd/setujuiSertifikasi/(:any)'] = 'C_superpower/validasi_dosen/VdataSertifikasi/setujuiSertifikasi/(:any)';



//  AJUAN DATA AIK

$route['vAik'] = 'C_superpower/validasi_dosen/VdataAik/index';

$route['ajuan_pdd/detailAik/(:any)/(:any)'] = 'C_superpower/validasi_dosen/VdataAik/detailAik/(:any)/(:any)';

$route['ajuan_pdd/tolakAik'] = 'C_superpower/validasi_dosen/VdataAik/tolakAik';

$route['ajuan_pdd/setujuiAik/(:any)'] = 'C_superpower/validasi_dosen/VdataAik/setujuiAik/(:any)';

//===================================================

//------------VALIDATOR ADMIN LPPM-------------------

//===================================================

//(VERIFIKASI - LPPM) INSENTIF ARTIKEL DIJURNAL

$route['vaji'] = 'C_verifikator_publis/Aji/index';

$route['vaji/detail/(:any)'] = 'C_verifikator_publis/Aji/detailAji/(:any)';

$route['vaji/history/(:any)'] = 'C_verifikator_publis/Aji/history/(:any)';

$route['vaji/setujui/(:any)'] = 'C_verifikator_publis/Aji/setujui/(:any)';

$route['vaji/setujuiGo'] = 'C_verifikator_publis/Aji/setujuiGo';

$route['vaji/tolak/(:any)'] = 'C_verifikator_publis/Aji/tolak/(:any)';

$route['vaji/tolakGo'] = 'C_verifikator_publis/Aji/tolakGo';

//(VERIFIKASI - LPPM) INSENTIF HKI

$route['vhki'] = 'C_verifikator_publis/Hki/index';

$route['vhki/detail/(:any)'] = 'C_verifikator_publis/Hki/detailHki/(:any)';

$route['vhki/history/(:any)'] = 'C_verifikator_publis/Hki/history/(:any)';

$route['vhki/setujui/(:any)'] = 'C_verifikator_publis/Hki/setujui/(:any)';

$route['vhki/setujuiGo'] = 'C_verifikator_publis/Hki/setujuiGo';

$route['vhki/tolak/(:any)'] = 'C_verifikator_publis/Hki/tolak/(:any)';

$route['vhki/tolakGo'] = 'C_verifikator_publis/Hki/tolakGo';

//(VERIFIKASI - LPPM) INSENTIF PENGELOLA JURNAL

$route['vpj'] = 'C_verifikator_publis/Pj/index';

$route['vpj/detail/(:any)'] = 'C_verifikator_publis/Pj/detailPj/(:any)';

$route['vpj/history/(:any)'] = 'C_verifikator_publis/Pj/history/(:any)';

$route['vpj/setujui/(:any)'] = 'C_verifikator_publis/Pj/setujui/(:any)';

$route['vpj/setujuiGo'] = 'C_verifikator_publis/Pj/setujuiGo';

$route['vpj/tolak/(:any)'] = 'C_verifikator_publis/Pj/tolak/(:any)';

$route['vpj/tolakGo'] = 'C_verifikator_publis/Pj/tolakGo';

//(VERIFIKASI - LPPM) INSENTIF KEYNOTE SPEAKER

$route['vks'] = 'C_verifikator_publis/Iks/index';

$route['vks/detail/(:any)'] = 'C_verifikator_publis/Iks/detailIks/(:any)';

$route['vks/history/(:any)'] = 'C_verifikator_publis/Iks/history/(:any)';

$route['vks/setujui/(:any)'] = 'C_verifikator_publis/Iks/setujui/(:any)';

$route['vks/setujuiGo'] = 'C_verifikator_publis/Iks/setujuiGo';

$route['vks/tolak/(:any)'] = 'C_verifikator_publis/Iks/tolak/(:any)';

$route['vks/tolakGo'] = 'C_verifikator_publis/Iks/tolakGo';

//(VERIFIKASI - LPPM) INSENTIF HKI PROSIDDING

$route['vifip'] = 'C_verifikator_publis/Ifip/index';

$route['vifip/detail/(:any)'] = 'C_verifikator_publis/Ifip/detailIfip/(:any)';

$route['vifip/history/(:any)'] = 'C_verifikator_publis/Ifip/history/(:any)';

$route['vifip/setujui/(:any)'] = 'C_verifikator_publis/Ifip/setujui/(:any)';

$route['vifip/setujuiGo'] = 'C_verifikator_publis/Ifip/setujuiGo';

$route['vifip/tolak/(:any)'] = 'C_verifikator_publis/Ifip/tolak/(:any)';

$route['vifip/tolakGo'] = 'C_verifikator_publis/Ifip/tolakGo';

//(VERIFIKASI - LPPM) INSENTIF BUKU AJAR DLL

$route['viba'] = 'C_verifikator_publis/Iba/index';

$route['viba/detail/(:any)'] = 'C_verifikator_publis/Iba/detailIba/(:any)';

$route['viba/history/(:any)'] = 'C_verifikator_publis/Iba/history/(:any)';

$route['viba/setujui/(:any)'] = 'C_verifikator_publis/Iba/setujui/(:any)';

$route['viba/setujuiGo'] = 'C_verifikator_publis/Iba/setujuiGo';

$route['viba/tolak/(:any)'] = 'C_verifikator_publis/Iba/tolak/(:any)';

$route['viba/tolakGo'] = 'C_verifikator_publis/Iba/tolakGo';

//(VERIFIKASI - LPPM) INSENTIF JURNAL DARI SKRIPSI

$route['vuipjs'] = 'C_verifikator_publis/Uipjs/index';

$route['vuipjs/detail/(:any)'] = 'C_verifikator_publis/Uipjs/detailUipjs/(:any)';

$route['vuipjs/history/(:any)'] = 'C_verifikator_publis/Uipjs/history/(:any)';

$route['vuipjs/setujui/(:any)'] = 'C_verifikator_publis/Uipjs/setujui/(:any)';

$route['vuipjs/setujuiGo'] = 'C_verifikator_publis/Uipjs/setujuiGo';

$route['vuipjs/tolak/(:any)'] = 'C_verifikator_publis/Uipjs/tolak/(:any)';

$route['vuipjs/tolakGo'] = 'C_verifikator_publis/Uipjs/tolakGo';

//(VERIFIKASI - LPPM) BANTUAN BIAYA PUBLIKASI

$route['vbbp'] = 'C_verifikator_publis/Bbp/index';

$route['vbbp/detail/(:any)'] = 'C_verifikator_publis/Bbp/detailBbp/(:any)';

$route['vbbp/history/(:any)'] = 'C_verifikator_publis/Bbp/history/(:any)';

$route['vbbp/setujui/(:any)'] = 'C_verifikator_publis/Bbp/setujui/(:any)';

$route['vbbp/setujuiGo'] = 'C_verifikator_publis/Bbp/setujuiGo';

$route['vbbp/tolak/(:any)'] = 'C_verifikator_publis/Bbp/tolak/(:any)';

$route['vbbp/tolakGo'] = 'C_verifikator_publis/Bbp/tolakGo';

//(VERIFIKASI - LPPM) BANTUAN SEMINAR NASIONAL/INTERNASIONAL

$route['vbfio'] = 'C_verifikator_publis/Bfio/index';

$route['vbfio/detail/(:any)'] = 'C_verifikator_publis/Bfio/detailBfio/(:any)';

$route['vbfio/history/(:any)'] = 'C_verifikator_publis/Bfio/history/(:any)';

$route['vbfio/setujui/(:any)'] = 'C_verifikator_publis/Bfio/setujui/(:any)';

$route['vbfio/setujuiGo'] = 'C_verifikator_publis/Bfio/setujuiGo';

$route['vbfio/tolak/(:any)'] = 'C_verifikator_publis/Bfio/tolak/(:any)';

$route['vbfio/tolakGo'] = 'C_verifikator_publis/Bfio/tolakGo';

//(VERIFIKASI - LPPM) BANTUAN BIAYA PUBLIKASI JURNAL DARI SKRIPSI

$route['vubpjs'] = 'C_verifikator_publis/Ubpjs/index';

$route['vubpjs/detail/(:any)'] = 'C_verifikator_publis/Ubpjs/detailUbpjs/(:any)';

$route['vubpjs/history/(:any)'] = 'C_verifikator_publis/Ubpjs/history/(:any)';

$route['vubpjs/setujui/(:any)'] = 'C_verifikator_publis/Ubpjs/setujui/(:any)';

$route['vubpjs/setujuiGo'] = 'C_verifikator_publis/Ubpjs/setujuiGo';

$route['vubpjs/tolak/(:any)'] = 'C_verifikator_publis/Ubpjs/tolak/(:any)';

$route['vubpjs/tolakGo'] = 'C_verifikator_publis/Ubpjs/tolakGo';





$route['vaji'] = 'C_verifikator_publis/Aji/index';

//Lihat Profil Dosen

$route['user/detail/(:any)'] = 'User/detailUser/(:any)';

//Statistik

$route['statistik'] = 'Statistik/index';



// MANAJEMEN SISTEM 

// =====================================================================================================

// MAN. USER

$route['muser'] = 'C_manajemen/Muser/index';

$route['muser/new'] = 'C_manajemen/Muser/new_user';

$route['muser/save'] = 'C_manajemen/Muser/save_user';

$route['muser/edit/(:any)'] = 'C_manajemen/Muser/edit_user/(:any)';

$route['muser/editGo'] = 'C_manajemen/Muser/edit_userGo';

$route['muser/detail/(:any)'] = 'C_manajemen/Muser/detail_user/(:any)';

$route['muser/off/(:any)'] = 'C_manajemen/Muser/off_user/(:any)';

$route['muser/on/(:any)'] = 'C_manajemen/Muser/on_user/(:any)';

$route['muser/reset/(:any)'] = 'C_manajemen/Muser/reset_user/(:any)';

// =====================================================================================================

// MAN. FAKULTAS

$route['mfak'] = 'C_manajemen/Mfak/index';

$route['mfak/new'] = 'C_manajemen/Mfak/new_fak';

$route['mfak/save'] = 'C_manajemen/Mfak/save_fak';

$route['mfak/editGo'] = 'C_manajemen/Mfak/editGo_fak';

$route['mfak/edit/(:any)'] = 'C_manajemen/Mfak/edit_fak/(:any)';

$route['mfak/hapus/(:any)'] = 'C_manajemen/Mfak/hapus_fak/(:any)';

// =====================================================================================================

// MAN. PROGRAM STUDI

$route['mprod'] = 'C_manajemen/Mprod/index';

$route['mprod/new'] = 'C_manajemen/Mprod/new_prod';

$route['mprod/save'] = 'C_manajemen/Mprod/save_prod';

$route['mprod/edit/(:any)'] = 'C_manajemen/Mprod/edit_prod/(:any)';

$route['mprod/editGo'] = 'C_manajemen/Mprod/editGo_prod';

$route['mprod/hapus/(:any)'] = 'C_manajemen/Mprod/hapus_prod/(:any)';

// =====================================================================================================

// MAN. AIK

$route['maik'] = 'C_manajemen/Maik/index';

$route['maik/new'] = 'C_manajemen/Maik/new_aik';

$route['maik/save'] = 'C_manajemen/Maik/save_aik';

$route['maik/editGo'] = 'C_manajemen/Maik/editGo_aik';

$route['maik/edit/(:any)'] = 'C_manajemen/Maik/edit_aik/(:any)';

$route['maik/hapus/(:any)'] = 'C_manajemen/Maik/hapus_aik/(:any)';

$route['maik/on/(:any)'] = 'C_manajemen/Maik/on_aik/(:any)';

// =====================================================================================================

// MAN. KAJUR

$route['mkajur'] = 'C_manajemen/Mkajur/index';

$route['mkajur/new'] = 'C_manajemen/Mkajur/new_kajur';

$route['mkajur/edit/(:any)'] = 'C_manajemen/Mkajur/edit_kajur/(:any)';

$route['mkajur/off/(:any)'] = 'C_manajemen/Mkajur/off_kajur/(:any)';

$route['mkajur/on/(:any)'] = 'C_manajemen/Mkajur/on_kajur/(:any)';

// =====================================================================================================

// MAN. LEMBAGA PENGINDEKS

$route['mlp'] = 'C_manajemen/Mlp/index';

$route['mlp/new'] = 'C_manajemen/Mlp/new_lp';

$route['mlp/edit/(:any)'] = 'C_manajemen/Mlp/edit_lp/(:any)';

$route['mlp/off/(:any)'] = 'C_manajemen/Mlp/off_lp/(:any)';

$route['mlp/on/(:any)'] = 'C_manajemen/Mlp/on_lp/(:any)';

// =====================================================================================================

// MAN. PANGKAT

$route['mpangk'] = 'C_manajemen/Mpangk/index';

$route['mpangk/new'] = 'C_manajemen/Mpangk/new_pangk';

$route['mpangk/edit/(:any)'] = 'C_manajemen/Mpangk/edit_pangk/(:any)';

$route['mpangk/off/(:any)'] = 'C_manajemen/Mpangk/off_pangk/(:any)';

$route['mpangk/on/(:any)'] = 'C_manajemen/Mpangk/on_pangk/(:any)';

// =====================================================================================================

// MAN. JABATAN FUNGSIONAL

$route['mjaf'] = 'C_manajemen/Mjaf/index';

$route['mjaf/new'] = 'C_manajemen/Mjaf/new_jaf';

$route['mjaf/save'] = 'C_manajemen/Mjaf/save_jaf';

$route['mjaf/edit/(:any)'] = 'C_manajemen/Mjaf/edit_jaf/(:any)';

$route['mjaf/editGo_jaf'] = 'C_manajemen/Mjaf/editGo_jaf';

$route['mjaf/hapus/(:any)'] = 'C_manajemen/Mjaf/hapus_jaf/(:any)';

$route['mjaf/on/(:any)'] = 'C_manajemen/Mjaf/on_jaf/(:any)';

// =====================================================================================================

// MAN. UNIT

$route['munit'] = 'C_manajemen/Munit/index';

$route['munit/new'] = 'C_manajemen/Munit/new_unit';

$route['munit/save'] = 'C_manajemen/Munit/save_unit';

$route['munit/editGo'] = 'C_manajemen/Munit/editGo_unit';

$route['munit/edit/(:any)'] = 'C_manajemen/Munit/edit_unit/(:any)';

$route['munit/hapus/(:any)'] = 'C_manajemen/Munit/hapus_unit/(:any)';



//===================================================

//------------------TACKING PUBLIS-------------------

//===================================================

$route['track/aji'] = 'C_track/Track/aji';

$route['track/pj'] = 'C_track/Track/pj';

$route['track/hki'] = 'C_track/Track/hki';

$route['track/iks'] = 'C_track/Track/iks';

$route['track/ifip'] = 'C_track/Track/ifip';

$route['track/iba'] = 'C_track/Track/iba';

$route['track/bbp'] = 'C_track/Track/bbp';

$route['track/uipjs'] = 'C_track/Track/uipjs';

$route['track/bfio'] = 'C_track/Track/bfio';

$route['track/ubpjs'] = 'C_track/Track/ubpjs';

//===================================================

//------------------REPORT PUBLIS--------------------

//===================================================

$route['reportpublis'] = 'C_report_publis/Report/index';

$route['reportpublis/cariData'] = 'C_report_publis/Report/cariData';

//===================================================

//------------------PDF PUBLIS----------------------

//===================================================

$route['reportpublis/pdf_Aji/(:any)'] = 'C_report_publis/Report/pdf_Aji/(:any)';

$route['reportpublis/pdf_Pj/(:any)'] = 'C_report_publis/Report/pdf_Pj/(:any)';

$route['reportpublis/pdf_Hki/(:any)'] = 'C_report_publis/Report/pdf_Hki/(:any)';

$route['reportpublis/pdf_Iks/(:any)'] = 'C_report_publis/Report/pdf_Iks/(:any)';

$route['reportpublis/pdf_Ifip/(:any)'] = 'C_report_publis/Report/pdf_Ifip/(:any)';

$route['reportpublis/pdf_Iba/(:any)'] = 'C_report_publis/Report/pdf_Iba/(:any)';

$route['reportpublis/pdf_Uipjs/(:any)'] = 'C_report_publis/Report/pdf_Uipjs/(:any)';

$route['reportpublis/pdf_Bbp/(:any)'] = 'C_report_publis/Report/pdf_Bbp/(:any)';

$route['reportpublis/pdf_Bfio/(:any)'] = 'C_report_publis/Report/pdf_Bfio/(:any)';

$route['reportpublis/pdf_Ubpjs/(:any)'] = 'C_report_publis/Report/pdf_Ubpjs/(:any)';

$route['reportpublis/pdf_Bhki/(:any)'] = 'C_report_publis/Report/pdf_Bhki/(:any)';

//===================================================

//------------------EXCEL PUBLIS---------------------

//===================================================

$route['reportpublis/excel_Aji/(:any)'] = 'C_report_publis/Report/excel_Aji/(:any)';

$route['reportpublis/excel_Pj/(:any)'] = 'C_report_publis/Report/excel_Pj/(:any)';

$route['reportpublis/excel_Hki/(:any)'] = 'C_report_publis/Report/excel_Hki/(:any)';

$route['reportpublis/excel_Iks/(:any)'] = 'C_report_publis/Report/excel_Iks/(:any)';

$route['reportpublis/excel_Ifip/(:any)'] = 'C_report_publis/Report/excel_Ifip/(:any)';

$route['reportpublis/excel_Iba/(:any)'] = 'C_report_publis/Report/excel_Iba/(:any)';

$route['reportpublis/excel_Uipjs/(:any)'] = 'C_report_publis/Report/excel_Uipjs/(:any)';

$route['reportpublis/excel_Bbp/(:any)'] = 'C_report_publis/Report/excel_Bbp/(:any)';

$route['reportpublis/excel_Bfio/(:any)'] = 'C_report_publis/Report/excel_Bfio/(:any)';

$route['reportpublis/excel_Ubpjs/(:any)'] = 'C_report_publis/Report/excel_Ubpjs/(:any)';

//===================================================

//------------------Cuti Tendik---------------------

//===================================================

$route['tendik/cuti'] = 'C_cuti/Ctendik/index';

$route['tendik/cuti/create'] = 'C_cuti/Ctendik/createCuti';

$route['tendik/cuti/umum'] = 'C_cuti/Ctendik/createUmum';

$route['tendik/cuti/umroh'] = 'C_cuti/Ctendik/createUmroh';

$route['tendik/cuti/hamil'] = 'C_cuti/Ctendik/createHamil';

$route['tendik/cuti/detail/(:any)'] = 'C_cuti/Ctendik/detail/(:any)';

$route['tendik/cuti/cetak/(:any)'] = 'C_cuti/Ctendik/cetak/(:any)';

//===================================================

//------------------Izin Sakit Tendik---------------------

//===================================================

$route['tendik/izinsakit'] = 'C_cuti/Ctendik/vizinsakit';

$route['tendik/izinsakit/create'] = 'C_cuti/Ctendik/createIzinsakit';

$route['tendik/izinsakit/ubahIzinSakit/(:any)'] = 'C_cuti/Ctendik/ubahIzinsakit/(:any)';

$route['tendik/izinsakit/update'] = 'C_cuti/Ctendik/ubahIzinsakitGo';

$route['tendik/izinsakit/cetak/(:any)'] = 'C_cuti/Ctendik/cetakIzinsakit/(:any)';

//===================================================

//------------------Cuti Admin-----------------------

//===================================================

$route['admin/cuti'] = 'C_cuti/Cadmin/index';

$route['admin/listCutiSetujui'] = 'C_cuti/Cadmin/listCutiSetujui';

$route['admin/listCutiTolak'] = 'C_cuti/Cadmin/listCutiTolak';

$route['admin/listCutiProses'] = 'C_cuti/Cadmin/listCutiProses';

$route['admin/cutiProses/(:any)'] = 'C_cuti/Cadmin/cutiProses/(:any)';

$route['admin/cutiSetujui/(:any)'] = 'C_cuti/Cadmin/cutiSetujui/(:any)';

$route['admin/cutiTolak/(:any)/(:any)'] = 'C_cuti/Cadmin/cutiTolak/(:any)/(:any)';

$route['admin/cutiTolakGo'] = 'C_cuti/Cadmin/cutiTolakGo';

//===================================================

//------------------Izin Sakit Admin-----------------

//===================================================

$route['admin/izinsakit'] = 'C_cuti/Cadmin/vizinsakit';

$route['admin/izinsakit/tolak'] = 'C_cuti/Cadmin/vizinsakit_tolak';

$route['admin/izinsakit/setujui'] = 'C_cuti/Cadmin/vizinsakit_setujui';

$route['admin/izinsakit/ftolak/(:any)'] = 'C_cuti/Cadmin/fizinsakit_tolak/(:any)';

$route['admin/izinsakit/ftolakGo'] = 'C_cuti/Cadmin/fizinsakit_tolak_go';

$route['admin/izinsakit/setujuGo/(:any)'] = 'C_cuti/Cadmin/izinsakit_setujuiGo/(:any)';

//===================================================

//------------------Cuti Pimpinan--------------------

//===================================================

$route['pimpinan/cuti'] = 'C_cuti/Cpimpinan/index';

$route['pimpinan/cutiSetujui/(:any)'] = 'C_cuti/Cpimpinan/cutiSetujui/(:any)';

$route['pimpinan/cutiTolak/(:any)/(:any)'] = 'C_cuti/Cpimpinan/cutiTolak/(:any)/(:any)';

$route['pimpinan/cutiTolakGo'] = 'C_cuti/Cpimpinan/cutiTolakGo';

//===================================================

$route['penempatanAdmin'] = 'C_superpower/data_pegawai/Penempatan/index';

$route['createPenempatan'] = 'C_superpower/data_pegawai/Penempatan/createPenempatan';

$route['savePenempatan'] = 'C_superpower/data_pegawai/Penempatan/savePenempatan';

$route['updatedPenempatanGo'] = 'C_superpower/data_pegawai/Penempatan/updatedPenempatanGo';

$route['updatePenempatan/(:any)'] = 'C_superpower/data_pegawai/Penempatan/updatePenempatan/(:any)';

$route['detailPenempatan/(:any)'] = 'C_superpower/data_pegawai/Penempatan/detailPenempatan/(:any)';

//===================================================

//------------------MASA PENSIUN--------------------

//===================================================

$route['masaPensiun'] = 'C_masa_pensiun/Pensiun/index';

$route['masaPensiun/search_pegawai'] = 'C_masa_pensiun/Pensiun/search_pegawai';


//===================================================
//------------------MASA PENGUMUMAN--------------------
//===================================================
$route['pengumuman'] = 'C_pengumuman/index';
//===================================================
//------------------KOTAK MASUK--------------------
//===================================================
$route['kotakMasuk'] = 'Kotakmasuk/index';
//===================================================
//------------------PRESENSI DOSEN-----------------
//===================================================
$route['presensi/listPresensi'] = 'C_operator/Operator/listPresensi';
$route['presensi/createPresensi'] = 'C_operator/Operator/createPresensi';
$route['presensi/ubahPresensi/(:any)'] = 'C_operator/Operator/ubahPresensi/(:any)';
$route['presensi/hapusPresensi/(:any)/(:any)'] = 'C_operator/Operator/hapusPresensi/(:any)/(:any)';
$route['presensi/ubahPresensiGo'] = 'C_operator/Operator/ubahPresensiGo';
$route['presensi/lapPresensi'] = 'C_operator/Operator/lapPresensi';
$route['presensi/v_lapPresensi'] = 'C_operator/Operator/v_lapPresensi';

$route['presensi/setDosen'] = 'C_operator/Operator/setDosen';
$route['presensi/invateDosen'] = 'C_operator/Operator/invateDosen';
$route['presensi/c_invateDosen'] = 'C_operator/Operator/c_invateDosen';
$route['presensi/hapus_setDosen/(:any)'] = 'C_operator/Operator/hapus_setDosen/(:any)';
$route['presensi/detailpengajaran/(:any)'] = 'C_operator/Operator/detailpengajaran/(:any)';
$route['presensi/tambahpengajaran/(:any)'] = 'C_operator/Operator/tambahpengajaran/(:any)';
$route['presensi/savepengajaran'] = 'C_operator/Operator/savepengajaran';

//===================================================
//---------------------------------------------------
//===================================================

$route['adminkeu/listPresensi'] = 'C_adminkeu/Adminkeu/listPresensi';
$route['adminkeu/detailPresensi/(:any)'] = 'C_adminkeu/Adminkeu/detailPresensi/(:any)';
$route['adminkeu/tarifDosen/(:any)/(:any)'] = 'C_adminkeu/Adminkeu/tarifDosen/(:any)/(:any)';
$route['adminkeu/simpanTarif'] = 'C_adminkeu/Adminkeu/simpanTarif';
$route['adminkeu/lapPresensi'] = 'C_adminkeu/Adminkeu/lapPresensi';
//===================================================
//---------------------------------------------------
//===================================================
$route['adminkeu/listPresensi_fai'] = 'C_adminkeu/Adminkeu/listPresensi_fai';
$route['adminkeu/detailPresensi_fai/(:any)'] = 'C_adminkeu/Adminkeu/detailPresensi_fai/(:any)';
$route['adminkeu/tarifDosen_fai/(:any)/(:any)'] = 'C_adminkeu/Adminkeu/tarifDosen_fai/(:any)/(:any)';
$route['adminkeu/simpanTarif_fai'] = 'C_adminkeu/Adminkeu/simpanTarif_fai';
$route['adminkeu/lapPresensi_fai'] = 'C_adminkeu/Adminkeu/lapPresensi_fai';
$route['adminkeu/editSksKehadiran'] = 'C_adminkeu/Adminkeu/editSksKehadiran';
//===================================================
//---------------------------------------------------
//===================================================
$route['adminkeu/listPresensi_fkip'] = 'C_adminkeu/Adminkeu/listPresensi_fkip';
$route['adminkeu/detailPresensi_fkip/(:any)'] = 'C_adminkeu/Adminkeu/detailPresensi_fkip/(:any)';
$route['adminkeu/tarifDosen_fkip/(:any)/(:any)'] = 'C_adminkeu/Adminkeu/tarifDosen_fkip/(:any)/(:any)';
$route['adminkeu/simpanTarif_fkip'] = 'C_adminkeu/Adminkeu/simpanTarif_fkip';
$route['adminkeu/lapPresensi_fkip'] = 'C_adminkeu/Adminkeu/lapPresensi_fkip';
$route['adminkeu/editSksKehadiran_fkip'] = 'C_adminkeu/Adminkeu/editSksKehadiran_fkip';
//===================================================
//---------------------------------------------------
//===================================================
$route['adminkeu/listPresensi_fisip'] = 'C_adminkeu/Adminkeu/listPresensi_fisip';
$route['adminkeu/detailPresensi_fisip/(:any)'] = 'C_adminkeu/Adminkeu/detailPresensi_fisip/(:any)';
$route['adminkeu/tarifDosen_fisip/(:any)/(:any)'] = 'C_adminkeu/Adminkeu/tarifDosen_fisip/(:any)/(:any)';
$route['adminkeu/simpanTarif_fisip'] = 'C_adminkeu/Adminkeu/simpanTarif_fisip';
$route['adminkeu/lapPresensi_fisip'] = 'C_adminkeu/Adminkeu/lapPresensi_fisip';
$route['adminkeu/editSksKehadiran_fisip'] = 'C_adminkeu/Adminkeu/editSksKehadiran_fisip';
//===================================================
//---------------------------------------------------
//===================================================
$route['adminkeu/listPresensi_faperta'] = 'C_adminkeu/Adminkeu/listPresensi_faperta';
$route['adminkeu/detailPresensi_faperta/(:any)'] = 'C_adminkeu/Adminkeu/detailPresensi_faperta/(:any)';
$route['adminkeu/tarifDosen_faperta/(:any)/(:any)'] = 'C_adminkeu/Adminkeu/tarifDosen_faperta/(:any)/(:any)';
$route['adminkeu/simpanTarif_faperta'] = 'C_adminkeu/Adminkeu/simpanTarif_faperta';
$route['adminkeu/lapPresensi_faperta'] = 'C_adminkeu/Adminkeu/lapPresensi_faperta';
$route['adminkeu/editSksKehadiran_faperta'] = 'C_adminkeu/Adminkeu/editSksKehadiran_faperta';
//===================================================
//---------------------------------------------------
//===================================================
$route['adminkeu/listPresensi_feb'] = 'C_adminkeu/Adminkeu/listPresensi_feb';
$route['adminkeu/detailPresensi_feb/(:any)'] = 'C_adminkeu/Adminkeu/detailPresensi_feb/(:any)';
$route['adminkeu/tarifDosen_feb/(:any)/(:any)'] = 'C_adminkeu/Adminkeu/tarifDosen_feb/(:any)/(:any)';
$route['adminkeu/simpanTarif_feb'] = 'C_adminkeu/Adminkeu/simpanTarif_feb';
$route['adminkeu/lapPresensi_feb'] = 'C_adminkeu/Adminkeu/lapPresensi_feb';
$route['adminkeu/editSksKehadiran_feb'] = 'C_adminkeu/Adminkeu/editSksKehadiran_feb';
//===================================================
//---------------------------------------------------
//===================================================
$route['adminkeu/listPresensi_fatek'] = 'C_adminkeu/Adminkeu/listPresensi_fatek';
$route['adminkeu/detailPresensi_fatek/(:any)'] = 'C_adminkeu/Adminkeu/detailPresensi_fatek/(:any)';
$route['adminkeu/tarifDosen_fatek/(:any)/(:any)'] = 'C_adminkeu/Adminkeu/tarifDosen_fatek/(:any)/(:any)';
$route['adminkeu/simpanTarif_fatek'] = 'C_adminkeu/Adminkeu/simpanTarif_fatek';
$route['adminkeu/lapPresensi_fatek'] = 'C_adminkeu/Adminkeu/lapPresensi_fatek';
$route['adminkeu/editSksKehadiran_fatek'] = 'C_adminkeu/Adminkeu/editSksKehadiran_fatek';
//===================================================
//---------------------------------------------------
//===================================================
$route['adminkeu/listPresensi_fahum'] = 'C_adminkeu/Adminkeu/listPresensi_fahum';
$route['adminkeu/detailPresensi_fahum/(:any)'] = 'C_adminkeu/Adminkeu/detailPresensi_fahum/(:any)';
$route['adminkeu/tarifDosen_fahum/(:any)/(:any)'] = 'C_adminkeu/Adminkeu/tarifDosen_fahum/(:any)/(:any)';
$route['adminkeu/simpanTarif_fahum'] = 'C_adminkeu/Adminkeu/simpanTarif_fahum';
$route['adminkeu/lapPresensi_fahum'] = 'C_adminkeu/Adminkeu/lapPresensi_fahum';
$route['adminkeu/editSksKehadiran_fahum'] = 'C_adminkeu/Adminkeu/editSksKehadiran_fahum';
//===================================================
//---------------------------------------------------
//===================================================
$route['adminkeu/listPresensi_fikti'] = 'C_adminkeu/Adminkeu/listPresensi_fikti';
$route['adminkeu/detailPresensi_fikti/(:any)'] = 'C_adminkeu/Adminkeu/detailPresensi_fikti/(:any)';
$route['adminkeu/tarifDosen_fikti/(:any)/(:any)'] = 'C_adminkeu/Adminkeu/tarifDosen_fikti/(:any)/(:any)';
$route['adminkeu/simpanTarif_fikti'] = 'C_adminkeu/Adminkeu/simpanTarif_fikti';
$route['adminkeu/lapPresensi_fikti'] = 'C_adminkeu/Adminkeu/lapPresensi_fikti';
$route['adminkeu/editSksKehadiran_fikti'] = 'C_adminkeu/Adminkeu/editSksKehadiran_fikti';
//===================================================
//---------------------------------------------------
//===================================================
$route['adminkeu/listPresensi_pns'] = 'C_adminkeu/Adminkeu/listPresensi_pns';
$route['adminkeu/detailPresensi_pns/(:any)'] = 'C_adminkeu/Adminkeu/detailPresensi_pns/(:any)';



//===================================================
//---------------------------------------------------
//===================================================
$route['adminkeu/mtarif'] = 'C_adminkeu/Adminkeu/mtarif';
$route['adminkeu/mtarifDosen/(:any)'] = 'C_adminkeu/Adminkeu/mtarifDosen/(:any)';
$route['adminkeu/mtarifDosenGo'] = 'C_adminkeu/Adminkeu/mtarifDosenGo';
//===================================================
//---------------------------------------------------
//===================================================
$route['adminkeu/mpotongan'] = 'C_adminkeu/Adminkeu/mpotongan';
$route['adminkeu/mpotonganDosen/(:any)'] = 'C_adminkeu/Adminkeu/mpotonganDosen/(:any)';
$route['adminkeu/mpotonganDosenGo'] = 'C_adminkeu/Adminkeu/mpotonganDosenGo';
$route['adminkeu/v_lapPresensi'] = 'C_adminkeu/Adminkeu/v_lapPresensi';
//===================================================
//---------------------------------------------------
//===================================================
$route['adminkeu/listPresensi_dt'] = 'C_adminkeu/Adminkeu/listPresensi_dt';
$route['adminkeu/detailPresensi_dt/(:any)'] = 'C_adminkeu/Adminkeu/detailPresensi_dt/(:any)';
$route['adminkeu/tarifDosen_dt/(:any)/(:any)'] = 'C_adminkeu/Adminkeu/tarifDosen_dt/(:any)/(:any)';
$route['adminkeu/simpanTarif_dt'] = 'C_adminkeu/Adminkeu/simpanTarif_dt';
$route['adminkeu/lapPresensi_dt'] = 'C_adminkeu/Adminkeu/lapPresensi';
$route['adminkeu/kelebihan_sks/(:any)'] = 'C_adminkeu/Adminkeu/kelebihan_sks/(:any)';
$route['adminkeu/simpan_kelebihan_sks'] = 'C_adminkeu/Adminkeu/simpan_kelebihan_sks';
$route['adminkeu/hapuslappengajaran/(:any)/(:any)'] = 'C_adminkeu/Adminkeu/hapuslappengajaran/(:any)/(:any)';
$route['adminkeu/kelebihan_sks_dt/(:any)'] = 'C_adminkeu/Adminkeu/kelebihan_sks_dt/(:any)';
$route['adminkeu/simpan_kelebihan_sks_dt'] = 'C_adminkeu/Adminkeu/simpan_kelebihan_sks_dt';
$route['adminkeu/hapuslappengajaran_dt/(:any)/(:any)'] = 'C_adminkeu/Adminkeu/hapuslappengajaran_dt/(:any)/(:any)';
//===================================================
//-----------STEMPEL-------------------
//===================================================
$route['stamp/v_pj'] = 'C_superpower/Stamp/v_pj';
$route['stamp/v_bbp'] = 'C_superpower/Stamp/v_bbp';
$route['stamp/v_ubpjs'] = 'C_superpower/Stamp/v_ubpjs';
$route['stamp/v_bfio'] = 'C_superpower/Stamp/v_bfio';
$route['stamp/v_bhki'] = 'C_superpower/Stamp/v_bhki';

$route['stamp/stamp_pj/(:any)'] = 'C_superpower/Stamp/stamp_pj/(:any)';
$route['stamp/stamp_bbp/(:any)'] = 'C_superpower/Stamp/stamp_bbp/(:any)';
$route['stamp/stamp_ubpjs/(:any)'] = 'C_superpower/Stamp/stamp_ubpjs/(:any)';
$route['stamp/stamp_bfio/(:any)'] = 'C_superpower/Stamp/stamp_bfio/(:any)';
$route['stamp/stamp_bhki/(:any)'] = 'C_superpower/Stamp/stamp_bhki/(:any)';

//===================================================
//---------------------LKK USER----------------------
//=================================================== 
$route['lkk/bansos'] = 'C_lkk/Lkk_user/index';
$route['lkk/createBantuan'] = 'C_lkk/Lkk_user/createBantuan';
$route['lkk/perbaikanBantuan/(:any)'] = 'C_lkk/Lkk_user/perbaikanBantuan/(:any)';
$route['lkk/cetakSurat/(:any)'] = 'C_lkk/Lkk_user/cetakSurat/(:any)';
$route['lkk/perbaikanBantuanGo'] = 'C_lkk/Lkk_user/perbaikanBantuanGo';
$route['lkk/hapusBantuan/(:any)'] = 'C_lkk/Lkk_user/hapusBantuan/(:any)';
$route['lkk/cetak_lkk_dosen/(:any)'] = 'C_lkk/Lkk_user/cetak_lkk_dosen/(:any)';
//===================================================
//---------------------LKK VERIF---------------------
//=================================================== 
$route['verif_lkk/vmasuk'] = 'C_lkk/Lkk_verif/vmasuk';
$route['verif_lkk/tolakBantuan'] = 'C_lkk/Lkk_verif/tolakBantuan';
$route['verif_lkk/prosesBantuan'] = 'C_lkk/Lkk_verif/prosesBantuan';
$route['verif_lkk/vproses'] = 'C_lkk/Lkk_verif/vproses';
$route['verif_lkk/vdisetujui'] = 'C_lkk/Lkk_verif/vdisetujui';
$route['verif_lkk/vditolak'] = 'C_lkk/Lkk_verif/vditolak';
$route['verif_lkk/vditerima'] = 'C_lkk/Lkk_verif/vditerima';
$route['verif_lkk/cetakKwitansi/(:any)'] = 'C_lkk/Lkk_verif/cetakKwitansi/(:any)';
$route['verif_lkk/penerimaan/(:any)'] = 'C_lkk/Lkk_verif/penerimaan/(:any)';
$route['verif_lkk/vlaporan'] = 'C_lkk/Lkk_verif/vlaporan';
$route['verif_lkk/laporanGo'] = 'C_lkk/Lkk_verif/laporanGo';
//===================================================
//---------------------PIMPINAN LKK------------------
//=================================================== 
$route['pimpinan_lkk/vmasuk_pimpinan'] = 'C_lkk/Lkk_Pimpinan/vmasuk_pimpinan';
$route['pimpinan_lkk/tolakBantuan'] = 'C_lkk/Lkk_Pimpinan/tolakBantuan';
$route['pimpinan_lkk/setujuiBantuan'] = 'C_lkk/Lkk_Pimpinan/setujuiBantuan';
//===================================================
//----------------SIMLITABMAS-- DOSEN----------------
//===================================================
$route['litabmas/dosen_usulan_penelitian'] = 'C_litabmas/Dosen/index';
$route['litabmas/cek'] = 'C_litabmas/Dosen/cek';
$route['litabmas/tahap1'] = 'C_litabmas/Dosen/tahap1';
$route['litabmas/tahap2'] = 'C_litabmas/Dosen/tahap2';
$route['litabmas/save_tahap2'] = 'C_litabmas/Dosen/save_tahap2';
$route['litabmas/tahap3'] = 'C_litabmas/Dosen/tahap3';
$route['litabmas/updateTahap2'] = 'C_litabmas/Dosen/updateTahap2';
$route['litabmas/dospem'] = 'C_litabmas/Dosen/dospem';
$route['litabmas/hapus_dospem/(:any)'] = 'C_litabmas/Dosen/hapus_dospem/(:any)';


$route['litabmas/anggotaPen'] = 'C_litabmas/Dosen/anggotaPen';
$route['litabmas/hapus_anggotaPen/(:any)'] = 'C_litabmas/Dosen/hapus_anggotaPen/(:any)';
$route['litabmas/anggotaMahasiswa'] = 'C_litabmas/Dosen/anggotaMahasiswa';
$route['litabmas/hapus_anggotaMahasiswa/(:any)'] = 'C_litabmas/Dosen/hapus_anggotaMahasiswa/(:any)';

$route['litabmas/iden_usul'] = 'C_litabmas/Dosen/tahap3';
$route['litabmas/sub_usul'] = 'C_litabmas/Dosen/sub_usul';
$route['litabmas/anggota_penelitian'] = 'C_litabmas/Dosen/anggota_penelitian';

$route['litabmas/luaranTC'] = 'C_litabmas/Dosen/luaranTC';
$route['litabmas/hapus_luaranTC/(:any)'] = 'C_litabmas/Dosen/hapus_luaranTC/(:any)';

$route['litabmas/luaranTambahan'] = 'C_litabmas/Dosen/luaranTambahan';
$route['litabmas/hapus_luaranTambahan/(:any)'] = 'C_litabmas/Dosen/hapus_luaranTambahan/(:any)';

$route['litabmas/uploadDokUsulan'] = 'C_litabmas/Dosen/uploadDokUsulan';

$route['litabmas/rab'] = 'C_litabmas/Dosen/rab';
//===================================================
//----------------SIMLITABMAS-- DOSEN----------------
//===================================================

$route['pimpinan_sppd/view_sppd'] = 'Sppd/view_sppd';
$route['pimpinan_sppd/setuju/(:any)'] = 'Sppd/setuju/(:any)';
$route['pimpinan_sppd/tolak'] = 'Sppd/tolak';
$route['pimpinan_sppd/detail/(:any)'] = 'Sppd/detail/(:any)';
//===================================================
//----------------------LAPORAN---------------------
//===================================================
$route['laporan/cuti'] = 'Laporan/cuti';
$route['laporan/cutiGo'] = 'Laporan/cutiGo';
$route['laporan/sakit'] = 'Laporan/sakit';
$route['laporan/sakitGo'] = 'Laporan/sakitGo';
//===================================================


$route['(:any)'] = 'errors/show_404';

$route['(:any)/(:any)'] = 'errors/show_404';

$route['(:any)/(:any)/(:any)'] = 'errors/show_404';
